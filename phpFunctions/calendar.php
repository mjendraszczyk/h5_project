<?php  require_once '../admin/Connections/high5.php';
 //include '../admin/functions.inc.php';
 require_once '../admin/calendar.class.php';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2">
<title>Untitled Document</title>
<link href="admin.css" rel="stylesheet" type="text/css">
</head>

<body>

<?php
if (empty($_GET['Month']) || empty($_GET['Year'])) {
    $today = getdate();
    $Year = $today['year'];
    $Month = $today['mon'];
} else {
    $Year = $_GET['Year'];
    $Month = $_GET['Month'];
}

$prevMonth = mktime(0, 0, 0, $Month - 1, 1, $Year);
$nextMonth = mktime(0, 0, 0, $Month + 1, 1, $Year);

$pMonth = date('n', $prevMonth);
$pYear = date('Y', $prevMonth);

$nMonth = date('n', $nextMonth);
$nYear = date('Y', $nextMonth);

$cal = new calendar();
// Calendar For Assigned Date
$cal->AssignDate($_GET['Month'], $_GET['Year']); // Required Parameter is Month and Year, respectively
$cal->GenerateCalendar();

// przewijanie
echo "\n<table width=\"400\" align=\"center\">";
echo "\n<tr>";
echo "\n\t<td width=\"50%\" align=\"center\">";
echo "\n\t<a href=\"calendar.php?Month=".$pMonth.'&Year='.$pYear.'"><<</a></td>';
echo "\n\t<td width=\"50%\" align=\"center\">";
echo "\n\t<a href=\"calendar.php?Month=".$nMonth.'&Year='.$nYear.'">>></a></td>';
?>
</tr>
</table>
</body>
</html>
