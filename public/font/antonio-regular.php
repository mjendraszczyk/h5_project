<?php
$type='TrueType';
$name='Antonio-Regular';
$desc=array('Ascent'=>1144,'Descent'=>-299,'CapHeight'=>859,'Flags'=>32,'FontBBox'=>'[-79 -327 1409 1163]','ItalicAngle'=>0,'StemV'=>70,'MissingWidth'=>500);
$up=0;
$ut=0;
$cw=array(
	chr(0)=>500,chr(1)=>500,chr(2)=>500,chr(3)=>500,chr(4)=>500,chr(5)=>500,chr(6)=>500,chr(7)=>500,chr(8)=>500,chr(9)=>500,chr(10)=>500,chr(11)=>500,chr(12)=>500,chr(13)=>500,chr(14)=>500,chr(15)=>500,chr(16)=>500,chr(17)=>500,chr(18)=>500,chr(19)=>500,chr(20)=>500,chr(21)=>500,
	chr(22)=>500,chr(23)=>500,chr(24)=>500,chr(25)=>500,chr(26)=>500,chr(27)=>500,chr(28)=>500,chr(29)=>500,chr(30)=>500,chr(31)=>500,' '=>246,'!'=>258,'"'=>461,'#'=>629,'$'=>430,'%'=>1063,'&'=>479,'\''=>202,'('=>277,')'=>277,'*'=>469,'+'=>395,
	','=>226,'-'=>340,'.'=>260,'/'=>382,'0'=>447,'1'=>315,'2'=>436,'3'=>443,'4'=>436,'5'=>436,'6'=>436,'7'=>436,'8'=>436,'9'=>436,':'=>271,';'=>274,'<'=>356,'='=>353,'>'=>356,'?'=>390,'@'=>687,'A'=>458,
	'B'=>465,'C'=>448,'D'=>463,'E'=>376,'F'=>364,'G'=>460,'H'=>480,'I'=>257,'J'=>419,'K'=>437,'L'=>348,'M'=>633,'N'=>495,'O'=>461,'P'=>435,'Q'=>461,'R'=>465,'S'=>426,'T'=>311,'U'=>470,'V'=>450,'W'=>664,
	'X'=>397,'Y'=>408,'Z'=>373,'['=>373,'\\'=>424,']'=>373,'^'=>503,'_'=>365,'`'=>317,'a'=>452,'b'=>458,'c'=>430,'d'=>458,'e'=>436,'f'=>304,'g'=>454,'h'=>473,'i'=>246,'j'=>260,'k'=>423,'l'=>245,'m'=>699,
	'n'=>474,'o'=>446,'p'=>458,'q'=>455,'r'=>318,'s'=>393,'t'=>299,'u'=>473,'v'=>368,'w'=>566,'x'=>363,'y'=>386,'z'=>332,'{'=>390,'|'=>262,'}'=>390,'~'=>545,chr(127)=>500,chr(128)=>500,chr(129)=>500,chr(130)=>500,chr(131)=>500,
	chr(132)=>500,chr(133)=>500,chr(134)=>500,chr(135)=>500,chr(136)=>500,chr(137)=>500,chr(138)=>500,chr(139)=>500,chr(140)=>500,chr(141)=>500,chr(142)=>500,chr(143)=>500,chr(144)=>500,chr(145)=>500,chr(146)=>500,chr(147)=>500,chr(148)=>500,chr(149)=>500,chr(150)=>500,chr(151)=>500,chr(152)=>500,chr(153)=>500,
	chr(154)=>500,chr(155)=>500,chr(156)=>500,chr(157)=>500,chr(158)=>500,chr(159)=>500,chr(160)=>246,chr(161)=>458,chr(162)=>496,chr(163)=>365,chr(164)=>566,chr(165)=>362,chr(166)=>423,chr(167)=>393,chr(168)=>477,chr(169)=>423,chr(170)=>423,chr(171)=>309,chr(172)=>373,chr(173)=>340,chr(174)=>373,chr(175)=>373,
	chr(176)=>447,chr(177)=>452,chr(178)=>496,chr(179)=>337,chr(180)=>317,chr(181)=>245,chr(182)=>393,chr(183)=>474,chr(184)=>496,chr(185)=>393,chr(186)=>393,chr(187)=>299,chr(188)=>332,chr(189)=>635,chr(190)=>332,chr(191)=>332,chr(192)=>465,chr(193)=>458,chr(194)=>458,chr(195)=>458,chr(196)=>458,chr(197)=>348,
	chr(198)=>448,chr(199)=>448,chr(200)=>448,chr(201)=>376,chr(202)=>376,chr(203)=>376,chr(204)=>376,chr(205)=>257,chr(206)=>257,chr(207)=>463,chr(208)=>463,chr(209)=>495,chr(210)=>495,chr(211)=>461,chr(212)=>461,chr(213)=>461,chr(214)=>461,chr(215)=>347,chr(216)=>465,chr(217)=>470,chr(218)=>470,chr(219)=>470,
	chr(220)=>470,chr(221)=>408,chr(222)=>500,chr(223)=>479,chr(224)=>318,chr(225)=>452,chr(226)=>452,chr(227)=>452,chr(228)=>452,chr(229)=>245,chr(230)=>430,chr(231)=>430,chr(232)=>430,chr(233)=>436,chr(234)=>436,chr(235)=>436,chr(236)=>436,chr(237)=>246,chr(238)=>246,chr(239)=>458,chr(240)=>458,chr(241)=>474,
	chr(242)=>474,chr(243)=>446,chr(244)=>446,chr(245)=>446,chr(246)=>446,chr(247)=>393,chr(248)=>318,chr(249)=>473,chr(250)=>473,chr(251)=>473,chr(252)=>473,chr(253)=>386,chr(254)=>500,chr(255)=>279);
$enc='iso-8859-2';
$diff='128 /.notdef 130 /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef 142 /.notdef 145 /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef 158 /.notdef /.notdef 161 /Aogonek /breve /Lslash 165 /Lcaron /Sacute 169 /Scaron /Scedilla /Tcaron /Zacute 174 /Zcaron /Zdotaccent 177 /aogonek /ogonek /lslash 181 /lcaron /sacute /caron 185 /scaron /scedilla /tcaron /zacute /hungarumlaut /zcaron /zdotaccent /Racute 195 /Abreve 197 /Lacute /Cacute 200 /Ccaron 202 /Eogonek 204 /Ecaron 207 /Dcaron /Dcroat /Nacute /Ncaron 213 /Ohungarumlaut 216 /Rcaron /Uring 219 /Uhungarumlaut 222 /Tcommaaccent 224 /racute 227 /abreve 229 /lacute /cacute 232 /ccaron 234 /eogonek 236 /ecaron 239 /dcaron /dmacron /nacute /ncaron 245 /ohungarumlaut 248 /rcaron /uring 251 /uhungarumlaut 254 /tcommaaccent /dotaccent';
$file='antonio-regular.z';
$originalsize=47944;
?>
