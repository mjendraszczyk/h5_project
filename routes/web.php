<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

// if (env('APP_ENV') === 'local') {
//     URL::forceSchema('https');
// }
/* Test erroru 500 */
Route::get('/error500/catch', function () {
    return gfdg;
});

Route::get('/dostep-zabroniony', function() {
    return 'Dostęp zabroniony';
})->name('redirect_403');
/* Strona glowna */
Route::get('/', 'HomeController@index')->name('homepage');

/* Szkolenia routing */
Route::get('/szkolenia', 'SzkoleniaController@index')->name('szkolenia');
Route::get('/szkolenia/szkolenia-otwarte', 'SzkoleniaController@otwarte')->name('szkolenia_otwarte');
Route::get('/szkolenia/szkolenia-zamkniete', 'SzkoleniaController@zamkniete')->name('szkolenia_zamkniete');
Route::get('/szkolenia/szkolenia-indywidualne', 'SzkoleniaController@indywidualne')->name('szkolenia_indywidualne');
//Route::get('/szkolenia/szkolenia-online', 'SzkoleniaController@online')->name('szkolenia_online');
Route::get('/szkolenia/szkolenia-online', function() {
    return redirect()->route('szkolenia_otwarte');
})->name('szkolenia_online');

Route::get('/szkolenia/szkolenie/{id}/{title}', 'SzkoleniaController@kategoria_otwarte')->name('szkolenia_kategoria');
Route::get('/szkolenia/online/{id}/{title}/{training_type}', function() {
    return redirect()->route('kategoria_otwarte',['id'=>$id,'title'=>$title,'training_type'=>$training_type]);
})->name('szkolenia_kategoria_online');
// Route::get('/szkolenia/online/{id}/{title}/{training_type}', 'SzkoleniaController@kategoria_online')->name('szkolenia_kategoria_online');
Route::get('/szkolenia/zamkniete/szkolenie/{id}/{title}', 'SzkoleniaController@kategoria_zamkniete')->name('szkolenia_kategoria_zamkniete');
Route::get('/szkolenia/szkolenie/{id}/{title}/{id_kurs}/{title_kurs}', 'SzkoleniaController@szkolenie')->name('szkolenia_strona');
// Route::get('/szkolenia/szkolenie-online/{id}/{title}/{id_kurs}/{title_kurs}', 'SzkoleniaController@szkolenie_online')->name('szkolenia_strona_online');
Route::get('/szkolenia/szkolenie-online/{id}/{title}/{id_kurs}/{title_kurs}', function($id,$title,$id_kurs,$title_kurs) {
    return redirect()->route('szkolenia_strona',['id'=>$id,'title'=>$title,'id_kurs'=>$id_kurs,'title_kurs'=>$title_kurs]);
})->name('szkolenia_strona_online');
Route::get('/szkolenia/szkolenie-new/{id}/{title}/{id_kurs}/{title_kurs}', 'SzkoleniaController@szkolenie_new')->name('szkolenia_strona_new');
Route::get('/tag/{tag}', 'SzkoleniaController@tag')->name('tag_szkolenia');
Route::post('/szkolenia/kontakt/mail', 'SzkoleniaController@zapytanie')->name('szkolenia_mail');
/* Hotele szkolenia */
Route::get('/hotel/{id}', 'HotelController@index')->name('hotel');


/* Hotele szkolenia / Wersja z dedykowanym modułem */
Route::get('/hotele/{id}', 'HoteleController@index')->name('hotele');
Route::get('/hotele/{trainingID}/all', 'HoteleController@all')->name('hotele_all');

Route::redirect('/szkolenia', '/', 301);

/* Diagnostyka routing  */
Route::get('/diagnostyka', 'DiagnostykaController@index')->name('diagnostyka');
Route::get('/diagnostyka/{id}/{title}', 'DiagnostykaController@diagnostyka')->name('diag');

/* Coaching routing */
Route::get('/coaching', 'CoachingController@index')->name('coaching');

/* Gry routing */
Route::get('/gry', 'GryController@index')->name('gry');
Route::get('/gry/szczegoly/{id}', 'GryController@gra')->name('gra_detal');

/* Certyfikacja PRISM */
// Route::get('/certyfikacja', 'CertyfikacjaController@index')->name('certyfikacja');
Route::get('/certyfikacja', function() {
    return redirect('/');
})->name('certyfikacja');

/* Terminy routing */
Route::get('/terminy', 'TerminyController@index')->name('terminy');
Route::get('/terminy/{sort}/', 'TerminyController@sort')->name('terminy_sort');
Route::get('/terminy/{id}/{title}', 'TerminyController@kategoria')->name('terminy_kategoria');
Route::get('/terminy/rezerwacja/szkolenie/{id}', 'TerminyController@rezerwacja')->name('rezerwacja_szkolenia');

Route::get('/terminy/rezerwacja/szkolenie-online/{id}', function() {
    return redirect()->route('rezerwacja_szkolenia',['id'=>$id]);
})->name('rezerwacja_szkolenia_online');

// Route::get('/terminy/rezerwacja/szkolenie-online/{id}', 'TerminyController@rezerwacja_online')->name('rezerwacja_szkolenia_online');

Route::post('/terminy/rezerwacja/szkolenie/{id}/zapisz', 'TerminyController@zapisz')->name('zapisz_terminy');
Route::get('/terminy/rezerwacja/szkolenie/{id}/zapisz', 'TerminyController@potwierdzenie')->name('potwierdzenie_termin');

/* API URL */
Route::get('/api/terminy/{miasto}', 'TerminyController@api_terminy')->name('api_terminy');
Route::get('/api/nip/{nip}', 'TerminyController@api_nip')->name('api_nip');
Route::get('/api/szkolenie/{id}', 'SzkoleniaController@api_szkolenie')->name('api_szkolenie');

Route::get('/api/szkolenie-online/{id}', function() {
    return redirect()->route('api_szkolenie',['id'=>$id]);
})->name('api_szkolenie_online');

// Route::get('/api/szkolenie-online/{id}', 'SzkoleniaController@api_szkolenie_online')->name('api_szkolenie_online');
Route::get('/api/program/{id}', 'ProgramyController@api_program')->name('api_program');
Route::get('/api/terminy/rezerwacja/szkolenie/{id}', 'TerminyController@zapisz')->name('api_zapisz_szkolenie');

Route::get('/api/zapytanie', 'SzkoleniaController@zapytanie')->name('api_zapytanie');
Route::post('/api/zapytanie', 'SzkoleniaController@zapytanie')->name('api_zapytanie');

// Sprawdź termin
Route::get('/api/sesje/termin/{id}', 'TerminyController@checkTermParent')->name('api_terminy_sesje');

/* Programy szkoleniowe routing */
Route::get('/programy-rozwojowe', 'ProgramyController@index')->name('programy');
Route::get('/programy-rozwojowe/program/{id}', 'ProgramyController@program')->name('program');
Route::get('/programy-rozwojowe/program/{id}/modul/{id_modul}', 'ProgramyController@modul')->name('modul');
Route::get('/programy-rozwojowe/rezerwacja/program/{id}', 'ProgramyController@rezerwacja')->name('rezerwacja_programy');
Route::post('/programy-rozwojowe/rezerwacja/program/{id}/zapisz', 'TerminyController@zapisz')->name('zapisz_program');
Route::get('/programy-rozwojowe/rezerwacja/program/{id}/zapisz', 'TerminyController@potwierdzenie')->name('potwierdzenie_termin');

/* Aktualnosci */
Route::get('/aktualnosci', 'HistoryController@index')->name('aktualnosci');
Route::get('/aktualnosci/rok/{rok}', 'HistoryController@newsy_kategoria')->name('aktualnosci_kategoria');
Route::get('/aktualnosci/rok/{rok}/news/{id}', 'HistoryController@news')->name('aktualnosci_news');

/* Firma routing */
Route::get('/firma', 'CMSController@index')->name('firma');
Route::get('/firma/trenerzy', 'CMSController@trainers')->name('trenerzy');
Route::get('/firma/kontakt', 'CMSController@contact')->name('kontakt');
Route::post('/firma/kontakt/mail', 'CMSController@contactMail')->name('kontakt_mail');
Route::get('/firma/galeria', 'CMSController@gallery')->name('galeria');
Route::get('/firma/standard-uslugi-szkoleniowej', 'CMSController@sus')->name('sus');
Route::get('/firma/trenerzy/{id}', 'CMSController@instruktor')->name('instruktor');
Route::get('/kfs', 'CMSController@kfs')->name('kfs');
Route::get('/firma/pp', 'CMSController@pp')->name('pp');
Route::get('/firma/voucher', 'CMSController@voucher')->name('voucher');

/* Referencje routing */
Route::get('/referencje/firmy', 'CMSController@referencje_firmy')->name('referencje_firmy');
Route::get('/referencje/osoby', 'CMSController@referencje_osoby')->name('referencje_osoby');
Route::get('/referencje/listy', 'CMSController@referencje_listy')->name('referencje_listy');

/* Blog routing */

Route::get('/blog', 'BlogController@index')->name('blog');
Route::get('/blog/post/{id}/{title}', 'BlogController@post')->name('blog_post');
Route::get('/blog/tag', 'BlogController@index')->name('blog_tagi');
Route::get('/blog/tag/{tag}', 'BlogController@tag')->name('blog_tag');
Route::get('/blog/kategoria/{id}/{title}', 'BlogController@kategoria_blog')->name('kategoria_blog');

/* Wyszukiwanie routing */
Route::get('/search', 'SearchController@index')->name('search');
Route::get('/szkolenia/szkolenia-sekretarki', 'BlogController@tag')->name('szkolenia_sekretarki');
Route::get('/szkolenia/szkolenia-handlowcy', 'BlogController@tag')->name('szkolenia_handlowcy');
Route::get('/szkolenia/szkolenia-menadzerowie', 'BlogController@tag')->name('szkolenia_menadzerowie');

Route::get('/callendar.xml', 'TerminyController@callendar_xml')->name('callendar_xml');

// Regex Routing
Route::get('/szkolenia/{category_name}-{id_category}/{szkolenia_name}{id_szkolenia}.htm', function ($category_name, $id_category, $szkolenia_name, $id_szkolenia) {
    return redirect('/szkolenia/szkolenie/'.$id_category.'/'.$category_name.'/'.$id_szkolenia.'/'.substr($szkolenia_name, 0, -1));
})
        ->where(['category_name' => '[a-z-]+[^-t0-9]', 'id_category' => '[0-9]+', 'szkolenia_name' => '[a-z-].*[-]', 'id_szkolenia' => '[0-9]+']);

// Sitemap Routing
Route::get('/sitemap.xml', 'SitemapController@index')->name('sitemap');

//Zgoda na marketing

Route::get('/marketing/zgoda/{email}', 'TerminyController@zgoda_dane')->name('zgoda_marketing');

// Katalog Szkoleń
// Route::get('/katalog.pdf', '/pdf/katalog.pdf', 301);
//Route::redirect('/piwik', '/piwik/index.php', 301);
Route::redirect('/pdf_gen.php', '/admin/pdf_gen.php', 301);
Route::redirect('/pdf_gen_rabat.php', '/admin/pdf_gen_rabat.php', 301);
Route::redirect('/calendar.php', '/phpFunctions/calendar.php', 301);

Route::redirect('/katalog.pdf', '/pdf/katalog.pdf', 301);
Route::redirect('/parkingi', '/pdf/Parkingi.pdf', 301);
Route::redirect('/mapka', '/pdf/LokalizacjaHIGH5.pdf', 301);
Route::redirect('/kontakt', '/firma/kontakt', 301);
Route::redirect('/galeria', '/firma/galeria', 301);
Route::redirect('/sus', '/firma/standard-uslugi-szkoleniowej', 301);
Route::redirect('/trenerzy', '/firma/trenerzy', 301);
Route::redirect('/referencje_osoby', '/referencje/osoby', 301);
Route::redirect('/referencje_firmy', '/referencje/firmy', 301);
Route::redirect('/referencje_listy', '/referencje/listy', 301);

Route::redirect('/programy', '/programy-rozwojowe', 301);
Route::redirect('/terminy_sort', '/terminy/cat', 301);
Route::redirect('/terminy_kategoria', '/terminy/cat', 301);

Route::redirect('/szkolenia_indywidualne', '/szkolenia/szkolenia-indywidualne', 301);
Route::redirect('/szkolenia_otwarte', '/szkolenia/szkolenia-otwarte', 301);
Route::redirect('/szkolenia_zamkniete', '/szkolenia/szkolenia-zamkniete', 301);

Route::redirect('/img/frontend/referencje/Izba Celna.pdf', '/img/frontend/referencje/izba-celna.pdf', 301);
Route::redirect('/img/frontend/referencje/Urzad Marszalkowski Wojewodztwa Slaskiego.pdf', '/img/frontend/referencje/urzad-marszalkowski-wojewodztwa-slaskiego.pdf', 301);
Route::redirect('/img/frontend/referencje/Ministerstwo Finansów.pdf', '/img/frontend/referencje/ministerstwo-finansow.pdf', 301);
Route::redirect('/img/frontend/referencje/Wyzsza Szkola Raciborz.pdf', '/img/frontend/referencje/wyzsza-szkola-raciborz.pdf', 301);

// Route::get('/firma.php?dzial={section_name}', function ($section_name) {
//     return redirect('/firma'.$section_name);
// })
// ->where(['section_name' => '[a-z].*']);
//Route::redirect('/firma.php', '/firma', 301);
//Route::redirect('/firma.php?dzial=kontakt', '/firma/kontakt', 301);
// -----------------------------------

/* Panel admina routing */

Route::get('/newsletter/usun', 'backend\NewsletterController@deleteNewsletter')->name('NewsletterDelete');
Route::post('/newsletter/wypisz', 'backend\NewsletterController@NewsletterWypisz')->name('NewsletterWypisz');

Route::get('backoffice/szkolenia/feed/icsfile', 'backend\CRMSzkolenieController@createISCFile')->name('createISCFile');


 Route::get('mail/zdarzenia','backend\CRMZdarzeniaController@cronZdarzeniaReminder')->name('cronZdarzeniaReminder');

            
/* Panel admina routing */
Route::group(['middleware' => ['auth']], function () {
    /// Ordering position



    Route::redirect('/backoffice', '/cmr/backoffice', 301);

    Route::prefix('cms')->group(function () {
        //Route::get('item/{id}/cat/{idCategory}/position/{position}/move/{operation}', 'backend\SzkoleniaController@change_position')->name('change_position');
        Route::post('backoffice/szkolenia/item/{id}/position/move', 'backend\SzkoleniaController@change_position')->name('change_position');

        Route::group(['middleware' => ['redaktor']], function () {
        Route::resource('backoffice/top10', 'backend\Top10Controller', [
    'names' => [
    'index' => 'indexTop10',
    ],
    ]);
  
    Route::resource('backoffice/hotele', 'backend\HoteleController', [
    'names' => [
    'index' => 'indexHotele',
    'create' => 'createHotele',
    'edit' => 'editHotele',
    'update' => 'updateHotele',
    'store' => 'storeHotele',
    'destroy' => 'destroyHotele',
    ],
    ]);



        Route::resource('backoffice/referencje', 'backend\ReferencjeController', [
    'names' => [
    'index' => 'indexReferencje',
    'create' => 'createReferencje',
    'edit' => 'editReferencje',
    'update' => 'updateReferencje',
    'store' => 'storeReferencje',
    'destroy' => 'destroyReferencje',
    ],
    ]);
        Route::resource('backoffice/szkolenia', 'backend\SzkoleniaController', [
    'names' => [
    'index' => 'indexSzkolenia',
    'create' => 'createSzkolenia',
    'edit' => 'editSzkolenia',
    'update' => 'updateSzkolenia',
    'store' => 'storeSzkolenia',
    'destroy' => 'destroySzkolenia',
    ],
    ]);

        Route::resource('backoffice/szkolenie/{szkolenieID}/opinie', 'backend\OpinieController', [
    'names' => [
    'index' => 'indexSzkoleniaOpinia',
    'create' => 'createSzkoleniaOpinia',
    'edit' => 'editSzkoleniaOpinia',
    'store' => 'storeSzkoleniaOpinia',
    'update' => 'updateSzkoleniaOpinia',
    'destroy' => 'destroySzkoleniaOpinia',
    ],
    ]);

        Route::resource('backoffice/link', 'backend\LinkController', [
    'names' => [
    'index' => 'indexLink',
    'create' => 'createLink',
    'edit' => 'editLink',
    'update' => 'updateLink',
    'store' => 'storeLink',
    'destroy' => 'destroyLink',
    ],
    ]);

        Route::post('backoffice/terminarz/filtr/rezultat', 'backend\TerminarzController@filtrTerminarz')->name('filtrTerminarz');
        Route::get('backoffice/terminarz/get/api/terminarz/{id}', 'backend\TerminarzController@getAPITerminarz')->name('getAPITerminarz');

        Route::resource('backoffice/blog', 'backend\BlogController', [
    'names' => [
    'index' => 'indexBlog',
    'create' => 'createBlog',
    'edit' => 'editBlog',
    'update' => 'updateBlog',
    'store' => 'storeBlog',
    'destroy' => 'destroyBlog',
    ],
    ]);

        Route::resource('backoffice/trenerzy', 'backend\InstructorController', [
    'names' => [
    'index' => 'indexInstructor',
    'create' => 'createInstructor',
    'edit' => 'editInstructor',
    'update' => 'updateInstructor',
    'store' => 'storeInstructor',
    'destroy' => 'destroyInstructor',
    ],
    ]
    );

        Route::resource('backoffice/headlines', 'backend\HeadlinesController', [
    'names' => [
    'index' => 'indexHeadline',
    'create' => 'createHeadline',
    'edit' => 'editHeadline',
    'update' => 'updateHeadline',
    'store' => 'storeHeadline',
    'destroy' => 'destroyHeadline',
    ],
    ]
    );

        Route::resource('backoffice/gry', 'backend\GryController', [
    'names' => [
    'index' => 'indexGry',
    'create' => 'createGry',
    'edit' => 'editGry',
    'update' => 'updateGry',
    'store' => 'storeGry',
    'destroy' => 'destroyGry',
    ],
    ]
    );

    
      

        Route::resource('backoffice/history', 'backend\HistoryController', [
    'names' => [
    'index' => 'indexHistory',
    'create' => 'createHistory',
    'edit' => 'editHistory',
    'update' => 'updateHistory',
    'store' => 'storeHistory',
    'destroy' => 'destroyHistory',
    ],
    ]
    );

        Route::get('backoffice/programy/program/{id}/lista/szkolenie/create', 'backend\ProgramyController@createModulyProgramy')->name('createModulyProgramy');
        Route::post('backoffice/programy/program/{id}/lista/szkolenie/', 'backend\ProgramyController@storeModulyProgramy')->name('storeModulyProgramy');
        Route::get('backoffice/programy/program/{id}/lista', 'backend\ProgramyController@indexModulyProgramy')->name('indexModulyProgramy');
        Route::delete('backoffice/programy/program/{id}/lista/szkolenie/{idSzkolenie}', 'backend\ProgramyController@destroyModulyProgramy')->name('destroyModulyProgramy');

        Route::post('backoffice/programy/program/{id}/lista/item/{kolejnosc}/position/move', 'backend\ProgramyController@change_position_program')->name('change_position_program');

        Route::resource('backoffice/programy', 'backend\ProgramyController', [
    'names' => [
    'index' => 'indexProgramy',
    'create' => 'createProgramy',
    'edit' => 'editProgramy',
    'update' => 'updateProgramy',
    'store' => 'storeProgramy',
    'destroy' => 'destroyProgramy',
    ],
    ]
    );

        Route::resource('backoffice/akademie', 'backend\AkademieController', [
    'names' => [
    'index' => 'indexAkademie',
    'create' => 'createAkademie',
    'edit' => 'editAkademie',
    'update' => 'updateAkademie',
    'store' => 'storeAkademie',
    'destroy' => 'destroyAkademie',
    ],
    ]
    );
        //Dodawanie szkolen
        Route::get('backoffice/akademie/{idAkademia}/lista', 'backend\AkademieController@listSzkolenia')->name('listSzkolenia');
        Route::get('backoffice/akademie/{idAkademia}/dodaj', 'backend\AkademieController@createSzkolenie')->name('createASzkolenie');
        Route::post('backoffice/akademie/{idAkademia}/zapisz', 'backend\AkademieController@storeSzkolenie')->name('storeSzkolenie');

        Route::get('backoffice/akademie/{idAkademia}/usun/{id}', 'backend\AkademieController@deleteASzkolenie')->name('deleteASzkolenie');

        Route::resource('backoffice/blog', 'backend\BlogController', [
    'names' => [
    'index' => 'indexBlog',
    'create' => 'createBlog',
    'edit' => 'editBlog',
    'update' => 'updateBlog',
    'store' => 'storeBlog',
    'destroy' => 'destroyBlog',
    ],
    ]
    );

        Route::resource('backoffice/terminarz', 'backend\TerminarzController', [
    'names' => [
    'index' => 'indexTerminarz',
    'create' => 'createTerminarz',
    'edit' => 'editTerminarz',
    'update' => 'updateTerminarz',
    'store' => 'storeTerminarz',
    'destroy' => 'destroyTerminarz',
    ],
    ]
    );

        Route::get('backoffice/terminarz/{id}/odepnij', 'backend\TerminarzController@odepnij')->name('odepnij_terminarz');

        Route::resource('backoffice/konsultacje', 'backend\KonsultacjeController', [
    'names' => [
    'index' => 'indexKonsultacje',
    'create' => 'createKonsultacje',
    'edit' => 'editKonsultacje',
    'update' => 'updateKonsultacje',
    'store' => 'storeKonsultacje',
    'destroy' => 'destroyKonsultacje',
    ],
    ]
    );

        Route::resource('backoffice/promocje', 'backend\PromocjeController', [
    'names' => [
    'index' => 'indexPromocje',
    'create' => 'createPromocje',
    'edit' => 'editPromocje',
    'update' => 'updatePromocje',
    'store' => 'storePromocje',
    'destroy' => 'destroyPromocje',
    ],
    ]
    );

        //Route::get('backoffice/modalwindow/{id}/shop')
        Route::resource('backoffice/modalwindow', 'backend\ModalWindowController', [
    'names' => [
    'index' => 'indexModalWindow',
    'create' => 'createModalWindow',
    'edit' => 'editModalWindow',
    'update' => 'updateModalWindow',
    'store' => 'storeModalWindow',
    'show' => 'showModalWindow',
    'destroy' => 'destroyModalWindow',
    ],
    ]);

       });
    });

    // koniec CMS


    // start CMR 
    Route::prefix('cmr')->group(function () {


 Route::group(['middleware' => 'admin'], function () {

            
         Route::resource('backoffice/uzytkownicy', 'backend\UzytkownicyController', [
    'names' => [
    'index' => 'indexUzytkownicy',
    'create' => 'createUzytkownicy',
    'edit' => 'editUzytkownicy',
    'update' => 'updateUzytkownicy',
    'store' => 'storeUzytkownicy',
    'destroy' => 'destroyUzytkownicy',
    ],
    ]);

  Route::resource('backoffice/role', 'backend\RoleController', [
    'names' => [
    'index' => 'indexRole',
    'create' => 'createRole',
    'edit' => 'editRole',
    'update' => 'updateRole',
    'store' => 'storeRole',
    'destroy' => 'destroyRole',
    ],
    ]
    );


        Route::resource('backoffice/newsletter', 'backend\NewsletterController', [
    'names' => [
    'index' => 'indexNewsletter',
    'create' => 'createNewsletter',
    'edit' => 'editNewsletter',
    'update' => 'updateNewsletter',
    'store' => 'storeNewsletter',
    'destroy' => 'destroyNewsletter',
    ],
    ]);

        Route::post('backoffice/newsletter/podglad', 'backend\NewsletterController@showNewsletter')->name('showNewsletter');
        Route::post('backoffice/newsletter/wyslij', 'backend\NewsletterController@NewsletterSend')->name('NewsletterSend');
        
    });

        Route::get('/backoffice', 'backend\DashboardController@index')->name('admin');
        Route::post('/backoffice', 'backend\DashboardController@filtrPlaceDashboard')->name('filtr_dash_miasto');

   
    

        Route::resource('backoffice/firmy', 'backend\CRMFirmyController', [
    'names' => [
    'index' => 'indexCRMFirmy',
    'create' => 'createCRMFirmy',
    'edit' => 'editCRMFirmy',
    'update' => 'updateCRMFirmy',
    'store' => 'storeCRMFirmy',
    'destroy' => 'destroyCRMFirmy',
    ],
    ]);

        Route::post('backoffice/firmy/filtr/telefon', 'backend\CRMFirmyController@filtrCRMFirmyPhone')->name('filtrCRMFirmyPhone');

        Route::get('backoffice/firmy/{klientID}/konsoliduj/{method}', 'backend\CRMFirmyController@konsolidujCRMFirmy')->name('konsolidujCRMFirmy');
        Route::post('backoffice/firmy/{klientID}/konsoliduj/proces', 'backend\CRMFirmyController@storekonsolidujCRMFirmy')->name('storekonsolidujCRMFirmy');
        Route::get('backoffice/firmy/letter/{char}', 'backend\CRMFirmyController@filtruj')->name('filtrujCRMFirmy');
        Route::post('backoffice/firma/find/result', 'backend\DashboardController@findFirmaDashboard')->name('findCRMFirmy');

        Route::resource('backoffice/wyszukiwanie', 'backend\CRMWyszukiwanieController', [
    'names' => [
    'index' => 'indexCRMWyszukiwanie',
    ],
    ]);

        Route::post('backoffice/wyszukiwanie/osoby', 'backend\CRMWyszukiwanieController@searchOsoby')->name('searchOsoby');
        Route::get('backoffice/wyszukiwanie/osoby/{result}', 'backend\CRMWyszukiwanieController@ResultSearchOsoby')->name('ResultSearchOsoby');

        Route::post('backoffice/wyszukiwanie/szkolenia', 'backend\CRMWyszukiwanieController@searchSzkolenia')->name('searchSzkolenia');
        Route::get('backoffice/wyszukiwanie/szkolenia/{result}/{ankieta}/{od}/{do}', 'backend\CRMWyszukiwanieController@ResultSearchSzkolenia')->name('ResultSearchSzkolenia');

        Route::post('backoffice/wyszukiwanie/zainteresowania', 'backend\CRMWyszukiwanieController@searchZainteresowania')->name('searchZainteresowania');
        Route::get('backoffice/wyszukiwanie/zainteresowania/{result}', 'backend\CRMWyszukiwanieController@ResultSearchZainteresowania')->name('ResultSearchZainteresowania');

        //Route::get('backoffice/wyszukiwanie/{nazwa}', 'backend\CRMWyszukiwanieController@index')->name('CRMWyszukiwanieController');
        Route::get('backoffice/wyszukiwanie/modal/mail', 'backend\CRMWyszukiwanieController@mailCRMWyszukiwanie')->name('mailCRMWyszukiwanie');
        Route::post('backoffice/wyszukiwanie/modal/mail/send/{typ}', 'backend\CRMWyszukiwanieController@SendmailCRMWyszukiwanie')->name('SendmailCRMWyszukiwanie');

        Route::resource('backoffice/lista-hr', 'backend\CRMListaHRController', [
    'names' => [
    'index' => 'indexCRMListaHR',
    'create' => 'createCRMListaHR',
    'edit' => 'editCRMListaHR',
    'update' => 'updateCRMListaHR',
    'store' => 'storeCRMListaHR',
    'destroy' => 'destroyCRMListaHR',
    ],
    ]);


    

    Route::get('backoffice/firmy/{klientID}/przypisz', 'backend\CRMFirmyController@przypiszOpiekuna')->name('przypiszOpiekuna');

        Route::get('backoffice/lista-hr/generate/excel/opiekun/{opiekunID}', 'backend\CRMListaHRController@getListaHR')->name('getListaHR');

        Route::get('backoffice/lista-hr/uczestnik/{id}/zdarzenia', 'backend\CRMListaHRController@getZdarzeniaFromHR')->name('getZdarzeniaFromHR');
        Route::post('backoffice/lista-hr/update/ajax/{id}/update', 'backend\CRMListaHRController@updateAjaxHROsoba')->name('updateAjaxHROsoba');

        Route::post('backoffice/lista-hr/opiekun', 'backend\CRMListaHRController@getHrForOpiekun')->name('getHrForOpiekun');
        Route::get('backoffice/lista-hr/opiekun/{id}', 'backend\CRMListaHRController@GETgetHrForOpiekun')->name('GETgetHrForOpiekun');

        //Faktury

        Route::get('backoffice/faktury', 'backend\CRMFakturyController@index')->name('indexCRMFaktury');
       

        //Route::get('backoffice/faktury/zaliczkowe/{klientID}/{szkolenieTermID}/create', 'backend\CRMFakturyController@create')->name('createZaliczkaCRMFaktury');
        Route::group(['middleware' => 'admin'], function () {
             Route::get('backoffice/faktury/{klientID}/{uczestnikID}/{szkolenieTermID}/create/typ/{typ}', 'backend\CRMFakturyController@create')->name('createCRMFaktury');

            Route::get('backoffice/faktury/{id}/edit', 'backend\CRMFakturyController@edit')->name('editCRMFaktury');
            Route::post('backoffice/faktury/', 'backend\CRMFakturyController@store')->name('storeCRMFaktury');
            Route::patch('backoffice/faktury/{id}/update', 'backend\CRMFakturyController@update')->name('updateCRMFaktury');
            Route::delete('backoffice/faktury/{id}/destroy', 'backend\CRMFakturyController@destroy')->name('destroyCRMFaktury');
            Route::get('backoffice/faktury/{id}/destroy', 'backend\CRMFakturyController@destroy')->name('GETdestroyCRMFaktury');
        });
        // Api Nabywca
        Route::get('backoffice/faktury/api/klient/{klientID}', 'backend\CRMFakturyController@NabywcaAPI')->name('NabywcaAPI');

        Route::post('backoffice/faktury/filtr/miesiac/{miesiac}/rok/{rok}', 'backend\CRMFakturyController@filtrFaktury')->name('filtrFaktury');

        Route::post('backoffice/faktury/update/wysylka/{id}/update', 'backend\CRMFakturyController@updateWysylka')->name('updateWysylka');

        //  updateAjax
        Route::post('backoffice/faktury/update/wysylka/ajax/{id}/update', 'backend\CRMFakturyController@updateAjaxWysylka')->name('updateAjaxWysylka');

        Route::get('backoffice/faktury/numeracja/typ/{typ}', 'backend\CRMFakturyController@getLastNrFaktury')->name('getLastNrFaktury');
        Route::get('backoffice/faktury/jpk/miesiac/{miesiac}/rok/{rok}', 'backend\CRMFakturyController@getJPKFromRange')->name('getJPKFromRange');

        Route::get('backoffice/faktury/zestawienie/miesiac/{miesiac}/rok/{rok}', 'backend\CRMFakturyController@createZestawienieFaktur')->name('createZestawienieFaktur');

        Route::get('backoffice/faktury/miesiac/{miesiac}/rok/{rok}', 'backend\CRMFakturyController@getFakturyFromRange')->name('getFakturyFromRange');

        Route::get('backoffice/faktury/api/gus/{nip}', 'backend\CRMFakturyController@gus_api')->name('GusApi');

        // Osoby
        Route::get('backoffice/osoby/{klientID}/create', 'backend\CRMOsobyController@create')->name('createCRMOsoby');
        Route::get('backoffice/osoby/{klientID}/{id}/edit', 'backend\CRMOsobyController@edit')->name('editCRMOsoby');
        Route::post('backoffice/osoby/{klientID}/', 'backend\CRMOsobyController@store')->name('storeCRMOsoby');
        Route::patch('backoffice/osoby/{klientID}/{id}/update', 'backend\CRMOsobyController@update')->name('updateCRMOsoby');
        Route::delete('backoffice/osoby/{klientID}/{id}/destroy', 'backend\CRMOsobyController@destroy')->name('destroyCRMOsoby');

        Route::get('backoffice/osoby/autocomplete', 'backend\CRMOsobyController@autocomplete')->name('osobyAutocomplete');

        Route::get('backoffice/wyszukiwnie/szkolenia/autocomplete', 'backend\CRMWyszukiwanieController@autocomplete')->name('szkoleniaAutocomplete');

        // CRMSzkolenie
        Route::get('backoffice/szkolenie/termin/{termID}', 'backend\CRMSzkolenieController@show')->name('showCRMSzkolenie');
        Route::get('backoffice/szkolenie/termin/create/{klientID}', 'backend\CRMSzkolenieController@create')->name('createCRMSzkolenie');
        Route::get('backoffice/szkolenie/termin/create/{klientID}/modal', 'backend\CRMSzkolenieController@modal')->name('ModalcreateCRMSzkolenie');
        Route::get('backoffice/szkolenie/termin/zmien/{crmszkolenieID}/modal', 'backend\CRMSzkolenieController@zmienTerminModal')->name('zmienTerminModal');
        Route::post('backoffice/szkolenia/termin/zmien/{crmszkolenieID}/modal/zapisz', 'backend\CRMSzkolenieController@zmienTerminCRMSzkolenie')->name('zmienTerminCRMSzkolenie');

        Route::get('backoffice/szkolenia/termin/mail/{termID}', 'backend\CRMSzkolenieController@mailCRMSzkolenie')->name('mailCRMSzkolenie');

        Route::post('backoffice/szkolenie/termin/store', 'backend\CRMSzkolenieController@store')->name('storeCRMSzkolenie');

        Route::delete('backoffice/szkolenie/termin/rezerwacja/{id}/delete', 'backend\CRMSzkolenieController@destroyRezerwacja')->name('deleteCRMSzkolenieRezerwacja');

        Route::post('backoffice/szkolenie/termin/rezerwacja/{id}/{field}', 'backend\CRMSzkolenieController@ajaxUpdateSzkolenie')->name('ajaxUpdateSzkolenie');

        Route::get('backoffice/szkolenie/termin/{termID}/karta-ternera', 'backend\CRMSzkolenieController@generateKartaTreneraCRMSzkolenie')->name('generateKartaTreneraCRMSzkolenie');
        Route::get('backoffice/szkolenie/termin/{termID}/lista-obecnosci', 'backend\CRMSzkolenieController@generateListaObecnosciCRMSzkolenie')->name('generateListaObecnosciCRMSzkolenie');

        // CRMSzkolenie
        // Zdarzenia stare
        Route::get('backoffice/stare/zdarzenia', 'backend\CRMZdarzeniaController@index')->name('indexCRMZdarzenia');
        Route::get('backoffice/stare/zdarzenia/{id}/edit', 'backend\CRMZdarzeniaController@edit')->name('editCRMZdarzenia');
        Route::get('backoffice/stare/zdarzenia/{id}/{method}/create/', 'backend\CRMZdarzeniaController@create')->name('createCRMZdarzenie');

        // Zdarzenia nowe
       

        Route::get('backoffice/zdarzenia', 'backend\CRMZdarzeniaController@indexNew')->name('indexCRMZdarzeniaNew');
        Route::get('backoffice/zdarzenia/{id}/edit', 'backend\CRMZdarzeniaController@editNew')->name('editCRMZdarzeniaNew');
        
        
        Route::post('backoffice/zdarzenia', 'backend\CRMZdarzeniaController@storeNew')->name('storeCRMZdarzeniaNew');

        Route::post('backoffice/zdarzenia/{id}/edit', 'backend\CRMZdarzeniaController@updateNew')->name('updateCRMZdarzeniaNew');
        Route::get('backoffice/zdarzenia/create/', 'backend\CRMZdarzeniaController@createNew')->name('createCRMZdarzeniaNew');
        Route::post('backoffice/zdarzenia/filtr', 'backend\CRMZdarzeniaController@filtrZdarzeniaNew')->name('filtrZdarzeniaNew');
         Route::get('backoffice/zdarzenia/typ/{typID}/opiekun/{opiekunID}', 'backend\CRMZdarzeniaController@getResultsZdarzeniaNew')->name('getResultsZdarzeniaNew');

        // Ankiety
        Route::get('backoffice/ankieta/firma/{klientID}/uczestnik/{uczestnikID}/term/{termID}/create', 'backend\CRMSzkolenieController@createCRMAnkieta')->name('createCRMAnkieta');
        Route::post('backoffice/ankieta/firma/{klientID}/uczestnik/{uczestnikID}/term/{termID}/store', 'backend\CRMSzkolenieController@storeCRMAnkieta')->name('storeCRMAnkieta');

        Route::get('backoffice/ankieta/szkolenia/get/all', 'backend\CRMSzkolenieController@getAllSzkolenia')->name('getAllSzkolenia');
        //Route::get('backoffice/zdarzenia/create/{id}/modal', 'backend\CRMZdarzeniaController@modal')->name('ModalcreateCRMZdarzenie');

        Route::get('backoffice/zdarzenia/aktywnosci/{uczestnikID}', 'backend\CRMZdarzeniaController@getZdarzeniaFromUczestnik')->name('getZdarzeniaFromUczestnik');
        Route::patch('backoffice/zdarzenia/{id}/update', 'backend\CRMZdarzeniaController@update')->name('updateCRMZdarzenia');
        Route::post('backoffice/zdarzenia/store', 'backend\CRMZdarzeniaController@store')->name('storeCRMZdarzenia');
        Route::post('backoffice/zdarzenia/nowe', 'backend\CRMZdarzeniaController@store')->name('ZdarzeniaNowe'); // ??
        Route::post('backoffice/zdarzenia/stare/filtr', 'backend\CRMZdarzeniaController@filtrZdarzenia')->name('filtrZdarzenia');
        Route::get('backoffice/zdarzenia/stare/typ/{typID}/opiekun/{opiekunID}', 'backend\CRMZdarzeniaController@getResultsZdarzenia')->name('getResultsZdarzenia');

        Route::post('backoffice/zdarzenia/ajax/{id}/update', 'backend\CRMZdarzeniaController@updateAjaxZdarzenie')->name('updateAjaxZdarzenie');

        Route::post('backoffice/zdarzenia/ajax/{id}/komentarz/update', 'backend\CRMZdarzeniaController@updateAjaxZdarzenieKomentarz')->name('updateAjaxZdarzenieKomentarz');

                Route::post('backoffice/zdarzenia/ajax/{id}/data/update', 'backend\CRMZdarzeniaController@updateAjaxZdarzenieData')->name('updateAjaxZdarzenieData');
        
        Route::post('backoffice/zdarzenia/ajax/{id}/opiekun/update', 'backend\CRMZdarzeniaController@updateAjaxZdarzenieOpiekun')->name('updateAjaxZdarzenieOpiekun');

        Route::post('backoffice/zdarzenia/ajax/{id}/stan/update', 'backend\CRMZdarzeniaController@updateAjaxZdarzenieStatus')->name('updateAjaxZdarzenieStatus');
    });

    // koniec CMR
});

Auth::routes();