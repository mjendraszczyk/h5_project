<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReferencesPriv extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('references_priv', function (Blueprint $table) {
            $table->increments('referencePrivID');
            $table->integer('szkolenieID');
            $table->text('tresc');
            $table->string('imie',64);
            $table->text('firma');
            $table->double('ocena');
            $table->enum('active',['y','n']);
            
             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('references_priv');
    }
}
