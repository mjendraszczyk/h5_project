<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCattrain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
 
        Schema::create('cattrain', function (Blueprint $table) {
            $table->increments('cattrainID');
            $table->integer('trainingID')->unsigned();
            $table->integer('categoryID')->unsigned();
            $table->integer('position')->nullable();
         
        //    $table->foreign('categoryID')->references('categoryID')->on('categories')->onUpdate('cascade')->onDelete('cascade');
        //    $table->foreign('trainingID')->references('trainingID')->on('training')->onUpdate('cascade')->onDelete('cascade');
             
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cattrain');
    }
}
