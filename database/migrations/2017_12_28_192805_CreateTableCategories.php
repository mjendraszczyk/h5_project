<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('categoryID');
            $table->integer('parentID');
            $table->string('category_title',128);
            $table->string('short_title',128);
            $table->string('title_rewrite',128);
            $table->integer('cat_position');
            $table->enum('open',['y','n']);
            $table->enum('close',['y','n']);

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
