<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInstructor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instructor', function (Blueprint $table) {
            $table->increments('instructorID');
            $table->string('instructor_name',48);
            $table->string('instructor_mail',32);
            $table->text('instructor_fakemail');
            $table->string('instructor_phone',32);
            $table->text('instructor_lid');
            $table->text('instructor_desc');
            $table->text('instructor_refs');
            $table->text('instructor_scope');
            $table->string('instructor_photo',255);
            $table->enum('instructor_accepted',['0','1']);
            $table->integer('instructor_position');
             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instructor');
    }
}
