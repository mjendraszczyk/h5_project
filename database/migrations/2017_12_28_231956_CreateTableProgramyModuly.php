<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProgramyModuly extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programy_moduly', function (Blueprint $table) {
            $table->increments('program_modulID');
            $table->integer('programID')->unsigned();
            $table->integer('trainingID');
            $table->integer('kolejnosc');
            // $table->integer('h5_programy_programID');
            // $table->timestamps();

            // $table->foreign('programID')->references('programID')->on('programy');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programy_moduly');
    }
}
