<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       	

        Schema::create('history', function (Blueprint $table) {
            $table->engine = 'MyISAM';
             $table->increments('newsID');
            $table->string('news_title',255);
            $table->text('news_lid');
            $table->text('news_text');
            $table->string('news_link',255);
            $table->date('news_data');
            $table->string('news_picture',255);
            $table->string('news_pictur_pos',50);
            $table->integer('news_place');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('histories');
    }
}
