<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHotele extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotele', function (Blueprint $table) {
            $table->increments('id_hotele');
            $table->text('zdjecie1');
            $table->text('zdjecie2');
            $table->float('odleglosc_od');
            $table->string('kod');
            $table->string('ulica');
            $table->string('telefon');
            $table->float('cena_pokoj_jedno');
            $table->text('uwagi');
            $table->integer('parking');
            $table->integer('wazne');
            $table->text('nazwa');
            $table->string('miasto');
            $table->float('cena_parking');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
