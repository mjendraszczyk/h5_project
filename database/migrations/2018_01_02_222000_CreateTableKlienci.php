<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKlienci extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_klienci', function (Blueprint $table) {
            $table->increments('klientID');
            $table->string('nazwa',255);
            $table->string('miasto',64);
            $table->string('kod',6);
            $table->string('adres',64);
            $table->string('nip',14);
            $table->string('tel',16);
            $table->string('fax',16);
             $table->string('branza',32);
             $table->enum('referencje',['0','1']);
             $table->smallInteger('opiekunID');
             $table->enum('vip',['0','1']);
             $table->smallInteger('rabat');
             $table->text('wazne');
             $table->tinyInteger('ranga');
             $table->tinyInteger('last_minute');
             $table->tinyInteger('typ_newslettera');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_klienci');
    }
}
