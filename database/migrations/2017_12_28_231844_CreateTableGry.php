<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gry', function (Blueprint $table) {
            $table->increments('graID');
            $table->text('nazwa');
            $table->text('podtytul');
            $table->text('opis');
            $table->text('dla_kogo');
            $table->text('organizacja');
            $table->text('korzysci');
            $table->string('zdjecie',256);
             
             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gry');
    }
}
