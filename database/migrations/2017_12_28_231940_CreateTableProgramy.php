<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProgramy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programy', function (Blueprint $table) {
            $table->increments('programID');
            $table->text('nazwa');
            $table->text('podtytul');
            
            $table->text('korzysc');
            $table->text('dla_kogo');
            $table->enum('aktywny',['0','1']);
            $table->integer('cena');

    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programy');
    }
}
