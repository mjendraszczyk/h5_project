<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTerms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terms', function (Blueprint $table) {
            $table->increments('termID');
            $table->integer('trainingID')->unsigned();
            $table->string('term_start',20);
            $table->string('term_end',20)->nullable();
             $table->string('term_place',255);
            $table->integer('term_placeID')->nullable();
            $table->integer('term_fdays');
            $table->integer('term_frabat');
            $table->integer('term_state');
            $table->integer('term_type');
            $table->string('term_color',11);
            $table->enum('term_closed',['y','n']);
            $table->integer('term_instructor');
            
        //    $table->foreign('trainingID')->references('trainingID')->on('training');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terms');
    }
}
