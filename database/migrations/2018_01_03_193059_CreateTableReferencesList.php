<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReferencesList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('references_list', function (Blueprint $table) {
            $table->increments('references_listID');
            $table->string('klient',64);
            $table->enum('zawiera_pdf',['0','1']);
            $table->string('plik_pdf',255);
            $table->string('obrazek',255);
            $table->text('szczegoly');
            $table->smallInteger('rok');
       
             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('references_list');
    }
}
