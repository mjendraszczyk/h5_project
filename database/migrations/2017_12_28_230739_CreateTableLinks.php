<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLinks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->increments('linkID');
            $table->integer('primaryID')->nullable();
            $table->integer('link1ID')->nullable();
            $table->integer('link2ID')->nullable();
            $table->integer('link3ID')->nullable();
            $table->integer('link4ID')->nullable();
            $table->integer('link5ID')->nullable();
               
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
