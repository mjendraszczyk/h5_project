<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTraining extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training', function (Blueprint $table) {
            $table->increments('trainingID');
            $table->string('training_title',255);
            $table->string('meta_title',255);
             $table->text('training_lid');
             $table->text('training_target');
             $table->text('training_demand');
             $table->text('training_advantage');
             $table->text('training_endings');
             $table->text('training_program');
             $table->double('training_price');
             $table->integer('training_hours')->nullable();
             $table->integer('training_tr1')->nullable();
             $table->integer('training_tr2')->nullable();
             $table->integer('training_tr3')->nullable();
             $table->string('training_code',11);
             $table->integer('training_cite');
             $table->timestamp('training_lastmodification');
             $table->integer('training_position')->nullable();
             $table->enum('training_sek',['0','1']);
             $table->enum('training_news',['0','1']);
             $table->string('training_activities',30);
             $table->text('dla_kogo');
             $table->enum('modul_programu',['0','1']);
             $table->enum('prism',['0','1']);
 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training');
    }
}
