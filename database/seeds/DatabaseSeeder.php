<?php

use Illuminate\Database\Seeder;
 use App\Cattrain;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(CrmKlienciTableSeeder::class);

        $path = Config::get('app.url').'/sql/foreign/h5_categories.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('KATEGORIE DODANE !');

        $path = Config::get('app.url').'/sql/foreign/h5_training.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('TRENINGI DODANE !');

        $path = Config::get('app.url').'/sql/foreign/h5_cattrain.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('CATTRAIN DODANE !');

    
        $path = Config::get('app.url').'/sql/foreign/h5_terms.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('TERMSY DODANE !');


 
 

        DB::table('headlines')->insert([
[
    'picture' => 'big_picture04.jpg',
    'title' => 'Akademia Menedżera',
    'content'=> 'Szkolenie zarządzanie zespołem, zarządzanie zespołem poziom zaawansowany',
    'link'=> 'http://www.high5.com.pl/szkolenia/dla-managerow-i-kierownikow-2/zarzadzanie-zespolem-62.htm',
    'position'=> 2,
   'created_at' => date('Y-m-d'),
   'updated_at' => date('Y-m-d')
], 
[
    'picture' => 'big_picture01.jpg',
    'title' => 'Szkolenia biznesowe, teraz w wersji PREMIUM',
    'content'=>  'Zarządzanie czasem, asertywność, komunikacja, zarządzanie stresem.',
    'link'=> 'https://www.high5.com.pl/szkolenia/dla-pracownikow-4/',
    'position'=> 1,
    'created_at' => date('Y-m-d'),
    'updated_at' => date('Y-m-d')
], 
[
    'picture' => 'big_picture03.jpg',
    'title' => 'Zarządzanie czasem PREMIUM',
    'content'=> 'Jak zdążyć ze wszystkim i mieć jeszcze więcej wolnego czasu. Szkolenie z grą.',
    'link'=> 'https://www.high5.com.pl/szkolenia/dla-pracownikow-4/zarzadzanie-czasem-time-management-premium-117.htm',
    'position'=> 3,
    'created_at' => date('Y-m-d'),
    'updated_at' => date('Y-m-d')
], 
[
    'picture' => 'big_picture02.jpg',
    'title' => 'Prezentacje i wystąpienia publiczne',
    'content'=>  'Wystąpienie publiczne i prezentacje jako narzędzie świadomego budowania własnej marki',
    'link'=> 'https://www.high5.com.pl/szkolenia/dla-pracownikow-4/akademia-prezentacji:-profesjonalne-prezentacje-i-wystapienia-publiczne-30.htm',
    'position'=> 4,
    'created_at' => date('Y-m-d'),
    'updated_at' => date('Y-m-d')
], 
[
    'picture' => 'big_picture06.jpg',
    'title' => 'Programy rozwojowe dla HR',
    'content'=> 'Szkoła Rekrutera, Szkoła HRBP, Szkoła Analityka HR, Akademia Employer Brandingu',
    'link'=> 'https://www.high5.com.pl/programy-rozwojowe/',
    'position'=> 5,
    'created_at' => date('Y-m-d'),
    'updated_at' => date('Y-m-d')
], 

        ]);
 
  
        DB::table('users')->insert([
'name' => 'admin',
'email' => 'michal.jendraszczyk@gmail.com',
'password' => bcrypt('123')
        ]);

        


        DB::table('references_list')->insert([
[
'klient'=>'Arcelor Mittal',
'zawiera_pdf'=>'0',
'plik_pdf'=>'',
'obrazek'=>'Arcelor.gif',
'szczegoly'=>'Do realizacji projektu wybraliśmy firmę HIGH5 Training Group ze względu na duże doświadczenie w prowadzeniu podobnych projektów. Z perspektywy czasu możemy ocenić, że <span class="listy_wyr">był to dobry wybór</span>.',
'rok'=>'2014'
],
[
    'klient'=>'Ministerstwo Pracy i Polityki Społecznej',
    'zawiera_pdf'=>'0',
    'plik_pdf'=>'',
    'obrazek'=>'MPiPS.gif',
    'szczegoly'=>'[...] Organizator szkoleń w pełni wywiązał się z przyjętych zobowiązań, prezentując przy tym aktywność i elastyczność w sprawach organizacyjno - technicznych. Stwierdzamy, że jesteśmy zadowoleni z usług świadczonych przez firmę High5 Training Group. W naszej ocenie jest ona <span class="listy_wyr">solidnym i godnym polecenia partnerem.</span>[...]',
    'rok'=>'2014'
    ],
    [
        'klient'=>'Telewizja Polska',
        'zawiera_pdf'=>'0',
        'plik_pdf'=>'',
        'obrazek'=>'TVP.gif',
        'szczegoly'=>'[...]Mozliwość aktywnego udziału w zajęciach, wspólna praca w grupie, <span class="listy_wyr">omawianie zagadnień na podstawie konkretnych przykładów</span> spotkało się z uznaniem uczestników szkolenia[...]',
        'rok'=>'2014'
        ],
        [
            'klient'=>'London Cambridge Properties',
            'zawiera_pdf'=>'0',
            'plik_pdf'=>'',
            'obrazek'=>'LCP.gif',
            'szczegoly'=>'Wszystkie działania związane z realizacją całego projektu zostały wykonane na bardzo
            wysokim poziomie i w sposób profesjonalny.[...]Na podkreślenie zasługuje również rzetelna analiza potrzeb i <span class="listy_wyr">dostosowanie programu do naszych wymagań</span>',
            'rok'=>'2014'
            ],
            [
                'klient'=>'Polska Spółka Gazownictwa',
                'zawiera_pdf'=>'0',
                'plik_pdf'=>'',
                'obrazek'=>'PSG.gif',
                'szczegoly'=>'[...]Uczestnicy szkoleń prowadzonych przez firmę High5 za każdym razem doceniali jej profesjonalizm. Z zajęć wynieśli <span class="listy_wyr">wiele praktycznych wskazówek, które są im przydatne w codziennej pracy</span>, a szczególnie podkreślali wysoki poziom przygotowania i dużą wiedzę merytoryczną trenerów.',
                'rok'=>'2014'
                ],
                [
                    'klient'=>'BGZ',
                    'zawiera_pdf'=>'0',
                    'plik_pdf'=>'',
                    'obrazek'=>'BGZ.gif',
                    'szczegoly'=>'Firma HIGHS zapewniła do realizacji projektu doświadczonych trenerów, którzy wykazali się bardzo dobrym przygotowaniem merytorycznym i zaangażowaniem. Na podkreślenie zasługuje <span class="listy_wyr">rzetelna analiza potrzeb szkoleniowych</span>, wysokie kwalifikacje trenerów oraz kreatywne podejście do realizacji tematów.[...]',
                    'rok'=>'2014'
                    ],
                    [
                        'klient'=>'Inergy',
                        'zawiera_pdf'=>'0',
                        'plik_pdf'=>'',
                        'obrazek'=>'Inergy.gif',
                        'szczegoly'=>'Efekty szkoleń, ich praktyczność, kreatywność i dostosowanie do naszych potrzeb, zadowolenie Uczestników oraz ich przełożonych były dla nas <span class="listy_wyr">dowodem na najwyższy profesjonalizm</span> HIGH5 Training Group',
                        'rok'=>'2014'
                        ],
                        [
                            'klient'=>'Ego',
                            'zawiera_pdf'=>'0',
                            'plik_pdf'=>'',
                            'obrazek'=>'Ego.gif',
                            'szczegoly'=>'[...] Przyjazna atmosfera, <span class="listy_wyr">praktyczne przykłady i aktywizujące zadania</span> pomogły łatwiej przyswoić wiedzę i zrozumieć techniki docierania do odbiorcy.[...]',
                            'rok'=>'2013'
                            ], [
                                'klient'=>'Komisja Europejska',
                                'zawiera_pdf'=>'0',
                                'plik_pdf'=>'',
                                'obrazek'=>'KE.gif',
                                'szczegoly'=>'[...]Uczestnicy bardzo wysoko ocenili poziom szkolenia oraz - co najważniejsze - jego  <span class="listy_wyr">użyteczność w codziennej pracy.</span>',
                                'rok'=>'2013'
                                ], 
                                [
                                    'klient'=>'Esri Polska',
                                    'zawiera_pdf'=>'0',
                                    'plik_pdf'=>'',
                                    'obrazek'=>'Esri.gif',
                                    'szczegoly'=>'[...] swoim profesjonalizmem, dostosowaniem się do naszych oczekiwań, elastycznością w pracy [...] potwierdzili, że <span class="listy_wyr">sprawdzony partner w biznesie jest współtwórcą sukcesu firmy.</span>',
                                    'rok'=>'2013'
                                    ], [
                                        'klient'=>'Kapsch',
                                        'zawiera_pdf'=>'0',
                                        'plik_pdf'=>'',
                                        'obrazek'=>'Kapsch.gif',
                                        'szczegoly'=>'[...] Rekomendacja szkoleń [...]  High5 jest dla nas <span class="listy_wyr">naturalnym zwieńczeniem procesu rozwoju</span> pracowników',
                                        'rok'=>'2013'
                                        ], [
                                            'klient'=>'Aegon',
                                            'zawiera_pdf'=>'0',
                                            'plik_pdf'=>'',
                                            'obrazek'=>'Aegon3.gif',
                                            'szczegoly'=>'[...]Cenimy i kontynuujemy współpracę z High5 bo <span class="listy_wyr">skupiają się na realizacji celów biznesowych</span> naszej firmy i dopasowują narzędzia rozwojowe do naszych potrzeb[...]',
                                            'rok'=>'2013'
                                            ], [
                                                'klient'=>'Autoliv',
                                                'zawiera_pdf'=>'0',
                                                'plik_pdf'=>'',
                                                'obrazek'=>'Autoliv.gif',
                                                'szczegoly'=>'[...] Prowadzący zbudował atmosferę otwartości i zaufania [...] i co bardzo ważne: <span class="listy_wyr">wspierał uczestników po zakończeniu sesji</span>[...]',
                                                'rok'=>'2012'
                                                ], [
                                                    'klient'=>'Izba Celna',
                                                    'zawiera_pdf'=>'0',
                                                    'plik_pdf'=>'',
                                                    'obrazek'=>'Izba-Celna2.gif',
                                                    'szczegoly'=>'[...] Szkolenia organizowane przez High5 to <span class="listy_wyr">gwarancja kompetentnej kadry</span> o wszechstronnej wiedzy i szerokim wachlarzu umiejętności warsztatowych[...]',
                                                    'rok'=>'2012'
                                                    ], [
                                                        'klient'=>'Aegon',
                                                        'zawiera_pdf'=>'0',
                                                        'plik_pdf'=>'',
                                                        'obrazek'=>'Aegon.gif',
                                                        'szczegoly'=>'[...]elastyczne dopasowanie know-how do naszych warunków i wymagań spowodowały, że cały <span class="listy_wyr">projekt przebiegał sprawnie, kreatywnie i zakończył się sukcesem</span>[...]',
                                                        'rok'=>'2012'
                                                        ], [
                                                            'klient'=>'Kraft Foods',
                                                            'zawiera_pdf'=>'0',
                                                            'plik_pdf'=>'',
                                                            'obrazek'=>'Kraft.gif',
                                                            'szczegoly'=>'[...] Założone cele szkoleniowe we wszystkich projektach zostaty osiągnięte, a <span class="listy_wyr">jakość obsługi i uzyskane efekty</span> pozwalają nam stwierdzić, że jest to firma profesjonalna i zdecydowanie godna polecenia [...]',
                                                            'rok'=>'2012'
                                                            ], [
                                                                'klient'=>'Poczta Polska',
                                                                'zawiera_pdf'=>'0',
                                                                'plik_pdf'=>'',
                                                                'obrazek'=>'PocztaPolska.gif',
                                                                'szczegoly'=>'[...] Założone zostały <span class="listy_wyr">wysokie standardy jakościowe i konkretne wymagania</span> dotyczące programu spotkania i praktycznych kompetencji, jakie uczestnicy powinni doskonalić w ramach warsztatów [...]',
                                                                'rok'=>'2012'
                                                                ], [
                                                                    'klient'=>'Ministerstwo Rozwoju Regionalnego',
                                                                    'zawiera_pdf'=>'0',
                                                                    'plik_pdf'=>'',
                                                                    'obrazek'=>'MRR2.gif',
                                                                    'szczegoly'=>'[...]Pozwoliły <span class="listy_wyr">usystematyzować i pogłębić dotychczasową wiedzę</span>. Trenerzy dzielili się bogatymi doświadczeniami i angażowali uczestników w wypracowywanie optymalnych rozwiązań.[...]',
                                                                    'rok'=>'2011'
                                                                    ],

                                                                    [
                                                                        'klient'=>'Centrum Unijnych Projektów Transportowych',
                                                                        'zawiera_pdf'=>'0',
                                                                        'plik_pdf'=>'',
                                                                        'obrazek'=>'CUPT.gif',
                                                                        'szczegoly'=>'[...]Dotychczasowa współpraca pozwala nam bardzo wysoko ocenić jakość świadczonych usług i <span class="listy_wyr">zachęca do tworzenia wspólnych, nowych projektów szkoleniowych </span> [...]',
                                                                        'rok'=>'2010'
                                                                        ],

                                                                        [
                                                                            'klient'=>'Volkswagen Bank',
                                                                            'zawiera_pdf'=>'0',
                                                                            'plik_pdf'=>'',
                                                                            'obrazek'=>'Volkswagen-Bank.gif',
                                                                            'szczegoly'=>'[...] Z pełną odpowiedzialnością polecamy firmę High5 jako <span class="listy_wyr">kompetentnego partnera</span> do współpracy [...]',
                                                                            'rok'=>'2009'
                                                                            ],

                                                                            [
                                                                                'klient'=>'Ministerstwo Infrastruktury',
                                                                                'zawiera_pdf'=>'0',
                                                                                'plik_pdf'=>'',
                                                                                'obrazek'=>'Ministerstwo-Infrastruktury.gif',
                                                                                'szczegoly'=>'[...] Potrafią stworzyć <span class="listy_wyr">przyjazną atmosferę</span> i szybko nawiązać kontakt z uczestnikami kursu [...]',
                                                                                'rok'=>'2008'
                                                                                ],

                                                                                [
                                                                                    'klient'=>'Total Polska Sp. z o.o.',
                                                                                    'zawiera_pdf'=>'0',
                                                                                    'plik_pdf'=>'',
                                                                                    'obrazek'=>'Total.gif',
                                                                                    'szczegoly'=>'[...] Sposób przekazania wiedzy pozwolił nie tylko na szybkie przyswojenie problemu, ale także <span class="listy_wyr">zmotywował</span> do wykorzystania zdobytych informacji na stanowisku pracy[...]',
                                                                                    'rok'=>'2008'
                                                                                    ],

                                                                                    [
                                                                                        'klient'=>'Media Promotion',
                                                                                        'zawiera_pdf'=>'1',
                                                                                        'plik_pdf'=>'Mediapromotion.pdf',
                                                                                        'obrazek'=>'Mediapromotion.gif',
                                                                                        'szczegoly'=>'',
                                                                                        'rok'=>'2012'
                                                                                        ],

                                                                                        [
                                                                                            'klient'=>'Aegon',
                                                                                            'zawiera_pdf'=>'1',
                                                                                            'plik_pdf'=>'Aegon.pdf',
                                                                                            'obrazek'=>'Aegon.gif',
                                                                                            'szczegoly'=>'',
                                                                                            'rok'=>'2012'
                                                                                            ],

                                                                                            [
                                                                                                'klient'=>'Ministerstwo Sprawiedliwości',
                                                                                                'zawiera_pdf'=>'1',
                                                                                                'plik_pdf'=>'MinisterstwoSprawiedliwosci.pdf',
                                                                                                'obrazek'=>'MinisterstwoSprawiedliwosci.gif',
                                                                                                'szczegoly'=>'',
                                                                                                'rok'=>'2012'
                                                                                                ],

                                                                                                [
                                                                                                    'klient'=>'ZPE',
                                                                                                    'zawiera_pdf'=>'1',
                                                                                                    'plik_pdf'=>'ZPE.pdf',
                                                                                                    'obrazek'=>'ZPE.gif',
                                                                                                    'szczegoly'=>'',
                                                                                                    'rok'=>'2012'
                                                                                                    ],

                                                                                                    [
                                                                                                        'klient'=>'PFRON',
                                                                                                        'zawiera_pdf'=>'1',
                                                                                                        'plik_pdf'=>'pfron.pdf',
                                                                                                        'obrazek'=>'pfron.gif',
                                                                                                        'szczegoly'=>'',
                                                                                                        'rok'=>'2011'
                                                                                                        ],

                                                                                                        [
                                                                                                            'klient'=>'Ministerstwo Sprawiedliwości',
                                                                                                            'zawiera_pdf'=>'1',
                                                                                                            'plik_pdf'=>'MinisterstwoSprawiedliwosci.pdf',
                                                                                                            'obrazek'=>'MinisterstwoSprawiedliwosci.gif',
                                                                                                            'szczegoly'=>'',
                                                                                                            'rok'=>'2012'
                                                                                                            ],

                                                                                                            [
                                                                                                                'klient'=>'Ministerstwo Sprawiedliwości',
                                                                                                                'zawiera_pdf'=>'1',
                                                                                                                'plik_pdf'=>'MinisterstwoSprawiedliwosci.pdf',
                                                                                                                'obrazek'=>'MinisterstwoSprawiedliwosci.gif',
                                                                                                                'szczegoly'=>'',
                                                                                                                'rok'=>'2012'
                                                                                                                ],

                                                                                                                [
                                                                                                                    'klient'=>'PARR',
                                                                                                                    'zawiera_pdf'=>'1',
                                                                                                                    'plik_pdf'=>'PARR.pdf',
                                                                                                                    'obrazek'=>'PARR.gif',
                                                                                                                    'szczegoly'=>'',
                                                                                                                    'rok'=>'2011'
                                                                                                                    ],

                                                                                                                    [
                                                                                                                        'klient'=>'Ministerstwo Finansów',
                                                                                                                        'zawiera_pdf'=>'1',
                                                                                                                        'plik_pdf'=>'Ministerstwo Finansów.pdf',
                                                                                                                        'obrazek'=>'Ministerstwo-Finansow.gif',
                                                                                                                        'szczegoly'=>'',
                                                                                                                        'rok'=>'2010'
                                                                                                                        ],

                                                                                                                        [
                                                                                                                            'klient'=>'AthenaSoft',
                                                                                                                            'zawiera_pdf'=>'1',
                                                                                                                            'plik_pdf'=>'AthenaSoft.pdf',
                                                                                                                            'obrazek'=>'AthenaSoft.gif',
                                                                                                                            'szczegoly'=>'',
                                                                                                                            'rok'=>'2010'
                                                                                                                            ],
                                                                                                                            [
                                                                                                                                'klient'=>'Urząd Marszałkowski',
                                                                                                                                'zawiera_pdf'=>'1',
                                                                                                                                'plik_pdf'=>'Urzad Marszalkowski Wojewodztwa Slaskiego.pdf',
                                                                                                                                'obrazek'=>'Ministerstwo-Infrastruktury.gif',
                                                                                                                                'szczegoly'=>'',
                                                                                                                                'rok'=>'2009'
                                                                                                                                ],

                                                                                                                                [
                                                                                                                                    'klient'=>'Izba Celna',
                                                                                                                                    'zawiera_pdf'=>'1',
                                                                                                                                    'plik_pdf'=>'Izba Celna.pdf',
                                                                                                                                    'obrazek'=>'Izba-Celna.gif',
                                                                                                                                    'szczegoly'=>'',
                                                                                                                                    'rok'=>'2009'
                                                                                                                                    ],

                                                                                                                                    [
                                                                                                                                        'klient'=>'Vimed',
                                                                                                                                        'zawiera_pdf'=>'1',
                                                                                                                                        'plik_pdf'=>'Vimed.pdf',
                                                                                                                                        'obrazek'=>'Vimed.gif',
                                                                                                                                        'szczegoly'=>'',
                                                                                                                                        'rok'=>'2009'
                                                                                                                                        ],

                                                                                                                                        [
                                                                                                                                            'klient'=>'Wyższa Szkoła w Raciborzu',
                                                                                                                                            'zawiera_pdf'=>'1',
                                                                                                                                            'plik_pdf'=>'Wyzsza Szkola Raciborz.pdf',
                                                                                                                                            'obrazek'=>'Wyzsza-Szkola-Raciborz.gif',
                                                                                                                                            'szczegoly'=>'',
                                                                                                                                            'rok'=>'2008'
                                                                                                                                            ],

                                                                                                                                            [
                                                                                                                                                'klient'=>'Altkom',
                                                                                                                                                'zawiera_pdf'=>'1',
                                                                                                                                                'plik_pdf'=>'Altkom.pdf',
                                                                                                                                                'obrazek'=>'Altkom.gif',
                                                                                                                                                'szczegoly'=>'',
                                                                                                                                                'rok'=>'2008'
                                                                                                                                                ],
                                                                                                                                                [
                                                                                                                                                    'klient'=>'Centrum Klima',
                                                                                                                                                    'zawiera_pdf'=>'1',
                                                                                                                                                    'plik_pdf'=>'Centrum Klima.pdf',
                                                                                                                                                    'obrazek'=>'Centrum-Klima.gif',
                                                                                                                                                    'szczegoly'=>'',
                                                                                                                                                    'rok'=>'2008'
                                                                                                                                                    ],

                        ]);
/* Seeding h5_blog, h5_diagnostyka, h5_gry, h5_history */



$list_sqls = scandir(public_path('sql'));
//$sqls = ['h5_blog','h5_diagnostyka','h5_gry','h5_history','h5_programy','h5_programy_moduly'];

 /////// WSZYSTKO DZIALA JESLI TABLEA H5_TRAINING ma mniej rekordów !!!
 //// SPRAWDZENIE H5_TERMS

foreach($list_sqls as $sql) { 
    
    if (strpos($sql, '.sql') !== false) :
         $path = Config::get('app.url').'/sql/'.$sql;
        DB::unprepared(file_get_contents($path));
        $this->command->info($sql.' seeded!');
    endif;
}
 

 


   
//$this->command->info('APP:'.config('app.url'));
//$this->command->info('files:'.var_dump($list_sqls));

//$this->call(CrmKlienciTableSeeder::class);
}
}
