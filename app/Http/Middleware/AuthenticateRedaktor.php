<?php

namespace App\Http\Middleware;
use Closure;

use Illuminate\Support\Facades\Auth;

class AuthenticateRedaktor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
                if ((Auth::user()->id_role == '1') || (Auth::user()->id_role == '2')) {
                    return $next($request);
                } else {
                    return redirect()->route('redirect_403');
                }
        }
        else {
            return redirect()->route('redirect_403');
        }
    }
}
