<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class DiagnostykaController extends Controller
{
    //----------------------------------------------------
    // WIDOK
    // DIAGNOSTYKA
    //----------------------------------------------------

    // USŁUGI > DIAGNOSTYKA
    public function index()
    {
        $meta = new MetaController();
        $diagnostyka = DB::table('h5_diagnostyka')->OrderBy('title', 'asc')->get();

        return view('frontend.cms.uslugi.diagnostyka.index', compact('diagnostyka'))->with('meta', $meta->MetaCore(2, null));
    }

    // USŁUGI > DIAGNOSTYKA > WIDOK DIAGNOSTYKI
    public function diagnostyka($id, $title)
    {
        $meta = new MetaController();
        $diagnostyka = DB::table('h5_diagnostyka')->OrderBy('title', 'asc')->get();
        $single = DB::table('h5_diagnostyka')->where('diagID', (int) $id)->get();

        return view('frontend.cms.uslugi.diagnostyka.diagnostyka', compact('single', 'diagnostyka'))->with('meta', $meta->MetaCore(2, null));
    }
}
