<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\MetaController; 

class HoteleController extends Controller
{
    public function index($id) { 
        $hotel = DB::table('h5_hotele')->where('id_hotele',$id)->limit(1)->get();
        return view('frontend.cms.hotele.index',compact('hotel'));
    }
    public function all($trainingID) { 
        $hotel = DB::table('h5_training')->
            join('h5_terms', 'h5_terms.trainingID', 'h5_training.trainingID')->
            join('h5_hotele', 'h5_hotele.term_placeID', 'h5_terms.term_placeID')->
            where([['h5_terms.trainingID', $trainingID], ['h5_terms.term_start', '>', date('Y-m-d')]])
            ->orderBy('h5_terms.term_start', 'asc')->groupBy('h5_hotele.id_hotele')
            ->get();

        return view('frontend.cms.hotele.index',compact('hotel'));
    }
}
