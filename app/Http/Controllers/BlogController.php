<?php

namespace App\Http\Controllers;

use LithiumDev\TagCloud\TagCloud;
use App\Blog;
use Illuminate\Support\Facades\DB;
use Config;
Use Exception;
use Illuminate\Database\QueryException;
use PDOException;

class BlogController extends Controller
{
    //----------------------------------------------------
    // WIDOK
    // BLOG
    //----------------------------------------------------

    // BLOG CORE
    public function listing($categoryID, $feature)
    {
        $meta = new MetaController();

        $getBlogs = DB::table('h5_blog')->pluck('categoryID')->all();
        $categories_blog = DB::table('h5_categories')->whereIn('categoryID', $getBlogs)->select()->get();

        $klucz = DB::table('hashtag_blog')->pluck('blogID');
        $klucz->toArray();
        
        // echo $klucz;
        // try {
        $tagi = DB::table('hashtag_blog')->whereIn('hashtag_blog.blogID', $klucz)->pluck('hashtag_blog.tag', 'hashtag_blog.hastagID')->toArray();
        // } catch(PDOException $ex){ 
        //     dd($ex->getMessage()); 
        // }
       
        $cloud = new TagCloud();
        $baseUrl = Config::get('url');

        $cloud->addTags($tagi);

        $cloud->setHtmlizeTagFunction(function ($tagi, $size) use ($baseUrl) {
            $link = '<a href="'.$baseUrl.'/blog/tag/'.str_slug($tagi['tag']).'">'.$tagi['tag'].'</a>';

            return "<span class='tag size{$size}'>{$link}</span> ";
        });

        $tagCloud = $cloud->render();

        if ($feature == 'home') :

            $blogs = DB::table('h5_blog')->join(
            'h5_categories', 'h5_blog.categoryID', '=', 'h5_categories.categoryID'
            )->join(
            'h5_instructor', 'h5_blog.instructorID', '=', 'h5_instructor.instructorID'
            )->OrderBy('created', 'desc')->get();

        endif;

        if ($feature == 'category') :
            $blogs = DB::table('h5_blog')->join(
            'h5_categories', 'h5_blog.categoryID', '=', 'h5_categories.categoryID'
            )->join(
            'h5_instructor', 'h5_blog.instructorID', '=', 'h5_instructor.instructorID'
            )->where('h5_blog.categoryID', '=', $categoryID)->OrderBy('created', 'desc')->get();

        endif;

        if ($feature == 'post') :

            $blogs = DB::table('h5_blog')->join(
            'h5_categories', 'h5_blog.categoryID', '=', 'h5_categories.categoryID'
            )->join(
            'h5_instructor', 'h5_blog.instructorID', '=', 'h5_instructor.instructorID'
            )->where('h5_blog.blogID', '=', $categoryID)->OrderBy('created', 'desc')->get();

        $tags = DB::table('h5_blog')->join(
            'h5_categories', 'h5_blog.categoryID', '=', 'h5_categories.categoryID'
            )->join(
            'h5_instructor', 'h5_blog.instructorID', '=', 'h5_instructor.instructorID'
            )->join(
            'hashtag_blog', 'h5_blog.blogID', '=', 'hashtag_blog.blogID'
            )->where('h5_blog.blogID', '=', $categoryID)->OrderBy('created', 'desc')->get();

        return view('frontend.cms.blog.post', compact('blogs', 'categories_blog', 'tags', 'tagCloud'))->with('meta', $meta->MetaCore(2, null));
        endif;

        if ($feature == 'tag') :
            ///[\s-]/
            ///[\s+\?]/
        $getTag = preg_replace('/[\s-]/', ' ', $categoryID);
        $tags = DB::table('hashtag_blog')->get();
        $getHashtag = DB::table('hashtag_blog')->where('tag', 'LIKE', '%'.$getTag.'%')->pluck('blogID')->all();

        // Wybierz newsy jesli blog id jest rowny wartosci z tablicy tagow
        $posts = DB::table('h5_blog')->join(
        'h5_instructor', 'h5_blog.instructorID', '=', 'h5_instructor.instructorID'
         )->whereIn('h5_blog.blogID', $getHashtag)->select()->OrderBy('h5_blog.created', 'desc')->get();

        $getHashtagFromPost = DB::table('hashtag_blog')->where('blogID', $getHashtag)->get();
        // wyswietl widok z wylistowanymi newsami ktore polaczone sa tagiem
        return view('frontend.cms.blog.tag', compact('categories_blog', 'tags', 'posts', 'tagCloud'))->with('tag', $getTag)->with('tagi', $getHashtagFromPost)->with('meta', $meta->MetaCore(2, null));

        endif;

        if (($feature == 'home') || ($feature == 'category')) :

            $tagi_blog_tab = [];

        $blogg = DB::table('h5_blog')->join(
                'hashtag_blog', 'hashtag_blog.blogID', 'h5_blog.blogID'
                )->OrderBy('created', 'desc')->select('hashtag_blog.blogID', 'hashtag_blog.tag')->get();

        $tags = $blogg;

        return view('frontend.cms.blog.index', compact('blogs', 'categories_blog', 'tags', 'klucz', 'tagCloud'))->with('meta', $meta->MetaCore(2, null));

        endif;
    }

    // BLOG
    public function index()
    {
        return $this->listing('/', 'home');
    }

    // BLOG > POST
    public function post($id, $title)
    {
        return $this->listing($id, 'post');
    }

    // BLOG > KATEGORIA
    public function kategoria_blog($id, $title)
    {
        return $this->listing($id, 'category');
    }

    // BLOG > TAG
    public function tag($tag)
    {
        return $this->listing($tag, 'tag');
    }

    // BLOG > TAGI (DLA NEWSOW)
    public function tagi()
    {
        return 'tag';
    }
}
