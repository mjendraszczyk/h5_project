<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;


use App\Http\Controllers\MetaController; 

class SitemapController extends Controller
{
    //----------------------------------------------------
    // WIDOK
    // SITEMAP
    //----------------------------------------------------

    // SITEMAP
     public function index() { 

        $routes_array = [route('homepage'),route('szkolenia'),route('firma'),route('trenerzy'),route('galeria'),route('kontakt'),route('blog'),route('referencje_osoby'),route('referencje_listy'),route('terminy'),route('programy'),route('aktualnosci'),route('diagnostyka'),route('gry'),route('szkolenia_otwarte'),route('szkolenia_zamkniete'),route('szkolenia_indywidualne')];

        $meta = new MetaController;

        $getBlog = DB::table('h5_blog')->get();

        $getSzkolenia = DB::table('h5_training')->join('h5_cattrain','h5_cattrain.trainingID','h5_training.trainingID')
        ->join('h5_categories','h5_categories.categoryID','h5_cattrain.categoryID')
        ->get();
         //return "gdfgdf";
        return response()->view('frontend.sitemap.index',compact('getSzkolenia','getBlog','routes_array'))->header('Content-Type', 'text/xml');
     }
    
}