<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;

use Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function getOpiekunName($id_user) {
        try {
            $user = DB::table('users')->where('id', $id_user)->first();
            return $user->name;
        } catch(\Exception $e) {
            return false;
        }
    }
    public static function getRola($id_role) {
        $rola = DB::table('role')->where('id_role',$id_role)->first();
        return $rola->name;
    }
    public function upload($request, $label, $path, $method, $id){ 

        if (!empty(Request::file($label))) {
            $image = Request::file($label);

            $input[$label] = time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path($path);
            $image->move($destinationPath, $input[$label]);
            return $input[$label];
        } else {
            if($method == 'update') {
                $getHotelImage = DB::table('h5_hotele')->where('id_hotele',$id)->first();

                return $getHotelImage->$label;
            } else {
                return 0;
            }
        }
    }
}
