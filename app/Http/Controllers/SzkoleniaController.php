<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use LithiumDev\TagCloud\TagCloud;
use App\Http\Requests\CreateZapytajRequest;
use Mail;
use Config;
use File;

//use HomeController;

class SzkoleniaController extends HomeController
{
    //----------------------------------------------------
    // WIDOK
    // SZKOLENIA
    //----------------------------------------------------
    public static function qtySzkoleniaFromCat($id_category, $trainingtype) {
        //Jezeli jest online
        if($trainingtype == 2) {
            $Qty = DB::table('h5_training')->join(
            'h5_cattrain',
            'h5_cattrain.trainingID',
            '=',
            'h5_training.trainingID'
                )->join(
                    'h5_categories',
                    'h5_categories.categoryID',
                    '=',
                    'h5_cattrain.categoryID'
                )->where('h5_cattrain.categoryID', $id_category)->where('h5_training.training_type', '!=', '1')->orderBy('h5_cattrain.position', 'asc')->count();
        } 
        //Jezeli nie jest online
        else {
            $Qty = DB::table('h5_training')->join(
            'h5_cattrain',
            'h5_cattrain.trainingID',
            '=',
            'h5_training.trainingID'
                )->join(
                    'h5_categories',
                    'h5_categories.categoryID',
                    '=',
                    'h5_cattrain.categoryID'
                )->where('h5_cattrain.categoryID', $id_category)->where('h5_training.training_type', '!=', '2')->orderBy('h5_cattrain.position', 'asc')->count();
        }
                return ($Qty);
    }
    // SZKOLENIA CORE
    public function szkolenia($id, $feature, $id_kurs)
    {
        $meta = new MetaController();
        $getCattrain = DB::table('h5_cattrain')->pluck('categoryID')->all();
        $cattrain = DB::table('h5_categories')->whereIn('categoryID', $getCattrain)->select()->orderBy('cat_position', 'asc')->get();

        // SZKOLENIA > TAGI (DLA KATEGORII SZKOLEŃ ORAZ POJEDYŃCZYCH WIDOKÓW)
        if (($feature == 'category_open') || ( $feature == 'category_online') || ($feature == 'category_closed') || ($feature == 'szkolenie') || ($feature == 'szkolenie_online') || ($feature == 'szkolenie_new')) :

            $tagi = DB::table('hashtag')->where([['categoryID', $id], ['hashtag.tag', '!=', '']])->pluck('hashtag.tag', 'hashtag.hastagID')->toArray();
        $cloud = new TagCloud();
        $baseUrl = Config::get('url');

        $cloud->addTags($tagi);

        $cloud->setHtmlizeTagFunction(function ($tagi, $size) use ($baseUrl) {
            $link = '<a href="'.$baseUrl.'/tag/'.str_slug($tagi['tag']).'">'.$tagi['tag'].'</a>';

            return "<span class='tag size{$size}'>{$link}</span> ";
        });
        $tagCloud = $cloud->render();
        endif;

        if ($feature == 'home') :

            $szkolenia = File::allFiles(resource_path('views/frontend/cms/szkolenia/szkolenia_columns'));
        $pliki_szkolenia = [];

        foreach ($szkolenia as $file) :
                array_push($pliki_szkolenia, (class_basename(str_before((string) $file, '.'))));
        endforeach;

        return view('frontend.cms.szkolenia.index', compact('pliki_szkolenia', 'cattrain'))->with('meta', $meta->MetaCore(2, null));
        endif;
        if ($feature == 'otwarte') :
            return view('frontend.cms.szkolenia.szkolenia_otwarte', compact('cattrain', 'tagCloud'))->with('meta', $meta->MetaCore(2, null));
        endif;
        if ($feature == 'zamkniete') :
            return view('frontend.cms.szkolenia.szkolenia_zamkniete', compact('cattrain'))->with('meta', $meta->MetaCore(2, null));
        endif;
        if ($feature == 'indywidualne') :
            return view('frontend.cms.szkolenia.szkolenia_indywidualne', compact('cattrain'))->with('meta', $meta->MetaCore(2, null));
        endif;
		if ($feature == 'online') :
            return view('frontend.cms.szkolenia.szkolenia_online', compact('cattrain','tagCloud'))->with('meta', $meta->MetaCore(2, null));
        endif;
        if (($feature == 'category_open') || ($feature == 'category_closed') || ($feature == 'category_online')) :

            // $szkolenia = DB::table('h5_cattrain')->join(
            //     'h5_training', 'h5_cattrain.trainingID', '=', 'h5_training.trainingID'
            //     )->join(
            //     'h5_categories', 'h5_categories.categoryID', '=', 'h5_cattrain.categoryID'
            //     )->where('h5_cattrain.categoryID',$id)->orderBy('h5_training.training_position','asc')->get();

            if ($feature == 'category_online') {
                $szkolenia = DB::table('h5_training')->join(
                'h5_cattrain', 'h5_cattrain.trainingID', '=', 'h5_training.trainingID'
                )->join(
                'h5_categories', 'h5_categories.categoryID', '=', 'h5_cattrain.categoryID'
                )->where('h5_cattrain.categoryID', $id)->where('h5_training.training_type','!=','1')->orderBy('h5_cattrain.position', 'asc')->get();

                return view('frontend.cms.szkolenia.szkolenia_kategoria_online', compact('cattrain', 'szkolenia'))->with('meta', $meta->MetaCore(2, null))->with('id_category',$id)->with('tagCloud',$tagCloud);
            } 
            if ($feature == 'category_open') {
                $szkolenia = DB::table('h5_training')->join(
                'h5_cattrain', 'h5_cattrain.trainingID', '=', 'h5_training.trainingID'
                )->join(
                'h5_categories', 'h5_categories.categoryID', '=', 'h5_cattrain.categoryID'
                )->where('h5_cattrain.categoryID', $id)->orderBy('h5_cattrain.position', 'asc')->get();

                return view('frontend.cms.szkolenia.szkolenia_kategoria', compact('cattrain', 'szkolenia', 'tagCloud'))->with('meta', $meta->MetaCore(2, null))->with('id_category',$id);
            }
        if ($feature == 'category_closed') :

            $szkolenia = DB::table('h5_training')->join(
                'h5_cattrain', 'h5_cattrain.trainingID', '=', 'h5_training.trainingID'
                )->join(
                'h5_categories', 'h5_categories.categoryID', '=', 'h5_cattrain.categoryID'
                )->where('h5_cattrain.categoryID', $id)->orderBy('h5_cattrain.position', 'asc')->get();
                
                $lista = array(4 => array(
                            'Komunikacja w biznesie',
                            'Komunikacja dla zaawansowanych SMART9',
                            'Trening asertywności',
                            'Wywieranie wpływu w biznesie',
                            'Warsztaty antystresowe',
                            'Profilaktyka wypalenia zawodowego',
                            'Efektywność osobista i zarządzanie sobą w czasie',
                            'Trening nawyków',
                            'Sztuka zarządzania sobą i zwiększania swojej skuteczności',
                            'Budowanie pewności siebie',
                            'Rozwój inteligencji emocjonalnej',
                            'Profesjonalne prezentacje i wystąpienia publiczne',
                            'Prezentacje i wystąpienia publiczne - poziom zaawansowany',
                            'Twój głos. Styl mówienia jako sposób kreowania wizerunku',
                            'Kobieta lider w biznesie - warsztaty autoprezentacji',
                            'Kobieta lider w biznesie - warsztaty przywódcze',
                            'Moja marka w biznesie / Personal branding',
                            'Kreowanie wizerunku - warsztaty dla kobiet',
                            'Kreowanie wizerunku - warsztaty dla mężczyzn',
                            'Coaching zdrowia',
                            'Work life balance',
                            'Kreatywność i inwencja w biznesie',
                            'Akademia rozwoju talentów zawodowych',
                    ),
                3 => array('Zarządzanie oparte na kompetencjach',
                            'Strategiczne zarządzanie HR',
                            'Zarządzanie szkoleniami',
                            'Zarządzanie projektami HR',
                            'Mierniki i wskaźniki w pracy HR',
                            'Badanie w pracy działu HR',
                            'Restrukturyzacja zatrudnienia',
                            'Rekrutacja i selekcja pracowników',
                            'Assessment center Development Center',
                            'Systemy Ocen Pracowniczych',
                            'Opisy stanowisk pracy',
                            'Wartościowanie stanowisk pracy',
                            'Zarządzanie wiekiem',
                            'Zarządzanie talentami',
                            'Budowanie lojalności pracowników',
                            'Controling personalny',
                            'Tworzenie i wdrażanie kompetencyjnych systemów ocen',
                            'Tworzenie motywacyjnych systemów wynagrodzeń',
                            'Budowanie strategii komunikacji wewnętrznej',
                            'Trening dla trenerów wewnętrznych',
                            'Trener wewnętrzny call center',
                            'Trener wewnętrzny handlowców',
                            'Trener wewnętrzny umiejętności miękkich',
                            'Akademia Trenera Wewnętrznego',
                        ),
                8 => array('Organizacja pracy sekretariatu',
                            'Asystentka Zarządu i Rady nadzorczej',
                            'Akademia Profesjonalnej Asystentki',
                            'Finanse dla asystentek',
                            'Księgowość, kadry i prawo w sekretariacie',
                            'Tworzenie korespondencji biznesowej',
                            'Tworzenie profesjonalnych notatek i protokołów',
                            'Profesjonalny office menadżer',
                            'Profesjonalna recepcja',
                            'Warsztaty wizerunkowe dla sekretarek i asystentek',
                        ),
                6 => array('Techniki sprzedaży',
                            'Prospecting',
                            'Zarządzanie kluczowymi klientami',
                            'Efektywna wizyta handlowa',
                            'Negocjacje handlowe',
                            'Negocjacje kupieckie',
                            'Warsztaty negocjacyjne dla zaawansowanych',
                            'Zarządzanie zespołem handlowym',
                            'Prezentacja handlowa',
                            'Język perswazji i NLP w sprzedaży',
                            'Profesjonalna obsługa klientów',
                            'Obsługa trudnego klienta',
                            'Telemarketing',
                            'Budowanie strategii wzrostu efektywności sprzedaży',
                            'Zarządzanie zespołem handlowym na odległość',
                            'Optymalizacja działu sprzedaży',
                            'Coaching handlowy',
                            'Windykacja należności',
                            'Zarządzanie reklamacjami',
                            'Korespondencja handlowa',
                            'Korespondencja z klientem korporacyjnym',
                            'Merchandising',
                            ),
                2 => array('Zarządzanie zespołem',
                            'Kobieta Liderem',
                            'Trening menadżerski SMART9',
                            'Menadżer liderem',
                            'Budowanie i rozwój zespołu',
                            'Zarządzanie konfliktem',
                            'Umiejętności interpersonalne menedżera',
                            'Zarządzanie zespołem rozproszonym',
                            'Motywowanie i ocenianie – umiejętności kierownicze',
                            'Delegowanie zadań i odpowiedzialności',
                            'Zarządzanie przez cele',
                            'Zarządzanie zmianą',
                            'Coaching menadżerski',
                            'Efektywne zebrania',
                            'Budowanie autorytetu przywódcy',
                            'Akademia menadżera',
                            'Projekty Top Talents',
                            'Mediacje  dla zarządów',
                            'Conflict Management w biznesie',
                            'Pokolenie Y i Z',
                            'Finanse dla menedżerów',
                            'Budowanie kultury organizacyjnej',
                            'Akademia Menadżera', ),
                13 => array('Biuro Zarządu i dokumentacja korporacyjna w spółkach kapitałowych',
                            'Weksel w obrocie gospodarczym',
                            'Umowy w obrocie gospodarczym',
                            'Zasady powoływania, kompetencji i odpowiedzialności zarządów spółek',
                            'KSH w praktyce. Jak wspierać i zabezpieczać interesy spółki?',
                            'Fuzje, przejęcia, łączenie i podział spółek',
                            'Dokumentacja spółki z o.o. i spółki akcyjnej',
                            'Przekształcanie spółek handlowych',
                            'Obieg dokumentów w firmie',
                            'Ochrona danych osobowych i informacji niejawnych w praktyce',
                            'Odpowiedzialność cywilna i karna kadry zarządzającej i nadzorczej',
                            'Prawo autorskie',
                            'Zabezpieczanie i odzyskiwanie należności',
                            'Prawo pracy',
                            'Prawo upadłościowe i naprawcze',
                            'Prawo zamówień publicznych',
                            'Związki zawodowe w przedsiębiorstwie', ),
                12 => array('Modelowanie procesów biznesowych wg BPMN 2.0',
                            'Modelowanie BPMN 2.0 - warsztaty zaawansowane',
                            'Zarządzanie procesami biznesowymi',
                            'Definiowanie wymagań biznesowych wobec systemu informatycznego',
                            'Przygotowanie do międzynarodowego egzaminu OCEB F',
                            'Zarządzanie systemowe',
                            'Zarządzanie projektami PRINCE2, PMI, Agile, RUP, SixSigma, IPMA',
                            'Zarządzanie portfelem projektów i programami',
                            'Budowanie poparcia dla projektu',
                            'Zarządzanie Zespołem Projektowym',
                            'Komunikacja w zespole projektowym',
                            'Zarządzanie Ryzykiem w złożonych projektach',
                            'Zarządzanie projektem w Microsoft Project',
                            'Microsoft Project Server dla kierowników projektów',
                            'Microsoft Project Server dla członkow zespołów projektowych',
                            'Prowadzenie Zarządzanie Projektem według PMI-PMBOK - przygotowanie do egzaminu CAPM',
                            'Zarządzanie Projektem według PMI-PMBOK - przygotowanie do egzaminu PMP',
                            'Zarządzanie projektami w metodyce PCM',
                            'Zarządzanie projektami w metodyce IPMA',
                            'Zarządzanie projektami w metodyce Agile SCRUM',
                            ),
                9 => array('Copywriting - skuteczna sprzedaż za pomocą sł��w',
                            'Kreacja skutecznej reklamy',
                            'Jak przygotować brief kreatywny?',
                            'Wystąpienia medialne i komunikacja z mediami',
                            'Planowanie mediów w kampaniach reklamowych',
                            'Zarządzanie strategią reklamową, współpraca z agencją, przetargi',
                            'Metody pomiaru efektywności działań promocyjnych',
                            'Budowanie tożsamości marki',
                            'Kontakty z mediami-trening medialny',
                            'Redagowanie  tekstów dla mediów',
                            'Komunikacja w sytuacji kryzysowej',
                            'Budowanie strategii PR',
                            'Realizacja kampanii społecznych',
                            ),
                10 => array('Kodeks postępowania administracyjnego',
                            'Public relations w administracji publicznej',
                            'Komunikacja wewnętrzna w służbie urzędu',
                            'Profesjonalna obsługa klienta w urzędzie',
                            'Obsługa trudnego klienta',
                            'Protokół dyplomatyczny i wizerunek w administracji',
                            'Standardy zarządzania zasobami ludzkimi w służbie cywilnej',
                            'Zarządzanie zasobami ludzki w urzędzie',
                            'Rozliczanie projektów finansowanych ze środków UE',
                            'Zarządzanie projektami z wykorzystaniem metodyki PCM',
                            'Zarządzanie ryzykiem',
                            'Zamówienia publiczne',
                            'Facebook w urzędzie',
                            'Budowa wizerunku instytucji i urzędów',
                            'Warsztaty antystresowe dla urzędników',
                            'Kierowanie zespołem w administracji',
                            'Współpraca zespołu zadaniowego w Jednostkach Samorządu Terytorialnego',
                            'Zarządzanie czasem w realizacji zadań w urzędzie',
                            'Przywództwo sytuacyjne – budowanie zespołu, zarządzanie sytuacyjne z uwzględnieniem poziomów rozwoju zespołu',
                            'Budowanie strategii komunikacji i zarządzania wiedzą w zespole',
                            'Rozwiązywanie konfliktów w zespole w zarządzaniu sytuacyjnym',
                            'Efektywna współpraca w zespole realizującym zadania z zakresu promocji zatrudnienia',
                            'Perswazja i radzenie sobie z manipulacjami w urzędzie',
                            'Odpowiedzialność i etyka w urzędzie',
                            'Partnerstwo publiczno-prywatne',
                            ),
                18 => array('Akademia trenera: train the trainer-poziom I',
                            'Akademia trenera: train the trainer-poziom II (narzędzia)',
                            'Akademia trenera: train the trainer-poziom III (poziom mistrzowski)',
                            'Moderator design thinking',
                            'Design thinking',
                            'Myślenie wizualne',
                            'Train the trainer w języku angielskim',
                            'Trening dramy dla trenerów',
                            'Teatr improwizowany dla trenerów',
                            ),
                );

        return view('frontend.cms.szkolenia.szkolenia_kategoria_zamkniete', compact('cattrain', 'szkolenia', 'lista'))->with('meta', $meta->MetaCore(2, null))->with('id_category',$id);
        endif;
        endif;
        if($feature == 'szkolenie_new') :

            $szkolenia = DB::table('h5_training')->join(
            'h5_cattrain', 'h5_cattrain.trainingID', '=', 'h5_training.trainingID'
            )->join(
            'h5_categories', 'h5_categories.categoryID', '=', 'h5_cattrain.categoryID'
            )->where('h5_training.trainingID', $id_kurs)->get();

        $terminy = DB::table('h5_terms')->leftJoin(
            'h5_training', 'h5_training.trainingID', '=', 'h5_terms.trainingID'
            )->where([['h5_terms.trainingID', $id_kurs], ['h5_terms.term_start', '>', date('Y-m-d')]])->orderBy('h5_terms.term_start', 'asc')->get();

        $opinie = DB::table('h5_references_priv')->where([['active', 'y'], ['szkolenieID', $id_kurs]])->get();

        $hotels = DB::table('h5_training')->
            join('h5_terms', 'h5_terms.trainingID', 'h5_training.trainingID')->
            join('h5_places', 'h5_places.placeID', 'h5_terms.term_placeID')->
            where([['h5_terms.trainingID', $id_kurs], ['h5_terms.term_start', '>', date('Y-m-d')]])
            ->orderBy('h5_terms.term_start', 'asc')
            ->get();

        $QtyHotels = $hotels->count();


        $noclegi = DB::table('h5_training')->
        join('h5_terms', 'h5_terms.trainingID', 'h5_training.trainingID')->
        join('h5_hotele', 'h5_hotele.id_hotele', 'h5_terms.term_placeID')->
        where([['h5_terms.trainingID', $id_kurs], ['h5_terms.term_start', '>', date('Y-m-d')]])
        ->orderBy('h5_terms.term_start', 'asc')->groupBy('h5_hotele.id_hotele')
        ->get();

        $aktywnosci = scandir(Config::get('url').'img/frontend/aktywnosci');
        $aktywnosci_tab = array_slice($aktywnosci, 3);

        $getSzkolenieCite = DB::table('h5_training')->where('h5_training.trainingID', $id_kurs)->select()->get()->first();
        $cite = DB::table('h5_cite')
            ->where('h5_cite.citeID', '=', $getSzkolenieCite->training_cite)
            ->get();

        $tag = DB::table('h5_training')->join('hashtag', 'h5_training.trainingID', '=', 'hashtag.trainingID')->where('hashtag.trainingID', $id_kurs)->get();

        $array_books = scandir(Config::get('url').'img/frontend/books/');

        // get Terminy
        $getTerms = new HomeController();

        $terms = $getTerms->getTerms('2', 'Warszawa', $id, 3);

        return view('frontend.cms.szkolenia.szkolenia_strona_w', compact('terms', 'array_books', 'cattrain', 'szkolenia', 'terminy', 'tag', 'cite', 'opinie', 'aktywnosci_tab', 'tagCloud', 'hotels', 'QtyHotels','noclegi'))->with('meta', $meta->MetaCore(8, $id_kurs));
        endif;
        if (($feature == 'szkolenie') || ($feature == 'szkolenie_online')){
            $szkolenia_raw = DB::table('h5_training')->join(
                'h5_cattrain',
                'h5_cattrain.trainingID',
                '=',
                'h5_training.trainingID'
            )->join(
                'h5_categories',
                'h5_categories.categoryID',
                '=',
                'h5_cattrain.categoryID'
            )->where('h5_training.trainingID', $id_kurs);

            $szkolenia = $szkolenia_raw->get();
            $szkolenia_first = $szkolenia_raw->first();

            $terminy = DB::table('h5_terms')->leftJoin(
            'h5_training',
            'h5_training.trainingID',
            '=',
            'h5_terms.trainingID'
            )->where([['h5_terms.trainingID', $id_kurs], ['h5_terms.term_start', '>', date('Y-m-d')]])->orderBy('h5_terms.term_start', 'asc')->get();

            $opinie = DB::table('h5_references_priv')->where([['active', 'y'], ['szkolenieID', $id_kurs]])->get();

            $hotels = DB::table('h5_training')->
            join('h5_terms', 'h5_terms.trainingID', 'h5_training.trainingID')->
            join('h5_places', 'h5_places.placeID', 'h5_terms.term_placeID')->
            where([['h5_terms.trainingID', $id_kurs], ['h5_terms.term_start', '>', date('Y-m-d')]])
            ->orderBy('h5_terms.term_start', 'asc')
            ->get();


            $noclegi = DB::table('h5_training')->
        join('h5_terms', 'h5_terms.trainingID', 'h5_training.trainingID')->
        join('h5_hotele', 'h5_hotele.term_placeID', 'h5_terms.term_placeID')->
        where([['h5_terms.trainingID', $id_kurs], ['h5_terms.term_start', '>', date('Y-m-d')]])
        ->orderBy('h5_terms.term_start', 'asc')->groupBy('h5_hotele.id_hotele')
        ->get();

            $QtyHotels = $hotels->count();

            $aktywnosci = scandir(Config::get('url').'img/frontend/aktywnosci');
            $aktywnosci_tab = array_slice($aktywnosci, 3);

            $getSzkolenieCite = DB::table('h5_training')->where('h5_training.trainingID', $id_kurs)->select()->get()->first();
            $cite = DB::table('h5_cite')
            ->where('h5_cite.citeID', '=', @$getSzkolenieCite->training_cite)
            ->get();

            $tag = DB::table('h5_training')->join('hashtag', 'h5_training.trainingID', '=', 'hashtag.trainingID')->where('hashtag.trainingID', $id_kurs)->get();

            $array_books = scandir(Config::get('url').'img/frontend/books/');

            // get Terminy
            $getTerms = new HomeController();

            $terms = $getTerms->getTerms('2', 'Warszawa', $id, 3);

        if ($feature == 'szkolenie_online') {
            return view('frontend.cms.szkolenia.szkolenia_strona_online', compact('terms', 'array_books', 'cattrain', 'szkolenia', 'terminy', 'tag', 'cite', 'opinie', 'aktywnosci_tab', 'tagCloud', 'hotels', 'QtyHotels', 'noclegi'))->with('meta', $meta->MetaCore(8, $id_kurs))->with('id_category', $id);
        }
        else {
              return view('frontend.cms.szkolenia.szkolenia_strona', compact('terms', 'array_books', 'cattrain', 'szkolenia', 'terminy', 'tag', 'cite', 'opinie', 'aktywnosci_tab', 'tagCloud', 'hotels', 'QtyHotels', 'noclegi'))->with('meta', $meta->MetaCore(8, $id_kurs))->with('id_category', $id);
          }
        }
        if ($feature == 'tag') {
            $tag_training = preg_replace('/[\s-]/', ' ', $id_kurs);
            $tag = DB::table('h5_training')
            ->join('hashtag', 'h5_training.trainingID', 'hashtag.trainingID')
            ->join('h5_categories', 'h5_categories.categoryID', 'hashtag.categoryID')
            ->where('hashtag.tag', '=', $tag_training)->get();

            return view('frontend.cms.szkolenia.tag', compact('tag', 'cattrain', 'tag_training'))->with('meta', $meta->MetaCore(2, null))->with('id_category', $id);
        }
    }

    // SZKOLENIA
    public function index()
    {
        return $this->szkolenia(null, 'home', null);
    }

    // SZKOLENIA > OTWARTE
    public function otwarte()
    {
        return $this->szkolenia(null, 'otwarte', null);
    }
    // SZKOLENIA > ONLINE 
    public function online() {
        return $this->szkolenia(null, 'online', null);
    }
    // SZKOLENIA > ZAMKNIETE
    public function zamkniete()
    {
        return $this->szkolenia(null, 'zamkniete', null);
    }

    // SZKOLENIA > INDYWIDUALNE
    public function indywidualne()
    {
        return $this->szkolenia(null, 'indywidualne', null);
    }
	

    // SZKOLENIA > KATEGORIA (WIDOK)

    public function kategoria_otwarte($id, $title)
    {
        return $this->szkolenia($id, 'category_open', null);
    }

    // SZKOLENIA > KATEGORIA ONLINE (WIDOK)

    public function kategoria_online($id, $title)
    {
        return $this->szkolenia($id, 'category_online', null);
    }

    // SZKOLENIA > ZAMKNIETE
    public function kategoria_zamkniete($id, $title)
    {
        return $this->szkolenia($id, 'category_closed', null);
    }

    // SZKOLENIA > SZKOLENIE (WIDOK POJEDYNCZEGO SZKOLENIA)
    public function szkolenie($id, $title, $id_kurs, $title_kurs)
    {
        return $this->szkolenia($id, 'szkolenie', $id_kurs);
    }
     // SZKOLENIA > SZKOLENIE ONLINE (WIDOK POJEDYNCZEGO SZKOLENIA ONLINE)
    public function szkolenie_online($id, $title, $id_kurs, $title_kurs)
    {
        return $this->szkolenia($id, 'szkolenie_online', $id_kurs);
    }

    // SZKOLENIA > SZKOLENIE (WIDOK POJEDYNCZEGO SZKOLENIA)
    public function szkolenie_new($id, $title, $id_kurs, $title_kurs)
    {
        return $this->szkolenia($id, 'szkolenie_new', $id_kurs);
    }
    // SZKOLENIA > API (Pobieranie Ajaxem informacji dot. szkolenia w widoku [terminy])
    public function api_szkolenie($id)
    {
        $szkolenie_info = DB::table('h5_training')
        ->join('h5_terms', 'h5_terms.trainingID', 'h5_training.trainingID')
        ->where([['h5_training.trainingID', $id], ['h5_terms.term_start', '>', date('Y-m-d')], ['h5_training.modul_programu', '!=', '1'], ['h5_terms.term_closed', '!=', 'y']])
        ->whereRaw('(h5_terms.termParentID = "0" OR h5_terms.termParentID = h5_terms.termID)')
        //->where('h5_training.training_type','!=','2')
        //->where('h5_terms.term_training_type','=','1')
        ->get();

        return $szkolenie_info->toArray();
    }

    // SZKOLENIA > API (Pobieranie Ajaxem informacji dot. szkolenia w widoku [terminy])
    public function api_szkolenie_online($id)
    {
        $szkolenie_info = DB::table('h5_training')
        ->join('h5_terms', 'h5_terms.trainingID', 'h5_training.trainingID')
        ->where([['h5_training.trainingID', $id], ['h5_terms.term_start', '>', date('Y-m-d')], ['h5_training.modul_programu', '!=', '1'], ['h5_terms.term_closed', '!=', 'y']])
        ->whereRaw('(h5_terms.termParentID = "0" OR h5_terms.termParentID = h5_terms.termID)')
        //->where('h5_training.training_type','!=','1')
        ->where('h5_terms.term_training_type','=','2')
        ->get();

        return $szkolenie_info->toArray();
    }

    //SZKOLENIA > TAG
    public function tag($tag)
    {
        return $this->szkolenia(null, 'tag', $tag);
    }

    //SZKOLENIA > ZAPYTANIE
    public function zapytanie(CreateZapytajRequest $request)
    {
        $contactName = $request->input('name');
        $contactEmail = $request->input('email');
        $contactMessage = $request->input('tresc');
        $contactUrl = $request->input('contactUrl');

        $data = array('contactName' => $contactName, 'contactEmail' => $contactEmail, 'contactMessage' => $contactMessage, 'contactUrl' => $contactUrl);
        Mail::send('frontend.emails.kontakt', $data, function ($message) use ($contactEmail, $contactName) {
            $message->from('biuro@high5.pl', 'Wiadomosc High5.com.pl | zapytanie od '.$contactName);

            if (!empty(env('EMAILS_ARRAY'))) {
                $message->to(env('MAIL_USERNAME'), 'Biuro')->subject('Wiadomosc z High5.com.pl | Zapytanie')->cc($contactEmail)->cc(explode(',', env('EMAILS_ARRAY')));
            } else {
                $message->to(env('MAIL_USERNAME'), 'Biuro')->subject('Wiadomosc z High5.com.pl | Zapytanie')->cc($contactEmail);
            }
        });

        return 'Wiadomosc została wysłana';
    }
}
