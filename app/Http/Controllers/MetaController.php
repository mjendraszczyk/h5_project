<?php

namespace App\Http\Controllers;

use Route;
use Illuminate\Support\Facades\DB;

class MetaController extends Controller
{
    public function MetaCore($limit, $id_kurs)
    {
        if ((Route::currentRouteName() == 'szkolenia_strona')) :
            $meta = DB::table('hashtag')->where([['tag', '!=', ''], ['trainingID', '=', $id_kurs]])->limit($limit)->get(); else :
                $meta = DB::table('hashtag')->where('tag', '!=', '')->inRandomOrder()->limit($limit)->get();
        endif;

        return $meta;
    }
}
