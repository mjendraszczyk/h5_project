<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSearchRequest;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    //----------------------------------------------------
    // WIDOK
    // WYSZUKIWARKI
    //----------------------------------------------------

    // SEARCH
    public function index(CreateSearchRequest $request)
    {
        $fraza = $request->input('text_search');

        if (strlen($fraza) >= 2) :

            $szukaj_szkolenia = DB::table('h5_training')
            ->join('h5_cattrain', 'h5_cattrain.trainingID', 'h5_training.trainingID')
            ->join('h5_categories', 'h5_categories.categoryID', 'h5_cattrain.categoryID')
            ->where('h5_training.training_title', 'like', '%'.$fraza.'%')->orWhere('training_lid', 'like', '%'.$fraza.'%')->orWhere('training_program', 'like', '%'.$fraza.'%')->orWhere('training_advantage', 'like', '%'.$fraza.'%')->get();
        $sprawdz_fraze = $szukaj_szkolenia->count();

        if ($sprawdz_fraze == 0) :
                $szukaj_szkolenia = '0';
        endif; else:
            $szukaj_szkolenia = '0';
        endif;
        $meta = new MetaController();

        return view('frontend.cms.wyszukiwanie.index', compact('fraza', 'szukaj_szkolenia'))->with('meta', $meta->MetaCore(4, null));
    }
}
