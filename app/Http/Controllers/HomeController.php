<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use LithiumDev\TagCloud\TagCloud;
use Config;

class HomeController extends Controller
{
    //----------------------------------------------------
    // WIDOK
    // STRONY GŁÓWNEJ
    //----------------------------------------------------
    // HOMEPAGE
    public function index()
    {
        $lastTerms = DB::table('h5_terms')
        ->join('h5_training', 'h5_terms.trainingID', 'h5_training.trainingID')
        ->join('h5_cattrain', 'h5_cattrain.trainingID', 'h5_training.trainingID')
        ->join('h5_categories', 'h5_categories.categoryID', 'h5_cattrain.categoryID')
        ->where([['h5_terms.term_start', '>', date('Y-m-d')], ['h5_terms.term_place', 'LIKE', '%Warszawa%']])->orderby('h5_terms.term_start', 'asc')->limit(10)->get();

        $last_sliders = DB::table('h5_headlines')->OrderBy('headlineID', 'desc')->get();
        $last_blog = DB::table('h5_blog')->OrderBy('blogID', 'desc')->limit('4')->get();
        $histories = DB::table('h5_history')->orderBy('news_data', 'desc')->limit(5)->get();

        $tagi = DB::table('hashtag')->inRandomOrder()->limit(30)->pluck('hashtag.tag', 'hashtag.hastagID')->toArray();
        $cloud = new TagCloud();
        $baseUrl = Config::get('url');

        $cloud->addTags($tagi);

        $cloud->setHtmlizeTagFunction(function ($tagi, $size) use ($baseUrl) {
            $link = '<a href="'.$baseUrl.'/tag/'.str_slug($tagi['tag']).'">'.$tagi['tag'].'</a>';

            return "<span class='tag size{$size}'>{$link}</span> ";
        });
        $tagCloud = $cloud->render();

        $meta = new MetaController();

        return view('frontend.homepage.index', compact('last_blog', 'last_sliders', 'histories', 'lastTerms', 'tagCloud'))->with('meta', $meta->MetaCore(2, null));
    }

    public function getTerms($place_id, $place_word, $kategoria, $limit)
    {
        // DB::table('h5_terms')
        // ->join('h5_training', 'h5_terms.trainingID', '=', 'h5_training.trainingID')
        // ->where('h5_terms.term_placeID', $placeId)
        // ->where('h5_terms.term_start', '>=', $term_start)
        // ->where('h5_terms.term_start', '<=', $term_end)
        // ->orderBy('h5_terms.term_start', 'asc')
        
        // $lastTermsGET = DB::table('h5_terms')
        //     ->leftJoin('h5_training', 'h5_terms.trainingID', 'h5_training.trainingID')
        //     ->leftJoin('h5_cattrain', 'h5_cattrain.trainingID', 'h5_training.trainingID')
        //     ->leftJoin('h5_categories', 'h5_categories.categoryID', 'h5_cattrain.categoryID') //->select('h5_training.training_title');
        //     ->select([DB::RAW('DISTINCT(h5_cattrain.trainingID)'), 'h5_training.trainingID','h5_terms.term_start','h5_terms.term_end','h5_training.training_title','h5_categories.categoryID','h5_terms.term_place','h5_training.training_price','h5_categories.category_title','h5_training.training_hours','h5_terms.term_state','h5_training.training_lid']);

        // if ($kategoria != '') {
        //     $lastTerms = $lastTermsGET->where([['h5_terms.term_start', '>', date('Y-m-d')], ['h5_terms.term_placeID', 'LIKE', '%'.$place_id.'%']])->orderby('h5_terms.term_start', 'asc')->limit($limit)->get();
        // } else {
        //     $lastTerms = $lastTermsGET->where([['h5_terms.term_start', '>', date('Y-m-d')], ['h5_terms.term_placeID', 'LIKE', '%'.$place_id.'%']])->orderby('h5_terms.term_start', 'asc')->limit($limit)->get(); // h5_cattrain.position
        // }
            if($kategoria != '') { 
                 $lastTerms = DB::select(
            'SELECT DISTINCT h5_terms.termID, h5_training.trainingID, h5_terms.term_start, h5_categories.category_title, h5_terms.term_state, h5_training.training_hours, h5_terms.term_end, h5_training.training_title, h5_training.training_price, h5_training.training_lid, h5_terms.term_placeID, h5_terms.term_place, h5_cattrain.trainingID, h5_cattrain.cattrainID, h5_categories.categoryID FROM h5_terms LEFT JOIN h5_training ON h5_training.trainingID = h5_terms.trainingID LEFT JOIN h5_cattrain ON h5_cattrain.trainingID = h5_training.trainingID LEFT JOIN h5_categories ON h5_cattrain.categoryID = h5_categories.categoryID WHERE h5_terms.term_start > CURRENT_DATE AND h5_cattrain.categoryID = "'.$kategoria.'" AND (h5_terms.term_placeID = "'.$place_id.'" OR h5_terms.term_place = "'.$place_word.'") GROUP BY h5_terms.termID ORDER BY `h5_terms`.`term_start` ASC LIMIT '.$limit
        );
            } else {
                $lastTerms = DB::select(
            'SELECT DISTINCT h5_terms.termID, h5_training.trainingID, h5_terms.term_start, h5_categories.category_title, h5_terms.term_state, h5_training.training_hours, h5_terms.term_end, h5_training.training_title, h5_training.training_price, h5_training.training_lid, h5_terms.term_placeID, h5_terms.term_place, h5_cattrain.trainingID, h5_cattrain.cattrainID, h5_categories.categoryID FROM h5_terms LEFT JOIN h5_training ON h5_training.trainingID = h5_terms.trainingID LEFT JOIN h5_cattrain ON h5_cattrain.trainingID = h5_training.trainingID LEFT JOIN h5_categories ON h5_cattrain.categoryID = h5_categories.categoryID WHERE h5_terms.term_start > CURRENT_DATE AND (h5_terms.term_placeID = "'.$place_id.'" OR h5_terms.term_place = "'.$place_word.'") GROUP BY h5_terms.termID ORDER BY `h5_terms`.`term_start` ASC LIMIT '.$limit
        );
            }
       // $lastTerms = DB::table('h5_terms')->select(DB::raw("SELECT * FROM h5_terms LEFT JOIN h5_training ON h5_training.trainingID = h5_terms.termID LEFT JOIN h5_cattrain ON h5_cattrain.trainingID = h5_training.trainingID LEFT JOIN h5_categories ON h5_categories.categoryID = h5_cattrain.categoryID WHERE h5_terms.term_start > CURRENT_DATE() AND h5_terms.term_placeID = '2' ORDER BY h5_terms.term_start ASC"))->get();

//return dd($lastTerms);
        return $lastTerms;

        // SELECT DISTINCT(h5_terms.termID), h5_terms.term_start, h5_terms.term_end, h5_training.training_title, h5_training.training_price, h5_training.training_lid, h5_terms.term_placeID, h5_terms.term_place, h5_cattrain.cattrainID, h5_categories.categoryID FROM h5_terms LEFT JOIN h5_training ON h5_training.trainingID = h5_terms.trainingID LEFT JOIN h5_cattrain ON h5_cattrain.trainingID = h5_training.trainingID LEFT JOIN h5_categories ON h5_cattrain.cattrainID = h5_categories.categoryID WHERE h5_terms.term_start > CURRENT_DATE ORDER BY h5_terms.term_start ASC


    }

}
