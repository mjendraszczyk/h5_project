<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Headline; 
use App\Http\Controllers\MetaController;   /// ????? wywalic 

class HeadlinesController extends Controller
{
    //----------------------------------------------------
    // WIDOK
    // HEADLINES (SLIDER)
    //----------------------------------------------------

    // HEADLINES
    public function index() {

        $headlines = DB::table('h5_headlines')->OrderBy('position','asc')->get();
        return view('backend.headlines.index',compact('headlines'));
    }
}