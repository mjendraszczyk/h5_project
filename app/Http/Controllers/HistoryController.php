<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class HistoryController extends Controller
{
    //----------------------------------------------------
    // WIDOK
    // HISTORY (NEWSY)
    //----------------------------------------------------

    // NEWSY CORE
    public function newsy($rok, $feature, $id)
    {
        $meta = new MetaController();
        $arrayLata = [];
        $lata = DB::table('h5_history')->pluck('news_data', 'newsID')->all();
        $getLata = DB::table('h5_history')->OrderBy('news_data', 'desc')->whereIn('news_data', $lata)->select()->get();

        if ($feature == 'home') :

            $history = DB::table('h5_history')->OrderBy('news_data', 'desc')->get();

        return view('frontend.cms.history.index', compact('history', 'getLata', 'arrayLata'))->with('meta', $meta->MetaCore(2, null));

        endif;

        if ($feature == 'category') :

            $history = DB::table('h5_history')->where([['news_data', 'LIKE', $rok.'%']])->OrderBy('news_data', 'desc')->get();

        return view('frontend.cms.history.index', compact('history', 'getLata', 'arrayLata'))->with('meta', $meta->MetaCore(2, null));

        endif;

        if ($feature == 'news') :

            $news = DB::table('h5_history')->where('newsID', $id)->get();

        $blog = DB::table('h5_blog')
            ->join('h5_instructor', 'h5_instructor.instructorID', 'h5_blog.instructorID')
            ->inRandomOrder()->limit(2)->get();

        $tagi_blog_tab = [];

        $blogg = DB::table('h5_blog')->join(
                'hashtag_blog', 'hashtag_blog.blogID', 'h5_blog.blogID'
                )->OrderBy('created', 'desc')->select('hashtag_blog.blogID', 'hashtag_blog.tag')->get();

        $tags = $blogg;

        // blogs rand
        return view('frontend.cms.history.news', compact('news', 'getLata', 'arrayLata', 'blog', 'tags'))->with('meta', $meta->MetaCore(2, null));

        endif;
    }

    // HISTORY
    public function index()
    {
        return $this->newsy(null, 'home', null);
    }

    // HISTORY > KATEGORIA HISTORY (WG LAT)
    public function newsy_kategoria($rok)
    {
        return $this->newsy($rok, 'category', null);
    }

    // HISTORY > KATEGORIA HISTORY (WG LAT) > WIDOK NEWS
    public function news($rok, $id)
    {
        return $this->newsy($rok, 'news', $id);
    }
}
