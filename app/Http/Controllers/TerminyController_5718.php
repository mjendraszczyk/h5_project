<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateTerminyRequest;
use Illuminate\Support\Facades\DB;
use Session;
use Mail;
use Route;
use Storage;

class TerminyController extends Controller
{
    //----------------------------------------------------
    // WIDOK
    // TERMINY
    //----------------------------------------------------
    // TERMINY ZAPIS CORE
    public function Terminy($request, $feature, $id)
    {
        $sort = '';

        $meta = new MetaController();
        $status = 0;

        $lastCategory = '0';
        $lastTraining = '0';
        $podsumowanie = '0';

        if ($feature != 'rezerwacja') :
            $getCattrain = DB::table('h5_cattrain')->pluck('categoryID')->all();
        $cattrain = DB::table('h5_categories')->whereIn('categoryID', $getCattrain)->select()->orderBy('cat_position', 'asc')->get();

        endif;

        if ($feature == 'ZapiszTermin') :

        $pNip = $request->input('nip');

        // Sprawdzanie NIPu
        if (!empty($pNip)) :
                $weights = array(6, 5, 7, 2, 3, 4, 5, 6, 7);
        (int) $nip = preg_replace('/[\s-]/', '', $pNip);

        if (strlen($nip) == 10 && is_numeric($nip)) :
                    $sum = 0;
        for ($i = 0; $i < 9; ++$i) {
            $sum += $nip[$i] * $weights[$i];
        }
        $checkNip = ($sum % 11) == $nip[9];

        // NIP poprawny - Zapisywanie terminu
        if ($checkNip != 0) :

                        //Szkolenie
                        $terminSzkolenie = $request->input('szkolenie');
        $terminTermin = $request->input('termin');
        $terminCena = $request->input('cena');
        $terminUwagi = $request->input('uwagi');

        //Osoba zglaszajaca
        $terminOzImie = $request->input('name');
        $terminOzEmail = $request->input('email');
        $terminOzPhone = $request->input('phone');

        //Uczestnik
        $terminUczImie = $request->input('username');
        $terminUczEmail = $request->input('user_mail');
        $terminUczPhone = $request->input('user_phone');

        //Firma
        $terminFirmaNip = $request->input('nip');
        $terminFirmaNazwa = $request->input('company');
        $terminFirmaUlica = $request->input('street');
        $terminFirmaKod = $request->input('zip');
        $terminFirmaMiasto = $request->input('city');

        //Checkboxy
        $terminCheckboxUczestnik = $request->input('uczestnik');
        $terminCheckboxPubliczne = $request->input('publiczne');
        $terminCheckboxAkceptacja = $request->input('akceptacja');

        $terminSesja = $this->checkTermParent($terminTermin);

        if (Route::currentRouteName() == 'zapisz_terminy') :

                            $terminSzkolenieString = DB::table('h5_training')->where('trainingID', $terminSzkolenie)->pluck('training_title', 'trainingID')->toArray();

        $terminTerminString = DB::table('h5_terms')->where('termID', $terminTermin)->pluck('term_start', 'termID')->toArray(); else :
                            $terminSzkolenieString = DB::table('h5_programy')->where('programID', $terminSzkolenie)->pluck('nazwa', 'programID')->toArray();
        $terminTerminString = null;
        endif;

        $data = array(
                            'terminSzkolenie' => $terminSzkolenieString[$terminSzkolenie],
                            'terminTermin' => $terminTerminString[$terminTermin],
                            'terminCena' => $terminCena,
                            'terminUwagi' => $terminUwagi,
                            'terminOzImie' => $terminOzImie,
                            'terminOzEmail' => $terminOzEmail,
                            'terminOzPhone' => $terminOzPhone,
                            'terminUczImie' => $terminUczImie,
                            'terminUczEmail' => $terminUczEmail,
                            'terminUczPhone' => $terminUczPhone,
                            'terminFirmaNip' => $terminFirmaNip,
                            'terminFirmaNazwa' => $terminFirmaNazwa,
                            'terminFirmaUlica' => $terminFirmaUlica,
                            'terminFirmaKod' => $terminFirmaKod,
                            'terminFirmaMiasto' => $terminFirmaMiasto,
                            //Dodaj dodatkowe terminy
                            'terminSesja' => $this->checkTermParent($terminTermin),
                        );

        // Mail dla osoby firmy

        Mail::send('frontend.emails.termin', $data, function ($message) use ($terminOzEmail, $terminOzImie) {
            $message->from(env('MAIL_USERNAME'), 'Wiadomosc High5.com.pl | Rezerwacja przez '.$terminOzImie);
            $message->to(env('MAIL_USERNAME'), 'Biuro')->subject('Wiadomosc z High5.com.pl | Rezerwacja przez '.$terminOzImie)->cc($terminOzEmail)->cc('asystentka03@high5.pl');
        });

        //------------------------------------------------------------------
        //
        //  DODAWANIE DANYCH DO BAZY
        //
        //------------------------------------------------------------------
        //------------------------------------------------------------------
        //  CRM_KLIENCI
        //------------------------------------------------------------------
        // Sprawdzenie czy klient jest w bazie
        $checkClient = DB::table('crm_klienci')->where('nip', $nip)->count();
        if ($checkClient == 0) :
                            //---------------------------------------
                            //Dodaj klienta zamawiajacego szkolenie   (FIRMA)
                            //---------------------------------------
                            DB::table('crm_klienci')->insert([
                                'nazwa' => $terminFirmaNazwa,
                                'miasto' => $terminFirmaMiasto,
                                'kod' => $terminFirmaKod,
                                'adres' => $terminFirmaUlica,
                                'nip' => $nip,
                                'tel' => $terminOzPhone,
                                'last_minute' => '0',
                                'typ_newslettera' => '0',
                                'wazne' => '',
                            ]);

        $lastIdKlienci = DB::getPdo()->lastInsertId();

        // Klient w bazie istnieje
        else :
                            $getClient = DB::table('crm_klienci')->where('nip', $nip)->pluck('klientID', 'nip')->toArray();
        $lastIdKlienci = $getClient[$nip];
        endif;

        //------------------------------------------------------------------------
        // CRM_OSOBY
        //------------------------------------------------------------------------
        //Dodawanie uczestników
        // Domyslnie ustaw HR na 0
        $ustawHr = 0;

        //--------------------
        // Sprawdź czy os. zglaszajaca jest uczestnikiem czy nie, jesli nie to dodaj ją jako HR
        //--------------------
        ////// ------> do sprawdzenia @down

        if ($request->input('uczestnik') != 'yes') :
                            $ustawHr = 1;

        // Sprawdź czy taka osoba zgłaszająca jest juz w bazie

        $checkUserOz = DB::table('crm_osoby')
                                            ->join('crm_klienci', 'crm_osoby.klientID', 'crm_klienci.klientID')
                                            ->where([['crm_osoby.email', 'LIKE', '%'.$terminOzEmail.'%'], ['crm_klienci.nip', $nip]])->count();

        if (count($terminUczImie) > 0) :

                                $parentID = 0; else:
                                $parentID = null; // ? nie wiem czy nie dac komenta
        endif;

        if ($checkUserOz > 0) :

                            else :
                                DB::table('crm_osoby')->insert([
                                    'klientID' => $lastIdKlienci,
                                    'parentID' => $parentID, //$parentID
                                    'imie_nazwisko' => $terminOzImie,
                                    'email' => $terminOzEmail,
                                    'user_phone' => $terminOzPhone,
                                    'user_cell' => '',
                                    'newsletter' => '1',
                                    'punkty' => '0',
                                    'nowy' => '1',
                                    'hr' => $ustawHr,
                                    'ostatni_kontakt' => date('Y-m-d'),
                                    'nastepny_kontakt' => date('Y-m-d'),
                                ]);
        endif;
        endif;
        ////// ------> do sprawdzenia @up
        //Dodaje uczestników szkolenia jesli istnieja to nie dodaje

        $checkAgainUser = 0;
        for ($i = 0; $i < count($terminUczImie); ++$i) {
            //ustawia domyslne HR = 0 przed dodawanie uczestnikow
            $ustawHr = 0;
            // Sprawdzam czy istnieje uczestnik w bazie z danej firmy (ostatnio bylo bez weryfikacji nipu z samym imieniem i nazwiskiem)
            $checkUser = DB::table('crm_osoby')
                                            ->join('crm_klienci', 'crm_osoby.klientID', 'crm_klienci.klientID')
                                            ->where([['crm_osoby.imie_nazwisko', 'LIKE', '%'.$terminUczImie[$i].'%'], ['crm_klienci.nip', $nip]])->count();

            $checkUserUczestnik = DB::table('crm_osoby')->where('imie_nazwisko', 'LIKE', '%'.$terminUczImie[0].'%')->count();

            //Sprawdź kto jest zgłaszającym jesli zglaszajacy jest tez uczestnikiem pobierz jego ID jesli nie zostaw pole NULL

            if ($request->input('uczestnik') == 'yes') :

//if ($i == 0) :
                                if (count($terminUczImie) == 1) :
                                    $ustawHr = 0; else :
                                    if ($i == 0) :
                                        $ustawHr = 1;
            endif;
            endif;

            if (($i == 0) || ($checkAgainUser == 1)):

                                    $parentID = 0;
            // Sprwadz czy zglaszajacy istnieje w bazie jesli tak pobierz jego id jesli nie daj parentID = 0

            if ($checkUserUczestnik > 0) :
                                        $getParentID = DB::table('crm_osoby')->where('email', 'LIKE', '%'.$terminUczEmail[0].'%')->pluck('uczestnikID', 'email')->toArray(); else:
                                        $checkAgainUser = 1;
            endif;

            endif;

            // Dla kolejnych uczestnikow dodaj ID zglaszajacego
            if ($i > 0) :
                                    $parentID = $getParentID[$terminUczEmail[0]];
            endif; else:
                                $getParentID = DB::table('crm_osoby')->where('email', 'LIKE', '%'.$terminOzEmail.'%')->pluck('uczestnikID', 'email')->toArray();
            //$parentID = null;
            $parentID = $getParentID[$terminOzEmail];
            endif;

            // Jesli uczestnik nie jest w bazie to go dodaj, jesli jest to pobierz jego ID
            if ($checkUser == 0) :

                                if (($terminUczEmail[$i] != '') && ($request->input('dane_marketing') == '1')):
                                    $newsletter = '1'; else:
                                    $newsletter = '0';
            endif;

            DB::table('crm_osoby')->insert([
                                    'klientID' => $lastIdKlienci,
                                    'parentID' => $parentID, //$parentID
                                    'imie_nazwisko' => $terminUczImie[$i],
                                    'email' => $terminUczEmail[$i],
                                    'user_phone' => $terminUczPhone[$i],
                                    'user_cell' => '',
                                    'newsletter' => $newsletter,
                                    'punkty' => '0',
                                    'hr' => $ustawHr,
                                    'nowy' => '1',
                                    'ostatni_kontakt' => date('Y-m-d'),
                                    'nastepny_kontakt' => date('Y-m-d'),
                                    'regulamin_zgoda' => $terminCheckboxAkceptacja == 'yes' ? '1' : '0',
                                    'dane_zgoda' => $request->input('dane_przetwarzanie') == '1' ? '1' : '0',
                                    'marketing_zgoda' => $request->input('dane_marketing') == '1' ? '1' : '0',
                                ]);

            // Za kazdym razem pobieraj id dodanego uczestnika
            $lastUczestnikID = DB::getPdo()->lastInsertId(); else :
                                $getOldUczestnik = DB::table('crm_osoby')->where('imie_nazwisko', 'LIKE', '%'.$terminUczImie[$i].'%')->orderBy('uczestnikID', 'desc')->pluck('uczestnikID', 'imie_nazwisko')->toArray();
            $lastUczestnikID = $getOldUczestnik[$terminUczImie[$i]];
            endif;

            //------------------------------------------------------------------------
            // CRM_SZKOLENIA
            //------------------------------------------------------------------------
            //Dodawanie szkoleń
            //Zmiana yes/no na wartosc 1/0
            if ($request->input('publiczne') == 'yes') :
                                $publiczne = '1'; else :
                                $publiczne = '0';
            endif;

            // Sprawdź kto dodał uczestnika
            if ($parentID == 0) : //wczesniej null
                                $whoAddedUczestnik = $lastUczestnikID; else:
                                $whoAdded = DB::table('crm_osoby')->where([['uczestnikID', $lastUczestnikID], ['parentID', '0']])->pluck('uczestnikID', 'parentID')->toArray();
            $whoAddedUczestnik = $parentID;
            endif;

            // Dodaje szkolenie

            if (Route::currentRouteName() == 'zapisz_terminy') :
               // return 'doadje szkolenie szkolenieID'.$terminSzkolenie.' uczestnikID'.$lastUczestnikID.' termID '.$terminTermin.'parentID'.$whoAddedUczestnik;
            // Ta kwestia dotyczy dodawania szkolenia wzgledem pojedynczego terminu pozniej route programy rozwojowe  i dodac petle modulow jesli oczywiscie sa to programy rozwojowe
            DB::table('crm_szkolenia')->insert([
                                    'szkolenieID' => $terminSzkolenie,
                                    'uczestnikID' => $lastUczestnikID, //======> ?? id uczestnika bioracego udzial w danym szkoleniu
                                    'termID' => $terminTermin,
                                    'parentID' => $whoAddedUczestnik, //=======> parentID // id uczestnika zglaszajacego ??
                                    'stan' => '1', // rodzaj szkolenia jego status ? ??
                                    'komentarz' => $request->input('uwagi'), // jesli proforma mozna dodac informacje do pola uwagi
                                    'data_zgloszenia' => date('Y-m-d'),
                                    'publiczne' => $publiczne,
                                ]);

            //-----------------Dodaj terminy w postaci sesji
            $checkTerminySesja = DB::table('h5_terms')->where('h5_terms.termParentID', $terminTermin)->count();

            if ($checkTerminySesja > 0) {
                $getTerminySesja = DB::table('h5_terms')->where('h5_terms.termParentID', $terminTermin)->pluck('termID')->toArray();

                for ($dSesja = 1; $dSesja < count($getTerminySesja); ++$dSesja) {
                    DB::table('crm_szkolenia')->insert([
                                    'szkolenieID' => $terminSzkolenie,
                                    'uczestnikID' => $lastUczestnikID, //======> ?? id uczestnika bioracego udzial w danym szkoleniu
                                    'termID' => $getTerminySesja[$dSesja],
                                    'parentID' => $whoAddedUczestnik, //=======> parentID // id uczestnika zglaszajacego ??
                                    'stan' => '1', // rodzaj szkolenia jego status ? ??
                                    'komentarz' => $request->input('uwagi'), // jesli proforma mozna dodac informacje do pola uwagi
                                    'data_zgloszenia' => date('Y-m-d'),
                                    'publiczne' => $publiczne,
                                ]);
                }
            } else {
            }
            //--------------------
            else :
                                // Sprwadz ile jest modułów w programie rozwojowym
                                $getProgramModule = DB::table('h5_programy')
                                                ->join('h5_programy_moduly', 'h5_programy_moduly.programID', 'h5_programy.programID')
                                                ->where('h5_programy.programID', $terminSzkolenie)->count(); // np 7 modulow
            // Pobierz terminy dla modułów w programie rozwojowym
            $getTermProgramModule = DB::table('h5_programy')
                                                ->join('h5_programy_moduly', 'h5_programy_moduly.programID', 'h5_programy.programID')
                                                ->join('h5_terms', 'h5_terms.trainingID', 'h5_programy_moduly.trainingID')
                                                ->where([['h5_programy.programID', $terminSzkolenie], ['h5_terms.term_start', '>', date('Y-m-d')]])->orderBy('h5_terms.term_start', 'asc')->get();

            foreach ($getTermProgramModule as $saveProgramModule) :

                                    DB::table('crm_szkolenia')->insert([
                                        'szkolenieID' => $saveProgramModule->trainingID,
                                        'uczestnikID' => $lastUczestnikID, // ?? id uczestnika bioracego udzial w danym szkoleniu
                                        'termID' => $saveProgramModule->termID,
                                        'parentID' => $whoAddedUczestnik, //parentID // id uczestnika zglaszajacego ??
                                        'stan' => '1', // rodzaj szkolenia jego status ? ??
                                        'komentarz' => $request->input('uwagi'), // jesli proforma mozna dodac informacje do pola uwagi
                                        'data_zgloszenia' => date('Y-m-d'),
                                        'publiczne' => $publiczne,
                                    ]);

            endforeach;

            endif;
        }

        // Dodanie logu zapisu
        // #nr | data | klientID | ZglaszajacyID |UczestnikID (forach) | <przegladarka> | URL
        $log = Storage::get('h5_zapisy_log.txt');

        $log .= "//#############################################################\n";
        $log .= "Data|KlientID|ZglaszajacyMail|Przegladarka|URL|IP|UczestnikName\n";
        $log .= "//#############################################################\n";
        $log .= date('d/m/Y H:i:s').'|'.$lastIdKlienci.'|'.$terminOzEmail.'|'.$_SERVER['HTTP_USER_AGENT'].'|'.$_SERVER['REQUEST_URI'].'|'.$_SERVER['REMOTE_ADDR']."\n";
        for ($dlog = 0; $dlog < count($terminUczImie); ++$dlog) {
            $log .= ($dlog + 1).')'.$terminUczImie[$dlog];
        }
        $log .= "\n//------ \n";

        Storage::disk('local')->put('h5_zapisy_log.txt', $log);

        // Dodawanie szkolenia

        Session::flash('termin_zapis_succ', 'Dziękujemy za wysłanie zgłoszenia. Postaramy się skontaktować najszybciej jak to będzie możliwe');

        if (Route::currentRouteName() == 'zapisz_terminy') :
            $podsumowanie = '1';
        $meta = $meta->MetaCore(2, $request->input('szkolenie'));

        $terminSzkolenie = $terminSzkolenieString[$terminSzkolenie];

        $terminTermin = $terminTerminString[$terminTermin];

        return view('frontend.cms.terminy.rezerwacja.index', compact('terminSzkolenie', 'terminTermin', 'terminCena', 'terminUwagi', 'terminOzImie', 'terminOzEmail', 'terminOzPhone', 'terminUczImie', 'terminUczEmail', 'terminUczPhone', 'terminFirmaNazwa', 'terminFirmaUlica', 'terminFirmaKod', 'terminFirmaMiasto', 'terminFirmaNip', 'podsumowanie', 'terminSesja', 'meta'));
        //    return redirect()->route('rezerwacja_szkolenia', ['id' => $request->input('szkolenie')]);
        //else:
        //  return redirect()->route('rezerwacja_programy', ['id' => $request->input('szkolenie')]);
        endif;
        //return $request->all();
        // NIP niepoprawny
        else :
                        Session::flash('termin_zapis_fail', 'Podany NIP jest niepoprawny');

        return back()->withInput()->with('reqest', $request->all());
        endif; else :
                    Session::flash('termin_zapis_fail', 'Podany NIP jest niepoprawny');

        return back()->withInput()->with('request', $request->all());
        endif;

        endif;

        endif;
        if (($feature == 'home') || ($feature == 'katalog_szkolen')):

            $terminy = DB::table('h5_categories')->join(
                            'h5_cattrain', 'h5_cattrain.categoryID', '=', 'h5_categories.categoryID'
                    )->join(
                            'h5_training', 'h5_training.trainingID', '=', 'h5_cattrain.trainingID'
                    )->join(
                            'h5_terms', 'h5_terms.trainingID', '=', 'h5_training.trainingID'
                    )->where([['h5_terms.term_start', '>', date('Y-m-d')], ['h5_terms.term_type', '=', '1']])->whereRaw('(h5_terms.termParentID = "0" OR h5_terms.termParentID = h5_terms.termID)')->OrderBy('h5_cattrain.categoryID', 'asc')->OrderBy('h5_training.trainingID', 'asc')->OrderBy('h5_terms.term_start', 'asc')->get();

        if ($feature == 'katalog_szkolen') :
                //return 'gdfg';
                 else :
                return view('frontend.cms.terminy.index', compact('terminy', 'kategorie', 'szkolenia', 'cattrain', 'lastCategory', 'lastTraining'))->with('meta', $meta->MetaCore(2, null))->with('sort', 'cat');
        endif;

        endif;
        if ($feature == 'category') :

            $terminy = DB::table('h5_categories')->join(
                            'h5_cattrain', 'h5_cattrain.categoryID', '=', 'h5_categories.categoryID'
                    )->join(
                            'h5_training', 'h5_training.trainingID', '=', 'h5_cattrain.trainingID'
                    )->join(
                            'h5_terms', 'h5_terms.trainingID', '=', 'h5_training.trainingID'
                    )->where([['h5_terms.term_start', '>', date('Y-m-d')], ['h5_terms.term_type', '=', '1'], ['h5_cattrain.categoryID', $id]])->whereRaw('(h5_terms.termParentID = "0" OR h5_terms.termParentID = h5_terms.termID)')->OrderBy('h5_cattrain.categoryID', 'asc')->OrderBy('h5_training.trainingID', 'asc')->OrderBy('h5_terms.term_start', 'asc')->get();

        return view('frontend.cms.terminy.index', compact('terminy', 'kategorie', 'szkolenia', 'cattrain', 'lastCategory', 'lastTraining'))->with('meta', $meta->MetaCore(2, null))->with('sort', '');

        endif;
        if ($feature == 'chrono') :

            $terminy = DB::table('h5_categories')->join(
                            'h5_cattrain', 'h5_cattrain.categoryID', '=', 'h5_categories.categoryID'
                    )->join(
                            'h5_training', 'h5_training.trainingID', '=', 'h5_cattrain.trainingID'
                    )->join(
                            'h5_terms', 'h5_terms.trainingID', '=', 'h5_training.trainingID'
                    )->where([['h5_terms.term_start', '>', date('Y-m-d')], ['h5_terms.term_type', '=', '1']])->whereRaw('(h5_terms.termParentID = "0" OR h5_terms.termParentID = h5_terms.termID)')->OrderBy('h5_terms.term_start', 'asc')->get(); //->OrderBy('training.training_title','asc')->orderBy('cattrain.categoryID','asc')->get();

        $lastWeek = '0';

        return view('frontend.cms.terminy.index', compact('terminy', 'kategorie', 'szkolenia', 'cattrain', 'lastCategory', 'lastWeek'))->with('meta', $meta->MetaCore(2, null))->with('sort', 'chrono');
        endif;
        if ($feature == 'rezerwacja') :

        $rezerwacje = DB::table('h5_training')->join('h5_terms', 'h5_terms.trainingID', 'h5_training.trainingID')->where([['h5_terms.term_start', '>=', date('Y-m-d')], ['h5_training.training_price', '!=', '0'], ['h5_terms.trainingID', 'h5_training.trainingID']])->OrderBy('h5_training.training_title', 'asc')->get();
        $szkolenia = DB::table('h5_training')->join('h5_terms', 'h5_terms.trainingID', 'h5_training.trainingID')->where([['h5_terms.term_start', '>=', date('Y-m-d')]])->pluck('h5_training.training_title', 'h5_training.trainingID')->toArray();
        $terminy = DB::table('h5_training')->join('h5_terms', 'h5_terms.trainingID', 'h5_training.trainingID')->where([['h5_terms.trainingID', $id], ['h5_terms.term_start', '>=', date('Y-m-d')]])->whereRaw('(h5_terms.termParentID = "0" OR h5_terms.termParentID = h5_terms.termID)')->orderBy('h5_terms.term_start')->pluck('h5_terms.term_start', 'h5_terms.termID')->toArray();

        return view('frontend.cms.terminy.rezerwacja.index', compact('rezerwacje', 'szkolenia', 'id', 'terminy', 'getTerms', 'getSzkolenia', 'status', 'podsumowanie'))->with('meta', $meta->MetaCore(2, null));

        endif;
    }

    // TERMINY
    public function index()
    {
        return $this->Terminy(null, 'home', null);
    }

    // TERMINY > SORT > CHRONO > ZAKRES DNI
    public function week_range($date)
    {
        $miesiace2 = [
            '01' => 'Styczeń',
            '02' => 'Luty',
            '03' => 'Marzec',
            '04' => 'Kwiecień',
            '05' => 'Maj',
            '06' => 'Czerwiec',
            '07' => 'Lipiec',
            '08' => 'Sierpień',
            '09' => 'Wrzesień',
            '10' => 'Październik',
            '11' => 'Listopad',
            '12' => 'Grudzień',
        ];

        $ts = strtotime($date);
        $start = strtotime('monday this week', $ts);
        $end = strtotime('sunday this week-1', $ts);
        $mstart = $miesiace2[date('m', $start)];
        $mend = $miesiace2[date('m', $end)];

        if ($mstart != $mend) {
            $dateRange = array(date('d', $start).' '.$mstart, ' - '.date('d', $end).' '.$mend);

            return $dateRange;
        } else {
            $dateRange = array(date('d', $start), ' - '.date('d', $end).' '.$mend);

            return $dateRange;
        }
    }

    // TERMINY > SORTOWOANIE > CHRONO
    public function chrono()
    {
        return $this->Terminy(null, 'chrono', null);
    }

    // TERMINY > REZERWACJA
    public function rezerwacja($id)
    {
        return $this->Terminy(null, 'rezerwacja', $id);
    }

    // TERMINY > REZERWACJA > ZAPISZ
    public function zapisz(CreateTerminyRequest $request)
    {
        return $this->Terminy($request, 'ZapiszTermin', null);
    }

    // TERMINY > KATEGORIA
    public function kategoria($id)
    {
        return $this->Terminy(null, 'category', $id);
    }

    // TERMINY > SORTOWANIE (WG KATEGORII LUB CHRONOLOGICZNIE)
    public function sort($sort)
    {
        if ($sort == 'cat') :
            return $this->index(); else :
            return $this->chrono();
        endif;
    }

    //TERMINY > TERMINY API (Pobieranie terminow wg szkolenia)
    public function api_terminy($miasto)
    {
        $latestTerms = DB::table('h5_training')->
                        join('h5_terms', 'h5_training.trainingID', 'h5_terms.trainingID')
                        ->where([['h5_terms.term_start', '>', date('Y-m-d H:i:s')], ['h5_terms.term_type', '1'], ['h5_terms.term_place', 'LIKE', '%Warszawa%']])->orderBy('h5_terms.term_start', 'desc')->limit(10)->get();

        return $latestTerms->toArray();
    }

    // TERMINY > NIP API (sprawdzanie nipu w bazie)
    public function api_nip($nip)
    {
        $check_nip = DB::table('crm_klienci')->where('crm_klienci.nip', $nip)->get();

        return $check_nip->toArray();
    }

    // TERMINY > KATALOG SZKOLEŃ
    public function katalog()
    {
        return $this->Terminy(null, 'katalog_szkolen', null);
    }

    //Tworzenie XML do MS Sharepoint
    public function callendar_xml()
    {
        header('Content-Type: application/xml');

        $return = '';
        $return .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        $return .= "<urlset xmlns=\"http://www.google.com/schemas/sitemap/0.84\">\n";

        $data_start = date('Y-m-d', strtotime('last Monday'));

        $query_rsTerms = DB::table('h5_terms')->join('h5_training', 'h5_training.trainingID', '=', 'h5_terms.trainingID')->where([['h5_terms.term_start', '>=', $data_start], ['h5_terms.term_state', '=', '2']])->OrderBy('h5_terms.term_start', 'asc')->get();

        foreach ($query_rsTerms as $reTerm) :
            $return .= "<url>\n";
        $return .= '<termID>'.$reTerm->termID."</termID>\n";
        $return .= '<startdate>'.$reTerm->term_start."</startdate>\n";
        $return .= '<enddate>'.$reTerm->term_end."</enddate>\n";
        $return .= '<training>'.$reTerm->training_title."</training>\n";
        $return .= '<termID>'.$reTerm->termID."</termID>\n";

        $getLudzie = DB::table('crm_szkolenia')->where('termID', '=', $reTerm->termID)->count();

        $return .= '<peoples>'.$getLudzie."</peoples>\n";

        $return .= '<place>'.$reTerm->term_place."</place>\n";
        $return .= '<type>'.$reTerm->term_type."</type>\n";
        $return .= '<week>'.date('W', strtotime($reTerm->term_start))."</week>\n";

        $return .= "</url>\n";

        endforeach;

        $return .= "</urlset>\n";
        echo $return;
    }

    public function zgoda_dane($email)
    {
        // jezeli email istnieje w bazie to aktualizuj zgode jesli nie to przekieruj na glowna

        $checkMail = DB::table('crm_osoby')->where('email', $email)->count();

        if ($checkMail > 0) :

                DB::table('crm_osoby')->where('email', $email)->update(['marketing_zgoda' => '1']);
        DB::table('crm_osoby')->where('email', $email)->update(['newsletter' => '1']);

        echo 'Dziękujemy za wyrażenie zgody na przetwarzanie danych w celach marketingowych. Za chwile nastąpi przekierowanie...';
        sleep(3);

        return redirect()->route('homepage'); else:
            echo 'Podany email:'.$email.' nie istnieje w naszej bazie. Za chwile nastąpi przekierowanie...';
        sleep(3);

        return redirect()->route('homepage');
        endif;
    }

    //Sprawdzanie połączeń terminów
    public function checkTermParent($id)
    {
        $checkTermParent = DB::table('h5_terms')->where('termParentID', $id)->count();

        if ($checkTermParent > 1) {
            $getTermParent = DB::table('h5_terms')->where('termParentID', $id)->orderBy('term_start', 'asc')->get();
            $dataTerm = '';
            foreach ($getTermParent as $parentTerm) {
                $dataTerm .= $parentTerm->term_start.' - '.$parentTerm->term_end.', '.$parentTerm->term_place.'<br/> ';
            }

            return 'To szkolenie jest w formie sesji, odbędzie się w terminach:<br/>'.$dataTerm;
        } else {
            // return 'Brak dodatkowych terminów'.$checkTermParent;
        }
    }
}
