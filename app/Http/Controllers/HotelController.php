<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\MetaController; 

class HotelController extends Controller
{
    //
    public function index($id) { 
        $hotel = DB::table('h5_places')->where('placeID',$id)->limit(1)->get();
        return view('frontend.cms.szkolenia.hotele.index',compact('hotel'));
    }
}
