<?php

namespace App\Http\Controllers;

use App\Program;
use Illuminate\Support\Facades\DB;
use Config;

class ProgramyController extends Controller
{
    public function index()
    {
        $meta = new MetaController();
        $programy = DB::table('h5_programy')->where('aktywny', '1')->get();

        return view('frontend.cms.uslugi.programy-rozwojowe.index', compact('programy'))->with('meta', $meta->MetaCore(2, null));
    }

    public function program($id)
    {
        $meta = new MetaController();
        $certyfikaty = scandir(Config('url').'img/frontend/certyfikaty');

        $programy_all = DB::table('h5_programy')->where('aktywny', '1')->get();

        $trenerzy_array = [];

        $programy_szczegol = DB::table('h5_programy')
        ->join('h5_programy_moduly', 'h5_programy_moduly.programID', '=', 'h5_programy.programID')
        ->join('h5_training', 'h5_training.trainingID', '=', 'h5_programy_moduly.trainingID')
        ->where([['h5_programy.aktywny', '1'], ['h5_programy.programID', $id]])->OrderBy('kolejnosc', 'asc')->get();

        $moduly = DB::table('h5_programy')
        ->join('h5_programy_moduly', 'h5_programy_moduly.programID', '=', 'h5_programy.programID')
        ->join('h5_training', 'h5_training.trainingID', '=', 'h5_programy_moduly.trainingID')
        ->join('h5_terms', 'h5_terms.trainingID', '=', 'h5_training.trainingID')
        ->where([['h5_programy.aktywny', '1'], ['h5_programy.programID', $id], ['h5_terms.term_start', '>', date('Y-m-d')]])->OrderBy('term_start', 'asc')->get(); //->limit(1)

        $trenerzy = DB::table('h5_instructor')->get();

        return view('frontend.cms.uslugi.programy-rozwojowe.program', compact('programy_szczegol', 'programy_all', 'certyfikaty', 'trenerzy_array', 'trenerzy', 'moduly'))->with('meta', $meta->MetaCore(2, null));
    }

    public function modul($id, $idmodul)
    {
        $meta = new MetaController();
        $programy_all = DB::table('h5_programy')->where('aktywny', '1')->get();

        $programy_szczegol = DB::table('h5_programy')
        ->join('h5_programy_moduly', 'h5_programy_moduly.programID', '=', 'h5_programy.programID')
        ->join('h5_training', 'h5_training.trainingID', '=', 'h5_programy_moduly.trainingID')
        ->where([['h5_programy.aktywny', '1'], ['h5_programy.programID', $id]])->OrderBy('kolejnosc', 'asc')->get();

        $moduly = DB::table('h5_training')
        ->join('h5_programy_moduly', 'h5_training.trainingID', '=', 'h5_programy_moduly.trainingID')
        ->leftjoin('h5_terms', 'h5_terms.trainingID', '=', 'h5_training.trainingID') // prawdopodobnie nie ma terminu dla tych szkolen dajmy left joina
        ->where([['h5_training.trainingID', $idmodul]])->OrderBy('term_start', 'desc')->limit(1)->get();

        $aktywnosci = scandir(Config::get('url').'img/frontend/aktywnosci');
        $aktywnosci_tab = array_slice($aktywnosci, 3);

        return view('frontend.cms.uslugi.programy-rozwojowe.modul', compact('moduly', 'programy_all', 'programy_szczegol', 'aktywnosci_tab'))->with('meta', $meta->MetaCore(2, null));
    }

    public function rezerwacja($id)
    {
        $podsumowanie = '0';
        $meta = new MetaController();
        $rezerwacje = DB::table('h5_programy')->join('h5_programy_moduly', 'h5_programy.programID', 'h5_programy_moduly.programID')->where('h5_programy.programID', $id)->get();
        $szkolenia = DB::table('h5_programy')->OrderBy('h5_programy.nazwa', 'asc')->pluck('h5_programy.nazwa', 'h5_programy.programID')->toArray();
        $terminy = '';

        return view('frontend.cms.terminy.rezerwacja.index', compact('rezerwacje', 'szkolenia', 'id', 'terminy', 'podsumowanie'))->with('meta', $meta->MetaCore(2, null));
    }

    public function api_program($id)
    {
        $program_info = DB::table('h5_programy')
        //->join('h5_terms','h5_terms.trainingID','h5_training.trainingID')
        ->where([['h5_programy.programID', $id]])->get();

        return $program_info->toArray();
    }
}
