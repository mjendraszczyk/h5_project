<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\CreateInstructorRequest;
use Session;

class InstructorController extends Controller
{
    public function index()
    {
        $instructor = DB::table('h5_instructor')->get();

        return view('backend.instructor.index', compact('instructor'));
    }

    public function edit($id)
    {
        $instructor = DB::table('h5_instructor')->where('instructorID', $id)->get();

        return view('backend.instructor.edit', compact('instructor'));
    }

    public function create()
    {
        return view('backend.instructor.create');
    }

    protected $aktywna;

    public function setActive($request)
    {
        if ($request->input('instructor_accepted') == null) {
            $this->aktywna = '0';
        } else {
            $this->aktywna = '1';
        }
    }

    public function update(CreateInstructorRequest $request, $id)
    {
        $this->setActive($request);

        $instructor = DB::table('h5_instructor')->where('instructorID', $id);

        if (!empty($request->file('instructor_photo'))) {
            $image = $request->file('instructor_photo');

            $input['instructor_photo'] = str_slug($request->input('instructor_name'), '_').'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/img/frontend/trainers/big');
            $image->move($destinationPath, $input['instructor_photo']);

            $instructor->update([
            'instructor_name' => $request->input('instructor_name'),

            'instructor_lid' => $request->input('instructor_lid'),
            'instructor_desc' => $request->input('instructor_desc'),
            'instructor_phone' => $request->input('instructor_phone'),

            'instructor_mail' => $request->input('instructor_mail'),

            'instructor_refs' => $request->input('instructor_refs'),
            'instructor_scope' => $request->input('instructor_scope'),
             'instructor_accepted' => $this->aktywna,
            'instructor_photo' => $input['instructor_photo'],
        ]);
        } else {
            $instructor->update([
            'instructor_name' => $request->input('instructor_name'),

            'instructor_lid' => $request->input('instructor_lid'),
            'instructor_desc' => $request->input('instructor_desc'),
            'instructor_phone' => $request->input('instructor_phone'),

            'instructor_mail' => $request->input('instructor_mail'),

            'instructor_refs' => $request->input('instructor_refs'),
            'instructor_scope' => $request->input('instructor_scope'),
            'instructor_accepted' => $this->aktywna,
        ]);
        }

        Session::flash('panelInfo', 'Zapisano pomyslnie.');

        return redirect()->route('editInstructor', ['id' => $id]);
    }

    public function store(CreateInstructorRequest $request)
    {
        $this->setActive($request);

        if (!empty($request->file('instructor_photo'))) {
            $image = $request->file('instructor_photo');

            $input['instructor_photo'] = str_slug($request->input('instructor_name'), '_').'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/img/frontend/trainers/big');

            $image->move($destinationPath, $input['instructor_photo']);

            $photo = $input['instructor_photo'];
        } else {
            $photo = '';
        }
        $instructor = DB::table('h5_instructor')->insert([
            'instructor_name' => $request->input('instructor_name'),

            'instructor_lid' => $request->input('instructor_lid'),
            'instructor_desc' => $request->input('instructor_desc'),
            'instructor_phone' => $request->input('instructor_phone'),

            'instructor_mail' => $request->input('instructor_mail'),

            'instructor_refs' => $request->input('instructor_refs'),
            'instructor_scope' => $request->input('instructor_scope'),

             'instructor_accepted' => $this->aktywna,

            'instructor_photo' => $photo,
    ]);

        Session::flash('panelInfo', 'Dodano pomyslnie.');

        $id = DB::getPdo()->lastInsertId();

        return redirect()->route('editInstructor', ['id' => $id]);
    }

    public function destroy($id)
    {
        $deleteInstructor = DB::table('h5_instructor')->where('instructorID', $id);
        $deleteInstructor->delete();

        return redirect()->route('indexInstructor');
    }
}
