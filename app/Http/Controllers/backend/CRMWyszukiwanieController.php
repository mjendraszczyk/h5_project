<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Requests\backend\CreateCRMWyszukiwanieRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Mail;
use Response;
use Session;

//use Request;

class CRMWyszukiwanieController extends Controller
{
    public function index()
    {
        return view('backend.CRMWyszukiwanie.index')->with('wyszukiwanieWynik', [])->with('fraza', ' ')->with('listaMaili', '');
    }

    /// Wyszkiwania ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public function searchSzkolenia(CreateCRMWyszukiwanieRequest $request)
    {
        if ($request->input('ankiety') == null) {
            $ankiety = '0';
        } else {
            $ankiety = 'ankieta';
        }

        return redirect()->route('ResultSearchSzkolenia', ['result' => $request->input('trainingID'), 'ankieta' => $ankiety, 'od' => $request->input('od'), 'do' => $request->input('do')]);
    }

    public function ResultSearchSzkolenia($trainingID, $ankieta, $od, $do)
    {
        if ($ankieta != '0') {
            $sql = DB::table('crm_osoby')->leftJoin('crm_ankiety', 'crm_ankiety.uczestnikID', '=', 'crm_osoby.uczestnikID')->leftJoin('crm_klienci', 'crm_klienci.klientID', '=', 'crm_osoby.klientID')->where('crm_ankiety.szkolenia', 'LIKE', '%'.$trainingID.'%')->whereBetween('crm_ankiety.data_dodania', [$od, $do]);
            $szkolenia = $sql->get();

            $getEmaile = $sql->where('crm_osoby.email', '!=', '')->pluck('crm_osoby.email')->toArray();
        } else {
            $sql = DB::table('h5_training')->leftJoin('h5_terms', 'h5_training.trainingID', '=', 'h5_terms.trainingID')->leftJoin('crm_szkolenia', 'h5_terms.termID', '=', 'crm_szkolenia.termID')->leftJoin('crm_osoby', 'crm_osoby.uczestnikID', '=', 'crm_szkolenia.uczestnikID')->leftJoin('crm_klienci', 'crm_klienci.klientID', '=', 'crm_osoby.klientID')->where('h5_training.trainingID', $trainingID)->whereBetween('h5_terms.term_start', [$od, $do])->whereRaw('crm_szkolenia.uczestnikID=crm_osoby.uczestnikID AND crm_osoby.klientID=crm_klienci.klientID AND crm_szkolenia.termID=h5_terms.termID');
            $szkolenia = $sql->get();

            $getEmaile = $sql->where('crm_osoby.email', '!=', '')->pluck('crm_osoby.email')->toArray();
        }

        Session::put('listaMaili', implode(',', $getEmaile));

        return view('backend.CRMWyszukiwanie.index')->with('wyszukiwanieWynik', $szkolenia)->with('fraza', $trainingID)->with('listaMaili', implode(',', $getEmaile));
    }

    public function searchOsoby(Request $request)
    {
        return redirect()->route('ResultSearchOsoby', ['result' => Request::get('osoby')]);
    }

    public function ResultSearchOsoby($result)
    {
        $sql = DB::table('crm_osoby')->where('imie_nazwisko', 'LIKE', '%'.$result.'%');
        $osoba = $sql->get();
        $getEmaile = $sql->where('crm_osoby.email', '!=', '')->pluck('crm_osoby.email')->toArray();

        Session::put('listaMaili', implode(',', $getEmaile));

        return view('backend.CRMWyszukiwanie.index')->with('wyszukiwanieWynik', $osoba)->with('fraza', $result)->with('listaMaili', implode(',', $getEmaile));
    }

    public function ResultSearchZainteresowania($result)
    {
        //$sql = DB::table('crm_osoby')->leftJoin('crm_zdarzenia', 'crm_zdarzenia.uczestnikID', '=', 'crm_osoby.uczestnikID')->leftJoin('crm_klienci', 'crm_osoby.klientID', '=', 'crm_klienci.klientID')->where('crm_zdarzenia.komentarz', 'LIKE', '%'.$result.'%')->orWhere('crm_osoby.zainteresowania', 'LIKE', '%'.$result.'%')->orWhere('crm_osoby.uwagi', 'LIKE', '%'.$result.'%');
        $sql = DB::table('crm_osoby as o')->leftJoin('crm_zdarzenia', 'crm_zdarzenia.uczestnikID', '=', 'o.uczestnikID')->leftJoin('crm_klienci', 'o.klientID', '=', 'crm_klienci.klientID')->where('crm_zdarzenia.komentarz', 'LIKE', '%'.$result.'%')->orWhere('o.zainteresowania', 'LIKE', '%'.$result.'%')->orWhere('o.uwagi', 'LIKE', '%'.$result.'%');
        $zainteresowania = $sql->select('o.uczestnikID', 'o.imie_nazwisko', 'o.uwagi', 'o.zainteresowania', 'o.email', 'crm_klienci.klientID', 'crm_klienci.nazwa', 'crm_zdarzenia.komentarz')->get();

        $getEmaile = $sql->where('o.email', '!=', '')->pluck('o.email')->toArray();

        Session::put('listaMaili', implode(',', $getEmaile));

        return view('backend.CRMWyszukiwanie.index')->with('wyszukiwanieWynik', $zainteresowania)->with('fraza', $result)->with('listaMaili', implode(',', $getEmaile));
    }

    public function searchZainteresowania(CreateCRMWyszukiwanieRequest $request)
    {
        return redirect()->route('ResultSearchZainteresowania', ['zainteresowania' => $request->input('zainteresowania')]);
    }

    /// Wyszkiwania ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function autocomplete(Request $request)
    {
        $query = Request::get('term', '');

        $szkolenia = DB::table('h5_training')->where('h5_training.training_title', 'LIKE', '%'.$query.'%')->get();
        foreach ($szkolenia as $Szkolenie) {
            $results[] = ['id' => $Szkolenie->trainingID, 'value' => $Szkolenie->training_title];
        }

        return Response::json($results);
    }

    /// Wyszkiwania ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //@ tu dzialamy
    public function mailCRMWyszukiwanie()
    {
        $listaMaili = Session::get('listaMaili');

        return view('backend.CRMWyszukiwanie.modal')->with('listaMaili', $listaMaili);
    }

    /// Wyszkiwania ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public function SendmailCRMWyszukiwanie(Request $request, $typ)
    {
        $data = [
            'tytul' => Request::get('tytul'),
            'tresc' => Request::get('tresc'),
        ];

        if ($typ == 'all') {
            $emails = explode(',', preg_replace('/\s+/', '', Request::get('odbiorcy')));
        } else {
            $emails = [env('MAIL_USERNAME')];
        }

        if (!empty(env('EMAILS_ARRAY'))) {
            Mail::send('backend.emails.wyszukiwanie', $data, function ($message) use ($emails) {
                $message->from(env('MAIL_USERNAME'), 'Biuro High5');

                $message->to(env('MAIL_USERNAME', 'Biuro High5'))->bcc($emails)->subject('High5 - '.Request::get('tytul'))->cc(explode(',', env('EMAILS_ARRAY'))); //->cc('asystentka03@high5.pl');
            });
        } else {
            Mail::send('backend.emails.wyszukiwanie', $data, function ($message) use ($emails) {
                $message->from(env('MAIL_USERNAME'), 'Biuro High5');

                $message->to(env('MAIL_USERNAME', 'Biuro High5'))->bcc($emails)->subject('High5 - '.Request::get('tytul')); //->cc('asystentka03@high5.pl');
            });
        }

        //       return preg_replace('/\s+/', '', Request::get('odbiorcy'));
        return redirect()->back();
    }

    public function show()
    {
        return redirect()->back();
    }
}
