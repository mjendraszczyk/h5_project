<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\CreateCRMFirmyRequest;
use App\Http\Requests\backend\CreateCRMFirmyPhoneRequest;

// use Illuminate\Support\Facades\Request;
use Session;
use Request;
use Auth;

class CRMFirmyController extends Controller
{
    private $letters;

    private $hr;
    private $osoby;
    private $faktury;

    private $branza;
    private $opiekun;

    public function filtrCRMFirmyPhone(CreateCRMFirmyPhoneRequest $request) {

        $users = DB::table('users')->pluck('name','id');
        if(!empty($request->input('phone')) && (!empty($request->input('nazwa'))))  {
            $getFirmy = DB::table('crm_klienci')->where('tel','LIKE','%'.$request->input('phone').'%')->where('nazwa','LIKE','%'.$request->input('nazwa').'%')->get();
        } elseif(!empty($request->input('nazwa'))) {
            $getFirmy = DB::table('crm_klienci')->where('nazwa','LIKE','%'.$request->input('nazwa').'%')->get();
            } else {
        $getFirmy = DB::table('crm_klienci')->where('tel','LIKE','%'.$request->input('phone').'%')->get();
        }

        Session::put('tel', $request->input('phone'));
        Session::put('nazwa', $request->input('nazwa'));

        return view('backend.CRMFirmy.index')->with('firmy',$getFirmy)->with('letters',$this->letters())->with('users', $users);
    }
    public function przypiszOpiekuna($klientID) {
        //->where('opiekunID', null)
        DB::table('crm_klienci')->where('klientID', $klientID)->update([
            'opiekunID' => Auth::user()->id
        ]);
        return redirect()->back();
    }
    public function coreFirma()
    {
        $this->branza = ['1' => 'Przemysł/produkcja', '2' => 'Usługi', '3' => 'Administracja publiczna', '4' => 'Finanse', '5' => 'FMCG', '6' => 'Inne'];
        $getOpiekun = DB::table('users')->get(['name', 'id']);

        $this->opiekun = [];
        $this->opiekun['0'] = '--Brak--';
        foreach ($getOpiekun as $ku => $user) {
            $this->opiekun[$user->id] = $user->name;
        }
        //$this->opiekun = DB::table('users')->pluck('name', 'id')->prepend('Brak', '999')->toArray();
        //$opiekun_none = ['0' => '--Brak--'];
        //array_merge($this->opiekun, $opiekun_none);

        //array_push($this->opiekun, $opiekun_none);

        return $this->opiekun;
    }

    public static function getFirma($klientID)
    {
        $firma = DB::table('crm_klienci')->where('klientID', $klientID)->get();

        return $firma;
    }

    public function getHr($klientID)
    {
        $this->hr = DB::table('crm_osoby')->where([['klientID', $klientID], ['hr', '!=', '0']])->orderBy('imie_nazwisko', 'asc')->get();

        return $this->hr;
    }

    public function getOsoby($klientID)
    {
        $this->osoby = DB::table('crm_osoby')->where([['klientID', $klientID], ['hr', '0']])->orWhere([['klientID', $klientID], ['hr', null]])->orderBy('imie_nazwisko', 'asc')->get();

        return $this->osoby;
    }

    public function getFaktury($klientID)
    {
        $this->faktury = DB::table('crm_faktury2016')->leftJoin('crm_wysylka', 'crm_faktury2016.fakturaID', '=', 'crm_wysylka.fakturaID')->where('klientID', $klientID)->orderby('data_wystawienia', 'desc')->get();
    }

    public function letters()
    {
        $this->letters = [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'Ł', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        ];
        array_push($this->letters, '0-9');

        return $this->letters;
    }

    public static function getQtyEmployers($klientID)
    {
        $qtyEmployers = DB::table('crm_osoby')->where('crm_osoby.klientID', '=', $klientID)->count();

        return $qtyEmployers;
    }

    public function index()
    {
        $this->letters();

        if (Auth::user()->id_role == '3') {
            $firmy = DB::table('crm_klienci')
            ->where('nazwa', 'LIKE', 'A%')
            ->where('opiekunID',Auth::user()->id)
            ->orderBy('nazwa', 'asc')
            ->get();
        } else {
            $firmy = DB::table('crm_klienci')
        ->where('nazwa', 'LIKE', 'A%')
        ->orderBy('nazwa', 'asc')
        ->get();
        }

        return view('backend.CRMFirmy.index', compact('firmy'))->with('letters', $this->letters);
    }

    public function filtruj($char)
    {
        $this->letters();
        if(Auth::user()->id_role == '3') {
            if ((strlen($char) > 1) && ($char != '0-9')) {
                $firmy = DB::table('crm_klienci')->where('nazwa', 'LIKE', '%'.$char.'%')
                ->where('opiekunID',Auth::user()->id)
                ->orderBy('nazwa', 'asc')->get();
            } else {
                if ($char == '0-9') {
                    $firmy = DB::table('crm_klienci')->where('nazwa', 'REGEXP', '^['.$char.']+')
                    ->where('opiekunID',Auth::user()->id)->orderBy('nazwa', 'asc')->get();
                } else {
                    $firmy = DB::table('crm_klienci')->where('nazwa', 'LIKE', $char.'%')->where('opiekunID',Auth::user()->id)->orderBy('nazwa', 'asc')->get();
                }
            }
        }
        else {
            if ((strlen($char) > 1) && ($char != '0-9')) {
                $firmy = DB::table('crm_klienci')->where('nazwa', 'LIKE', '%'.$char.'%')->orderBy('nazwa', 'asc')->get();
            } else {
                if ($char == '0-9') {
                    $firmy = DB::table('crm_klienci')->where('nazwa', 'REGEXP', '^['.$char.']+')->orderBy('nazwa', 'asc')->get();
                } else {
                    $firmy = DB::table('crm_klienci')->where('nazwa', 'LIKE', $char.'%')->orderBy('nazwa', 'asc')->get();
                }
            }
        }

        return view('backend.CRMFirmy.index', compact('firmy'))->with('letters', $this->letters);
    }

    public function create()
    {
        $this->coreFirma();

        return view('backend.CRMFirmy.create')->with('opiekun', $this->opiekun)->with('branza', $this->branza);
    }

    public function edit($id)
    {
        $this->coreFirma();

        $getOferty = DB::table('crm_zdarzenia_nowe')->where('klientID',$id)->where('typ','7')->get();

        if (Auth::user()->id_role == '3') {
            $CRMFirmyCount = DB::table('crm_klienci')->where('crm_klienci.klientID', $id)->where('opiekunID', Auth::user()->id)->count();
            if($CRMFirmyCount == 0) {
                return "Brak dostępu";
            } else {
                $CRMFakturyController = new CRMFakturyController();
            $coreFormaWysylki = $CRMFakturyController->formaWysylki();
            $typyFaktury = $CRMFakturyController->typyFaktury();

            $CRMFirmy = DB::table('crm_klienci')->where('crm_klienci.klientID', $id)->get();

            $this->getHr($id);
            $this->getOsoby($id);

            $this->getFaktury($id);
            }
        } else {
            $CRMFakturyController = new CRMFakturyController();
            $coreFormaWysylki = $CRMFakturyController->formaWysylki();
            $typyFaktury = $CRMFakturyController->typyFaktury();

            $CRMFirmy = DB::table('crm_klienci')->where('crm_klienci.klientID', $id)->get();

            $this->getHr($id);
            $this->getOsoby($id);

            $this->getFaktury($id);
        }
        $zdarzeniaCore = new CRMZdarzeniaController();
        return view('backend.CRMFirmy.edit', compact('CRMFirmy', 'id'))->with('typyFaktury', $typyFaktury)->with('klientID', $id)->with('osoby', $this->osoby)->with('opiekun', $this->opiekun)->with('hr', $this->hr)->with('faktury', $this->faktury)->with('forma_wysylki', $coreFormaWysylki)->with('branza', $this->branza)->with('getOferty',$getOferty)->with('priorytet',$zdarzeniaCore->priorytetZdarzenia())->with('stany_zdarzenia',$zdarzeniaCore->stanyZdarzenia()); //->with('vat', $this->vat)->with('typy', $typy_fakury)->with('jm', $this->jm)->with('forma', $this->forma)->with('termin', $this->termin)->with('forma_wysylki', $this->forma_wysylki);
    }

    private $referencje;
    private $vip;
    private $last_minute;
    private $rabat;
    private $wazne;

    public function setCheckboxStatus($request)
    {
        if ($request->input('referencje') == null) {
            $this->referencje = '0';
        } else {
            $this->referencje = '1';
        }

        if ($request->input('vip') == null) {
            $this->vip = '0';
        } else {
            $this->vip = '1';
        }

        if ($request->input('last_minute') == null) {
            $this->last_minute = '0';
        } else {
            $this->last_minute = '1';
        }
        if ($request->input('rabat') == null) {
            $this->rabat = '0';
        } else {
            $this->rabat = $request->input('rabat');
        }
        if ($request->input('wazne') == null) {
            $this->wazne = '';
        } else {
            $this->wazne = $request->input('wazne');
        }
    }

    public function store(CreateCRMFirmyRequest $request)
    {
        $this->setCheckboxStatus($request);

        $firma = DB::table('crm_klienci')->insert([
             'nazwa' => $request->input('nazwa'),
            'miasto' => $request->input('miasto'),
            'kod' => $request->input('kod'),
            'adres' => $request->input('adres'),
            'nip' => $request->input('nip'),
            'tel' => $request->input('tel'),
            'branza' => $request->input('branza'),
            'referencje' => $this->referencje,
            'opiekunID' => $request->input('opiekun'),
            'vip' => $this->vip,
            'last_minute' => $this->last_minute,
            'rabat' => $this->rabat,
            'wazne' => $this->wazne,
            'ranga' => $request->input('ranga'),
            'typ_newslettera' => '0',
    ]);

        $id = DB::getPdo()->lastInsertId();

        return redirect()->route('editCRMFirmy', ['id' => $id]);
    }

    public function update(CreateCRMFirmyRequest $request, $id)
    {
        $this->setCheckboxStatus($request);

        $firma = DB::table('crm_klienci')->where('klientID', $id);

        if ($request->input('opiekun') != null) {
            $nowy = '0';
        } else {
            $nowy = '1';
        }
        $firma->update([
             'nazwa' => $request->input('nazwa'),
            'miasto' => $request->input('miasto'),
            'kod' => $request->input('kod'),
            'adres' => $request->input('adres'),
            'nip' => $request->input('nip'),
            'tel' => $request->input('tel'),
            'branza' => $request->input('branza'),
            'referencje' => $this->referencje,
            'opiekunID' => $request->input('opiekun'),
            'vip' => $this->vip,
            'last_minute' => $this->last_minute,
            'rabat' => $this->rabat,
            'wazne' => $this->wazne,
            'ranga' => $request->input('ranga'),
            'typ_newslettera' => '0',
    ]);

        $uczestnik = DB::table('crm_osoby')->where('klientID', $id);

        $uczestnik->update([
        'nowy' => $nowy,
        ]);

        Session::flash('panelInfo', 'Zapisano pomyslnie.');

        return redirect()->route('editCRMFirmy', ['id' => $id]);
    }

    public function konsolidujCRMFirmy($klientID, $method)
    {
        return view('backend.CRMFirmy.konsoliduj')->with('klientID', $klientID);
    }

    public function storekonsolidujCRMFirmy(Request $request, $klientID)
    {
        $createKlientIDArray = explode(',', Request::get('konsoliduj_klientID'));

        for ($konsolidacja = 0; $konsolidacja < count($createKlientIDArray); ++$konsolidacja) {
            $getFakturyKlientArray = DB::table('crm_faktury2016')->where('klientID', $createKlientIDArray[$konsolidacja]);

            $getFakturyKlientArray->update([
                'klientID' => $klientID,
            ]);

            $getUczestnicyKlientArray = DB::table('crm_osoby')->where('klientID', $createKlientIDArray[$konsolidacja]);

            $getUczestnicyKlientArray->update([
                'klientID' => $klientID,
            ]);
        }

        return redirect()->back();
    }

    public function destroy($id)
    {
        $deleteFirma = DB::table('crm_klienci')->where('klientID', $id);
        $deleteFirma->delete();

        return redirect()->route('indexCRMFirmy');
    }
}
