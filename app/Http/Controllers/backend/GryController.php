<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\CreateGryRequest;
use Session;

class GryController extends Controller
{
    public function index()
    {
        $gry = DB::table('h5_gry')->get();

        return view('backend.gry.index', compact('gry'));
    }

    public function edit($id)
    {
        $gry = DB::table('h5_gry')->where('graID', $id)->get();

        return view('backend.gry.edit', compact('gry'));
    }

    public function create()
    {
        return view('backend.gry.create');
    }

    //Session::flash('success', 'Zapisano pomyslnie.');

    public function update(CreateGryRequest $request, $id)
    {
        // $update_shop = Setting::findOrFail($id);

        $gry = DB::table('h5_gry')->where('graID', $id);

        if (!empty($request->file('image'))) {
            $image = $request->file('image');

            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/img/frontend/');
            $image->move($destinationPath, $input['imagename']);

            $gry->update([
            'zdjecie' => $input['imagename'],
            'nazwa' => $request->input('nazwa'),
            'podtytul' => $request->input('podtytul'),
            'opis' => $request->input('opis'),
            'dla_kogo' => $request->input('dla_kogo'),
            'organizacja' => $request->input('organizacja'),
            'korzysci' => $request->input('korzysci'),
            //'position' => $request->input('position'),
        ]);
        } else {
            $gry->update([
            'nazwa' => $request->input('nazwa'),
            'podtytul' => $request->input('podtytul'),
            'opis' => $request->input('opis'),
            'dla_kogo' => $request->input('dla_kogo'),
            'organizacja' => $request->input('organizacja'),
            'korzysci' => $request->input('korzysci'),
            //'position' => $request->input('position'),
        ]);
        }

        Session::flash('panelInfo', 'Zapisano pomyslnie.');

        // $id = DB::getPdo()->lastInsertId();

        return redirect()->route('editGry', ['id' => $id]);
    }

    public function store(CreateGryRequest $request)
    {
        $image = $request->file('image');

        if (!empty($request->file('image'))) {
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/img/frontend/');

            $image->move($destinationPath, $input['imagename']);

            $gry = DB::table('h5_gry')->insert([
    'zdjecie' => $input['imagename'],
    'nazwa' => $request->input('nazwa'),
            'podtytul' => $request->input('podtytul'),
            'opis' => $request->input('opis'),
            'dla_kogo' => $request->input('dla_kogo'),
            'organizacja' => $request->input('organizacja'),
            'korzysci' => $request->input('korzysci'),
    ]);
        } else {
            $gry = DB::table('h5_gry')->insert([
    'zdjecie' => '',
    'nazwa' => $request->input('nazwa'),
            'podtytul' => $request->input('podtytul'),
            'opis' => $request->input('opis'),
            'dla_kogo' => $request->input('dla_kogo'),
            'organizacja' => $request->input('organizacja'),
            'korzysci' => $request->input('korzysci'),
    ]);
        }

        Session::flash('panelInfo', 'Dodano pomyslnie.');

        $id = DB::getPdo()->lastInsertId();

        return redirect()->route('editGry', ['id' => $id]);
    }

    public function destroy($id)
    {
        $deleteGry = DB::table('h5_gry')->where('graID', $id);
        $deleteGry->delete();

        return redirect()->route('indexGry');
    }
}
