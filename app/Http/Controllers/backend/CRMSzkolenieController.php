<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Session;

class CRMSzkolenieController extends Controller
{
    private $trainingID;
    private $stan;

    private $parentID;

    public function getSzkolenie()
    {
        $this->trainingID = DB::table('h5_training')->orderBy('training_title', 'asc')->pluck('training_title', 'trainingID');
        $this->stan = ['1' => 'zainteresowanie', '2' => 'zgłoszenie', '3' => 'potwierdzone', '4' => 'zrealizowane', '5' => 'zafakturowane', '6' => 'anulowane'];
    }

    public static function getMailOZ($parentID, $state)
    {
        $getMail = DB::table('crm_osoby')->where('uczestnikID', $parentID)->get(['email', 'imie_nazwisko']);

        foreach ($getMail as $mail) {
            return $mail->email;
        }
    }

    public function coreSzkolenie($klientID)
    {
        $this->getSzkolenie();
        $this->parentID = DB::table('crm_osoby')->where('klientID', $klientID)->pluck('imie_nazwisko', 'uczestnikID');
    }

    public function show($termID)
    {
        $this->getSzkolenie();
        //nowa_ankieta.php
        $getSzkoleniaTermDetail = DB::table('h5_terms')->leftJoin('h5_training', 'h5_terms.trainingID', '=', 'h5_training.trainingID')->leftJoin('h5_instructor', 'h5_terms.term_instructor', '=', 'h5_instructor.instructorID')->where('h5_terms.termID', $termID)->get();

        $sqlTermUsers = DB::table('crm_osoby')->leftJoin('crm_szkolenia', 'crm_szkolenia.uczestnikID', '=', 'crm_osoby.uczestnikID')->leftJoin('crm_klienci', 'crm_osoby.klientID', '=', 'crm_klienci.klientID')->where('crm_szkolenia.termID', $termID)->orderBy('crm_klienci.nazwa', 'asc');
        $getSzkoleniaTermUsers = $sqlTermUsers->get();

        return view('backend.CRMSzkolenie.show', compact('getSzkoleniaTermDetail', 'getSzkoleniaTermUsers'))->with('termID', $termID)->with('stan', $this->stan)->with('term_state', $this->stan);
    }

    public static function checkIfHaveZaliczka($uczestnikID, $termID)
    {
        $checkIfHaveFzal = DB::table('crm_faktury2016')->where([['crm_faktury2016.termID', $termID], ['crm_faktury2016.uczestnikID', $uczestnikID], ['crm_faktury2016.typ', '3']])->count();

        return $checkIfHaveFzal;
    }

    public function modal($klientID)
    {
        $this->coreSzkolenie($klientID);
        $getFirstSzkolenieID = DB::table('h5_training')->OrderBy('training_title', 'asc')->get(['trainingID'])->first();
        $terminy = DB::table('h5_terms')->where('h5_terms.term_start', '>=', date('Y-m-d'))->where('h5_terms.trainingID', json_encode($getFirstSzkolenieID->trainingID))->pluck('h5_terms.term_start', 'h5_terms.termID');

        return view('backend.CRMSzkolenie.modal')->with('trainingID', $this->trainingID)->with('stan', $this->stan)->with('parentID', $this->parentID)->with('terminy', $terminy);
    }

    public function destroyRezerwacja($id)
    {
        $deleteSzkolenie = DB::table('crm_szkolenia')->where('crmszkolenieID', $id);
        $deleteSzkolenie->delete();

        return redirect()->back();
    }

    public function mailCRMSzkolenie($termID)
    {
        $getEmaile = DB::table('crm_szkolenia')->leftJoin('crm_osoby', 'crm_szkolenia.uczestnikID', '=', 'crm_osoby.uczestnikID')->leftJoin('crm_klienci', 'crm_osoby.klientID', '=', 'crm_klienci.klientID')->where('crm_szkolenia.termID', $termID)->where('email', '!=', '')->pluck('email')->toArray();

        //  Session::put('listaMailiSzkolenie', implode(',', $getEmaile));

        $listaMaili = implode(',', $getEmaile);

        return view('backend.CRMWyszukiwanie.modal')->with('listaMaili', $listaMaili);
    }

    public function zmienTerminModal($crmszkolenieID)
    {
        $getTermin = DB::table('crm_szkolenia')->where('crmszkolenieID', $crmszkolenieID)->get(['szkolenieID', 'termID'])->first();

        $terminy = DB::table('h5_terms')->where('h5_terms.term_start', '>=', date('Y-m-d'))->where('h5_terms.trainingID', json_encode($getTermin->szkolenieID))->orderBy('h5_terms.term_start', 'asc')->pluck('h5_terms.term_start', 'h5_terms.termID');

        return view('backend.CRMSzkolenie.termin.form')->with('crmszkolenieID', $crmszkolenieID)->with('terminy', $terminy)->with('getTermin', json_encode($getTermin->termID));
    }

    public function zmienTerminCRMSzkolenie(Request $request, $crmszkolenieID)
    {
        $szkolenie_zmienTermin = DB::table('crm_szkolenia')->where('crmszkolenieID', $crmszkolenieID);
        $szkolenie_zmienTermin->update([
            'termID' => Request::get('termID'),
        ]);

        return redirect()->back();
    }

    public function store(Request $request)
    {
        $getCurrentUser = \Auth::user()->id;

        DB::table('crm_szkolenia')->insert([
            'szkolenieID' => Request::get('trainingID'),
            'termID' => Request::get('termID'),
            'uczestnikID' => Request::get('uczestnikID'),
            'stan' => Request::get('stan'),
            'parentID' => Request::get('parentID'),
            'komentarz' => Request::get('komentarz'),
            'data_zgloszenia' => date('Y-m-d'),
            'publiczne' => '0',
            'userID' => $getCurrentUser,
        ]);

        return redirect()->back();
    }

    public function ajaxUpdateSzkolenie(Request $request, $id, $field)
    {
        if ($field == 'stan') {
            $szkolenieUpdate = DB::table('crm_szkolenia')->where('crmszkolenieID', $id);
            $szkolenieUpdate->update([
            'stan' => Request::get('stan'),
        ]);
        }
        if ($field == 'komentarz') {
            $szkolenieUpdate = DB::table('crm_szkolenia')->where('crmszkolenieID', $id);
            $szkolenieUpdate->update([
            'komentarz' => Request::get('komentarz'),
        ]);
        }
        if ($field == 'term_state') {
            $szkolenieTerminUpdate = DB::table('h5_terms')->where('termID', $id);
            $szkolenieTerminUpdate->update([
            'term_state' => Request::get('term_state'),
        ]);
        }

        return 'Zaktulizowano dane';
    }

    public function generateListaObecnosciCRMSzkolenie($termID)
    {
        $this->buildExcelSzkolenie($termID, 'ListaObecnosci', null);
    }

    public function buildExcelSzkolenie($termID, $rodzaj, $opiekunID)
    {
        $firma = new CRMFirmyController();
        $getOpiekun = new CRMOsobyController();

        if ($rodzaj == 'ListaHR') {
            if ($opiekunID == '0') {
                $opiekunID = null;
                //$ListaHR = DB::table('crm_osoby')->leftJoin('crm_klienci', 'crm_osoby.klientID', '=', 'crm_klienci.klientID')->where('crm_osoby.hr', '1')->get();  //->where('crm_klienci.opiekunID','')
                $ListaHR = DB::table('crm_osoby')->leftJoin('crm_klienci', 'crm_osoby.klientID', '=', 'crm_klienci.klientID')->where('crm_osoby.hr', '1')->where('crm_klienci.opiekunID', $opiekunID)->get();
            } else {
                $ListaHR = DB::table('crm_osoby')->leftJoin('crm_klienci', 'crm_osoby.klientID', '=', 'crm_klienci.klientID')->where('crm_osoby.hr', '1')->where('crm_klienci.opiekunID', $opiekunID)->get();  //->where('crm_klienci.opiekunID','')
            }
        }

        if ($rodzaj != 'ListaHR') {
            $szkolenia = DB::table('crm_szkolenia')->leftJoin('h5_terms', 'crm_szkolenia.termID', '=', 'h5_terms.termID')->leftJoin('h5_instructor', 'h5_terms.term_instructor', 'h5_instructor.instructorID')->leftJoin('h5_training', 'h5_training.trainingID', '=', 'h5_terms.termID')->leftJoin('crm_osoby', 'crm_osoby.uczestnikID', '=', 'crm_szkolenia.uczestnikID')->where('crm_szkolenia.termID', $termID)->get();

            $uczestnicy = DB::table('crm_szkolenia')->leftJoin('crm_osoby', 'crm_osoby.uczestnikID', '=', 'crm_szkolenia.uczestnikID')->leftJoin('crm_klienci', 'crm_klienci.klientID', '=', 'crm_osoby.klientID')->where('crm_szkolenia.termID', $termID)->get();
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Lista HR');

        $rowNumber = 1;
        $col = 'A';

        // Karta Trenera --------------------------------------------------------------------------------------------------------------------------------------------
        if ($rodzaj == 'KartaTrenera') {
            foreach ($szkolenia as $keyS => $szkolenie) {
                if ($keyS == 0) {
                    $sheet->setCellValue('A1', 'Szkolenie: '.$szkolenie->training_title);

                    if ($szkolenie->term_start == $szkolenie->term_end) {
                        $sheet->setCellValue('A2', 'Data: '.date('d.m.Y', strtotime($szkolenie->term_start)));
                    } else {
                        $sheet->setCellValue('A2', 'Data: '.date('d.m.Y', strtotime($szkolenie->term_start)).' - '.date('d.m.Y', strtotime($szkolenie->term_end)));
                    }

                    $sheet->setCellValue('A3', 'Trener:'.$szkolenie->instructor_name);
                }
            }

            $sheet->setCellValue('D5', 'KARTA KLIENTA');
            $sheet->getStyle('D5')->getFont()->setBold(true);
            $sheet->getStyle('D5')->getFont()->setSize(13);
        }

        if ($rodzaj == 'ListaHR') {
            $sheet->setCellValue('D5', 'LISTA HR');
            $sheet->getStyle('D5')->getFont()->setBold(true);
            $sheet->getStyle('D5')->getFont()->setSize(13);
        }
        // Karta trenera  --------------------------------------------------------------------------------------------------------------------------------------------
        if ($rodzaj == 'KartaTrenera') {
            $sheet->setCellValue('A7', 'Lp.');
            $sheet->setCellValue('B7', 'Imię i nazwisko');
            $sheet->setCellValue('C7', 'Stanowisko');
            $sheet->setCellValue('D7', 'Firma');
            $nextCol = 'E';
        }

        // Lista HR --------------------------------------------------------------------------------------------------------------------------------------------
        if ($rodzaj == 'ListaHR') {
            $sheet->setCellValue('A7', 'Lp.');
            $sheet->setCellValue('B7', 'Imię i nazwisko');
            $sheet->setCellValue('C7', 'E-mail');
            $sheet->setCellValue('D7', 'Firma');
            $sheet->setCellValue('E7', 'Opiekun');
            $sheet->setCellValue('F7', 'Rabat');
            $nextCol = 'F';
        }

        // Lista obecnosci --------------------------------------------------------------------------------------------------------------------------------------------
        if ($rodzaj == 'ListaObecnosci') {
            $sheet->setCellValue('A7', 'Lp.');
            $sheet->setCellValue('B7', 'Imię i nazwisko');

            foreach ($szkolenia as $keyS => $szkolenie) {
                if ($keyS == 0) {
                    $sheet->setCellValue('C7', $szkolenie->term_start);
                }
            }
            $nextCol = 'D';
        }

        // -----------------------------------------------------------------------------------------------------------------------------------------------

        $rowNumber = 8;
        if ($rodzaj != 'ListaHR') {
            $lastRow = count($uczestnicy) + 7;
        }
        if ($rodzaj == 'ListaHR') {
            $lastRow = count($ListaHR) + 7;
        }

        // -----------------------------------------------------------------------------------------------------------------------------------------------

        if ($rodzaj != 'ListaHR') {
            $sheet->setCellValue($nextCol.'7', 'Uwagi');

            foreach ($uczestnicy as $Keynumer => $uczestnik) {
                $numer = $Keynumer + 8;
                $sheet->setCellValue('A'.$numer, ($Keynumer + 1));
                $sheet->setCellValue('B'.$numer, $uczestnik->imie_nazwisko);
                if ($rodzaj == 'KartaTrenera') {
                    $sheet->setCellValue('C'.$numer, $uczestnik->stanowisko);
                    $sheet->setCellValue('D'.$numer, $uczestnik->nazwa);
                }
                if ($rodzaj == 'ListaObecnosci') {
                }
                $sheet->setCellValue($nextCol.$numer, $uczestnik->uwagi);
            }
        }

        // -----------------------------------------------------------------------------------------------------------------------------------------------

        if ($rodzaj == 'ListaHR') {
            foreach ($ListaHR as $HRKeynumer => $hr) {
                $numer = $HRKeynumer + 8;
                $sheet->setCellValue('A'.$numer, ($HRKeynumer + 1));
                $sheet->setCellValue('B'.$numer, $hr->imie_nazwisko);
                $sheet->setCellValue('C'.$numer, $hr->email);
                $sheet->setCellValue('D'.$numer, $hr->nazwa);
                $sheet->setCellValue('E'.$numer, $getOpiekun->getOpiekun($hr->opiekunID));
                $sheet->setCellValue('F'.$numer, $hr->rabat);
            }
        }

        // -----------------------------------------------------------------------------------------------------------------------------------------------

        $sheet->getColumnDimension('A')->setWidth(5);
        $sheet->getColumnDimension('B')->setWidth(25);

        if ($rodzaj != 'ListaHR') {
            if ($rodzaj == 'KartaTrenera') {
                $sheet->getColumnDimension('C')->setWidth(25);
                $sheet->getColumnDimension('D')->setWidth(35);
            }
            if ($rodzaj == 'ListaObecnosci') {
                $sheet->getColumnDimension('C')->setWidth(30);
            }
        }

        if ($rodzaj == 'ListaHR') {
            $sheet->getColumnDimension('C')->setWidth(25);
            $sheet->getColumnDimension('D')->setWidth(35);
            $sheet->getColumnDimension('E')->setWidth(25);
            $sheet->getColumnDimension('F')->setWidth(15);
        }

        if ($rodzaj != 'ListaHR') {
            $sheet->getColumnDimension($nextCol)->setWidth(45);
        }

        $styleArray = array(
        'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => ['rgb' => '000000'],
        ],
    ],
    );
        $sheet->getStyle('A7:'.$nextCol.$lastRow)->applyFromArray($styleArray);
        $sheet->getStyle('A7:'.$nextCol.$lastRow)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A7:'.$nextCol.$lastRow)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
        $sheet->getStyle('A7:'.$nextCol.$lastRow)->getAlignment()->setWrapText(true);

        $sheet->getPageSetup()->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
        $sheet->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);

        $sheet->getPageSetup()->setFitToWidth(1);

        $writer = new Xlsx($spreadsheet);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="karta.xls"');
        header('Cache-Control: max-age=0');

        return $writer->save('php://output');
        exit();
    }

    public function generateKartaTreneraCRMSzkolenie($termID)
    {
        $this->buildExcelSzkolenie($termID, 'KartaTrenera', null);
    }

    //SKOLENIA > ANKIETY

    public function getAllSzkolenia()
    {
        $data = '';
        $getAllSzkolenia = DB::table('h5_training')->where('training_title', 'LIKE', '%'.request('szkolenie').'%')->orderBy('training_title', 'asc')->get(['training_title', 'trainingID']); //->toArray();

        //return $this->getAllSzkolenia;
        foreach ($getAllSzkolenia as $key => $szkolenie) {
            $data .= '<option value="'.$szkolenie->trainingID.'">'.$szkolenie->training_title.'</option>';
        }

        return $data;
    }

    public function createCRMAnkieta($klientID, $uczestnikID, $termID)
    {
        //$getAll
        $getSzkolenia = DB::table('h5_training')->orderBy('training_title', 'asc')->pluck('training_title', 'trainingID')->toArray();
        $storeSzkolenia = [];

        return view('backend.CRMSzkolenie.ankiety.create')->with('getSzkolenia', $getSzkolenia)->with('klientID', $klientID)->with('uczestnikID', $uczestnikID)->with('termID', $termID)->with('storeSzkolenia', $storeSzkolenia);
    }

    public function storeCRMAnkieta(Request $request)
    {
        $dodajAnkiete = DB::table('crm_ankiety')->insert([
            'uwagi' => Request::get('uwagi'),
            'data_dodania' => date('Y-m-d'),
            'szkolenia' => implode(',', Request::get('duallistbox_demo1')), //explode(',', Request::get('szkolenia')),
            'uczestnikID' => Request::get('uczestnikID'),
        ]);

        return  redirect()->route('editCRMOsoby', ['klientID' => Request::get('klientID'), 'uczestnikID' => Request::get('uczestnikID')]);
    }

    public function createISCFile()
    {
        $feed = '';
        $getSzkoleniaTerms = DB::table('crm_szkolenia')->join('h5_terms', 'crm_szkolenia.termID', '=', 'h5_terms.termID')->leftJoin('h5_training', 'h5_training.trainingID', '=', 'h5_terms.trainingID')->groupBy('crm_szkolenia.termID')->get(); //['termID']

        function dateToCal($type, $time)
        {
            if ($type == 'start') {
                return date('Ymd', strtotime($time));
            } else {
                return date('Ymd', strtotime($time.' +1 day'));
            }
        }

        $ical = "BEGIN:VCALENDAR
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:PUBLISH
PRODID:-//Thomas Multimedia//Clinic Time//EN\n";

        foreach ($getSzkoleniaTerms as $termin) {
            $getUczestnikCount = DB::table('crm_szkolenia')->where('termID', $termin->termID)->count();

            if ($getUczestnikCount > 1) {
                $ical .= "BEGIN:VEVENT\n";
                $ical .= 'DTEND:'.dateToCal('end', $termin->term_end)."\n";
                $ical .= 'UID:'.md5($termin->training_title)."\n";
                $ical .= 'DTSTAMP:'.time()."\n";
                $ical .= 'LOCATION:'.addslashes($termin->term_place)."\n";
                $ical .= 'DESCRIPTION:'.addslashes(route('showCRMSzkolenie', ['termID' => $termin->termID]))."\n";
                $ical .= 'URL;VALUE=URI: '.route('showCRMSzkolenie', ['termID' => $termin->termID])."\n";
                $ical .= 'SUMMARY:'.addslashes($termin->training_title).' - Ilość osób('.$getUczestnikCount.")\n";
                $ical .= 'DTSTART:'.dateToCal('start', $termin->term_start)."\n";
                $ical .= "END:VEVENT\n";
            }
        }
        $ical .= 'END:VCALENDAR';

        // Data startu
        // Data zakończenia
        // Tytuł - Tytuł szkolenia . (ilość osób)
        // Lokacja: Miejsce
        // Description: Link
        // (https://www.high5.pl/cmr/backoffice/szkolenie/termin/IDterminu)
        ///return 'generate ics';
        // return $feed;

        header('Content-type: text/calendar; charset=utf-8');
        header('Content-Disposition: inline; filename=calendar.ics');

        echo $ical;
    }
}
