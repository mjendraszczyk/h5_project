<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\CreateKonsultacjeRequest;
use Session;

class KonsultacjeController extends Controller
{
    public function index()
    {
        $konsultacje = DB::table('h5_consulting')->join('h5_categories', 'h5_consulting.consulting_category', '=', 'h5_categories.categoryID')->orderBy('h5_categories.category_title', 'asc')->get();

        return view('backend.konsultacje.index', compact('konsultacje'));
    }

    public function create()
    {
        $konsultacje_tab = DB::table('h5_categories')->pluck('category_title', 'categoryID')->toArray();

        return view('backend.konsultacje.create', compact('konsultacje_tab'));
    }

    public function edit($id)
    {
        $konsultacje_tab = DB::table('h5_categories')->pluck('category_title', 'categoryID')->toArray();

        $konsultacje = DB::table('h5_consulting')->join('h5_categories', 'h5_consulting.consulting_category', '=', 'h5_categories.categoryID')->where('h5_consulting.consultingID', $id)->orderBy('h5_categories.category_title', 'asc')->get();

        return view('backend.konsultacje.edit', compact('konsultacje', 'konsultacje_tab'));
    }

    public function update(CreateKonsultacjeRequest $request, $id)
    {
        $konsultacje = DB::table('h5_consulting')->where('consultingID', $id);

        $konsultacje->update([
            'consulting_subject' => $request->input('consulting_subject'),
            'consulting_problems' => $request->input('consulting_problems'),
            'consulting_category' => $request->input('consulting_category'),
            'consulting_price' => $request->input('consulting_price'),
        ]);

        Session::flash('panelInfo', 'Zapisano pomyslnie.');

        return redirect()->route('editKonsultacje', ['id' => $id]);
    }

    public function store(CreateKonsultacjeRequest $request)
    {
        $konsultacje = DB::table('h5_consulting')->insert([
             'consulting_subject' => $request->input('consulting_subject'),
            'consulting_problems' => $request->input('consulting_problems'),
            'consulting_category' => $request->input('consulting_category'),
            'consulting_price' => $request->input('consulting_price'),
    ]);

        Session::flash('panelInfo', 'Dodano pomyslnie.');

        $id = DB::getPdo()->lastInsertId();

        return redirect()->route('editKonsultacje', ['id' => $id]);
    }

    public function destroy($id)
    {
        $deleteKonsultacje = DB::table('h5_consulting')->where('consultingID', $id);
        $deleteKonsultacje->delete();

        return redirect()->route('indexKonsultacje');
    }
}
