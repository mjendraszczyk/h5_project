<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\CreateReferencjeRequest;
use Session;

class ReferencjeController extends Controller
{
    private $type;

    public function CoreReferences()
    {
        $this->type = ['p' => 'Prawda', 'f' => 'Fałsz'];
    }

    public function index()
    {
        $referencje = DB::table('h5_references')->get();

        return view('backend.referencje.index', compact('referencje'));
    }

    public function edit($id)
    {
        $this->CoreReferences();
        $referencje = DB::table('h5_references')->where('refID', $id)->get();

        return view('backend.referencje.edit', compact('referencje'))->with('type', $this->type);
    }

    public function create()
    {
        $this->CoreReferences();

        return view('backend.referencje.create')->with('type', $this->type);
    }

    //Session::flash('success', 'Zapisano pomyslnie.');

    public function update(CreateReferencjeRequest $request, $id)
    {
        $referencje = DB::table('h5_references')->where('refID', $id);

        $referencje->update([
            'firma' => $request->input('firma'),
            'imie' => $request->input('imie'),
            'tresc' => $request->input('tresc'),
             'type' => $request->input('type'),
            'funkcja' => $request->input('funkcja'),
        ]);

        Session::flash('panelInfo', 'Zapisano pomyslnie.');

        return redirect()->route('editReferencje', ['id' => $id]);
    }

    public function store(CreateReferencjeRequest $request)
    {
        $referencje = DB::table('h5_references')->insert([
             'firma' => $request->input('firma'),
            'imie' => $request->input('imie'),
            'tresc' => $request->input('tresc'),
            'funkcja' => $request->input('funkcja'),
            'type' => 'f',
    ]);

        Session::flash('panelInfo', 'Dodano pomyslnie.');

        $id = DB::getPdo()->lastInsertId();

        return redirect()->route('editReferencje', ['id' => $id]);
    }

    public function destroy($id)
    {
        $deleteReferencje = DB::table('h5_references')->where('refID', $id);
        $deleteReferencje->delete();

        return redirect()->route('indexReferencje');
    }
}
