<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\CreateTerminarzRequest;
use App\Http\Requests\backend\CreateTermFilterRequest;
use Session;

class TerminarzController extends Controller
{
    private $typy_szkolenia;
    private $miejscowosci_szkolenia;
    private $termin_typ_szkolenia;

    public function __construct() {
        $this->termin_typ_szkolenia = array(
            "1" => "Otwarte",
            "2" => "Online"
        );
    }
    public function CoreTeminarzIndex()
    {
        $this->typy_szkolenia = ['0' => 'Wszystkie', '2' => 'Pewne'];
        $this->miejscowosci_szkolenia = DB::table('h5_places')->pluck('nazwa', 'placeID')->toArray();
        array_unshift($this->miejscowosci_szkolenia, 'Wszystkie');
    }

    public function index()
    {
        $this->CoreTeminarzIndex();

        $getTerminy = DB::table('h5_terms')->join('h5_training', 'h5_terms.trainingID', '=', 'h5_training.trainingID')->where('term_placeID', '2')->orderBy('h5_terms.term_start', 'asc')->get();

        foreach ($getTerminy as $termin) :

            if ($termin->term_color == null) {
                $kolor = '#3097d1';
            } else {
                $kolor = $termin->term_color;
            }
        $events[] = \Calendar::event(
    $termin->training_title, //event title
    true, //full day event?
    new \DateTime($termin->term_start), //start time (you can also use Carbon instead of DateTime)
    new \DateTime($termin->term_end.' +1day'), //end time (you can also use Carbon instead of DateTime)
    'stringEventId', //optionally, you can specify an event ID,
    [
        'url' => route('editTerminarz', ['id' => $termin->termID]),
        'color' => $kolor,
    ]
        );

        endforeach;

        $calendar = \Calendar::addEvents($events) //add an array with addEvents
    ->setOptions([ //set fullcalendar options
        'firstDay' => 1,
    ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
        'viewRender' => 'function() {}',
    ]);

        return view('backend.terminy.index')->with('calendar', $calendar)->with('typy_szkolenia', $this->typy_szkolenia)->with('miejscowosci_szkolenia', $this->miejscowosci_szkolenia)->with("termin_typ_szkolenia",$this->termin_typ_szkolenia);
    }

    public function filtrTerminarz(CreateTermFilterRequest $request)
    {
        // jesli term_state != 0
        // jesli term_place != 0
        // dawaj where
        // else nie dawaj where
        $this->CoreTeminarzIndex();

        if (($request->input('term_state') != '0') && ($request->input('term_place') != '0')) {
            $getTerminy = DB::table('h5_terms')->join('h5_training', 'h5_terms.trainingID', '=', 'h5_training.trainingID')->where('h5_terms.term_state', $request->input('term_state'))->where('h5_terms.term_placeID', $request->input('term_place'))->orderBy('h5_terms.term_start', 'asc')->get();
        }

        if (($request->input('term_state') == '0')) {
            $getTerminy = DB::table('h5_terms')->join('h5_training', 'h5_terms.trainingID', '=', 'h5_training.trainingID')->where('h5_terms.term_placeID', $request->input('term_place'))->orderBy('h5_terms.term_start', 'asc')->get();
        }
        if (($request->input('term_place') == '0')) {
            $getTerminy = DB::table('h5_terms')->join('h5_training', 'h5_terms.trainingID', '=', 'h5_training.trainingID')->where('h5_terms.term_state', $request->input('term_state'))->orderBy('h5_terms.term_start', 'asc')->get();
        }
        if (($request->input('term_state') == '0') && ($request->input('term_place') == '0')) {
            $getTerminy = DB::table('h5_terms')->join('h5_training', 'h5_terms.trainingID', '=', 'h5_training.trainingID')->orderBy('h5_terms.term_start', 'asc')->get();
        }

        if ($getTerminy->count() == 0) {
            $events = [];
        } else {
            foreach ($getTerminy as $termin) :

 $events[] = \Calendar::event(
    $termin->training_title, //event title
    true, //full day event?
    new \DateTime($termin->term_start), //start time (you can also use Carbon instead of DateTime)
    new \DateTime($termin->term_end.' +1 day'), //end time (you can also use Carbon instead of DateTime)
    'stringEventId', //optionally, you can specify an event ID,
    [
        'url' => route('editTerminarz', ['id' => $termin->termID]),
    ]
        );

            endforeach;
        }

        $calendar = \Calendar::addEvents($events) //add an array with addEvents
    ->setOptions([ //set fullcalendar options
        'firstDay' => 1,
        'locale' => 'pl-pl',
    ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
        'viewRender' => 'function() {}',
    ]);

        return view('backend.terminy.index')->with('calendar', $calendar)->with('typy_szkolenia', $this->typy_szkolenia)->with('miejscowosci_szkolenia', $this->miejscowosci_szkolenia)->with("termin_typ_szkolenia",$this->termin_typ_szkolenia);
    }

    private $kursy;
    private $miejsca;
    private $trenerzy;
    private $typy;
    private $term_state;

    public function CoreTerminarzData()
    {
        $this->kursy =
        DB::table('h5_training')->orderBy('training_title', 'asc')->pluck('training_title', 'trainingID')->toArray();
        $this->miejsca = DB::table('h5_places')->pluck('nazwa', 'placeID')->toArray();

        $this->trenerzy = DB::table('h5_instructor')->pluck('instructor_name', 'instructorID')->toArray();

        $this->typy = ['1' => 'Otwarte', '2' => 'Zamkniete', '3' => 'Sesja'];

        $this->term_state = ['1' => 'planowany', '2' => 'zatwierdzony', '3' => 'zrealizowany', '4' => 'anulowany'];
    }

    public function create()
    {
        $this->CoreTerminarzData();

        return view('backend.terminy.create')->with('kursy', $this->kursy)->with('miejsca', $this->miejsca)->with('trenerzy', $this->trenerzy)->with('typy', $this->typy)->with('term_state', $this->term_state)->with("termin_typ_szkolenia",$this->termin_typ_szkolenia);
    }

    public function edit($id)
    {
        $getTerminy = DB::table('h5_terms')->join('h5_training', 'h5_terms.trainingID', '=', 'h5_training.trainingID')->where('h5_terms.termID', $id)->orderBy('h5_terms.term_start', 'asc')->get();

        //Wyswietl tablice z aktualnym terminem
        $getTermParentID = DB::table('h5_terms')->where('termID', $id)->pluck('termParentID');

        // Pobierz wszystkie rekordy gdzie ParentID takie jak terminu wybrany
        if ($getTermParentID[0] != 0) {
            $tabParentID = DB::table('h5_terms')->join('h5_training', 'h5_terms.trainingID', '=', 'h5_training.trainingID')->where('termParentID', $getTermParentID[0])->orderBy('h5_terms.term_start', 'asc')->get();
        } else {
            $tabParentID = null;
        }

        $this->CoreTerminarzData();

        return view('backend.terminy.edit', compact('getTerminy', 'tabParentID'))->with('kursy', $this->kursy)->with('miejsca', $this->miejsca)->with('trenerzy', $this->trenerzy)->with('typy', $this->typy)->with('term_state', $this->term_state)->with("termin_typ_szkolenia",$this->termin_typ_szkolenia);
    }

    public function update(CreateTerminarzRequest $request, $id)
    {
        if ($request->input('term_frabat') == null) {
            $rabat = '0';
        } else {
            $rabat = $request->input('term_frabat');
        }
        if ($request->input('term_closed') == null) {
            $term_closed = 'n';
        } else {
            $term_closed = 'y';
        }
        $terminarz = DB::table('h5_terms')->where('termID', $id);

        $getTermParentID = DB::table('h5_terms')->where('termID', $id)->pluck('termParentID');

        if (count($request->input('term_start') > 1)) {
            for ($dta = 0; $dta < count($request->input('term_start')); ++$dta) {
                if ($dta == 0) {
                    // jezeli aktualizujemy termin bez przypisanego innego terminu
                    if (($getTermParentID[$dta]) == 0) {
                        $nextId = $id;
                    }
                    // jesli juz ma jakis zapisany termin
                    else {
                        $nextId = $getTermParentID[$dta];
                    }

                    $terminarz->update([
                            'trainingID' => $request->input('training_title'),
                            'term_start' => $request->input('term_start')[0],
                            'term_end' => $request->input('term_end')[0],
                            'term_place' => $request->input('term_place'),
                            'term_placeID' => $request->input('term_placeID'),
                            'term_instructor' => $request->input('term_instructor'),
                            'term_fdays' => $request->input('term_fdays'),
                            'term_state' => $request->input('term_state'),
                            'term_frabat' => $rabat,
                            'term_color' => $request->input('term_color'),
                            'term_training_type' => $request->input('term_training_type'),
                            'termParentID' => $nextId,
                            'term_closed' => $term_closed,
                         ]);
                }
                // @EDIT: dla kolejnych elementow  przypisuj terminy do glownego
                else {
                    // Przypisuj kolejne terminy do glownego
                    //if ($nextId != $id) { ?
                    //return 'test2';
                    $terminarz = DB::table('h5_terms')->insert([
                        'trainingID' => $request->input('training_title'),
                        'term_start' => $request->input('term_start')[$dta],
                        'term_end' => $request->input('term_end')[$dta],
                        'term_place' => $request->input('term_place'),
                        'term_instructor' => $request->input('term_instructor'),
                        'term_fdays' => $request->input('term_fdays'),
                        'term_frabat' => $rabat,
                        'term_state' => $request->input('term_state'),
                        'term_color' => $request->input('term_color'),
                        'term_training_type' => $request->input('term_training_type'),
                        'termParentID' => $nextId,
                        'term_closed' => $term_closed,
                    ]);
                    //  }
                }
            }
        }
        Session::flash('panelInfo', 'Zapisano pomyslnie.');

        return redirect()->route('editTerminarz', ['id' => $id]);
    }

    public function store(CreateTerminarzRequest $request)
    {
        // jesli wykyje w tablicy wiecej niz 1 date to je grupuje powiazujac terminy szkolenia ktore bedzie odbywalo sie w formie sesji

        if ($request->input('term_frabat') == null) {
            $rabat = '0';
        } else {
            $rabat = $request->input('term_frabat');
        }

        if ($request->input('term_closed') == null) {
            $term_closed = 'n';
        } else {
            $term_closed = 'y';
        }
        // Jezeli ma posiadac inne terminy
        if (count($request->input('term_start') > 1)) {
            for ($dta = 0; $dta < count($request->input('term_start')); ++$dta) {
                if ($dta == 0) {
                    //Pobierz ID które będzie wstrzykiwane jako ParentID dla terminu
                    $nextId = DB::table('h5_terms')->max('termID') + 1;
                }

                $terminarz = DB::table('h5_terms')->insert([
                    'trainingID' => $request->input('training_title'),
                    'term_start' => $request->input('term_start')[$dta],
                    'term_end' => $request->input('term_end')[$dta],
                    'term_place' => $request->input('term_place'),
                    'term_placeID' => $request->input('term_placeID'),
                    'term_instructor' => $request->input('term_instructor'),
                    'term_fdays' => $request->input('term_fdays'),
                    'term_frabat' => $rabat,
                    'term_state' => $request->input('term_state'),
                    'term_color' => $request->input('term_color'),
                    'term_training_type' => $request->input('term_training_type'),
                    'termParentID' => $nextId,
                    'term_closed' => $term_closed,
                ]);
            }
        }
        // Jesli ma byc to pojedynczy termin
        else {
            $terminarz = DB::table('h5_terms')->insert([
                'trainingID' => $request->input('training_title'),
            'term_start' => $request->input('term_start')[0],
            'term_end' => $request->input('term_end')[0],
            'term_place' => $request->input('term_place'),
            'term_placeID' => $request->input('term_placeID'),
            'term_instructor' => $request->input('term_instructor'),
            'term_fdays' => $request->input('term_fdays'),
            'term_frabat' => $rabat,
            'term_state' => $request->input('term_state'),
            'term_color' => $request->input('term_color'),
            'term_training_type' => $request->input('term_training_type'),
            'term_closed' => $term_closed,
    ]);
        }

        //return redirect()->route('editTerminarz', ['id' => $nextId]);
        return redirect()->route('indexTerminarz');
    }

    public function destroy($id)
    {
        $deleteTerminarz = DB::table('h5_terms')->where('termID', $id);
        $deleteTerminarz->delete();

        return redirect()->route('indexTerminarz');
    }

    // jesli uzytkownik kliknie odepnij to podobonie jak w przypadku delete zaktualizuje dany rekord
    public function odepnij($id)
    {
        $odepnij_termin = DB::table('h5_terms')->where('termID', $id);

        $odepnij_termin->update([
        'termParentID' => '0',
        ]);

        return redirect()->route('editTerminarz', ['id' => $id]);
    }

    public function getAPITerminarz($id)
    {
        $getAPITerminarz = DB::table('h5_training')->where('trainingID', $id)->get();

        return json_encode($getAPITerminarz);
    }
}
