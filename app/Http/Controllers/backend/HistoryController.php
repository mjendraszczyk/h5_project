<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\CreateHistoryRequest;
use Session;

class HistoryController extends Controller
{
    public function index()
    {
        $history = DB::table('h5_history')->orderBy('news_data', 'desc')->paginate(15);

        return  view('backend.history.index', compact('history'));
    }

    public function edit($id)
    {
        $history = DB::table('h5_history')->where('newsID', $id)->get();

        return view('backend.history.edit', compact('history'));
    }

    public function create()
    {
        return view('backend.history.create');
    }

    //Session::flash('success', 'Zapisano pomyslnie.');

    public function update(CreateHistoryRequest $request, $id)
    {
        // $update_shop = Setting::findOrFail($id);

        $history = DB::table('h5_history')->where('newsID', $id);

        if (!empty($request->file('image'))) {
            $image = $request->file('image');

            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/img/frontend/news/');
            $image->move($destinationPath, $input['imagename']);

            $history->update([
        'news_picture' => $input['imagename'],
    'news_title' => $request->input('news_title'),
            'news_lid' => $request->input('news_lid'),
            'news_text' => $request->input('news_text'),
            'news_link' => $request->input('news_link'),
            'news_data' => date('Y-m-d'),
            'news_pictur_pos' => $request->input('news_pictur_pos'),
            'news_place' => $request->input('news_place'),
        ]);
        } else {
            $history->update([
    'news_title' => $request->input('news_title'),
            'news_lid' => $request->input('news_lid'),
            'news_text' => $request->input('news_text'),
            'news_link' => $request->input('news_link'),
            'news_data' => date('Y-m-d'),
            'news_pictur_pos' => $request->input('news_pictur_pos'),
            'news_place' => $request->input('news_place'),
        ]);
        }

        Session::flash('panelInfo', 'Zapisano pomyslnie.');

        // $id = DB::getPdo()->lastInsertId();

        return redirect()->route('editHistory', ['id' => $id]);
    }

    public function store(CreateHistoryRequest $request)
    {
        if (!empty($request->file('image'))) {
            $image = $request->file('image');

            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/img/frontend/news/');
            $image->move($destinationPath, $input['imagename']);

            $history = DB::table('h5_history')->insert([
        'news_picture' => $input['imagename'],
    'news_title' => $request->input('news_title'),
            'news_lid' => $request->input('news_lid'),
            'news_text' => $request->input('news_text'),
            'news_link' => $request->input('news_link'),
            'news_data' => date('Y-m-d'),
            'news_pictur_pos' => $request->input('news_pictur_pos'),
            'news_place' => $request->input('news_place'),
        ]);
        } else {
            $history = DB::table('h5_history')->insert([
    'news_title' => $request->input('news_title'),
            'news_lid' => $request->input('news_lid'),
            'news_text' => $request->input('news_text'),
            'news_link' => $request->input('news_link'),
            'news_data' => date('Y-m-d'),
            'news_pictur_pos' => $request->input('news_pictur_pos'),
            'news_place' => $request->input('news_place'),
        ]);
        }
        Session::flash('panelInfo', 'Dodano pomyslnie.');

        $id = DB::getPdo()->lastInsertId();

        return redirect()->route('editHistory', ['id' => $id]);
    }

    public function destroy($id)
    {
        $deleteHistory = DB::table('h5_history')->where('newsID', $id);
        $deleteHistory->delete();

        return redirect()->route('indexHistory');
    }
}
