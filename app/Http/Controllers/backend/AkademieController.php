<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\CreateAkademiaRequest;
use App\Http\Requests\backend\CreateASzkolenieRequest;
use Session;

class AkademieController extends Controller
{
    public function index()
    {
        $akademie = DB::table('h5_akademia')->get();

        return view('backend.akademie.index', compact('akademie'));
    }

    public function create()
    {
        return view('backend.akademie.create');
    }

    public function edit($id)
    {
        $akademie = DB::table('h5_akademia')->where('akademiaID', $id)->get(); //->join('h5_akademia_szkolenia', 'h5_akademia_szkolenia.akademiaID', '=', 'h5_akademia.akademiaID')->where('h5_akademia.akademiaID', $id)->get();

        return view('backend.akademie.edit', compact('akademie'));
    }

    protected $aktywna;

    public function setActive($request)
    {
        if ($request->input('aktywna') == null) {
            $this->aktywna = '0';
        } else {
            $this->aktywna = '1';
        }

        //return 1;
    }

    public function update(CreateAkademiaRequest $request, $id)
    {
        $this->setActive($request);
        $akademia = DB::table('h5_akademia')->where('akademiaID', $id);

        $akademia->update([
            'nazwa' => $request->input('nazwa'),
            'opis_krotki' => $request->input('opis_krotki'),
            'opis' => $request->input('opis'),
            'aktywna' => $this->aktywna,
         ]);

        Session::flash('panelInfo', 'Zapisano pomyslnie.');

        return redirect()->route('editAkademie', ['id' => $id]);
    }

    public function store(CreateAkademiaRequest $request)
    {
        $this->setActive($request);

        $akademia = DB::table('h5_akademia')->insert([
             'nazwa' => $request->input('nazwa'),
            'opis_krotki' => $request->input('opis_krotki'),
            'opis' => $request->input('opis'),
            'aktywna' => $this->aktywna,
    ]);

        $id = DB::getPdo()->lastInsertId();

        return redirect()->route('editAkademie', ['id' => $id]);
    }

    public function destroy($id)
    {
        $deleteAkademia = DB::table('h5_akademia')->where('akademiaID', $id);
        $deleteAkademia->delete();

        return redirect()->route('indexAkademie');
    }

    public function createSzkolenie($idAkademia)
    {
        $trainingID = DB::table('h5_training')->pluck('training_title', 'trainingID')->toArray();

        return view('backend.akademie.createSzkolenia', compact('idAkademia', 'trainingID'));
    }

    public function storeSzkolenie(CreateASzkolenieRequest $request, $idAkademia)
    {
        $dodajSzkolenie = DB::table('h5_akademia_szkolenia')->insert([
            'akademiaID' => $request->akademiaID,
            'trainingID' => $request->trainingID,
            'kolejnosc' => '0',
        ]);

        return redirect()->route('listSzkolenia', ['idAkademia' => $idAkademia]);
    }

    public function listSzkolenia($idAkademia)
    {
        $listSzkolenia = DB::table('h5_akademia_szkolenia')->join('h5_training', 'h5_akademia_szkolenia.trainingID', 'h5_training.trainingID')->where('akademiaID', $idAkademia)->get();

        return view('backend.akademie.listSzkolenia', compact('listSzkolenia', 'idAkademia'));
    }

    public function deleteASzkolenie($idAkademia, $id)
    {
        $usunSzkolenie = DB::table('h5_akademia_szkolenia')->where('akademia_szkolenieID', $id)->delete();

        return redirect()->route('listSzkolenia', ['idAkademia' => $idAkademia]);
    }
}
