<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\backend\CreateRoleRequest;
use Session;
class RoleController extends Controller
{
     public function index()
    {
        $role = DB::table('role')->get();
        return view('backend.role.index', compact('role'));
    }

    public function create()
    {
        return view('backend.role.create');
    }

    public function edit($id)
    {
        $role = DB::table('role')->where('id_role', $id)->get();

        return view('backend.role.edit', compact('role'));
    }

    public function update(CreateRoleRequest $request, $id)
    {
        $role = DB::table('role')->where('id_role', $id);

            $role->update([
            'name' => $request->input('name'),
        ]);
       

        Session::flash('panelInfo', 'Zapisano pomyslnie.');

        return redirect()->route('indexRole');
    }

    public function store(CreateRoleRequest $request)
    {
        $role = DB::table('role')->insert([
              'name' => $request->input('name'),
        ]);

        return redirect()->route('indexRole');
    }

    public function destroy($id)
    {
        $deleteRole = DB::table('role')->where('id_role', $id);
        $deleteRole->delete();

        return redirect()->route('indexRole');
    }

}
