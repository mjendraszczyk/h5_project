<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\CreateHeadlinesRequest;
use Session;

class HeadlinesController extends Controller
{
    public function index()
    {
        $headlines = DB::table('h5_headlines')->get();

        return view('backend.headlines.index', compact('headlines'));
    }

    public function edit($id)
    {
        $headlines = DB::table('h5_headlines')->where('headlineID', $id)->get();

        return view('backend.headlines.edit', compact('headlines'));
    }

    public function create()
    {
        return view('backend.headlines.create');
    }

    public function update(CreateHeadlinesRequest $request, $id)
    {
        // $update_shop = Setting::findOrFail($id);

        $headline = DB::table('h5_headlines')->where('headlineID', $id);

        if (!empty($request->file('image'))) {
            $image = $request->file('image');

            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/img/frontend/headlines');
            $image->move($destinationPath, $input['imagename']);

            $headline->update([
            'picture' => $input['imagename'],
            'title' => $request->input('title'),
            'content' => $request->input('content'),
            'link' => $request->input('link'),
            'position' => $request->input('position'),
        ]);
        } else {
            $headline->update(['title' => $request->input('title'), 'content' => $request->input('content'),
'link' => $request->input('link'), 'position' => $request->input('position'), ]);
        }

        Session::flash('success', 'Zapisano pomyslnie.');

        return redirect()->route('editHeadline', ['id' => $id]);
    }

    public function store(CreateHeadlinesRequest $request)
    {
        if (!empty($request->file('image'))) {
            $image = $request->file('image');

            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/img/frontend/headlines');

            $image->move($destinationPath, $input['imagename']);

            $obrazek = $input['imagename'];
        } else {
            $obrazek = '';
        }

        $headline = DB::table('h5_headlines')->insert(['picture' => $obrazek, 'title' => $request->input('title'),
'content' => $request->input('content'), 'link' => $request->input('link'), 'position' => $request->input('position'), ]);

        Session::flash('success', 'Dodano pomyslnie.');

        $id = DB::getPdo()->lastInsertId();

        return redirect()->route('editHeadline', ['id' => $id]);
    }

    public function destroy($id)
    {
        $deleteHeadline = DB::table('h5_headlines')->where('headlineID', $id);
        $deleteHeadline->delete();

        return redirect()->route('indexHeadline');
    }
}
