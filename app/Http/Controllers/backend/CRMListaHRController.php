<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\CreateCRMListaHRRequest;
use App\Http\Requests\backend\CreateCRMListaHROpiekunRequest;
use Config;
use Request;
use Auth;

class CRMListaHRController extends Controller
{
    public function index()
    {
        $CRMFirmyController = new CRMFirmyController();
        $opiekun = $CRMFirmyController->coreFirma();

        if (Auth::user()->id_role == '3') {
            
            $getListaHR = DB::table('crm_osoby')->leftJoin('crm_klienci', 'crm_osoby.klientID', '=', 'crm_klienci.klientID')->where('crm_osoby.hr', '1')->where('crm_klienci.opiekunID',Auth::user()->id)->orderBy('crm_osoby.uczestnikID', 'desc')->get();
        } else {
            $getListaHR = DB::table('crm_osoby')->leftJoin('crm_klienci', 'crm_osoby.klientID', '=', 'crm_klienci.klientID')->where('opiekunID', null)->where('crm_osoby.hr', '1')->orderBy('crm_osoby.uczestnikID', 'desc')->get();
        }
        return view('backend.CRMListaHR.index', compact('getListaHR'))->with('opiekun', $opiekun);
    }

    public function getHrForOpiekun(CreateCRMListaHROpiekunRequest $request)
    {
        return redirect()->route('GETgetHrForOpiekun', ['opiekunID' => $request->input('opiekun')]);
    }

    public function GETgetHrForOpiekun($opiekunID)
    {
        $CRMFirmyController = new CRMFirmyController();
        $opiekun = $CRMFirmyController->coreFirma();

        if ($opiekunID == '0') {
            $opiekunNr = null;
        } else {
            $opiekunNr = $opiekunID;
        }
        $getListaHR = DB::table('crm_osoby')->leftJoin('crm_klienci', 'crm_osoby.klientID', '=', 'crm_klienci.klientID')->where('crm_osoby.hr', '1')->where('crm_klienci.opiekunID', $opiekunNr)->orderBy('crm_osoby.uczestnikID', 'desc')->get();

        return view('backend.CRMListaHR.index', compact('getListaHR'))->with('opiekun', $opiekun)->with('opiekunID', $opiekunID);
    }

    public function create()
    {
    }

    public function edit($id)
    {
    }

    public function update(CreateCRMListaHRRequest $request, $id)
    {
    }

    public function store(CreateCRMListaHRRequest $request)
    {
    }

    public function updateAjaxHROsoba($id)
    {
        $hr = DB::table('crm_osoby')->where('uczestnikID', $id);

        $hr->update([
            'nastepny_kontakt' => request('nastepny_kontakt'),
        ]);

        return Config::get('app.save_status');
    }

    public function getListaHR($opiekunID)
    {
        $getListaHR = new CRMSzkolenieController();

        $generateXLSX = $getListaHR->buildExcelSzkolenie(null, 'ListaHR', $opiekunID);

        return $generateXLSX;
    }

    public function getZdarzeniaFromHR($id)
    {
        $typy_zdarzenia = new CRMZdarzeniaController();

        $zdarzenie = $typy_zdarzenia->typyZdarzenia();

        $hrZdarzenia = DB::table('crm_zdarzenia')->leftJoin('crm_osoby', 'crm_zdarzenia.uczestnikID', '=', 'crm_osoby.uczestnikID')->where('crm_zdarzenia.uczestnikID', $id)->orderBy('crm_zdarzenia.data_dodania', 'desc')->get();

        return view('backend.CRMListaHR.modal', compact('hrZdarzenia'))->with('typy_zdarzenia', $zdarzenie);
    }
}
