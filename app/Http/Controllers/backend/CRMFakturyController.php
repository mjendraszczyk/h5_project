<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\backend\CreateCRMFakturyRequest;
use App\Http\Requests\backend\CreateCRMFakturyFiltrRequest;
use Illuminate\Support\Facades\DB;
use GusApi\Exception\InvalidUserKeyException;
use GusApi\GusApi;
use GusApi\ReportTypes;
use Auth;

//use App\Http\Controllers\backend\GusAPI;
use PDF;
use Request;
use Config;
use Route;
use NumberToWords\NumberToWords;
use Session;
use DateTime;

class CRMFakturyController extends Controller
{
    private $typy;
    public $vat;
    private $forma;
    private $jm;
    private $termin;
    private $forma_wysylki;

    private $slowa_cyfry;

    public function coreFaktury()
    {
        $this->slowa_cyfry = ['0' => 'zero', '1' => 'jeden', '2' => 'dwa', '3' => 'trzy', '4' => 'cztery', '5' => 'pięć', '6' => 'sześć', '7' => 'siedem', '8' => 'osiem', '9' => 'dziewięć'];
        $this->typy = [0 => 'Wszystkie', 1 => 'VAT', 2 => 'Proforma', 3 => 'Zaliczkowa', 4 => 'Rozliczeniowa'];
        //$this->vat = ['0' => '0', '23' => '23'];
        $this->vat = ['0' => 'ZW', '23' => '23'];

        $this->jm = ['os.' => 'os.', 'szt.' => 'szt.'];

        $this->forma = [0 => 'Przelew', 1 => 'Gotówka'];

        $this->forma_wysylki = ['1' => 'Nieodebrana', '2' => 'Poczta', '3' => 'Kurier', '4' => 'Osobiscie', '5' => 'Anulowana', '6' => 'Email', '7' => 'W toku'];

        $this->termin = ['14' => '14 dni', '3' => '3 dni', '7' => '7 dni', '21' => '21 dni', '28' => '28 dni', '30' => '30 dni', '45' => '45 dni', '60' => '60 dni', '90' => '90 dni', '100' => 'Zapłacono gotówką', '101' => 'Zapłacono przelewem'];
    }

    public function typyFaktury()
    {
        $this->coreFaktury();

        $typyFaktury = $this->typy;

        return $typyFaktury;
    }

    public function index()
    {
        $this->coreFaktury();

        return $this->getFakturyFromRange(date('m'), date('Y'));
    }

    public function create($klientID, $uczestnikID, $szkolenieTermID, $typ)
    {
        $this->coreFaktury();

        // jezeli typ = vat lub proforma return standardowo
        $typy_fakury = $this->typy;

        unset($typy_fakury['0']);

        for ($t = 1; $t <= 4; ++$t) {
            if ($t != $typ) {
                unset($typy_fakury[$t]);
            }
        }

        if ($szkolenieTermID == '0') {
            return view('backend.CRMFaktury.create')->with('klientID', $klientID)->with('termID', '0')->with('uczestnikID', '0')->with('vat', $this->vat)->with('jm', $this->jm)->with('typy', $typy_fakury)->with('forma', $this->forma)->with('termin', $this->termin)->with('szkolenieTerm_title', '0')->with('typ', $typ);
        } else {
            $sqlTerm = DB::table('h5_terms')->join('h5_training', 'h5_terms.trainingID', '=', 'h5_training.trainingID')->where('h5_terms.termID', $szkolenieTermID);
            $szkolenieTerm_title = $sqlTerm->get(['h5_training.training_title'])->first();
            $szkolenieTerm_price = $sqlTerm->get(['h5_training.training_price'])->first();

            return view('backend.CRMFaktury.create')->with('klientID', $klientID)->with('termID', $szkolenieTermID)->with('uczestnikID', $uczestnikID)->with('vat', $this->vat)->with('jm', $this->jm)->with('typy', $typy_fakury)->with('forma', $this->forma)->with('termin', $this->termin)->with('szkolenieTerm_title', $szkolenieTerm_title)->with('szkolenieTerm_price', $szkolenieTerm_price)->with('typ', $typ);
        }
    }

    public function edit($id)
    {
        $this->coreFaktury();

        Session::put('FakturowanieZaliczkowe', '0');
        Session::put('FakturowanieRozliczeniowe', '0');

        $typy_fakury = $this->typy;

        $faktury = DB::table('crm_faktury2016')->leftJoin('crm_klienci', 'crm_faktury2016.klientID', '=', 'crm_klienci.klientID')->where('fakturaID', $id)->get();

        //return dd($faktury);
        //return date('Y', strtotime($faktury[0]->data_wystawienia));
        //->whereRaw('data_wystawienia LIKE  "%'.date('Y', strtotime($faktury[0]->data_wystawienia)).'-%"')
        $fzaliczkowe = DB::table('crm_faktury2016')->where('crm_faktury2016.typ', '3')->orWhere('crm_faktury2016.typ', '4')->get();

        $klientID = DB::table('crm_faktury2016')->where('fakturaID', $id)->pluck('klientID'); // Pobierz IDKlienta dla faktury

        foreach ($faktury as $faktura) {
            unset($typy_fakury['0']);
            if ($faktura->typ != '4') {
                unset($typy_fakury['4']);
            }
            $zaplacono = DB::table('crm_faktury2016')->where('FZID', $faktura->FZID)->sum('zaplacono');

            $sprawdzPDF = file_exists(public_path().'/admin/faktury/'.$id.'_'.$faktura->nr_faktury.'.pdf');
        }

        return view('backend.CRMFaktury.edit', compact('faktury', 'fzaliczkowe'))->with('klientID', $klientID[0])->with('vat', $this->vat)->with('typy', $typy_fakury)->with('jm', $this->jm)->with('forma', $this->forma)->with('termin', $this->termin)->with('zaplacono', $zaplacono)->with('sprawdzPDF', $sprawdzPDF);
    }

    public static function getZaplacono($FZID)
    {
        $zaplacono = DB::table('crm_faktury2016')->where('FZID', $FZID)->sum('zaplacono');

        return sprintf('%0.2f', $zaplacono);
    }

    private $resultGenerateNrFakturyNowy;

    public function GenerateNrFakturyNowy($typf, $nr_faktury, $FZID, $nr_faktury_nowy)
    {
        // Generowanie nowej numeracji dla FV zaliczkowej
        if (($typf == '3') || ($typf == '4')) {
            if (Route::CurrentRouteName() == 'updateCRMFaktury') {
                $nr_faktury = $nr_faktury_nowy;
                $FZID = $FZID;
            } else {
                $nr_faktury = $nr_faktury;
                $FZID = $nr_faktury;
            }
        }
        // Dla tworzenia pozostałych FV
        else {
            $nr_faktury = $nr_faktury;
            $FZID = '0';
        }
        $typ = $typf;

        $this->resultGenerateNrFakturyNowy = ['typ' => $typf, 'FZID' => $FZID, 'nr_faktury' => $nr_faktury];
    }

    public function store(CreateCRMFakturyRequest $request)
    {
        $this->coreFaktury();
        // Generowanie Numeracji dla zaliczek
        if (Session::get('FakturowanieZaliczkowe') == '1') {
            $getLastNrFV = DB::table('crm_faktury2016')->where('typ', '3')->whereRaw('data_wystawienia LIKE  "%'.date('Y').'-%"')->orderBy('nr_faktury', 'desc')->pluck('nr_faktury')->first();
            $this->GenerateNrFakturyNowy('3', $request->input('nr_faktury'), $request->input('FZID'), ($getLastNrFV + 1));
        }
        // Generowanie Numeracji dla Rozliczeń
        elseif (Session::get('FakturowanieRozliczeniowe') == '1') {
            $getLastNrFV = DB::table('crm_faktury2016')->where('typ', '4')->whereRaw('data_wystawienia LIKE  "%'.date('Y').'-%"')->orderBy('nr_faktury', 'desc')->pluck('nr_faktury')->first();

            $this->GenerateNrFakturyNowy('4', $request->input('nr_faktury'), $request->input('FZID'), ($getLastNrFV + 1)); //!!
        }
        // Pozostałe
        else {
            $this->GenerateNrFakturyNowy($request->input('typ'), $request->input('nr_faktury'), $request->input('FZID'), $request->input('nr_faktury_nowy'));
        }

        // Dla faktury koncowej automatycznie daj kwotę pozostałosci zobowiazan
        if (Session::get('FakturowanieRozliczeniowe') == '1') {
            $zaplacono = $request->input('pozostalozFV');
            $pozostalo = '0';
        } else {
            if (Session::get('FakturowanieZaliczkowe') == '1') {
                if ($request->typ == '2') {
                    $zaplacono = $request->input('kwota_brutto');
                    $pozostalo = '0';
                } else {
                    $zaplacono = $request->input('zaplacono');
                    $pozostalo = $request->input('pozostalo');
                }
            } else {
                $zaplacono = $request->input('zaplacono');
                $pozostalo = $request->input('pozostalo');
            }
        }

        $faktury = DB::table('crm_faktury2016')->insert([
            'rok' => date('Y'),
            'klientID' => $request->input('klientID'),
            'termID' => $request->input('termID'),
            'nr_faktury' => $this->resultGenerateNrFakturyNowy['nr_faktury'],
            'opis' => $request->input('opis'),
            'cena_jednostkowa' => $request->input('cena_jednostkowa'),
            'kwota' => $request->input('kwota'),
            'VAT' => $request->input('VAT'),
            'kwota_VAT' => $request->input('kwota_VAT'),
            'kwota_brutto' => $request->input('kwota_brutto'),
            'stan' => '1',
            'ilosc' => $request->input('ilosc'),
            'jm' => $request->input('jm'),
            'data_wystawienia' => $request->input('data_wystawienia'),
            'data_sprzedazy' => $request->input('data_sprzedazy'),
            'termin' => $request->input('termin'),
            'typ' => $this->resultGenerateNrFakturyNowy['typ'],
            'forma' => $request->input('forma'),
            'rabat_f' => $request->input('rabat'),
            'FZID' => $request->input('FZID'), //$this->resultGenerateNrFakturyNowy['FZID'],
            'pozostalo' => $pozostalo,
            'zaplacono' => $zaplacono,
            'cena_jednostkowa_org' => $request->input('cena_jednostkowa_org'),
            'uczestnikID' => $request->input('uczestnikID'),
    ]);

        $id = DB::getPdo()->lastInsertId();

        $faktury_wysylka = DB::table('crm_wysylka')->insert([
            'fakturaID' => $id,
            'forma' => '1',
            'data_wysylki' => date('Y-m-d'), //'0000-00-00',
            'oplacona' => '0',
    ]);

        $listaZaliczek = $this->FvListaZaliczek($request->input('typ'), $this->resultGenerateNrFakturyNowy['FZID']);

        $forma_platnosci = $this->FvForma($request->input('forma'));
        $termin_platnosci = $this->FvTermin($request->input('termin'));
        $typy = $this->FvTyp($request->input('typ'));

        $data = $request->all();

        $slowa_cyfry = $this->FvKwotaSlownie($request->input('kwota_brutto'));

        if ((Session::get('FakturowanieZaliczkowe') == '1') || (Session::get('FakturowanieRozliczeniowe') == '1')) {
            return redirect()->route('editCRMFaktury', ['id' => $id]);
        } else {
            return redirect()->route('editCRMFaktury', ['id' => $id]);
            //return PDF::loadView('backend.CRMFaktury.szablon_faktury', compact('data', 'forma_platnosci', 'termin_platnosci', 'typy', 'listaZaliczek', 'slowa_cyfry'))->save(public_path().'/admin/faktury/'.$id.'_'.$this->resultGenerateNrFakturyNowy['nr_faktury'].'.pdf')->stream('Faktura_'.$id.'_'.$this->resultGenerateNrFakturyNowy['nr_faktury'].'.pdf');
        }
    }

    public function FvListaZaliczek($typ, $FZID)
    {
        if (($typ == '3') || ($typ == '4')) {
            $listaZaliczek = DB::table('crm_faktury2016')->where('FZID', $FZID)->get();
        } else {
            $listaZaliczek = '';
        }

        return $listaZaliczek;
    }

    public static function getNextNrFakturyStatic($typ)
    {
        //$getLastNr = DB::table('crm_faktury2016')->where('typ', $typ)->where('rok', date('Y'))->get(['nr_faktury'])->last();
        //$getLastNr = DB::table('crm_faktury2016')->where('typ', $typ)->whereRaw('data_wystawienia LIKE  "%'.date('Y').'-%"')->get(['nr_faktury'])->max('nr_faktury');

        /**
         * Z kazdym wywolaniem metody pobierz maksymalny ID faktury i zwieksz go o 1, zasadniczo mozna pominac tez typ.
         */

        //$getLastNr = DB::table('crm_faktury2016')->where('typ', $typ)->get(['fakturaID'])->max('fakturaID');

        // $getLastNr = DB::table('crm_faktury2016')->get(['fakturaID'])->max('fakturaID');
        $getLastNr = DB::select("SHOW TABLE STATUS LIKE 'crm_faktury2016'");

        // return dd($getLastNr[0]->Auto_increment);

        return $getLastNr[0]->Auto_increment;

        // return $getLastNr[0]->Auto_increment;
    }

    public function getLastNrFaktury($typ)
    {
        //date('Y', strtotime('2068-06-15'))

        $getLastNr = DB::table('crm_faktury2016')->where('typ', $typ)->whereRaw('data_wystawienia LIKE  "%'.date('Y').'-%"')->get(['nr_faktury'])->max('nr_faktury');

        if (count($getLastNr) == 0) {
            $getNrFV = response()->json(['nr_faktury' => 0]);
        } else {
            $getNrFV = response()->json(['nr_faktury' => $getLastNr]);
        }

        return $getNrFV;
    }

    public function formaWysylki()
    {
        $this->coreFaktury();

        $formaWysylki = $this->forma_wysylki;

        return $formaWysylki;
    }

    public function FvForma($forma)
    {
        $this->coreFaktury();

        $forma_platnosci = $this->forma;
        $resultForma = $forma_platnosci[$forma];

        return $resultForma;
    }

    public function FvTyp($typ)
    {
        $this->coreFaktury();
        $typy = $this->typy;

        $resultTyp = $typy[$typ];

        return $resultTyp;
    }

    public function FvTermin($termin)
    {
        $this->coreFaktury();

        $termin_platnosci = $this->termin;
        $resultTermin = $termin_platnosci[$termin];

        return $resultTermin;
    }

    public function FvKwotaSlownie($kwota)
    {
        $numberToWords = new NumberToWords();
        $numberTransformer = $numberToWords->getNumberTransformer('pl');
        $slowa_cyfry = $numberTransformer->toWords($kwota);

        return $slowa_cyfry;
    }

    public function tworzFaktureZaliczkowa()
    {
    }

    public function tworzFaktureKoncowa()
    {
    }

    public function update(CreateCRMFakturyRequest $request, $id)
    {
        if ($request->input('submitBtn') == 'faktura-zaliczkowa') {
            Session::put('FakturowanieZaliczkowe', '1');

            return  $this->store($request);
        } elseif ($request->input('submitBtn') == 'faktura-koncowa') {
            Session::put('FakturowanieRozliczeniowe', '1');

            return  $this->store($request);
        } else {
            $faktury = DB::table('crm_faktury2016')->where('fakturaID', $id);
            $faktury->update([
           // 'termID' => null,
            'nr_faktury' => $request->input('nr_faktury'),
            'opis' => $request->input('opis'),
            'cena_jednostkowa' => $request->input('cena_jednostkowa'),
            'kwota' => $request->input('kwota'),
            'VAT' => $request->input('VAT'),
            'kwota_VAT' => $request->input('kwota_VAT'),
            'kwota_brutto' => $request->input('kwota_brutto'),
            'ilosc' => $request->input('ilosc'),
            'jm' => $request->input('jm'),
            'data_wystawienia' => $request->input('data_wystawienia'),
            'data_sprzedazy' => $request->input('data_sprzedazy'),
            'termin' => $request->input('termin'),
            'typ' => $request->input('typ'),
            'forma' => $request->input('forma'),
            'rabat_f' => $request->input('rabat'),
            'pozostalo' => $request->input('pozostalo'),
            'zaplacono' => $request->input('zaplacono'),
            'cena_jednostkowa_org' => $request->input('cena_jednostkowa_org'),
        ]);
        }

        $forma_platnosci = $this->FvForma($request->input('forma'));
        $termin_platnosci = $this->FvTermin($request->input('termin'));
        $typy = $this->FvTyp($request->input('typ'));

        $data = $request->all();

        $slowa_cyfry = $this->FvKwotaSlownie($request->input('kwota_brutto'));

        $listaZaliczek = $this->FvListaZaliczek($request->input('typ'), $request->input('FZID'));  //??

        //return PDF::loadView('backend.CRMFaktury.szablon_faktury', compact('data', 'forma_platnosci', 'termin_platnosci', 'typy', 'slowa_cyfry', 'kwota_slownie', 'listaZaliczek'))->save(public_path().'/admin/faktury/'.$id.'_'.$request->input('nr_faktury').'.pdf')->stream('Faktura_'.$id.'_'.$request->input('nr_faktury').'.pdf'); //->redirect()->back();

        if ($request->input('submitBtn') == 'save-faktura') {
            $destinationPath = public_path('../admin/faktury/'.''.$id.'_'.$request->input('nr_faktury').'.pdf'); // public_path
            PDF::loadView('backend.CRMFaktury.szablon_faktury', compact('data', 'forma_platnosci', 'termin_platnosci', 'typy', 'slowa_cyfry', 'kwota_slownie', 'listaZaliczek'))->save($destinationPath);

            return redirect()->back();
        }
        if ($request->input('submitBtn') == 'update') {
            //   $destinationPath = public_path('../admin/faktury/'.''.$id.'_'.$request->input('nr_faktury').'.pdf'); // public_path

            // return PDF::loadView('backend.CRMFaktury.szablon_faktury', compact('data', 'forma_platnosci', 'termin_platnosci', 'typy', 'slowa_cyfry', 'kwota_slownie', 'listaZaliczek'))->save($destinationPath)->download('Faktura_'.$id.'_'.$request->input('nr_faktury').'.pdf');
            return PDF::loadView('backend.CRMFaktury.szablon_faktury', compact('data', 'forma_platnosci', 'termin_platnosci', 'typy', 'slowa_cyfry', 'kwota_slownie', 'listaZaliczek'))->download('Faktura_'.$id.'_'.$request->input('nr_faktury').'.pdf');
        }
    }

    public static function NabywcaAPI($klientID)
    {
        $getKlient = DB::table('crm_klienci')->where('klientID', $klientID)->get();

        return $getKlient;
    }

    public function updateWysylka($id)
    {
        $faktura_wysylka = DB::table('crm_wysylka')->where('fakturaID', $id);

        $faktura_wysylka->update([
            'oplacona' => request('oplacona'),
        ]);

        return Config::get('app.save_status');
    }

    public function updateAjaxWysylka($id)
    {
        $updateAjax = DB::table('crm_wysylka')->where('fakturaID', $id);

        if (request('forma') != null) {
            $updateAjax->update([
          'forma' => request('forma'),
          ]);
        } elseif (request('oplacona') != null) {
            $updateAjax->update([
                'oplacona' => request('oplacona'),
            ]);
        } else {
            $updateAjax->update([
          'data_wysylki' => request('data_wysylki'),
          ]);
        }

        return Config::get('app.save_status');
    }

    public function getFakturyFromRange($miesiac, $rok)
    {
        $this->coreFaktury();

        $currentMonth = $miesiac;
        $currentYear = $rok;

          if (Auth::user()->id_role == '3') {
                 $faktury = DB::table('crm_faktury2016')->leftJoin('crm_wysylka', 'crm_faktury2016.fakturaID', '=', 'crm_wysylka.fakturaID')->leftJoin('crm_klienci', 'crm_faktury2016.klientID', '=', 'crm_klienci.klientID')->whereRaw('MONTH(data_wystawienia) = ?', [$currentMonth])->whereRaw('YEAR(data_wystawienia) = ?', [$currentYear])->orderBy('crm_faktury2016.nr_faktury', 'desc')
        ->select('crm_faktury2016.nr_faktury', 'crm_faktury2016.fakturaID', 'crm_faktury2016.data_wystawienia', 'crm_klienci.nazwa', 'crm_faktury2016.data_sprzedazy', 'crm_faktury2016.typ', 'crm_faktury2016.kwota_brutto', 'crm_wysylka.oplacona', 'crm_wysylka.data_wysylki', 'crm_wysylka.forma', 'crm_faktury2016.klientID', 'crm_faktury2016.rok')
        ->where('crm_klienci.opiekunID',Auth::user()->id)
        ->get();
          } else {
              $faktury = DB::table('crm_faktury2016')->leftJoin('crm_wysylka', 'crm_faktury2016.fakturaID', '=', 'crm_wysylka.fakturaID')->leftJoin('crm_klienci', 'crm_faktury2016.klientID', '=', 'crm_klienci.klientID')->whereRaw('MONTH(data_wystawienia) = ?', [$currentMonth])->whereRaw('YEAR(data_wystawienia) = ?', [$currentYear])->orderBy('crm_faktury2016.nr_faktury', 'desc')
        ->select('crm_faktury2016.nr_faktury', 'crm_faktury2016.fakturaID', 'crm_faktury2016.data_wystawienia', 'crm_klienci.nazwa', 'crm_faktury2016.data_sprzedazy', 'crm_faktury2016.typ', 'crm_faktury2016.kwota_brutto', 'crm_wysylka.oplacona', 'crm_wysylka.data_wysylki', 'crm_wysylka.forma', 'crm_faktury2016.klientID', 'crm_faktury2016.rok')->get();
          }
        return view('backend.CRMFaktury.index', compact('faktury'))->with('typy', $this->typy)->with('forma_wysylki', $this->forma_wysylki)->with('miesiac', $miesiac)->with('rok', $rok);
    }

    public function getJPKFromRange($miesiac, $rok)
    {
        $jpk = DB::table('crm_faktury2016')->leftJoin('crm_klienci', 'crm_faktury2016.klientID', '=', 'crm_klienci.klientID')->where([['crm_faktury2016.data_wystawienia', '>=', date('Y-m-d', strtotime($rok.'-'.$miesiac.'-01'))], ['crm_faktury2016.data_wystawienia', '<=', date('Y-m-t', strtotime($rok.'-'.$miesiac))]])->where('crm_faktury2016.typ', '!=', '2')->get();

        $ilosc_sprzedazy = count($jpk);

        $podatekNalezny = $jpk->sum('kwota_VAT');

        return response()->view('backend.CRMFaktury.jpk', compact('miesiac', 'rok', 'jpk', 'ilosc_sprzedazy', 'podatekNalezny'))->header('Content-Type', 'text/xml')->header('Content-Disposition', 'attachment; filename=JPK_FA_'.$miesiac.'_'.$rok.'.xml');
    }

    public function filtrFaktury(CreateCRMFakturyFiltrRequest $request, $miesiac, $rok)
    {
        $this->coreFaktury();

        $currentMonth = $miesiac;
        $currentYear = $rok;

        if(Auth::user()->id_role == '3') {

if ($request->input('typ_faktury') != '0') { // czyli jesli nie sa to wszystkie faktury
                $faktury = DB::table('crm_faktury2016')->leftJoin('crm_wysylka', 'crm_faktury2016.fakturaID', '=', 'crm_wysylka.fakturaID')->leftJoin('crm_klienci', 'crm_faktury2016.klientID', '=', 'crm_klienci.klientID')->where('typ', $request->input('typ_faktury'))->whereRaw('MONTH(data_wystawienia) = ?', [$currentMonth])->whereRaw('YEAR(data_wystawienia) = ?', [$currentYear])
                ->where('crm_klienci.opiekunID',Auth::user()->id)
                ->orderBy('crm_faktury2016.nr_faktury', 'desc')
                ->get();
            } else {
                $faktury = DB::table('crm_faktury2016')->leftJoin('crm_wysylka', 'crm_faktury2016.fakturaID', '=', 'crm_wysylka.fakturaID')->leftJoin('crm_klienci', 'crm_faktury2016.klientID', '=', 'crm_klienci.klientID')->whereRaw('MONTH(data_wystawienia) = ?', [$currentMonth])->whereRaw('YEAR(data_wystawienia) = ?', [$currentYear])
                ->where('crm_klienci.opiekunID',Auth::user()->id)->orderBy('crm_faktury2016.nr_faktury', 'desc')
                ->get();
            }
        } else {
            if ($request->input('typ_faktury') != '0') { // czyli jesli nie sa to wszystkie faktury
                $faktury = DB::table('crm_faktury2016')->leftJoin('crm_wysylka', 'crm_faktury2016.fakturaID', '=', 'crm_wysylka.fakturaID')->leftJoin('crm_klienci', 'crm_faktury2016.klientID', '=', 'crm_klienci.klientID')->where('typ', $request->input('typ_faktury'))->whereRaw('MONTH(data_wystawienia) = ?', [$currentMonth])->whereRaw('YEAR(data_wystawienia) = ?', [$currentYear])->orderBy('crm_faktury2016.nr_faktury', 'desc')->get();
            } else {
                $faktury = DB::table('crm_faktury2016')->leftJoin('crm_wysylka', 'crm_faktury2016.fakturaID', '=', 'crm_wysylka.fakturaID')->leftJoin('crm_klienci', 'crm_faktury2016.klientID', '=', 'crm_klienci.klientID')->whereRaw('MONTH(data_wystawienia) = ?', [$currentMonth])->whereRaw('YEAR(data_wystawienia) = ?', [$currentYear])->orderBy('crm_faktury2016.nr_faktury', 'desc')->get();
            }
        }

        return view('backend.CRMFaktury.index', compact('faktury'))->with('typy', $this->typy)->with('forma_wysylki', $this->forma_wysylki)->with('miesiac', $miesiac)->with('rok', $rok);
    }

    public function gus_api($nip)
    {
        $gus = new GusApi('b1ef9824d9a04e0f8d8e','prod'); // 
 
        $getNIPdetails = [];

        try {
            $nipToCheck = $nip; //change to valid nip value  8992737184   || 5261839365
            $gus->login();

            #echo $gus->getSessionId();
            $gusReports = $gus->getByNip($nipToCheck);

            foreach ($gusReports as $gusReport) {
                
                // Jesli nip jest pod spolki lub osoby fizyczne
                if($gusReport->getType() == 'p') {
                    $reportType = ReportTypes::REPORT_PUBLIC_LAW;            
                } else { 
                    $reportType = ReportTypes::REPORT_ACTIVITY_PHYSIC_CEIDG;
                }
                $fullReport = $gus->getFullReport($gusReport, $reportType);
 
                $adres = '';

                if($gusReport->getType() == 'f') {
                $adres = $fullReport->fiz_adSiedzUlica_Nazwa.' '.$fullReport->fiz_adSiedzNumerNieruchomosci;
                } else {
                $adres = $fullReport->praw_adSiedzUlica_Nazwa.' '.$fullReport->praw_adSiedzNumerNieruchomosci;
                }

                $getNIPdetails["nip"]=$nip;
                $getNIPdetails["nazwa"]=$gusReport->getName();
                $getNIPdetails["ulica"]=$adres;
                $getNIPdetails["kod"]=$gusReport->getZipCode();
                $getNIPdetails["miasto"]=$gusReport->getCity();

                //print_r($fullReport);
                $getFirma = DB::table('crm_klienci')->where('nip',$nip)->get();
                // $getFirma->update([
                //     'nazwa' => '',
                //     'kod' => '',
                //     'miasto' => '',
                //     'adres' => '',
                // ]);

                //print_r($getFirma);
                //echo "ETEST:";
                //echo $getFirma[0]->nazwa;
                // update database 


                // send email about new company data 
                return $getNIPdetails;
                //print_r($getNIPdetails);
            }
        } catch (InvalidUserKeyException $e) {
            echo 'Bad user key';
        } catch (\GusApi\Exception\NotFoundException $e) {
            #echo 'No data found <br>';
            #echo 'For more information read server message below: <br>';
            #echo $gus->getResultSearchMessage();
        }
    }

    public static function clarionDate($dateTimeString = 'now')
    {
        $d1 = new DateTime('1800-12-28'); //strtotime(date('Y-m-d'), '1800-12-28')
        $d2 = new DateTime($dateTimeString);
        $interval = $d1->diff($d2);

        return $interval->format('%a');
    }

    public function in_multiarray($elem, $array)
    {
        $top = sizeof($array) - 1;
        $bottom = 0;
        while ($bottom <= $top) {
            if ($array[$bottom] == $elem) {
                return true;
            } elseif (is_array($array[$bottom])) {
                if ($this->in_multiarray($elem, ($array[$bottom]))) {
                    return true;
                }
            }

            ++$bottom;
        }

        return false;
    }

    public function createZestawienieFaktur($miesiac, $rok)
    {
        $this->coreFaktury();

        $currentMonth = $miesiac;
        $currentYear = $rok;

        $getFakturyZestawienie = DB::table('crm_faktury2016')->leftJoin('crm_klienci', 'crm_faktury2016.klientID', '=', 'crm_klienci.klientID')->whereRaw('MONTH(data_wystawienia) = ?', [$currentMonth])->whereRaw('YEAR(data_wystawienia) = ?', [$currentYear])->orderBy('crm_faktury2016.data_wystawienia', 'asc')->where('crm_faktury2016.typ', '!=', '2')->get();

        $di = 0;
        $kontrahenci = [];

        foreach ($getFakturyZestawienie as $zestawienie) {
            if (!$this->in_multiarray($zestawienie->nip, $kontrahenci)) {
                $kontrahenci[$di][0] = $zestawienie->klientID;
                $kontrahenci[$di][1] = $zestawienie->nazwa;
                $kontrahenci[$di][2] = $zestawienie->nip;
                $kontrahenci[$di][3] = $zestawienie->kod;
                $kontrahenci[$di][4] = $zestawienie->miasto;
                $kontrahenci[$di][5] = $zestawienie->adres;
                $kontrahenci[$di][6] = $zestawienie->nazwa;
                $kontrahenci[$di][7] = $zestawienie->kod.' '.$zestawienie->miasto.', '.$zestawienie->adres;
                ++$di;
            }
        }
        //$getFakturyKontrahenci = DB::table
        return response()->view('backend.CRMFaktury.zestawienie', compact('getFakturyZestawienie', 'di', 'kontrahenci'))->header('Content-Type', 'text/xml')->header('Content-Disposition', 'attachment; filename=ZESTAWIENIE_FA_'.$miesiac.'_'.$rok.'.xml');
    }

    public static function protectFV($faktura)
    {
        try {
            //return response()->download(Public_path('admin/faktury/'.$faktura), null, [], null);
            return 'OK';
        } catch (\Exception $e) {
            return 'NONE';
        }
    }

    public function destroy($id)
    {
        $deleteFaktury = DB::table('crm_faktury2016')->where('fakturaID', $id);
        $deleteFaktury->delete();

        return redirect()->route('indexCRMFaktury');
    }
}
