<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\CreateCRMZdarzeniaRequest;
use Session;
use Config;
// use Request;
use Auth;
use Mail;


class CRMZdarzeniaController extends Controller
{
    private $typy_zdarzenia;
    private $hr;
    private $stanyZdarzenia;
    private $priorytetZdarzenia;

    public function hrZdarzenia()
    {
        $this->hr = ['0' => 'Zwykłe', '1' => 'HR'];

        return $this->hr;
    }
        public function priorytetZdarzenia()
    {
        $this->priorytetZdarzenia = ['0' => 'Zwykły', '1' => 'Pilne'];

        return $this->priorytetZdarzenia;
    }

    public function typyZdarzenia()
    {
        $this->typy_zdarzenia = [1 => 'Ankieta', 2 => 'Inne', 3 => 'Mail', 4 => 'Telefon', 5 => 'Wysłane zaproszenie', 6 => 'Spotkanie',7=>'Oferta'];

        return $this->typy_zdarzenia;
    }
    public function stanyZdarzenia() {
        $this->stanyZdarzenia = [ 
            1 => 'Złożona',
            2 => 'Wygrana',
            3 => 'Odrzucona',
            4 => 'Zrealizowana'
        ];
        return $this->stanyZdarzenia;
    }

    public function coreZdarzenia()
    {
        $this->hrZdarzenia();
        $this->typyZdarzenia();
        $this->stanyZdarzenia();
        $this->priorytetZdarzenia();
    }

    public function index()
    {
        $this->coreZdarzenia();
        $CRMFirmyController = new CRMFirmyController();
        $opiekun = $CRMFirmyController->coreFirma();

        if(Auth::user()->id_role == '3') {
             $zdarzenia = DB::table('crm_zdarzenia')->join('crm_osoby', 'crm_zdarzenia.uczestnikID', '=', 'crm_osoby.uczestnikID')->join('crm_klienci', 'crm_klienci.klientID', '=', 'crm_osoby.klientID')->where('crm_klienci.opiekunID', Auth::user()->id)
            ->select(
'crm_zdarzenia.zdarzenieID',
'crm_zdarzenia.typ',
'crm_zdarzenia.data_dodania',
'crm_zdarzenia.komentarz',
'crm_zdarzenia.uczestnikID',
'crm_zdarzenia.hr',
'crm_zdarzenia.active',
'crm_zdarzenia.parentID',
'crm_osoby.klientID',
'crm_klienci.klientID',
'crm_osoby.imie_nazwisko',
'crm_klienci.nazwa')
->orderBy('zdarzenieID', 'desc')
            ->get();
        } else {
            $zdarzenia = DB::table('crm_zdarzenia')->orderBy('zdarzenieID', 'desc')->limit(300)->get();
        }

        return view('backend.CRMZdarzenia.index', compact('zdarzenia'))->with('typy_zdarzenia', $this->typy_zdarzenia)->with('opiekun', $opiekun)->with('hr', $this->hr)->with('stany_zdarzenia',$this->stanyZdarzenia);
    }

    public function filtrZdarzenia(Request $request)
    {
        return redirect()->route('getResultsZdarzenia', ['typID' => \Request::get('typ_zdarzenia'), 'opiekunID' => \Request::get('opiekun')]);
    }

    public function getResultsZdarzenia($typID, $opiekunID)
    {
        $this->coreZdarzenia();
        $CRMFirmyController = new CRMFirmyController();
        $opiekun = $CRMFirmyController->coreFirma();

        if ($opiekunID != '0') {
            $zdarzenia = DB::table('crm_zdarzenia')->join('crm_osoby', 'crm_zdarzenia.uczestnikID', '=', 'crm_osoby.uczestnikID')->join('crm_klienci', 'crm_klienci.klientID', '=', 'crm_osoby.klientID')->where([['crm_zdarzenia.typ', $typID]])->where('crm_klienci.opiekunID', $opiekunID)
            ->select(
'crm_zdarzenia.zdarzenieID',
'crm_zdarzenia.typ',
'crm_zdarzenia.data_dodania',
'crm_zdarzenia.komentarz',
'crm_zdarzenia.uczestnikID',
'crm_zdarzenia.hr',
'crm_zdarzenia.active',
'crm_zdarzenia.parentID',
'crm_osoby.klientID',
'crm_klienci.klientID',
'crm_osoby.imie_nazwisko',
'crm_klienci.nazwa')
            ->get();
        } else {
            $zdarzenia = DB::table('crm_zdarzenia')->where([['typ', $typID]])->get();
        }

        return view('backend.CRMZdarzenia.index', compact('zdarzenia'))->with('typy_zdarzenia', $this->typy_zdarzenia)->with('stany_zdarzenia',$this->stanyZdarzenia())->with('opiekun', $opiekun)->with('hr', $this->hr)->with('opiekunID', $opiekunID)->with('typID', $typID);
    }

    public static function create($uczestnikID, $method)
    {
        $CRMZdazeniaTypy = new CRMZdarzeniaController();
        $typy = $CRMZdazeniaTypy->typyZdarzenia();
        $stany = $CRMZdazeniaTypy->stanyZdarzenia();
        $CRMZdazeniaTypy->priorytetZdarzenia();

        if ($method == 'modal') {
            return view('backend.CRMZdarzenia.modal')->with('typy_zdarzenia', $typy)->with('uczestnikID', $uczestnikID)->with('priorytet',$priorytetZdarzenia);
        } else {
            return view('backend.CRMZdarzenia.create')->with('typy_zdarzenia', $typy)->with('uczestnikID', $uczestnikID)->with('stany_zdarzenia', $stany)->with('priorytet',$CRMZdazeniaTypy->priorytetZdarzenia());
        }
    }

    public function edit($id)
    {
        return view('backend.CRMZdarzenia.edit');
    }

    public function store(CreateCRMZdarzeniaRequest $request)
    {
        $CRMUczestnikHR = new CRMOsobyController();
        $hr = $CRMUczestnikHR->getUczestnik($request->input('uczestnikID'));

        $zdarzenie = DB::table('crm_zdarzenia_nowe')->insert([
         'typ' => $request->input('typ'),
         'data_dodania' => $request->input('data_dodania'),
         'komentarz' => $request->input('komentarz'),
         'uczestnikID' => $request->input('uczestnikID'),
         //'active' => '1',
         //'parentID' => null,
          //'hr' => (string) (json_decode($hr)[0]->hr),
        ]);

        $osoba = DB::table('crm_osoby')->where('uczestnikID', $request->input('uczestnikID'));

        $osoba->update([
            'ostatni_kontakt' => date('Y-m-d'),
            'nastepny_kontakt' => $request->input('data_dodania'),
            'nowy' => '0',
        ]);

        Session::flash('panelInfo', 'Dodano pomyslnie.');

        return back();
    }
    public function updateAjaxZdarzenieKomentarz($id) {
        $updateAjax = DB::table('crm_zdarzenia_nowe')->where('crm_zdarzenia_nowe.zdarzenieID', $id);

        $updateAjax->update([
          'dzialanie_komentarz' => request('komentarz'),
          ]);

        return Config::get('app.save_status');
    }
     public function updateAjaxZdarzenieStatus($id)
    {
        $updateAjax = DB::table('crm_zdarzenia_nowe')->where('crm_zdarzenia_nowe.zdarzenieID', $id);

        $updateAjax->update([
          'stan' => request('stan'),
          ]);

        return Config::get('app.save_status');
        //return request('hr');
    }

    public function updateAjaxZdarzenieOpiekun($id)
    {
        $updateAjax = DB::table('crm_zdarzenia_nowe')->where('crm_zdarzenia_nowe.zdarzenieID', $id);

        $updateAjax->update([
          'opiekunID' => request('opiekun'),
          ]);

        return Config::get('app.save_status');
        //return request('hr');
    }
    public function updateAjaxZdarzenieData($id) {
        $updateAjax = DB::table('crm_zdarzenia_nowe')->where('crm_zdarzenia_nowe.zdarzenieID', $id);

        $updateAjax->update([
          'dzialanie_data' => request('dzialanie_data'),
          ]);

        return Config::get('app.save_status');
    }
    public function updateAjaxZdarzenie($id)
    {
        $updateAjax = DB::table('crm_zdarzenia')->where('crm_zdarzenia.zdarzenieID', $id);

        $updateAjax->update([
          'hr' => request('hr'),
          ]);

        return Config::get('app.save_status');
        //return request('hr');
    }

    public function update()
    {
        return 'update';
    }

    public function getZdarzeniaFromUczestnik($uczestnikID)
    {
        $CRMZdazeniaTypy = new CRMZdarzeniaController();
        $typy = $CRMZdazeniaTypy->typyZdarzenia();

        $szkolenia = DB::table('crm_szkolenia')->leftJoin('h5_terms', 'crm_szkolenia.termID', '=', 'h5_terms.termID')->leftJoin('h5_training', 'h5_training.trainingID', '=', 'h5_terms.trainingID')->where('crm_szkolenia.uczestnikID', $uczestnikID)->get();
        $zdarzenia = DB::table('crm_zdarzenia')->where('crm_zdarzenia.uczestnikID', $uczestnikID)->get();

        return view('backend.CRMZdarzenia.aktywnosci', compact('szkolenia', 'zdarzenia'))->with('typy', $typy);
    }
    public function indexNew() {

         $this->coreZdarzenia();
        $CRMFirmyController = new CRMFirmyController();
        $opiekun = $CRMFirmyController->coreFirma();

        if(Auth::user()->id_role == '3') {
             $zdarzenia = DB::table('crm_zdarzenia_nowe')->leftJoin('crm_osoby', 'crm_zdarzenia_nowe.uczestnikID', '=', 'crm_osoby.uczestnikID')->join('crm_klienci', 'crm_klienci.klientID', '=', 'crm_zdarzenia_nowe.klientID')
            ->select(
'crm_zdarzenia_nowe.zdarzenieID',
'crm_zdarzenia_nowe.typ',
'crm_zdarzenia_nowe.data_dodania',
'crm_zdarzenia_nowe.komentarz',
'crm_zdarzenia_nowe.opiekunID',
'crm_zdarzenia_nowe.klientID',
'crm_zdarzenia_nowe.uczestnikID',
'crm_zdarzenia_nowe.kwota',
'crm_zdarzenia_nowe.stan',
'crm_zdarzenia_nowe.dzialanie_komentarz',
'crm_zdarzenia_nowe.dzialanie_data',
'crm_zdarzenia_nowe.temat',
'crm_zdarzenia_nowe.priorytet',
'crm_osoby.klientID',
'crm_klienci.klientID',
'crm_osoby.imie_nazwisko',
'crm_klienci.nazwa')
->where('crm_zdarzenia_nowe.opiekunID', Auth::user()->id)
->orderBy('zdarzenieID', 'desc')
            ->get();
            // dd($zdarzenia);
        } else {
            $zdarzenia = DB::table('crm_zdarzenia_nowe')->orderBy('zdarzenieID', 'desc')->limit(300)->get();
        }

        return view('backend.CRMZdarzeniaNowe.index', compact('zdarzenia'))->with('typy_zdarzenia', $this->typy_zdarzenia)->with('opiekun', $opiekun)->with('hr', $this->hr)->with('stany_zdarzenia',$this->stanyZdarzenia)->with('priorytet',$this->priorytetZdarzenia());
    }
    function filtrZdarzeniaNew() {
        return redirect()->route('getResultsZdarzeniaNew', ['typID' => \Request::get('typ_zdarzenia'), 'opiekunID' => \Request::get('opiekun')]);
    }

    public function getResultsZdarzeniaNew($typID,$opiekunID)
    {
      
        $this->coreZdarzenia();
        $CRMFirmyController = new CRMFirmyController();
        $opiekun = $CRMFirmyController->coreFirma();

        if ($opiekunID != '0') {
            $zdarzenia = DB::table('crm_zdarzenia_nowe')->join('crm_osoby', 'crm_zdarzenia_nowe.uczestnikID', '=', 'crm_osoby.uczestnikID')->join('crm_klienci', 'crm_klienci.klientID', '=', 'crm_osoby.klientID')->where([['crm_zdarzenia_nowe.typ', $typID]])->where('crm_zdarzenia_nowe.opiekunID', $opiekunID)
            ->select(
'crm_zdarzenia_nowe.zdarzenieID',
'crm_zdarzenia_nowe.typ',
'crm_zdarzenia_nowe.data_dodania',
'crm_zdarzenia_nowe.komentarz',
'crm_zdarzenia_nowe.opiekunID',
'crm_zdarzenia_nowe.klientID',
'crm_zdarzenia_nowe.uczestnikID',
'crm_zdarzenia_nowe.kwota',
'crm_zdarzenia_nowe.stan',
'crm_zdarzenia_nowe.dzialanie_komentarz',
'crm_zdarzenia_nowe.dzialanie_data',
'crm_zdarzenia_nowe.temat',
'crm_zdarzenia_nowe.priorytet',
'crm_klienci.klientID',
'crm_osoby.imie_nazwisko',
'crm_klienci.nazwa')
            ->get();
        } else {
            $zdarzenia = DB::table('crm_zdarzenia_nowe')->where([['typ', $typID]])->get();
        }

        return view('backend.CRMZdarzeniaNowe.index', compact('zdarzenia'))->with('typy_zdarzenia', $this->typy_zdarzenia)->with('stany_zdarzenia',$this->stanyZdarzenia)->with('opiekun', $opiekun)->with('hr', $this->hr)->with('opiekunID', $opiekunID)->with('typID', $typID)->with('priorytet',$this->priorytetZdarzenia());
    }
    public function editNew($id) {
         $this->coreZdarzenia();
        $CRMFirmyController = new CRMFirmyController();
        $opiekun = $CRMFirmyController->coreFirma();
         $klienci = DB::table('crm_klienci')->pluck('nazwa','klientID');
        $zdarzenia = DB::table('crm_zdarzenia_nowe')->where('zdarzenieID',$id)->first();
        return view('backend.CRMZdarzeniaNowe.edit')->with('zdarzenie',$zdarzenia)->with('typy_zdarzenia', $this->typy_zdarzenia)->with('opiekun', $opiekun)->with('hr', $this->hr)->with('stany_zdarzenia',$this->stanyZdarzenia)->with('priorytet',$this->priorytetZdarzenia())->with('klienci',$klienci)->with('uczestnikID',$zdarzenia->uczestnikID);
    }
    public static function createNew($view = null) {
        $CRMZdazenia = new CRMZdarzeniaController();
        $typy = $CRMZdazenia->typyZdarzenia();
        $stany = $CRMZdazenia->stanyZdarzenia();
        $CRMZdazenia->priorytetZdarzenia();
        //$this->coreZdarzenia();
        $CRMFirmyController = new CRMFirmyController();
        $opiekun = $CRMFirmyController->coreFirma();
         $klienci = DB::table('crm_klienci')->pluck('nazwa','klientID');
        //    $zdarzenia = DB::table('crm_zdarzenia_nowe')->where('zdarzenieID',$id)->first();
         
        if($view != null) {
            return view('backend.CRMZdarzeniaNowe.'.$view)->with('typy_zdarzenia', $CRMZdazenia->typy_zdarzenia)->with('opiekun', $opiekun)->with('hr', $CRMZdazenia->hr)->with('stany_zdarzenia',$CRMZdazenia->stanyZdarzenia)->with('priorytet',$CRMZdazenia->priorytetZdarzenia())->with('klienci',$klienci);
        } else {
            return view('backend.CRMZdarzeniaNowe.create')->with('typy_zdarzenia', $CRMZdazenia->typy_zdarzenia)->with('opiekun', $opiekun)->with('hr', $CRMZdazenia->hr)->with('stany_zdarzenia',$CRMZdazenia->stanyZdarzenia)->with('priorytet',$CRMZdazenia->priorytetZdarzenia())->with('klienci',$klienci);
        }
        
    }
    public function storeNew(Request $request) {

         $CRMUczestnikHR = new CRMOsobyController();

        if(!empty($request->input('uczestnikID'))) {
            $getKlient = DB::table('crm_osoby')->where('uczestnikID',$request->input('uczestnikID'))->first();
            $klientID = $getKlient->klientID;
         }
          else {
              $klientID = $request->input('klientID');
          }

        //   if(empty($request->input('klientID'))) {
        //       $getKlient = DB::table('crm_osoby')->where('uczestnikID',$request->input('uczestnikID'))->first();
        //     $klientID = $getKlient->klientID;
        //   } else {
        //       $klientID = $request->input('klientID');
        //   }

        $zdarzenie = DB::table('crm_zdarzenia_nowe')->insert([
         'typ' => $request->input('typ'),
         'data_dodania' => $request->input('data_dodania'),
         'komentarz' => $request->input('komentarz'),
         'temat' => $request->input('temat'),
         'kwota' => $request->input('kwota'),
         'stan' => $request->input('stan'),
         'klientID' => $klientID,
         'dzialanie_data' => $request->input('data_przypomnienia'),
         'dzialanie_komentarz' => $request->input('dzialanie_komentarz'),
         'uczestnikID' => $request->input('uczestnikID'),
         'opiekunID' => Auth::user()->id,
         'priorytet' => $request->input('priorytet'),
         'przypomnij' => $request->input('przypomnij')
        ]);

        $osoba = DB::table('crm_osoby')->where('uczestnikID', $request->input('uczestnikID'));

        $osoba->update([
            'ostatni_kontakt' => date('Y-m-d'),
            'nastepny_kontakt' => $request->input('dzialanie_data'),
            'nowy' => '0',
        ]);

        Session::flash('panelInfo', 'Dodano pomyslnie.');

        return back();
    }
    public function updateNew(Request $request, $id) {

        // dd($request->all());
         $zdarzenie = DB::table('crm_zdarzenia_nowe')->where('zdarzenieID',$id)->update([
         'typ' => $request->input('typ'),
         'dzialanie_data' => $request->input('data_przypomnienia'),
         'temat' => $request->input('temat'),
         'dzialanie_komentarz' => $request->input('dzialanie_komentarz'),
         'priorytet' => $request->input('priorytet'),
         'kwota' => $request->input('kwota'),
          'klientID' => $request->input('klientID'),
          'stan' => $request->input('stan'),
          'przypomnij' => $request->input('przypomnij')
        ]);
        return back();
    }
    public function cronZdarzeniaReminder() {
        $getZdarzenia = DB::table('crm_zdarzenia_nowe')->where('crm_zdarzenia_nowe.dzialanie_data',date('Y-m-d'))->where('crm_zdarzenia_nowe.przypomnij', '1')->groupBy('crm_zdarzenia_nowe.opiekunID')->get();

//                     dd($getZdarzenia);
// exit();
        foreach($getZdarzenia as $zdarzenie) {
            $getOpiekun = DB::table('users')->where('id',$zdarzenie->opiekunID)->first();

            $contactName =  $getOpiekun->name;
            $contactEmail = $getOpiekun->email;
            $contactMessage = 'Przypomnienie o zdarzeniu: <br/><br/><ul> <li><strong>'.$zdarzenie->temat.'</strong><br/>'.$zdarzenie->komentarz.'<br/><i>'.$zdarzenie->dzialanie_data.'</i></li></ul>';
            $contactUrl = 'Backoffice High5';

            $data = array('contactName' => $contactName, 'contactEmail' => $contactEmail, 'contactMessage' => $contactMessage, 'contactUrl' => $contactUrl);
            Mail::send('frontend.emails.przypomnienia', $data, function ($message) use ($contactEmail, $contactName) {
                $message->from('biuro@high5.pl', 'Przypomnienie High5.com.pl | zdarzenia');

                // if (!empty(env('EMAILS_ARRAY'))) {
                //     $message->to(env('MAIL_USERNAME'), 'Biuro')->subject('Wiadomosc z High5.com.pl | Zapytanie')->cc($contactEmail)->cc(explode(',', env('EMAILS_ARRAY')));
                // } else {
                    $message->to(env('MAIL_USERNAME'), 'Biuro')->subject('Przypomnienie High5.com.pl | zdarzenia')->cc($contactEmail);
                //}
            });

             $updateZdarzenia = DB::table('crm_zdarzenia_nowe')->where('crm_zdarzenia_nowe.zdarzenieID',$zdarzenie->zdarzenieID)->where('crm_zdarzenia_nowe.przypomnij', '1')->update([
                'przypomnij' => '0'
             ]);
        }
        echo "Przypomnienia wysłano pomyslnie";
        exit();
//             dd($getZdarzenia);
// exit();
        

    }
}
