<?php

namespace App\Http\Controllers\backend;

use App\Http\Requests\backend\CreateLinkRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Session;

class LinkController extends Controller
{
    public function index()
    {
        $links = DB::table('h5_links')->join('h5_training', 'h5_links.primaryID', '=', 'h5_training.trainingID')->orderBy('h5_training.training_title')->get();

        return view('backend.link.index', compact('links'));
    }

    public function create()
    {
        $link_tab = DB::table('h5_training')->pluck('h5_training.training_title', 'h5_training.trainingID')->prepend('brak', '0');

        return view('backend.link.create', compact('link_tab'));
    }

    public function edit($id)
    {
        $links = DB::table('h5_links')->join('h5_training', 'h5_links.primaryID', '=', 'h5_training.trainingID')->where('linkID', $id)->orderBy('h5_training.training_title')->get();

        $link_tab = DB::table('h5_training')->pluck('h5_training.training_title', 'h5_training.trainingID')->prepend('brak', '0');

        return view('backend.link.edit', compact('links', 'link_tab'));
    }

    public function update(CreateLinkRequest $request, $id)
    {
        // $update_shop = Setting::findOrFail($id);

        $links = DB::table('h5_links')->where('linkID', $id);

        $links->update([
            'primaryID' => $request->input('primaryID'),
            'link1ID' => $request->input('link1ID'),
            'link2ID' => $request->input('link2ID'),
            'link3ID' => $request->input('link3ID'),
            'link4ID' => $request->input('link4ID'),
            'link5ID' => $request->input('link5ID'),
            //'position' => $request->input('position'),
        ]);

        Session::flash('panelInfo', 'Zapisano pomyslnie.');

        // $id = DB::getPdo()->lastInsertId();

        return redirect()->route('editLink', ['id' => $id]);
    }

    public function store(CreateLinkRequest $request)
    {
        $links = DB::table('h5_links')->insert([
             'primaryID' => $request->input('primaryID'),
            'link1ID' => $request->input('link1ID'),
            'link2ID' => $request->input('link2ID'),
            'link3ID' => $request->input('link3ID'),
            'link4ID' => $request->input('link4ID'),
            'link5ID' => $request->input('link5ID'),
    ]);

        Session::flash('panelInfo', 'Dodano pomyslnie.');

        $id = DB::getPdo()->lastInsertId();

        return redirect()->route('editLink', ['id' => $id]);
    }

    public function destroy($id)
    {
        $deleteLink = DB::table('h5_links')->where('linkID', $id);
        $deleteLink->delete();

        return redirect()->route('indexLink');
    }
}
