<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\CreateUzytkownicyRequest;
use Session;

class UzytkownicyController extends Controller
{
    public function index()
    {
        $uzytkownicy = DB::table('users')->get();
        //->join('h5_terms', 'crm_szkolenia.termID', '=', 'h5_terms.termID')

        return view('backend.uzytkownicy.index', compact('uzytkownicy'));
    }

    public function create()
    {
        $role = DB::table('role')->pluck('name','id_role');
        return view('backend.uzytkownicy.create')->with('role',$role);
    }

    public function edit($id)
    {
        $uzytkownicy = DB::table('users')->where('id', $id)->get();

        $role = DB::table('role')->pluck('name','id_role');

        return view('backend.uzytkownicy.edit', compact('uzytkownicy'))->with('role',$role);
    }

    public function update(CreateUzytkownicyRequest $request, $id)
    {
        $uzytkownicy = DB::table('users')->where('id', $id);

        if (!empty($request->input('password'))) {
            $uzytkownicy->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'id_role' => $request->input('id_role'),
        ]);
        } else {
            $uzytkownicy->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'id_role' => $request->input('id_role'),
        ]);
        }

        Session::flash('panelInfo', 'Zapisano pomyslnie.');

        return redirect()->route('indexUzytkownicy');
    }

    public function store(CreateUzytkownicyRequest $request)
    {
        $uzytkownicy = DB::table('users')->insert([
              'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'id_role' => $request->input('id_role'),
    ]);

        return redirect()->route('indexUzytkownicy');
    }

    public function destroy($id)
    {
        $deleteUzytkownicy = DB::table('users')->where('id', $id);
        $deleteUzytkownicy->delete();

        return redirect()->route('indexUzytkownicy');
    }

    public static function getUzytkownik($id)
    {
        $uzytkownik = DB::table('users')->where('id', $id)->get();

        return $uzytkownik;
    }
}
