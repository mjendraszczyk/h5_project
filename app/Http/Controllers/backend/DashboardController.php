<?php

namespace App\Http\Controllers\backend;

use App\Http\Requests\backend\CreateCRMFindFirmaRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Route;
use Request;
use Auth;

class DashboardController extends Controller
{
    public function __construct() {
    //     dd(Auth::check());
    //    else {

    //     }
    }

    public static function getQtyUczestnicy($idTerm)
    {
        $getQtyUczestnicy = DB::table('crm_szkolenia')->where('termID', $idTerm)->count();

        return $getQtyUczestnicy;
    }

    private $lastTerminy;

    public function getDashboardLastTerminy($placeId, $term_start, $term_end)
    {
        if ($placeId == '0') {
            $this->lastTerminy = DB::table('h5_terms')
        ->join('h5_training', 'h5_terms.trainingID', '=', 'h5_training.trainingID')
        ->where('h5_terms.term_start', '>=', $term_start)
        ->where('h5_terms.term_start', '<=', $term_end)
        ->orderBy('h5_terms.term_start', 'asc')
        //->limit(5)
        ->get();
        } else {
            $this->lastTerminy = DB::table('h5_terms')
        ->join('h5_training', 'h5_terms.trainingID', '=', 'h5_training.trainingID')
        ->where('h5_terms.term_placeID', $placeId)
        ->where('h5_terms.term_start', '>=', $term_start)
        ->where('h5_terms.term_start', '<=', $term_end)
        ->orderBy('h5_terms.term_start', 'asc')
       // ->limit(5)
        ->get();
        }

        return $this->lastTerminy;
    }

    public function index()
    {
         if((Auth::user()->id_role == '1')) {
            return redirect()->route('indexSzkolenia');
        } 
        $miasto = DB::table('h5_places')->pluck('nazwa', 'placeId')->toArray();
        array_unshift($miasto, 'Wszystkie');

        $term_state = ['1' => 'Otwarte', '2' => 'Zamkniete', '3' => 'Sesja'];
        if (Route::CurrentRouteName() == 'admin') {
            $this->getDashboardLastTerminy('2', date('Y-m-d', strtotime('-7 days')), date('Y-m-d', strtotime('+7 days')));
        } else {
            $this->getDashboardLastTerminy($this->placeId, $this->term_start, $this->term_end);
        }

        $qtyUczestnicy = DB::table('h5_terms')
        ->join('crm_szkolenia', 'h5_terms.termID', '=', 'crm_szkolenia.termID')
        ->where([['h5_terms.term_start', '<=', date('Y-m-d', time() + 1339200 * 1)], ['h5_terms.term_start', '>', date('Y-m-d')]])
        ->pluck('h5_terms.termID', 'crm_szkolenia.szkolenieID')->toArray();


        if ((Auth::user()->id_role == '3')) {

            $lastRezerwacje = DB::table('crm_osoby')
        ->join('crm_szkolenia', 'crm_szkolenia.uczestnikID', '=', 'crm_osoby.uczestnikID')
        ->join('h5_terms', 'crm_szkolenia.termID', '=', 'h5_terms.termID')
        ->join('h5_training', 'h5_training.trainingID', '=', 'h5_terms.trainingID')
        ->join('crm_klienci', 'crm_klienci.klientID', '=', 'crm_osoby.klientID')
        ->where('crm_klienci.opiekunID',Auth::user()->id)
        ->orderBy('crm_osoby.uczestnikID', 'desc')
        ->limit(10)->get();

            $lastZdarzenia = DB::table('crm_zdarzenia_nowe')->leftJoin('crm_osoby', 'crm_osoby.uczestnikID', '=', 'crm_zdarzenia_nowe.uczestnikID')->leftJoin('crm_klienci', 'crm_klienci.klientID', '=', 'crm_osoby.klientID')
            ->where('crm_klienci.opiekunID',Auth::user()->id)
            ->orderBy('data_dodania', 'desc')->limit(10)->get();

        } else {
            $lastRezerwacje = DB::table('crm_osoby')
        ->join('crm_szkolenia', 'crm_szkolenia.uczestnikID', '=', 'crm_osoby.uczestnikID')
        ->join('h5_terms', 'crm_szkolenia.termID', '=', 'h5_terms.termID')
        ->join('h5_training', 'h5_training.trainingID', '=', 'h5_terms.trainingID')
        ->orderBy('crm_osoby.uczestnikID', 'desc')
        ->limit(10)->get();

            $lastZdarzenia = DB::table('crm_zdarzenia_nowe')->leftJoin('crm_osoby', 'crm_osoby.uczestnikID', '=', 'crm_zdarzenia_nowe.uczestnikID')->leftJoin('crm_klienci', 'crm_klienci.klientID', '=', 'crm_zdarzenia_nowe.klientID')->orderBy('data_dodania', 'desc')->limit(10)->get();
        }
        $getContactMail = DB::table('crm_osoby')->pluck('email', 'uczestnikID')->toArray();
        $getContactPhone = DB::table('crm_osoby')->pluck('user_phone', 'uczestnikID')->toArray();

        $iloscDniTab = [];
        return view('backend.admin', compact('lastRezerwacje', 'getContactMail', 'getContactPhone', 'qtyUczestnicy', 'iloscDniTab'))->with('miasto', $miasto)->with('lastZdarzenia', $lastZdarzenia)->with('term_state', $term_state)->with('lastTerminy', $this->lastTerminy);
    }

    private $placeId;
    private $term_start;
    private $term_end;

    public function filtrPlaceDashboard(Request $request)
    {
        $this->placeId = Request::get('filtruj_szkolenia');

        if (Request::get('filtruj') == 'next') {
            $this->term_start = date('Y-m-d', strtotime('+7 days', strtotime(Request::get('term_start'))));
            $this->term_end = date('Y-m-d', strtotime('+7 days', strtotime(Request::get('term_end'))));

            Request::merge(['term_start' => $this->term_start]);
            Request::merge(['term_end' => $this->term_end]);
        }
        if (Request::get('filtruj') == 'prev') {
            $this->term_start = date('Y-m-d', strtotime('-7 days', strtotime(Request::get('term_start'))));
            $this->term_end = date('Y-m-d', strtotime('-7 days', strtotime(Request::get('term_end'))));

            Request::merge(['term_start' => $this->term_start]);
            Request::merge(['term_end' => $this->term_end]);
        }
        if (Request::get('filtruj') == 'filtr') {
            $this->term_start = Request::get('term_start');
            $this->term_end = Request::get('term_end');
        }

        return $this->index();
    }

    private $nazwa_firmy;

    public function findFirmaDashboard(CreateCRMFindFirmaRequest $request)
    {
        $this->nazwa_firmy = $request->input('znajdz_firme');

        $firma = new CRMFirmyController();

        return $firma->filtruj($this->nazwa_firmy);
    }

    public function trenerzy()
    {
        $trenerzy = DB::table('h5_instructor')->get();

        return view('backend.admin', compact('trenerzy'));
    }
}
