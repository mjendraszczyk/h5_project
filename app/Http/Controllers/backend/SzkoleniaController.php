<?php

namespace App\Http\Controllers\backend;

use App\Http\Requests\backend\CreateSzkoleniaRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Session;

class SzkoleniaController extends Controller
{
    private $aktywnosci_tab;
    private $kategorie;
    private $cytaty;
    private $trenerzy;
    private $pozycje;
    private $typy_szkolenia; 

    public function __construct() {
        $this->typy_szkolenia = array('Otwarte (domyslnie)','Online','Otwarte/online');
    }
    public function getCoreSzkolenia()
    {
        $this->aktywnosci_tab = [
            'Case studies',
            'Dyskusja',
            'Feedback',
            'Zadanie wdrożeniowe',
            'Konsultacje on-line',
            'Gry szkoleniowe',
            'Inspirujący mail',
            'Praca z kamerą',
            'Fiszki / one-pager',
            'Pre-work',
            'Materiały multimedialne',
            'Platforma WWW',
         ];

        $this->kategorie = DB::table('h5_categories')->get();

        $this->cytaty = DB::table('h5_cite')->pluck('cite_text', 'citeID')->prepend('brak', null);

        $this->trenerzy = DB::table('h5_instructor')->pluck('instructor_name', 'instructorID')->prepend('brak', null);
    }

    public static function opinieQty($szkolenieID)
    {
        $opinieQty = DB::table('h5_references_priv')->where('szkolenieID', $szkolenieID)->count();

        return $opinieQty;
    }

    public function index()
    {
        $this->getCoreSzkolenia();

        $szkolenia = DB::table('h5_training')->join('h5_cattrain', 'h5_training.trainingID', '=', 'h5_cattrain.trainingID')->orderBy('h5_cattrain.position', 'asc')->get();

        $getSieroty = DB::table('h5_training')->leftJoin('h5_cattrain', 'h5_training.trainingID', '=', 'h5_cattrain.trainingID')->select('h5_training.trainingID', 'h5_training.training_title', 'h5_training.training_price','h5_training.training_type')->whereNull('h5_cattrain.trainingID')->orderBy('h5_training.training_title', 'asc')->get();

        return view('backend.szkolenia.index', compact('szkolenia', 'getSieroty'))->with('pozycje', $this->pozycje)->with('kategorie', $this->kategorie)->with('aktywnosci_tab', $this->aktywnosci_tab);
    }

    public function edit($id)
    {
        $this->getCoreSzkolenia();

        $cattrain = DB::table('h5_cattrain')->where('h5_cattrain.trainingID', $id)->pluck('categoryID')->toArray();

        $szkolenia = DB::table('h5_training')->where('h5_training.trainingID', $id)->get();

//        $szkolenia = DB::table('h5_training')->where('h5_training.trainingID', $id)->get(); //->orderBy('h5_cattrain.position', 'asc')->get();

        $tagi = DB::table('hashtag')->where('hashtag.trainingID', '=', $id)->pluck('hashtag.tag', 'hashtag.hastagID')->toArray();

        return view('backend.szkolenia.edit', compact('szkolenia', 'tagi', 'cattrain'))->with('kategorie', $this->kategorie)->with('trenerzy', $this->trenerzy)->with('cytaty', $this->cytaty)->with('aktywnosci_tab', $this->aktywnosci_tab)->with('typy_szkolenia',$this->typy_szkolenia);
    }

    public function create()
    {
        $this->getCoreSzkolenia();

        return view('backend.szkolenia.create')->with('kategorie', $this->kategorie)->with('trenerzy', $this->trenerzy)->with('cytaty', $this->cytaty)->with('aktywnosci_tab', $this->aktywnosci_tab)->with('typy_szkolenia',$this->typy_szkolenia);
    }

    //Session::flash('success', 'Zapisano pomyslnie.');

    public function update(CreateSzkoleniaRequest $request, $id)
    {
        // $update_shop = Setting::findOrFail($id);

        $szkolenia = DB::table('h5_training')->where('trainingID', $id);

        if ($request->input('training_news') == null) :
        $training_news = '0'; else :
            $training_news = '1';
        endif;

        if ($request->input('modul_programu') == null) :
        $modul_programu = '0'; else :
            $modul_programu = '1';
        endif;

        // usun wszystkie dotychczasowe rekordy gdzie Szkolenie mialo dana kategorie
        // dodaj nowa cattrain wg foreacha

        $cattrain = DB::table('h5_cattrain')->where('trainingID', $id)->delete();

        for ($dc = 0; $dc < count($request->input('categoryID')); ++$dc) {
            $addCattrain = DB::table('h5_cattrain')->insert([
                'trainingID' => $id,
                'categoryID' => $request->input('categoryID')[$dc],
                ]);
        }

        $training_activities = '';
        $training_activities1 = '';

        if (count($request->input('training_activities') > 0)) {
            for ($dta = 0; $dta < count($request->input('training_activities')); ++$dta) {
                $training_activities .= $request->input('training_activities')[$dta].';';
            }
        } else {
        }

         if (count($request->input('training_activities1') > 0)) {
            for ($dta = 0; $dta < count($request->input('training_activities1')); ++$dta) {
                $training_activities1 .= $request->input('training_activities1')[$dta].';';
            }
        } else {
        }

        $szkolenia->update([
            'training_title' => $request->input('training_title'),
            'training_lid' => $request->input('training_lid'),
            'meta_title' => '',
            'training_cite' => $request->input('training_cite'),
            'training_advantage' => $request->input('training_advantage'),
            'training_program' => $request->input('training_program'),
            'training_demand' => $request->input('training_demand'),
'training_price' => $request->input('training_price'),
'training_hours' => $request->input('training_hours'),
'training_tr1' => $request->input('training_tr1'),
'training_tr2' => $request->input('training_tr2'),
'training_tr3' => $request->input('training_tr3'),
'dla_kogo' => '',
'training_code' => '',
'training_activities' => $training_activities,
'training_news' => $training_news,
'modul_programu' => $modul_programu,
'training_type' => $request->input('training_type'),
'training_price1' => $request->input('training_price1'),
'training_hours1' => $request->input('training_hours1'),
'training_activities1' => $training_activities1,
        ]);

        $cattrain = DB::table('h5_cattrain')->where('trainingID', $id)->update([
            'position' => $request->input('position'),
        ]);

        Session::flash('panelInfo', 'Zapisano pomyslnie.');

        // $id = DB::getPdo()->lastInsertId();

        //return $request->input('categoryID');
        return redirect()->route('editSzkolenia', ['id' => $id]);
    }

    public function store(CreateSzkoleniaRequest $request)
    {
        if ($request->input('training_news') == null) {
            $nowosc = '0';
        } else {
            $nowosc = '1';
        }

        if ($request->input('modul_programu') == null) {
            $modul = '0';
        } else {
            $modul = $request->input('modul_programu');
        }

        $training_activities = '';
        $training_activities1 = '';

        if (count($request->input('training_activities') > 0)) {
            for ($dta = 0; $dta < count($request->input('training_activities')); ++$dta) {
                $training_activities .= $request->input('training_activities')[$dta].';';
            }
        } else {
        }

        if (count($request->input('training_activities1') > 0)) {
            for ($dta = 0; $dta < count($request->input('training_activities1')); ++$dta) {
                $training_activities1 .= $request->input('training_activities1')[$dta].';';
            }
        } else {
        }

        $szkolenia = DB::table('h5_training')->insert([
            'training_title' => $request->input('training_title'),
            'training_lid' => $request->input('training_lid'),
            'meta_title' => '',
            'training_cite' => $request->input('training_cite'),
            'training_advantage' => $request->input('training_advantage'),
            'training_program' => $request->input('training_program'),
            'training_demand' => $request->input('training_demand'),
'training_price' => $request->input('training_price'),
'training_hours' => $request->input('training_hours'),
'training_tr1' => $request->input('training_tr1'),
'training_tr2' => $request->input('training_tr2'),
'training_tr3' => $request->input('training_tr3'),
'training_news' => $nowosc,
'training_code' => '',
'training_activities' => $training_activities,
'dla_kogo' => '',
'modul_programu' => $modul,
'training_type' => $request->input('training_type'),
'training_price1' => $request->input('training_price1'),
'training_hours1' => $request->input('training_hours1'),
'training_activities1' => $training_activities1,
    ]);

        Session::flash('panelInfo', 'Dodano pomyslnie.');

        $id = DB::getPdo()->lastInsertId();

        $cattrain = DB::table('h5_cattrain')->where('trainingID', $id)->delete();

        for ($dc = 0; $dc < count($request->input('categoryID')); ++$dc) {
            $checkQty = DB::table('h5_cattrain')->where('categoryID', $request->input('categoryID')[$dc])->count();

            $addCattrain = DB::table('h5_cattrain')->insert([
                'trainingID' => $id,
                'categoryID' => $request->input('categoryID')[$dc],
                'position' => $checkQty + 1,
                ]);
        }

        return redirect()->route('editSzkolenia', ['id' => $id]);
    }

    public function destroy($id)
    {
        $deleteSzkolenia = DB::table('h5_training')->where('trainingID', $id);
        $deleteSzkolenia->delete();

        return redirect()->route('indexSzkolenia');
    }

    public function change_position($id) // $operation  $id, $position
    {
        $updatePosition = DB::table('h5_cattrain')->where('cattrainID', $id)->update([
                'position' => request('position'),
            ]);

        return 'Zapisano pomylnie';
    }

    // public function change_position($id, $idCategory, $position, $operation)
    // {
    //     if ($operation == 'up') {
    //         if ($position > 1) {
    //             $updateBefore = DB::table('h5_cattrain')->where('categoryID', $idCategory)->where('position', $position - 1)->update([
    //             'position' => $position,
    //         ]);

    //             $updateCurrent = DB::table('h5_cattrain')->where('categoryID', $idCategory)->where('cattrainID', $id)->update([
    //             'position' => $position - 1,
    //         ]);
    //         }

    //         return redirect()->route('indexSzkolenia');
    //     }
    //     if ($operation == 'down') {
    //         $countRows = DB::table('h5_cattrain')->where('categoryID', $idCategory)->count();
    //         if ($position < $countRows) {
    //             $updateAfter = DB::table('h5_cattrain')->where('categoryID', $idCategory)->where('position', $position + 1)->update([
    //             'position' => $position,
    //         ]);

    //             $updateCurrent = DB::table('h5_cattrain')->where('categoryID', $idCategory)->where('cattrainID', $id)->update([
    //             'position' => $position + 1,
    //         ]);
    //         }

    //         return redirect()->route('indexSzkolenia');
    //     }
    // }
}
