<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\CreateProgramyRequest;
use Session;
use Request;

class ProgramyController extends Controller
{
    public function index()
    {
        $programy = DB::table('h5_programy')->get();

        return view('backend.programy.index', compact('programy'));
    }

    public function edit($id)
    {
        $programy = DB::table('h5_programy')->where('programID', $id)->get();

        return view('backend.programy.edit', compact('programy'));
    }

    public function create()
    {
        return view('backend.programy.create');
    }

    //Session::flash('success', 'Zapisano pomyslnie.');

    public function update(CreateProgramyRequest $request, $id)
    {
        // $update_shop = Setting::findOrFail($id);

        $programy = DB::table('h5_programy')->where('programID', $id);

        $programy->update([
            'nazwa' => $request->input('nazwa'),
            'podtytul' => $request->input('podtytul'),
            'korzysc' => $request->input('korzysc'),
            'dla_kogo' => $request->input('dla_kogo'),
            'aktywny' => $request->input('aktywny'),
            'cena' => $request->input('cena'),
            //'position' => $request->input('position'),
        ]);

        Session::flash('panelInfo', 'Zapisano pomyslnie.');

        // $id = DB::getPdo()->lastInsertId();

        return redirect()->route('editProgramy', ['id' => $id]);
    }

    public function store(CreateProgramyRequest $request)
    {
        $programy = DB::table('h5_programy')->insert([
            'nazwa' => $request->input('nazwa'),
            'podtytul' => $request->input('podtytul'),
            'korzysc' => $request->input('korzysc'),
            'dla_kogo' => $request->input('dla_kogo'),
            'aktywny' => $request->input('aktywny'),
            'cena' => $request->input('cena'),
    ]);

        Session::flash('panelInfo', 'Dodano pomyslnie.');

        $id = DB::getPdo()->lastInsertId();

        return redirect()->route('editProgramy', ['id' => $id]);
    }

    public function createModulyProgramy($id)
    {
        $getSzkoleniaDependProgram = DB::table('h5_training')->pluck('training_title', 'trainingID');

        $getProgramName = DB::table('h5_programy')->where('programID', $id)->get(['nazwa']);

        return view('backend.programy.moduly.create', compact('getSzkoleniaDependProgram', 'getProgramName', 'id')); // zwroc programy szkoleniowe dla modulu
    }

    public function storeModulyProgramy(Request $request, $id)
    {
        $dodajModulProgramu = DB::table('h5_programy_moduly')->insert([
            'programID' => $id,
            'trainingID' => Request::get('trainingID'),
            'kolejnosc' => '0',
    ]);

        Session::flash('panelInfo', 'Dodano pomyslnie.');

        $idRow = DB::getPdo()->lastInsertId();

        return redirect()->route('indexModulyProgramy', ['id' => $id]);
    }

    public function change_position_program($id) // $operation  $id, $position
    {
        $updatePosition = DB::table('h5_programy_moduly')->where('program_modulID', $id)->update([
                'kolejnosc' => request('position'),
            ]);

        return 'Zapisano pomylnie';
    }

    public function destroyModulyProgramy($id, $SzkolenieID)
    {
        $deleteProgramy = DB::table('h5_programy_moduly')->where('program_modulID', $SzkolenieID);
        $deleteProgramy->delete();

        //       return 'usuwam:)';
        return redirect()->route('indexModulyProgramy', ['id' => $id]);
    }

    public function indexModulyProgramy($id)
    {
        $getSzkoleniaDependProgram = DB::table('h5_programy_moduly')->leftJoin('h5_training', 'h5_programy_moduly.trainingID', '=', 'h5_training.trainingID')->where('h5_programy_moduly.programID', $id)->orderBy('h5_programy_moduly.kolejnosc', 'asc')->get();

        return view('backend.programy.moduly.index', compact('getSzkoleniaDependProgram')); // zwroc programy szkoleniowe dla modulu
    }

    public function destroy($id)
    {
        $deleteProgramy = DB::table('h5_programy')->where('programID', $id);
        $deleteProgramy->delete();

        return redirect()->route('indexProgramy');
    }
}
