<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\CreateCRMOsobyRequest;
use Session;
use Illuminate\Http\Request;
use Response;

class CRMOsobyController extends Controller
{
    private $getZdarzenia;

    public function getZdarzeniaUczestnika($id)
    {
        $this->getZdarzenia = DB::table('crm_zdarzenia_nowe')->where('uczestnikID', $id)->get();
    }

    private $getSzkolenia;

    public function getSzkoleniaUczestnika($id)
    {
        $this->getSzkolenia = DB::table('crm_szkolenia')->leftJoin('h5_training', 'crm_szkolenia.szkolenieID', '=', 'h5_training.trainingID')->where('uczestnikID', $id)->get();
    }

    public function index()
    {
        $osoby = DB::table('crm_osoby')->limit(300)->orderBy('uczestnikID', 'desc')->get();

        return view('backend.CRMOsoby.index', compact('osoby'));
    }

    public function create($klientID)
    {
        return view('backend.CRMOsoby.create')->with('klientID', $klientID);
    }

    public function edit($klientID, $id)
    {
        $CRMZdarzeniaController = new CRMZdarzeniaController();
        $typy_zdarzenia = $CRMZdarzeniaController->typyZdarzenia();

        $osoby = DB::table('crm_osoby')->where('crm_osoby.uczestnikID', $id)->get();

        $this->getSzkoleniaUczestnika($id);

        $this->getZdarzeniaUczestnika($id);

        return view('backend.CRMOsoby.edit', compact('osoby', 'klientID'))->with('typy_zdarzenia', $typy_zdarzenia)->with('szkolenia', $this->getSzkolenia)->with('zdarzenia', $this->getZdarzenia);
    }

    public function getUczestnik($id)
    {
        $osoby = DB::table('crm_osoby')->where('crm_osoby.uczestnikID', $id)->get();

        return $osoby;
    }

    public static function getUczestnikName($id)
    {
        $imie_nazwisko = DB::table('crm_osoby')->where('crm_osoby.uczestnikID', $id)->get();

        return $imie_nazwisko;
    }

    public function setCheckboxStatus($request)
    {
        if ($request->input('hr') == null) {
            $this->hr = '0';
        } else {
            $this->hr = '1';
        }

        if ($request->input('newsletter') == null) {
            $this->newsletter = '0';
        } else {
            $this->newsletter = '1';
        }
    }

    public function store($klientID, CreateCRMOsobyRequest $request)
    {
        $this->setCheckboxStatus($request);

        $osoba = DB::table('crm_osoby')->insert([
            'klientID' => $klientID,
             'imie_nazwisko' => $request->input('imie_nazwisko'),
             'email' => $request->input('email'),
             'user_phone' => $request->input('user_phone'),
             'user_cell' => $request->input('user_cell'),
            'newsletter' => $this->newsletter,
            'hr' => $this->hr,
             'uwagi' => $request->input('uwagi'),
             'zainteresowania' => $request->input('zainteresowania'),
            'stanowisko' => $request->input('stanowisko'),
            'ostatni_kontakt' => date('Y-m-d'),
            'nastepny_kontakt' => date('Y-m-d'),
    ]);

        $id = DB::getPdo()->lastInsertId();

        return redirect()->route('editCRMFirmy', ['id' => $klientID]);
    }

    public function update($klientID, CreateCRMOsobyRequest $request, $id)
    {
        $this->setCheckboxStatus($request);

        $osoba = DB::table('crm_osoby')->where('uczestnikID', $id);

        $osoba->update([
             'imie_nazwisko' => $request->input('imie_nazwisko'),
             'email' => $request->input('email'),
             'user_phone' => $request->input('user_phone'),
             'user_cell' => $request->input('user_cell'),
            'newsletter' => $this->newsletter,
            'hr' => $this->hr,
             'uwagi' => $request->input('uwagi'),
             'zainteresowania' => $request->input('zainteresowania'),
            'stanowisko' => $request->input('stanowisko'),
    ]);

        Session::flash('panelInfo', 'Zapisano pomyslnie.');

        return redirect()->route('editCRMFirmy', ['id' => $klientID]);
    }

    public function destroy($klientID, $id)
    {
        $deleteOsoba = DB::table('crm_osoby')->where('uczestnikID', $id);
        $deleteOsoba->delete();

        return redirect()->route('editCRMFirmy', ['id' => $klientID]);
    }

    public function autocomplete(Request $request)
    {
        $query = $request->get('term', '');

        $Uczestnicy = DB::table('crm_osoby')->leftJoin('crm_klienci', 'crm_osoby.klientID', '=', 'crm_klienci.klientID')->where('crm_osoby.imie_nazwisko', 'LIKE', '%'.$query.'%')->take(5)->get();

        foreach ($Uczestnicy as $Uczestnik) {
            $results[] = ['id' => $Uczestnik->uczestnikID, 'value' => $Uczestnik->imie_nazwisko.', '.$Uczestnik->nazwa];
        }

        return Response::json($results);
    }

    public function getOpiekun($id)
    {
        $opiekun = DB::table('users')->where('id', $id)->get();

        $name = '';
        foreach ($opiekun as $o) {
            $name = $o->name;
        }

        return $name;
    }
}
