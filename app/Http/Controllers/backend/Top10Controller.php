<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
// use App\Http\Requests\backend\CreateAkademiaRequest;
use Session;

// $sql= "SELECT
// h5_training.training_title,
// Count(crm_szkolenia.szkolenieID) AS ile
// FROM
// crm_szkolenia
// Inner Join h5_terms ON crm_szkolenia.termID = h5_terms.termID
// Inner Join h5_training ON h5_terms.trainingID = h5_training.trainingID
// GROUP BY
// h5_training.training_title
// ORDER BY
// ile DESC
// LIMIT 0, 10";

    // $sql="Select * from h5_top10 WHERE position=".$i;
    // 					$rsCurrent=mysql_query($sql) or die(mysql_error());
    // 					$row_rsCurrent=mysql_fetch_assoc($rsCurrent);
    // 				echo '<tr>';
    // 				echo '	<td>'.$i.'</td>';
    // 				echo '	<td >'.select("position_".$i, $szkolenia,$row_rsCurrent['trainingID']).'</td>';
    // 				echo '</tr>';

class Top10Controller extends Controller
{
    public function index()
    {
        $top10 = DB::table('crm_szkolenia')
        ->join('h5_terms', 'crm_szkolenia.termID', '=', 'h5_terms.termID')
        ->join('h5_training', 'h5_terms.trainingID', '=', 'h5_training.trainingID')
        ->select('h5_training.training_title', 'crm_szkolenia.szkolenieID as ile')
        ->groupBy('h5_training.training_title', 'ile')
        ->orderBy('ile', 'DESC')
        ->limit(10)
        ->get();
        //->join('h5_terms', 'crm_szkolenia.termID', '=', 'h5_terms.termID')

        return view('backend.top10.index', compact('top10'));
    }

    // public function edit($id)
    // {
    //     //      $a = DB::table('h5_blog')->join('h5_instructor', 'h5_blog.instructorID', '=', 'h5_instructor.instructorID')->join('h5_categories', 'h5_blog.categoryID', '=', 'h5_categories.categoryID')->where('blogID', $id)->get();
    //     $akademie = DB::table('h5_akademia')->where('akademiaID', $id)->get(); //->join('h5_akademia_szkolenia', 'h5_akademia_szkolenia.akademiaID', '=', 'h5_akademia.akademiaID')->where('h5_akademia.akademiaID', $id)->get();

    //     //>join('h5_training', 'h5_akademia_szkolenia.trainingID', '=', 'h5_training.h5_training')
    //     return view('backend.akademie.edit', compact('akademie'));
    // }

    // public function update(CreateAkademiaRequest $request, $id)
    // {
    //     // $update_shop = Setting::findOrFail($id);

    //     $akademia = DB::table('h5_akademia')->where('akademiaID', $id);

    //     $akademia->update([
    //         'nazwa' => $request->input('nazwa'),
    //         'opis_krotki' => $request->input('opis_krotki'),
    //         'opis' => $request->input('opis'),
    //         'aktywny' => $request->input('aktywny'),
    //         //'position' => $request->input('position'),
    //     ]);

    //     Session::flash('panelInfo', 'Zapisano pomyslnie.');

    //     // $id = DB::getPdo()->lastInsertId();

    //     return redirect()->route('editAkademie', ['id' => $id]);
    // }

    // public function store(CreateAkademiaRequest $request)
    // {
    //     $akademia = DB::table('h5_akademia')->insert([
    //          'nazwa' => $request->input('nazwa'),
    //         'opis_krotki' => $request->input('opis_krotki'),
    //         'opis' => $request->input('opis'),
    //         'aktywny' => $request->input('aktywny'),
    // ]);
    // }

    // public function destroy($id)
    // {
    //     $deleteAkademia = DB::table('h5_akademia')->where('akademiaID', $id);
    //     $deleteAkademia->delete();

    //     return redirect()->route('indexAkademie');
    // }
}
