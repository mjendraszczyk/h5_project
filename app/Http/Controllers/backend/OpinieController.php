<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\CreateOpinieRequest;
use Session;

class OpinieController extends Controller
{
    private $oceny;

    public function coreOpinie()
    {
        $this->oceny = ['1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'];
    }

    public function index($szkolenieID)
    {
        $opinie = DB::table('h5_references_priv')->where('szkolenieID', $szkolenieID)->get();

        $trening = DB::table('h5_training')->where('trainingID', $szkolenieID)->get();

        return view('backend.opinie.index', compact('opinie', 'trening'))->with('szkolenieID', $szkolenieID);
    }

    public function create($szkolenieID)
    {
        Session::put('szkolenieID', $szkolenieID);

        $this->coreOpinie();

        return view('backend.opinie.create')->with('szkolenieID', $szkolenieID)->with('oceny', $this->oceny);
    }

    public function edit($szkolenieID, $id)
    {
        $this->coreOpinie();

        $opinie = DB::table('h5_references_priv')->where('referencePrivID', $id)->get();

        $trening = DB::table('h5_training')->where('trainingID', $szkolenieID)->get();

        Session::put('szkolenieID', $szkolenieID);

        return view('backend.opinie.edit', compact('opinie', 'trening'))->with('szkolenieID', $szkolenieID)->with('oceny', $this->oceny);
        //return 'fgdfg';
    }

    public function update(CreateOpinieRequest $request, $id, $szkolenieID)
    {
        $opiniaEdit = DB::table('h5_references_priv')->where('referencePrivID', $szkolenieID);

        $opiniaEdit->update([
            'tresc' => $request->input('tresc'),
            'imie' => $request->input('imie'),
            'firma' => $request->input('firma'),
            'ocena' => $request->input('ocena'),
        ]);

        Session::flash('panelInfo', 'Zapisano pomyslnie.');

        // $id = DB::getPdo()->lastInsertId();

        return redirect()->route('indexSzkoleniaOpinia', ['szkolenieID' => Session::get('szkolenieID')]);
    }

    public function store(CreateOpinieRequest $request)
    {
        $opinie = DB::table('h5_references_priv')->insert([
            'tresc' => $request->input('tresc'),
            'imie' => $request->input('imie'),
            'firma' => $request->input('firma'),
            'ocena' => $request->input('ocena'),
            'szkolenieID' => Session::get('szkolenieID'),
    ]);

        Session::flash('panelInfo', 'Dodano pomyslnie.');

        $id = DB::getPdo()->lastInsertId();

        return redirect()->route('indexSzkoleniaOpinia', ['szkolenieID' => Session::get('szkolenieID')]);
    }

    public function destroy($szkolenieID, $id)
    {
        $deleteOpinie = DB::table('h5_references_priv')->where('referencePrivID', $id);
        $deleteOpinie->delete();

        return redirect()->route('indexSzkoleniaOpinia', ['szkolenieID' => $szkolenieID]);
    }
}
