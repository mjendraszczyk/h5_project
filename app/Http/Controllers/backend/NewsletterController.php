<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\CreateNewsletterRequest;
use App\Http\Requests\backend\CreateSubscriptionRequest;
use Mail;
use Session;

class NewsletterController extends Controller
{
    public function index()
    {
        $newsletter = DB::table('h5_training')->join('h5_terms', 'h5_training.trainingID', '=', 'h5_terms.trainingID')->where([['h5_terms.term_start', '>', date('Y-m-d H:i:s')], ['h5_terms.term_start', '<=', date('Y-m-d H:i:s', time() + 2678400 * 2)]])->OrderBy('h5_terms.term_start')->get();

        return view('backend.newsletter.index', compact('newsletter'));
    }

    public function MailModel($request, $status)
    {
        $newsletter = DB::table('h5_training')->join('h5_terms', 'h5_training.trainingID', '=', 'h5_terms.trainingID')->where([['h5_terms.term_start', '>', date('Y-m-d H:i:s')], ['h5_terms.term_start', '<=', date('Y-m-d H:i:s', time() + 2678400 * 2)]])->OrderBy('h5_terms.term_start')->get();

        if ($status == 'podglad') {
            $widok = [];
            array_push($widok, ['opis' => $request->input('opis')]);
            //array_push($widok, ['opis' => 'dfdsfdsfsd']);

            foreach ($newsletter as $newslett) :

                    if ($request->input($newslett->termID) == '1') {
                        if ($request->input('price_'.$newslett->termID) != '') :
                        array_push($widok, [
                            'term_start' => $newslett->term_start,
                            'term_end' => $newslett->term_end,
                            'term_place' => $newslett->term_place,
                            'trainingID' => $newslett->trainingID,
                            'training_title' => $newslett->training_title,
                            'trainig_price' => $request->input('price_'.$newslett->termID),
                            ]); else :
                               array_push($widok, [
                            'term_start' => $newslett->term_start,
                            'term_end' => $newslett->term_end,
                            'term_place' => $newslett->term_place,
                            'trainingID' => $newslett->trainingID,
                            'training_title' => $newslett->training_title,
                            'trainig_price' => $newslett->training_price,
                            ]);
                        endif;
                    }

            endforeach;

            Session::put('newsletter', $widok);

            return view('backend.newsletter.preview', compact('widok'));
        }

        if ($status == 'wyslij') {
            if ($request->wyslij == 'Testuj') {
                $data = [
                'widok' => Session::get('newsletter'),
            ];

                Mail::send('backend.emails.newsletter', $data, function ($message) {
                    $message->from(env('MAIL_USERNAME'), 'Biuro High5');

                    $message->to(env('MAIL_USERNAME', 'Biuro High5'))->subject('Newsletter High5 - Szkolenia na najbliższy miesiąc'); //->cc('asystentka03@high5.pl');
                });

                Session::flash('panelInfo', 'Wysłano newsletter tylko do Ciebie');
            }
            if ($request->wyslij == 'Wyślij') {
                //$emails = DB::table('crm_osoby')->where([['newsletter', '1'], ['email', 'LIKE', '%@%']])->pluck('email')->toArray();
                $emails = DB::table('crm_osoby')->where([['newsletter', '1'], ['email', 'LIKE', '%@%'], ['marketing_zgoda', '1']])->pluck('email')->toArray();
                $ilosc = DB::table('crm_osoby')->where([['newsletter', '1'], ['email', 'LIKE', '%@%'], ['marketing_zgoda', '1']])->count();
                //foreach ($emails as $email) :

                $data = [
                'widok' => Session::get('newsletter'),
            ];

                Mail::send('backend.emails.newsletter', $data, function ($message) use ($emails) {
                    $message->from(env('MAIL_USERNAME'), 'Biuro High5');

                    $message->to(env('MAIL_USERNAME', 'Biuro High5'))->bcc($emails)->subject('Newsletter High5 - Szkolenia na najbliższy miesiąc'); //->cc('asystentka03@high5.pl');
                });

                Session::flash('panelInfo', 'Wysłano newsletter do '.$ilosc.' osób');
            }

            return $this->index();
        }
    }

    public function showNewsletter(CreateNewsletterRequest $request)
    {
        return $this->MailModel($request, 'podglad');
    }

    public function NewsletterSend(CreateNewsletterRequest $request)
    {
        return  $this->MailModel($request, 'wyslij');
    }

    public function NewsltterSendTest(CreateNewsletterRequest $request)
    {
        return  $this->MailModel($request, 'test');
    }

    public function deleteNewsletter()
    {
        return view('frontend.emails.delete');
    }

    public function NewsletterWypisz(CreateSubscriptionRequest $request)
    {
        $wypisz = DB::table('crm_osoby')->where('email', $request->input('email'))->get();

        if (count($wypisz) > 0) {
            $wypisz_update = DB::table('crm_osoby')->where('email', $request->input('email'));
            $wypisz_update->update(['newsletter' => '0', 'marketing_zgoda' => '0']);

            echo 'Subskrypcja anulowana. Za chwile przekierujemy Cię na stronę główną...';
            sleep(3);

            return redirect()->route('homepage');
        } else {
            Session::flash('panelInfo', 'Nie znaleziono w bazie podanego adresu email: '.$request->input('email').'');

            return redirect()->route('NewsletterDelete');
        }
    }
}
