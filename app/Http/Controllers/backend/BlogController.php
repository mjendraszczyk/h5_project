<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\CreateBlogRequest;
use Session;
use File;

class BlogController extends Controller
{
    public function index()
    {
        $blogs = DB::table('h5_blog')->join('h5_instructor', 'h5_blog.instructorID', '=', 'h5_instructor.instructorID')->join('h5_categories', 'h5_blog.categoryID', '=', 'h5_categories.categoryID')->OrderBy('created', 'DESC')->get();

        return view('backend.blog.index', compact('blogs'));
    }

    public function edit($id)
    {
        $blogs = DB::table('h5_blog')->join('h5_instructor', 'h5_blog.instructorID', '=', 'h5_instructor.instructorID')->join('h5_categories', 'h5_blog.categoryID', '=', 'h5_categories.categoryID')->where('blogID', $id)->get();

        $category = DB::table('h5_categories')->pluck('category_title', 'categoryID');

        $instructor = DB::table('h5_instructor')->pluck('instructor_name', 'instructorID');

        return view('backend.blog.edit', compact('blogs', 'category', 'instructor'));
    }

    public function create()
    {
        $category = DB::table('h5_categories')->pluck('category_title', 'categoryID');

        $instructor = DB::table('h5_instructor')->pluck('instructor_name', 'instructorID');

        return view('backend.blog.create', compact('category', 'instructor'));
    }

    public function add_tag($request, $id)
    {
        if (!empty($request->input('tagi'))) {
            // ok za kazdym razem kiedy zapiszemy blog lub zaktualizujemy usuwaj wszystkie tagi wg blogID i dodaj je wg podanych w formie
            $tagiBlog = DB::table('hashtag_blog')->where('blogID', $id)->delete();

            $tabela_tagi = explode(',', $request->input('tagi'));

            for ($dt = 0; $dt < count($tabela_tagi); ++$dt) {
                DB::table('hashtag_blog')->insert([
                    'categoryID' => '0',
                    'blogID' => $id,
                    'tag' => $tabela_tagi[$dt],
                ]);
            }
        }
    }

    public function update(CreateBlogRequest $request, $id)
    {
        // $update_shop = Setting::findOrFail($id);

        $blog = DB::table('h5_blog')->where('blogID', $id);

        $this->add_tag($request, $id);
        if (empty($request->input('tagi'))) {
            $tagi = '';
        } else {
            $tagi = $request->input('tagi');
        }

        $upload = new Controller();
        // if (!empty($request->file('image'))) {
            // $image = $request->file('image');

            // $input['imagename'] = 'blog'.str_slug($request->input('tytul')).time();

            // $destinationPath = public_path('../img/frontend/blog/');

            // $image->move($destinationPath, $input['imagename'].'_small.jpg');
            // $picture_blog = $input['imagename'];

            // if (File::copy($destinationPath.$input['imagename'].'_small.jpg', $destinationPath.$input['imagename'].'_big.jpg')) {
            // }
            //$image->move($destinationPath, $input['imagename'].'.jpg');

            $blog->update([
            'picture' => $upload->upload($request->file('image'), 'image', '/img/frontend/blog/', 'update', $id),
            'title' => $request->input('tytul'),
            'categoryID' => $request->input('category'),
            'instructorID' => $request->input('author'),
           // 'created' => date('Y-m-d'),
            'lid' => $request->input('lid'),
            'tekst' => $request->input('tekst'),
            'tagi' => $tagi,
            //'position' => $request->input('position'),
        ]);
        // }
        //  else {
        //     $blog->update([
        //     'title' => $request->input('tytul'),
        //     'categoryID' => $request->input('category'),
        //     'instructorID' => $request->input('author'),
        //     //'created' => date('Y-m-d'),
        //     'lid' => $request->input('lid'),
        //     'tekst' => $request->input('tekst'),
        //     'tagi' => $tagi,
        // ]);
        // }

        Session::flash('panelInfo', 'Zapisano pomyslnie.');

        // $id = DB::getPdo()->lastInsertId();

        return redirect()->route('editBlog', ['id' => $id]);
    }

    public function store(CreateBlogRequest $request)
    {
        // $image = $request->file('image');

        // $input['imagename'] = 'blog'.str_slug($request->input('tytul')).time();

        // $destinationPath = public_path('../img/frontend/blog/');
        // if (!empty($image)) {
        //     $image->move($destinationPath, $input['imagename'].'_small.jpg');
        //     $picture_blog = $input['imagename'];

        //     if (File::copy($destinationPath.$input['imagename'].'_small.jpg', $destinationPath.$input['imagename'].'_big.jpg')) {
        //     }
        // } else {
        //     $picture_blog = '';
        // }
        if (empty($request->input('tagi'))) {
            $tagi = '';
        } else {
            $tagi = $request->input('tagi');
        }

        $upload = new Controller();
        $blog = DB::table('h5_blog')->insert([
            'picture' => $upload->upload($request->file('image'), 'image', '/img/frontend/blog/', 'store', null),
            'title' => $request->input('tytul'),
            'categoryID' => $request->input('category'),
            'instructorID' => $request->input('author'),
            'created' => date('Y-m-d'),
            'lid' => $request->input('lid'),
            'tekst' => $request->input('tekst'),
             'tagi' => $tagi,
    ]);

        Session::flash('panelInfo', 'Dodano pomyslnie.');

        $id = DB::getPdo()->lastInsertId();
        $this->add_tag($request, $id);

        return redirect()->route('editBlog', ['id' => $id]);
    }

    public function destroy($id)
    {
        $deleteBlog = DB::table('h5_blog')->where('blogID', $id);
        $deleteBlog->delete();

        return redirect()->route('indexBlog');
    }
}
