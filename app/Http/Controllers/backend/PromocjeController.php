<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\CreatePromocjeRequest;
use Session;

class PromocjeController extends Controller
{
    public function index()
    {
        $promocje = DB::table('h5_news')->orderBy('newsID', 'desc')->get();

        return view('backend.promocje.index', compact('promocje'));
    }

    public function create()
    {
        return view('backend.promocje.create');
    }

    public function edit($id)
    {
        $promocje = DB::table('h5_news')->where('newsID', $id)->get();

        return view('backend.promocje.edit', compact('promocje'));
    }

    public function update(CreatePromocjeRequest $request, $id)
    {
        $promocja = DB::table('h5_news')->where('newsID', $id);

        if (!empty($request->file('news_picture'))) {
            $image = $request->file('news_picture');

            $input['imagename'] = str_slug($request->input('news_title').time(), '_').'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/img/frontend/news');
            $image->move($destinationPath, $input['imagename']);

            $promocja->update([
            'news_title' => $request->input('news_title'),
            'news_lid' => $request->input('news_lid'),
            'news_text' => $request->input('news_text'),
            'news_link' => $request->input('news_link'),
            'news_data' => date('Y-m-d'),
            'news_pictur_pos' => $request->input('news_pictur_pos'),
            'news_picture' => $input['imagename'],
        ]);
        } else {
            $promocja->update([
             'news_title' => $request->input('news_title'),
            'news_lid' => $request->input('news_lid'),
            'news_text' => $request->input('news_text'),
            'news_data' => date('Y-m-d'),
            'news_link' => $request->input('news_link'),
            'news_pictur_pos' => $request->input('news_pictur_pos'),
    ]);
        }
        Session::flash('panelInfo', 'Zapisano pomyslnie.');

        return redirect()->route('editPromocje', ['id' => $id]);
    }

    public function store(CreatePromocjeRequest $request)
    {
        if (!empty($request->file('news_picture'))) {
            $image = $request->file('news_picture');

            $input['imagename'] = str_slug($request->input('news_title').time(), '_').'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/img/frontend/news');
            $image->move($destinationPath, $input['imagename']);

            $obrazek = $input['imagename'];
        } else {
            $obrazek = '';
        }

        $promocja = DB::table('h5_news')->insert([
            'news_picture' => $obrazek,
             'news_title' => $request->input('news_title'),
            'news_lid' => $request->input('news_lid'),
            'news_text' => $request->input('news_text'),
            'news_data' => date('Y-m-d'),
            'news_link' => $request->input('news_link'),
            'news_pictur_pos' => $request->input('news_pictur_pos'),
    ]);

        Session::flash('panelInfo', 'Dodano pomyslnie.');

        $id = DB::getPdo()->lastInsertId();

        return redirect()->route('editPromocje', ['id' => $id]);
    }

    public function destroy($id)
    {
        $deletePromocja = DB::table('h5_news')->where('newsID', $id);
        $deletePromocja->delete();

        return redirect()->route('indexPromocje');
    }
}
