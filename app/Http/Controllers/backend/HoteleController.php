<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\CreateHoteleRequest;
use Session;

class HoteleController extends Controller
{
     public function index()
    {
        $hotele = DB::table('h5_hotele')->orderBy('id_hotele', 'desc')->paginate(15);

        return  view('backend.hotele.index', compact('hotele'));
    }

    public function edit($id)
    {
        $term_placeID = DB::table('h5_places')->get()->pluck('nazwa','placeId');
        $hotele = DB::table('h5_hotele')->where('id_hotele', $id)->get();

        return view('backend.hotele.edit', compact('hotele','term_placeID'));
    }

    public function create()
    {
        $term_placeID = DB::table('h5_places')->get();
        return view('backend.hotele.create',compact('term_placeID'));
    }

    public function update(CreateHoteleRequest $request, $id)
    {
        $hotele = DB::table('h5_hotele')->where('id_hotele', $id);

            $upload = new Controller();

            $hotele = DB::table('h5_hotele')->where('id_hotele', $id)->update([
            'nazwa' => $request->input('nazwa'),
            'zdjecie1' => $upload->upload($request->file('zdjecie1'), 'zdjecie1', '/img/frontend/hotele/', 'update', $id),
            'zdjecie2' => $upload->upload($request->file('zdjecie2'), 'zdjecie2', '/img/frontend/hotele/', 'update', $id),
            'odleglosc_od' => $request->input('odleglosc_od'),
            'miasto' => $request->input('miasto'),
            'kod' => $request->input('kod'),
            'ulica' => $request->input('ulica'),
            'telefon' => $request->input('telefon'),
            'cena_pokoj_jedno' => $request->input('cena_pokoj_jedno'),
            'uwagi' => $request->input('uwagi'),
            'parking' => $request->input('parking'),
            'cena_parking' => $request->input('cena_parking'),
            'wazne' => $request->input('wazne'),
            'term_placeID' => $request->input('term_placeID')
        ]);

        Session::flash('panelInfo', 'Zapisano pomyslnie.');

        return redirect()->route('editHotele', ['id' => $id]);
    }

    public function store(CreateHoteleRequest $request)
    {
            $upload = new Controller();

            $hotele = DB::table('h5_hotele')->insert([
            'zdjecie1' => $upload->upload($request->file('zdjecie1'), 'zdjecie1', '/img/frontend/hotele/', 'store', null),
            'miasto' => $request->input('miasto'),
            'zdjecie2' => $upload->upload($request->file('zdjecie2'), 'zdjecie2', '/img/frontend/hotele/', 'store', null),
            'nazwa' => $request->input('nazwa'),
            'odleglosc_od' => $request->input('odleglosc_od'),
            'kod' => $request->input('kod'),
            'ulica' => $request->input('ulica'),
            'telefon' => $request->input('telefon'),
            'cena_pokoj_jedno' => $request->input('cena_pokoj_jedno'),
            'uwagi' => $request->input('uwagi'),
            'parking' => $request->input('parking'),
            'cena_parking' => $request->input('cena_parking'),
            'wazne' => $request->input('wazne'),
            'term_placeID' => $request->input('term_placeID')
        ]);
        
        Session::flash('panelInfo', 'Dodano pomyslnie.');

        $id = DB::getPdo()->lastInsertId();

        return redirect()->route('editHotele', ['id' => $id]);
    }

    public function destroy($id)
    {
        $deleteHotele = DB::table('h5_hotele')->where('id_hotele', $id);
        $deleteHotele->delete();

        return redirect()->route('indexHotele');
    }
}
