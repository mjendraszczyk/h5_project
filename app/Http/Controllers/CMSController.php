<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateKontaktRequest;
use Illuminate\Support\Facades\DB;
use Config;
use Mail;
use App\Mail\Kontakt;
use Session;

if(env('APP_ENV') == 'local') {
include_once base_path()."/public/js/frontend/sblamtest.php";
} else {
    include_once base_path()."/js/frontend/sblamtest.php";
}

class CMSController extends Controller
{

    public $resultatSblam;

    //----------------------------------------------------
    // WIDOK
    // FIRMA
    //----------------------------------------------------

    // FIRMA > KIM JESTESMY
    public function index()
    {
        $meta = new MetaController();

        return view('frontend.cms.firma.index')->with('meta', $meta->MetaCore(4, null));
    }

    // FIRMA > TRENERZY
    public function trainers()
    {
        $meta = new MetaController();
        $trenerzy = DB::table('h5_instructor')->where('instructor_accepted', '1')->get();

        return view('frontend.cms.firma.trenerzy', compact('trenerzy'))->with('meta', $meta->MetaCore(4, null));
    }

    // FIRMA > KSF
    public function kfs()
    {
        $meta = new MetaController();

        return view('frontend.cms.kfs.index')->with('meta', $meta->MetaCore(4, null));
    }

    // FIRMA > SUS
    public function sus()
    {
        $meta = new MetaController();

        return view('frontend.cms.firma.sus')->with('meta', $meta->MetaCore(4, null));
    }

    // FIRMA > GALERIA
    public function gallery()
    {
        $meta = new MetaController();
        $galeria_full = scandir(Config::get('url').'img/frontend/wronia/full');
        $galeria = scandir(Config::get('url').'img/frontend/wronia/min');

        return view('frontend.cms.firma.galeria', compact('galeria_full'))->with('meta', $meta->MetaCore(4, null));
    }

    // FIRMA > KONTAKT
    public function contact()
    {
        $meta = new MetaController();
        $this->resultatSblam = sblamtestpost( array("email_kontakt","tresc_kontakt","name_kontakt") , "KUVxQnvNgly82ADQBM"); //sblamtestpost();
        $resultat = $this->resultatSblam;

        return view('frontend.cms.firma.kontakt')->with('meta', $meta->MetaCore(2, null))->with('resultat','')->with('reportUrl','');
    }

    // FIRMA > PP
    public function pp()
    {
        $meta = new MetaController();

        return view('frontend.cms.firma.pp')->with('meta', $meta->MetaCore(2, null));
    }

    // FIRMA > KONTAKT > WYSLIJ
    public function contactMail(CreateKontaktRequest $request)
    {
        $contactName = $request->input('name_kontakt');
        $contactEmail = $request->input('email_kontakt');
        $contactMessage = $request->input('tresc_kontakt');
        $currentUrl = $request->input('url_kontakt');

        // dd($request);
        $gcaptcha = $request->input('g-recaptcha-response');
            //captcha verification
            $googleVerificationUrl = 'https://www.google.com/recaptcha/api/siteverify';
            $googleSec = '6Le58L0UAAAAALxYr1rq53vWuZWmwS922nNnOrTM';
           
            $googleResponse=file_get_contents($googleVerificationUrl.'?secret='.$googleSec.'&response='.$gcaptcha);
            $googleResponseObj = json_decode($googleResponse);
            if($googleResponseObj->success !== true)
            {
                Session::flash('kontakt_recaptcha', 'Błąd weryfikacji recaptcha');
            }  else {

        $data = array('contactName' => $contactName, 'contactEmail' => $contactEmail, 'contactMessage' => $contactMessage, 'contactUrl' => $currentUrl);
        Mail::send('frontend.emails.kontakt', $data, function ($message) use ($contactEmail, $contactName) {
            $message->from(env('MAIL_USERNAME'), 'Wiadomosc High5.com.pl | kontakt od '.$contactName);

            if (!empty(env('EMAILS_ARRAY'))) {
                // echo "TEST";
                // echo "T:".count(explode(',', env('EMAILS_ARRAY')));
                // exit();
                $message->to(env('MAIL_USERNAME'), 'Biuro')->subject('Wiadomosc z High5.com.pl | Kontakt')->cc($contactEmail)->cc(explode(',', env('EMAILS_ARRAY')));
            } else {
                $message->to(env('MAIL_USERNAME'), 'Biuro')->subject('Wiadomosc z High5.com.pl | Kontakt')->cc($contactEmail);
            }
        });
        Session::flash('kontakt', 'Wiadomosc została wysłana');
        }

        return redirect()->route('kontakt');
    }

    // FIRMA > TRENERZY > WIDOK TRENERA
    public function instruktor($id)
    {
        $meta = new MetaController();
        $instruktorzy = DB::table('h5_instructor')->where('instructorID', $id)->get();

        return view('frontend.cms.firma.trenerzy.instruktor', compact('instruktorzy'))->with('meta', $meta->MetaCore(4, null));
    }

    //----------------------------------------------------
    // WIDOK
    // REFERENCJE
    //----------------------------------------------------

    // REFERENCJE > FIRMY DLA KTORYCH PRACOWALISMY
    public function referencje_firmy()
    {
        $meta = new MetaController();
        $branza = [
            '1' => 'Przemysł/produkcja',
            '2' => 'Uslugi',
            '3' => 'Administracja publiczna',
            '4' => 'Finanse',
            '5' => 'FMCG',
            '6' => 'Inne',
        ];

        $klienci = DB::table('crm_klienci')->where('referencje', '=', '1')->OrderBy('branza', 'asc')->select('nazwa', 'branza')->get();

        $grouped = $klienci->groupBy('branza');
        $loga = scandir(Config::get('url').'img/frontend/logo');

        return view('frontend.cms.referencje.index', compact('loga', 'klienci', 'grouped', 'branza'))->with('meta', $meta->MetaCore(4, null));
    }

    // REFERENCJE > OPINIE UCZESTNIKOW
    public function referencje_osoby()
    {
        $meta = new MetaController();
        $referencje = DB::table('h5_references')->where('type', 'p')->OrderBy('firma', 'asc')->get();

        return view('frontend.cms.referencje.osoby', compact('referencje'))->with('meta', $meta->MetaCore(4, null));
    }

    //REFERENCJE > LISTY REFERENCYJNE
    public function referencje_listy()
    {
        $meta = new MetaController();

        $references_list = DB::table('h5_references_list')->OrderBy('zawiera_pdf', 'asc')->OrderBy('rok', 'desc')->get();

        return view('frontend.cms.referencje.listy', compact('references_list'))->with('meta', $meta->MetaCore(4, null));
    }

    // FIRMA > VOUCHER
    public function voucher()
    {
        $meta = new MetaController();

        return view('frontend.cms.firma.voucher')->with('meta', $meta->MetaCore(4, null));
    }
}
