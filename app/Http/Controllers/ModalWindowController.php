<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\backend\CreateModalWindowRequest;
use Session;

class ModalWindowController extends Controller
{
    public function index()
    {
        $ModalWindow = DB::table('h5_modal_window')->get();

        return view('backend.modalwindow.index', compact('ModalWindow'));
    }

    public function create()
    {
        return view('backend.modalwindow.create');
    }

    public function edit($id)
    {
        $ModalWindow = DB::table('h5_modal_window')->where('modal_windowID', $id)->get();

        return view('backend.modalwindow.edit', compact('ModalWindow'));
    }

    public function update(CreateModalWindowRequest $request, $id)
    {
        $updateModal = DB::table('h5_modal_window')->where('modal_windowID', $id);

        if (!empty($request->file('modal_img'))) {
            $image = $request->file('modal_img');

            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

            $destinationPath = storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'frontend'.DIRECTORY_SEPARATOR.'modal';

            //$destinationPath = public_path('../img/frontend/modal');
            $image->move($destinationPath, $input['imagename']);

            $img_modal = $input['imagename'];
        } else {
            $img_modal = '';
        }

        if (!empty($request->input('modal_active'))) {
            $aktywny = '1';
        } else {
            $aktywny = '0';
        }

        if (!empty($request->input('modal_timer'))) {
            $timer = '1';
        } else {
            $timer = '0';
        }

        $updateModal->update([
            'modal_url' => $request->input('modal_url'),
            'modal_active' => $aktywny,
            'modal_timer' => $timer,
            'modal_name' => $request->input('modal_name'),
            'modal_desc' => $request->input('modal_desc'),
            'modal_date_from' => $request->input('modal_date_from'),
            'modal_date_to' => $request->input('modal_date_to'),
        ]);

        if ($img_modal != '') {
            $updateModal->update([
                'modal_img' => $img_modal,
            ]);
        }

        Session::flash('success', 'Zapisano pomyslnie.');

        return redirect()->route('editModalWindow', ['id' => $id]);
    }

    public function store(CreateModalWindowRequest $request)
    {
        if (!empty($request->file('modal_img'))) {
            $image = $request->file('modal_img');

            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/img/frontend/modal');
            $image->move($destinationPath, $input['imagename']);

            $img_modal = $input['imagename'];
        } else {
            $img_modal = '';
        }

        if (!empty($request->input('modal_timer'))) {
            $timer = '1';
        } else {
            $timer = '0';
        }

        if (!empty($request->input('modal_active'))) {
            $aktywny = '1';
        } else {
            $aktywny = '0';
        }
        $headline = DB::table('h5_modal_window')->insert([
            'modal_url' => $request->input('modal_url'),
            'modal_img' => $img_modal,
            'modal_active' => $aktywny,
            'modal_timer' => $timer,
            'modal_name' => $request->input('modal_name'),
            'modal_desc' => $request->input('modal_desc'),
            'modal_date_from' => $request->input('modal_date_from'),
            'modal_date_to' => $request->input('modal_date_to'),
        ]);

        Session::flash('success', 'Dodano pomyslnie.');

        $id = DB::getPdo()->lastInsertId();

        return redirect()->route('editModalWindow', ['id' => $id]);
    }

    public static function showFrontendModal()
    {
        $getFrontendModal = DB::table('h5_modal_window')->where('modal_active', '1')->orderBy('modal_windowID', 'desc')->get();

        return view('frontend.main.modal', compact('getFrontendModal'));
    }

    public function show($id)
    {
        $getFrontendModal = DB::table('h5_modal_window')->where('modal_windowID', $id)->get();

        return view('frontend.main.modal', compact('getFrontendModal'));
    }

    public function destroy($id)
    {
        $deleteModal = DB::table('h5_modal_window')->where('modal_windowID', $id);
        $deleteModal->delete();

        return redirect()->route('indexModalWindow');
    }
}
