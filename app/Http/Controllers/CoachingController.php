<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class CoachingController extends Controller
{
    //----------------------------------------------------
    // WIDOK
    // COACHING
    //----------------------------------------------------

    // USŁUGI > COACHING
    public function index()
    {
        $meta = new MetaController();
        $getSzkolenia = DB::table('h5_categories')->pluck('categoryID')->all();
        $categories_szkolenia = DB::table('h5_categories')->whereIn('categoryID', $getSzkolenia)->select()->get();

        return view('frontend.cms.uslugi.coaching.index', compact('categories_szkolenia'))->with('meta', $meta->MetaCore(2, null));
    }
}
