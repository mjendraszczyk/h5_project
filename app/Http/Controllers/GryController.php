<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class GryController extends Controller
{
    //----------------------------------------------------
    // WIDOK
    // GRY
    //----------------------------------------------------

    // USŁUGI > GRY
    public function index()
    {
        $meta = new MetaController();
        $gry_lista = DB::table('h5_gry')->get();

        return view('frontend.cms.uslugi.gry.index', compact('gry_lista'))->with('meta', $meta->MetaCore(2, null));
    }

    // USŁUGI > GRY > WIDOK GRY
    public function gra($id)
    {
        $meta = new MetaController();

        $gry_lista = DB::table('h5_gry')->get();
        $gra_info = DB::table('h5_gry')->where('graID', $id)->get();

        return view('frontend.cms.uslugi.gry.gra', compact('gra_info', 'gry_lista'))->with('meta', $meta->MetaCore(2, null));
    }
}
