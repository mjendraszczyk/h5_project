<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTerminyRequest;
use Illuminate\Support\Facades\DB;
use Session;
use Mail;
use Route;
use Storage;

use App\Http\Controllers\backend\CRMFakturyController;

class TerminyController extends Controller
{
    protected $lastIdKlienci;
    protected $lastUczestnikID;

    public $zmianaDanychFirma = '0';

    public $OldNazwaFirmy;
    public $OldKodFirmy;
    public $OldAdresFirmy;
    public $OldMiastoFirmy;

    public $GusNazwaFirmy;
    public $GusKodFirmy;
    public $GusAdresFirmy;
    public $GusMiastoFirmy;

    public function getErrorNIP($request)
    {
        Session::flash('termin_zapis_fail', 'Podany NIP jest niepoprawny');

        return back()->withInput()->with('request', $request);
    }

    public function dodajKlienta($terminFirmaNazwa, $terminFirmaMiasto, $terminFirmaKod, $terminFirmaUlica, $nip, $terminOzPhone)
    {
        // Sprawdzenie czy klient jest w bazie
        $checkClient = DB::table('crm_klienci')->where('nip', $nip)->count();
        if ($checkClient == 0) {
            //---------------------------------------
            //Dodaj klienta zamawiajacego szkolenie   (FIRMA)
            //---------------------------------------
            DB::table('crm_klienci')->insert([
                                'nazwa' => $terminFirmaNazwa,
                                'miasto' => $terminFirmaMiasto,
                                'kod' => $terminFirmaKod,
                                'adres' => $terminFirmaUlica,
                                'nip' => $nip,
                                'tel' => $terminOzPhone,
                                'last_minute' => '0',
                                'typ_newslettera' => '0',
                                'wazne' => '',
                            ]);

            $this->lastIdKlienci = DB::getPdo()->lastInsertId();
        }
        // Klient w bazie istnieje
        else {
            $getClient = DB::table('crm_klienci')->where('nip', $nip)->pluck('klientID', 'nip')->toArray();
            $this->lastIdKlienci = $getClient[$nip];
            // dla istniejacych klientow porownaj dane

            $getClientDetail = DB::table('crm_klienci')->where('nip',$nip)->get();
            $nazwa = $getClientDetail[0]->nazwa;
            $miasto = $getClientDetail[0]->miasto;
            $kod = $getClientDetail[0]->kod;
            $adres = $getClientDetail[0]->adres;

            if(($nazwa != $terminFirmaNazwa) 
            || ($miasto != $terminFirmaMiasto) 
            || ($kod != $terminFirmaKod) 
            || ($adres != $terminFirmaUlica) 
            )
            {
                // tu wysylamy maila ;) 
                $this->zmianaDanychFirma = '1';

                $this->OldNazwaFirmy = $nazwa;
                $this->OldKodFirmy = $kod;
                $this->OldAdresFirmy = $adres;
                $this->OldMiastoFirmy = $miasto;
            }

        }
    }

    public function dodajUczestnika($lastIdKlienci, $parentID, $terminOzImie, $terminOzEmail, $terminOzPhone, $newsletter, $ustawHr, $regulamin_zgoda, $dane_zgoda, $marketing_zgoda)
    {
        DB::table('crm_osoby')->insert([
                                    'klientID' => $lastIdKlienci,
                                    'parentID' => $parentID, //$parentID
                                    'imie_nazwisko' => $terminOzImie,
                                    'email' => $terminOzEmail,
                                    'user_phone' => $terminOzPhone,
                                    'user_cell' => '',
                                    'newsletter' => '1',
                                    'newsletter' => $newsletter == '1' ? '1' : '0',
                                    'punkty' => '0',
                                    'nowy' => '1',
                                    'hr' => $ustawHr,
                                    'ostatni_kontakt' => date('Y-m-d'),
                                    'nastepny_kontakt' => date('Y-m-d'),
                                    'regulamin_zgoda' => $regulamin_zgoda == 'yes' ? '1' : '0',
                                    'dane_zgoda' => $dane_zgoda == '1' ? '1' : '0',
                                    'marketing_zgoda' => $marketing_zgoda == '1' ? '1' : '0',
                                ]);

        // Za kazdym razem pobieraj id dodanego uczestnika
        $this->lastUczestnikID = DB::getPdo()->lastInsertId();
    }

    public function dodajProgram($publiczne, $parentID, $terminSzkolenie, $lastUczestnikID, $uwagi)
    {
        $publiczne = ($publiczne == 'yes' ? '1' : '0');

        // Sprawdź kto dodał uczestnika ??
        if ($parentID == 0) {
            $whoAddedUczestnik = $lastUczestnikID;
        } else {
            $whoAdded = DB::table('crm_osoby')->where([['uczestnikID', $lastUczestnikID], ['parentID', '0']])
                        ->pluck('uczestnikID', 'parentID')->toArray();

            $whoAddedUczestnik = $parentID;
        }

        // Sprwadz ile jest modułów w programie rozwojowym
        $getProgramModule = DB::table('h5_programy')->join('h5_programy_moduly', 'h5_programy_moduly.programID', 'h5_programy.programID')
                        ->where('h5_programy.programID', $terminSzkolenie)->count(); // np 7 modulow

        // Pobierz terminy dla modułów w programie rozwojowym

        $getTermProgramModule = DB::table('h5_programy')->join('h5_programy_moduly', 'h5_programy_moduly.programID', 'h5_programy.programID')
                        ->join('h5_terms', 'h5_terms.trainingID', 'h5_programy_moduly.trainingID')
                        ->where([['h5_programy.programID', $terminSzkolenie], ['h5_terms.term_start', '>', date('Y-m-d')]])
                        ->orderBy('h5_terms.term_start', 'asc')->get();

        foreach ($getTermProgramModule as $saveProgramModule) {
            DB::table('crm_szkolenia')->insert([
                                        'szkolenieID' => $saveProgramModule->trainingID,
                                        'uczestnikID' => $lastUczestnikID, // ?? id uczestnika bioracego udzial w danym szkoleniu
                                        'termID' => $saveProgramModule->termID,
                                        'parentID' => $whoAddedUczestnik, //parentID // id uczestnika zglaszajacego ??
                                        'stan' => '1', // rodzaj szkolenia jego status ? ??
                                        'komentarz' => $uwagi, // jesli proforma mozna dodac informacje do pola uwagi
                                        'data_zgloszenia' => date('Y-m-d'),
                                        'publiczne' => $publiczne,
                                    ]);
        }
    }

    public function dodajSzkolenie($publiczne, $parentID, $terminSzkolenie, $lastUczestnikID, $terminTermin, $uwagi)
    {
        $publiczne = ($publiczne == 'yes' ? '1' : '0');

        // Sprawdź kto dodał uczestnika ??
        if ($parentID == 0) {
            $whoAddedUczestnik = $lastUczestnikID;
        } else {
            $whoAdded = DB::table('crm_osoby')->where([['uczestnikID', $lastUczestnikID], ['parentID', '0']])
                        ->pluck('uczestnikID', 'parentID')->toArray();

            $whoAddedUczestnik = $parentID;
        }

        // Dodaje pojedyńcze szkolenia
        if (Route::currentRouteName() == 'zapisz_terminy') {
            //Dodaj terminy w postaci sesji
            $checkTerminySesja = DB::table('h5_terms')->where('h5_terms.termParentID', $terminTermin)->count();

            //Jeśli termin występuje w postaci sesji
            if ($checkTerminySesja > 0) {
                $getTerminySesja = DB::table('h5_terms')->where('h5_terms.termParentID', $terminTermin)->pluck('termID')->toArray();

                for ($dSesja = 0; $dSesja < count($getTerminySesja); ++$dSesja) {
                    DB::table('crm_szkolenia')->insert([
                                    'szkolenieID' => $terminSzkolenie,
                                    'uczestnikID' => $lastUczestnikID, //======> ?? id uczestnika bioracego udzial w danym szkoleniu
                                    'termID' => $getTerminySesja[$dSesja],
                                    'parentID' => $whoAddedUczestnik, //=======> parentID // id uczestnika zglaszajacego ??
                                    'stan' => '1', // rodzaj szkolenia jego status ? ??
                                    'komentarz' => $uwagi, // jesli proforma mozna dodac informacje do pola uwagi
                                    'data_zgloszenia' => date('Y-m-d'),
                                    'publiczne' => $publiczne,
                                ]);
                }
            } else {
                // Terminy zwykłe
                DB::table('crm_szkolenia')->insert([
                                    'szkolenieID' => $terminSzkolenie,
                                    'uczestnikID' => $lastUczestnikID, //======> ?? id uczestnika bioracego udzial w danym szkoleniu
                                    'termID' => $terminTermin,
                                    'parentID' => $whoAddedUczestnik, //=======> parentID // id uczestnika zglaszajacego ??
                                    'stan' => '1', // rodzaj szkolenia jego status ? ??
                                    'komentarz' => $uwagi, // jesli proforma mozna dodac informacje do pola uwagi
                                    'data_zgloszenia' => date('Y-m-d'),
                                    'publiczne' => $publiczne,
                                ]);
            }
        }
    }

    //----------------------------------------------------
    // WIDOK
    // TERMINY
    //----------------------------------------------------
    // TERMINY ZAPIS CORE
    public function Terminy($request, $feature, $id)
    {
        $sort = '';

        $meta = new MetaController();
        $status = 0;

        $lastCategory = '0';
        $lastTraining = '0';
        $podsumowanie = '0';

        if ($feature != 'rezerwacja') {
            $getCattrain = DB::table('h5_cattrain')->pluck('categoryID')->all();
            $cattrain = DB::table('h5_categories')->whereIn('categoryID', $getCattrain)->select()->orderBy('cat_position', 'asc')->get();
        }

        if ($feature == 'ZapiszTermin') {
            $pNip = $request->input('nip');

            
            // Sprawdzanie NIPu
            if (!empty($pNip)) {
                $weights = array(6, 5, 7, 2, 3, 4, 5, 6, 7);
                (int) $nip = preg_replace('/[\s-]/', '', $pNip);

                if (strlen($nip) >= 9) { //&& is_numeric($nip)
            if(is_string($nip)) { 
                $checkNip = 1;
            } else {
                    $sum = 0;
                    for ($i = 0; $i < 9; ++$i) {
                        $sum += $nip[$i] * $weights[$i];
                    }
                    $checkNip = ($sum % 11) == $nip[9];
                    }
    
                    // NIP poprawny - Zapisywanie terminu
                    if ($checkNip != 0) {
            
                        //Szkolenie
                        $terminSzkolenie = $request->input('szkolenie');
                        $terminTermin = $request->input('termin');
                        $terminCena = $request->input('cena');
                        $terminUwagi = $request->input('uwagi');

                        //Osoba zglaszajaca
                        $terminOzImie = $request->input('name');
                        $terminOzEmail = $request->input('email');
                        $terminOzPhone = $request->input('phone');

                        //Uczestnik
                        $terminUczImie = $request->input('username');
                        $terminUczEmail = $request->input('user_mail');
                        $terminUczPhone = $request->input('user_phone');

                        //Firma
                        $terminFirmaNip = $request->input('nip');
                        $terminFirmaNazwa = $request->input('company');
                        $terminFirmaUlica = $request->input('street');
                        $terminFirmaKod = $request->input('zip');
                        $terminFirmaMiasto = $request->input('city');

                        //Checkboxy
                        $terminCheckboxUczestnik = $request->input('uczestnik');
                        $terminCheckboxPubliczne = $request->input('publiczne');
                        $terminCheckboxAkceptacja = $request->input('akceptacja');

                        $terminSesja = $this->checkTermParent($terminTermin);

                        //Sprawdź czy klient rezerwuje termin czy program szkoleniowy
                        //1 Termin
                        if (Route::currentRouteName() == 'zapisz_terminy') {
                            $terminSzkolenieString = DB::table('h5_training')->where('trainingID', $terminSzkolenie)->pluck('training_title', 'trainingID')->toArray();

                            $terminTerminString = DB::table('h5_terms')->where('termID', $terminTermin)->pluck('term_start', 'termID')->toArray();
                        }
                        //2 Program
                        else {
                            $terminSzkolenieString = DB::table('h5_programy')->where('programID', $terminSzkolenie)->pluck('nazwa', 'programID')->toArray();
                            $terminTerminString = null;
                        }
                        //Tablica wartości dla maila
                        $data = array(
                            'terminSzkolenie' => $terminSzkolenieString[$terminSzkolenie],
                            'terminTermin' => $terminTerminString[$terminTermin],
                            'terminCena' => $terminCena,
                            'terminUwagi' => $terminUwagi,
                            'terminOzImie' => $terminOzImie,
                            'terminOzEmail' => $terminOzEmail,
                            'terminOzPhone' => $terminOzPhone,
                            'terminUczImie' => $terminUczImie,
                            'terminUczEmail' => $terminUczEmail,
                            'terminUczPhone' => $terminUczPhone,
                            'terminFirmaNip' => $terminFirmaNip,
                            'terminFirmaNazwa' => $terminFirmaNazwa,
                            'terminFirmaUlica' => $terminFirmaUlica,
                            'terminFirmaKod' => $terminFirmaKod,
                            'terminFirmaMiasto' => $terminFirmaMiasto,
                            //Dodaj dodatkowe terminy
                            'terminSesja' => $this->checkTermParent($terminTermin),
                        );

                        //Mail dla osoby i firmy
                        if (!empty(env('EMAILS_ARRAY'))) {
                            Mail::send('frontend.emails.termin', $data, function ($message) use ($terminOzEmail, $terminOzImie) {
                                $message->from(env('MAIL_USERNAME'), 'Wiadomosc High5.com.pl | Rezerwacja przez '.$terminOzImie);
                                $message->to(env('MAIL_USERNAME'), 'Biuro')->subject('Wiadomosc z High5.com.pl | Rezerwacja przez '.$terminOzImie)->cc(explode(',', env('EMAILS_ARRAY'))); //->cc($terminOzEmail)
                            });
                        } else {
                            Mail::send('frontend.emails.termin', $data, function ($message) use ($terminOzEmail, $terminOzImie) {
                                $message->from(env('MAIL_USERNAME'), 'Wiadomosc High5.com.pl | Rezerwacja przez '.$terminOzImie);
                                $message->to(env('MAIL_USERNAME'), 'Biuro')->subject('Wiadomosc z High5.com.pl | Rezerwacja przez '.$terminOzImie); //->cc($terminOzEmail)
                            });
                        }

                        //------------------------------------------------------------------
                        //
                        //  DODAWANIE DANYCH DO BAZY
                        //
                        //------------------------------------------------------------------
                        //------------------------------------------------------------------
                        //  CRM_KLIENCI - Dodaję firme (klienta)
                        //------------------------------------------------------------------

                        $this->dodajKlienta($terminFirmaNazwa, $terminFirmaMiasto, $terminFirmaKod, $terminFirmaUlica, $nip, $terminOzPhone);
            
                        $gusAPI = new CRMFakturyController();
                        

                        if($this->zmianaDanychFirma == '1') {

                            $dataFirma = [
                                'OldNazwaFirmy' => $this->OldNazwaFirmy,
                                'OldMiastoFirmy' => $this->OldMiastoFirmy,
                                'OldKodFirmy' => $this->OldKodFirmy,
                                'OldAdresFirmy' => $this->OldAdresFirmy,
                                'OldNipFirmy' => $terminFirmaNip,

                                'NewNazwaFirmy' => $terminFirmaNazwa,
                                'NewMiastoFirmy' => $terminFirmaMiasto,
                                'NewKodFirmy' => $terminFirmaKod,
                                'NewAdresFirmy' => $terminFirmaUlica,
                                'NewNipFirmy' => $terminFirmaNip,

                                'GusNazwaFirmy' => '',//$gusAPI->gus_api($nip)['nazwa'],
                                'GusMiastoFirmy' => '',//$gusAPI->gus_api($nip)['miasto'],
                                'GusKodFirmy' => '',//$gusAPI->gus_api($nip)['kod'],
                                'GusAdresFirmy' => '',//$gusAPI->gus_api($nip)['ulica'],
                                'GusNipFirmy' => '',//$gusAPI->gus_api($nip)['nip'],
                            ];
            

                            Mail::send('frontend.emails.firma', $dataFirma, function ($message) use ($terminOzEmail, $terminOzImie) {
                                $message->from(env('MAIL_USERNAME'), 'Wiadomosc High5.com.pl | Zmiana danych firmy '.$this->OldNazwaFirmy);
                                $message->to(env('MAIL_USERNAME'), 'Biuro')->subject('Wiadomosc z High5.com.pl | Zmiana danych firmy '.$this->OldNazwaFirmy)->cc('asystentka03@high5.pl'); //->cc($terminOzEmail)
                            });
                        }
                        //------------------------------------------------------------------------
                        // CRM_OSOBY - Dodaję uczestników (osoby)
                        //------------------------------------------------------------------------
                        //Dodawanie uczestników

                        // Domyslnie ustaw HR na 0
                        $ustawHr = 0;

                        //--------------------
                        // Sprawdź czy os. zglaszajaca jest uczestnikiem czy nie, jesli nie to dodaj ją jako HR
                        //--------------------
                        if ($request->input('uczestnik') != 'yes') {
                            $ustawHr = 1;
                        } else {
                            if (count($terminUczImie) == 1) {
                                $ustawHr = 0;
                            } else {
                                $ustawHr = 1;
                            }
                        }

                        // Sprawdź czy taka osoba zgłaszająca jest juz w bazie

                        $checkUserOz = DB::table('crm_osoby')->join('crm_klienci', 'crm_osoby.klientID', 'crm_klienci.klientID')->where([['crm_osoby.email', 'LIKE', '%'.$terminOzEmail.'%'], ['crm_klienci.nip', $nip]])->orderBy('crm_osoby.uczestnikID', 'desc')->pluck('crm_osoby.uczestnikID');

                        //Jeśli jest osoba zgłaszająca nie istnieje w bazie
                        if (count($checkUserOz) == 0) {
                            $parentID = 0;
                            $this->dodajUczestnika($this->lastIdKlienci, $parentID, $terminOzImie, $terminOzEmail, $terminOzPhone, $request->input('dane_marketing'), $ustawHr, $terminCheckboxAkceptacja, $request->input('dane_przetwarzanie'), $request->input('dane_marketing'));

                            $parentID = $this->lastUczestnikID;
                        } else {
                            $parentID = $checkUserOz[0];

                            // Aktualizuj pole ostatni kontakt dla osoby zglaszajacej :)
                            $OsobaOZ = DB::table('crm_osoby')->where('uczestnikID', $parentID);
                            $OsobaOZ->update([
                                'ostatni_kontakt' => date('Y-m-d'),
                                'nastepny_kontakt' => date('Y-m-d'),
                            ]);
                        }

                        //Sprawdź czy osoba ta ma uczestników jeśli tak daj jej parentID=0 (zasadniczo ma ich prawie zawsze, chyba że osoba zgłaszająca wyśle forularz bez żadnego uczestnika)
//                        if (count($terminUczImie) > 0) {
//                            $parentID = 0;
//                        } else {
//                            $parentID = null;
//                        }

                        // Osoba zgłaszająca jest już w bazie
//                        if (count($checkUserOz) > 0) {
//                            // Nie dodaję jej, pomijam krok
//                        } else {
//
//
//                            // Jesli osoba zglaszajaca sama uczestniczy to juz też nie dodawaj jej do osob bo przy dodawaniu uczestników zahaczy również o osobę zgłaszającą
//                            if (($request->input('uczestnik') != 'yes')) {
//                                // [ ! ]
//                                $this->dodajUczestnika($this->lastIdKlienci, $parentID, $terminOzImie, $terminOzEmail, $terminOzPhone, $request->input('dane_marketing'), $ustawHr, $terminCheckboxAkceptacja, $request->input('dane_prztwarzanie'), $request->input('dane_marketing'));
//                            }
//                        }

                        //Dodaje uczestników szkolenia

                        for ($i = 0; $i < count($terminUczImie); ++$i) {
                            //ustawia domyslne HR = 0 przed dodawanie uczestnikow
                            $ustawHr = 0;

                            //
                            // Sprawdzam czy istnieje uczestnik w bazie z danej firmy wg imienia i nazwiska oraz NIPu
                            // $checkUser = DB::table('crm_osoby')->join('crm_klienci', 'crm_osoby.klientID', 'crm_klienci.klientID')->where([['crm_osoby.imie_nazwisko', 'LIKE', '%'.$terminUczImie[$i].'%'], ['crm_klienci.nip', $nip]])->count();

                            //$checkUser = DB::table('crm_osoby')->join('crm_klienci', 'crm_osoby.klientID', 'crm_klienci.klientID')->where([['crm_osoby.imie_nazwisko', 'LIKE', '%'.$terminUczImie[$i].'%'], ['crm_klienci.nip', $nip]])->count();

                            //Jeśli zgłaszający jest uczestnikiem
                            if ($request->input('uczestnik') == 'yes') {
                                //i zgłasza sam siebie to nie jest z HR
                                if (count($terminUczImie) == 1) {
                                    $ustawHr = 0;
                                }
                                //jeśli zgłasza siebie i innych to zakładamy że jest z HR
                                else {
                                    if ($i == 0) {
                                        $ustawHr = 1;
                                    }
                                }
                            }

                            // if (($i == 0)) {
                            // Sprawdz czy zglaszajacy istnieje w bazie jesli tak pobierz jego id jesli nie daj parentID = 0

                            // * parentID = 0
                            //!! [ !! ]

//                            $checkUserOzAfterAdd = DB::table('crm_osoby')->join('crm_klienci', 'crm_osoby.klientID', 'crm_klienci.klientID')->where([['crm_osoby.email', 'LIKE', '%'.$terminOzEmail.'%'], ['crm_klienci.nip', $nip]])->orderBy('crm_osoby.uczestnikID', 'desc')->pluck('crm_osoby.uczestnikID');
                            ////                                return "ilosc ".count($checkUserOzAfterAdd);
//                                //Zgłaszający istnieje w bazie
//                                if (count($checkUserOzAfterAdd) > 0) {
//                                    $getParentID = DB::table('crm_osoby')->join('crm_klienci', 'crm_klienci.klientID', '=', 'crm_osoby.klientID')->where([['crm_osoby.email', 'LIKE', '%'.$terminOzEmail.'%'], ['crm_klienci.nip', '=', $terminFirmaNip]])->orderBy('crm_osoby.uczestnikID', 'desc')->pluck('uczestnikID')->toArray();
//                                    //return "ilosc ".print_r($getParentID);
//
//                                } else {
//                                    $parentID = 0;
//                                }
//                            }
                            //!! @down dotąd jest OK
                            //??========

                            // Dla kolejnych uczestnikow dodaj ID zglaszajacego
//                            if (($i > 0)) {
//                                //@down dodano
//                                $getParentID = DB::table('crm_osoby')->join('crm_klienci', 'crm_klienci.klientID', '=', 'crm_osoby.klientID')->where([['crm_osoby.email', 'LIKE', '%'.$terminOzEmail.'%'], ['crm_klienci.nip', '=', $terminFirmaNip]])->orderBy('crm_osoby.uczestnikID', 'desc')->pluck('uczestnikID')->toArray();
//
//                                $parentID = $getParentID[0];
//                            }

                            //??========
                            //!! @up dotąd jest OK

                            // Jeśli uczestnik posiada mail i wyraził zgodę na przetwarzanie danych w caleach marektingowych dopisz go do newslettera

                            if (($terminUczEmail[$i] != '') && ($request->input('dane_marketing') == '1')) {
                                $newsletter = '1';
                            } else {
                                $newsletter = '0';
                            }

                            //Jeśli zgłaszający istnieje w bazie i to go nie dodawaj

//                            if (($i == 0) && (count($checkUserOz) > 0)) {
//
//                                $parentID = $checkUserOz[0];
//                                $this->lastUczestnikID = $checkUserOz[0];
//
//                                //return $this->lastUczestnikID.'+'.$parentID." ilosc ".$getParentID[0]." loop ".$i;
//                            } else {
//
//                            }
                            //Przy dodawaniu osob nie dubluj Osoby zglaszajacej i uczestnika jesli nim jest
                            if (($i == 0) && ($request->input('uczestnik') == 'yes')) {
                            } else {
                                $this->dodajUczestnika($this->lastIdKlienci, $parentID, $terminUczImie[$i], $terminUczEmail[$i], $terminUczPhone[$i], $newsletter, $ustawHr, $terminCheckboxAkceptacja, $request->input('dane_przetwarzanie'), $request->input('dane_marketing'));
                            }
                            //------------------------------------------------------------------------
                            // CRM_SZKOLENIA
                            //------------------------------------------------------------------------
                            //Dodawanie szkoleń
                            // jesli sa dodawaniu uzytkownicy to wszystko ok gorzej jak uzyje istniejacego usera i dorzuce go do szkolenia  .. .
                            if ($this->lastUczestnikID == null) {
                                $this->lastUczestnikID = $checkUserOz[0];
                            }
                            if (Route::currentRouteName() == 'zapisz_terminy') {
                                $this->dodajSzkolenie($request->input('publiczne'), $parentID, $terminSzkolenie, $this->lastUczestnikID, $terminTermin, $request->input('uwagi'));
                            }
                            if (Route::currentRouteName() == 'zapisz_program') {
                                $this->dodajProgram($request->input('publiczne'), $parentID, $terminSzkolenie, $this->lastUczestnikID, $request->input('uwagi'));
                            }
                        }

                        //  $this->SaveLogs($this->lastIdKlienci, $terminOzEmail, $terminUczImie);

                        // Dodawanie szkolenia

                        Session::flash('termin_zapis_succ', 'Dziękujemy za wysłanie zgłoszenia. Postaramy się skontaktować najszybciej jak to będzie możliwe');

                        if ((Route::currentRouteName() == 'zapisz_terminy') || (Route::currentRouteName() == 'zapisz_program')) {
                            $podsumowanie = '1';
                            $meta = $meta->MetaCore(2, $request->input('szkolenie'));

                            $terminSzkolenie = $terminSzkolenieString[$terminSzkolenie];

                            $terminTermin = $terminTerminString[$terminTermin];

                            return view('frontend.cms.terminy.rezerwacja.index', compact('terminSzkolenie', 'terminTermin', 'terminCena', 'terminUwagi', 'terminOzImie', 'terminOzEmail', 'terminOzPhone', 'terminUczImie', 'terminUczEmail', 'terminUczPhone', 'terminFirmaNazwa', 'terminFirmaUlica', 'terminFirmaKod', 'terminFirmaMiasto', 'terminFirmaNip', 'podsumowanie', 'terminSesja', 'meta'));
                        }
                    }
                    // NIP niepoprawny
                    else {
                        $this->getErrorNIP($request->all());
                    }
                } else {
                    $this->getErrorNIP($request->all());
                }
            } else {
                $this->getErrorNIP($request->all());
            }
        }

        if (($feature == 'home') || ($feature == 'katalog_szkolen')) :

            $terminy = DB::table('h5_categories')->join(
                            'h5_cattrain', 'h5_cattrain.categoryID', '=', 'h5_categories.categoryID'
                    )->join(
                            'h5_training', 'h5_training.trainingID', '=', 'h5_cattrain.trainingID'
                    )->join(
                            'h5_terms', 'h5_terms.trainingID', '=', 'h5_training.trainingID'
                    )->where([['h5_terms.term_start', '>', date('Y-m-d')], ['h5_terms.term_type', '=', '1'], ['h5_terms.term_closed', '!=', 'y']])->whereRaw('(h5_terms.termParentID = "0" OR h5_terms.termParentID = h5_terms.termID)')->OrderBy('h5_cattrain.categoryID', 'asc')->OrderBy('h5_training.trainingID', 'asc')->OrderBy('h5_terms.term_start', 'asc')->get();

        if ($feature == 'katalog_szkolen') :

        else :
                return view('frontend.cms.terminy.index', compact('terminy', 'kategorie', 'szkolenia', 'cattrain', 'lastCategory', 'lastTraining'))->with('meta', $meta->MetaCore(2, null))->with('sort', 'cat');
        endif;

        endif;
        if ($feature == 'category') :

            $terminy = DB::table('h5_categories')->join(
                            'h5_cattrain', 'h5_cattrain.categoryID', '=', 'h5_categories.categoryID'
                    )->join(
                            'h5_training', 'h5_training.trainingID', '=', 'h5_cattrain.trainingID'
                    )->join(
                            'h5_terms', 'h5_terms.trainingID', '=', 'h5_training.trainingID'
                    )->where([['h5_terms.term_start', '>', date('Y-m-d')], ['h5_terms.term_type', '=', '1'], ['h5_terms.term_closed', '!=', 'y'], ['h5_cattrain.categoryID', $id]])->whereRaw('(h5_terms.termParentID = "0" OR h5_terms.termParentID = h5_terms.termID)')->OrderBy('h5_cattrain.categoryID', 'asc')->OrderBy('h5_training.trainingID', 'asc')->OrderBy('h5_terms.term_start', 'asc')->get();

        return view('frontend.cms.terminy.index', compact('terminy', 'kategorie', 'szkolenia', 'cattrain', 'lastCategory', 'lastTraining'))->with('meta', $meta->MetaCore(2, null))->with('sort', '');

        endif;
        if ($feature == 'chrono') :

            $terminy = DB::table('h5_categories')->join(
                            'h5_cattrain', 'h5_cattrain.categoryID', '=', 'h5_categories.categoryID'
                    )->join(
                            'h5_training', 'h5_training.trainingID', '=', 'h5_cattrain.trainingID'
                    )->join(
                            'h5_terms', 'h5_terms.trainingID', '=', 'h5_training.trainingID'
                    )->where([['h5_terms.term_start', '>', date('Y-m-d')], ['h5_terms.term_type', '=', '1'], ['h5_terms.term_closed', '!=', 'y']])->whereRaw('(h5_terms.termParentID = "0" OR h5_terms.termParentID = h5_terms.termID)')->OrderBy('h5_terms.term_start', 'asc')->get(); //->OrderBy('training.training_title','asc')->orderBy('cattrain.categoryID','asc')->get();

        $lastWeek = '0';

        return view('frontend.cms.terminy.index', compact('terminy', 'kategorie', 'szkolenia', 'cattrain', 'lastCategory', 'lastTraining', 'lastWeek'))->with('meta', $meta->MetaCore(2, null))->with('sort', 'chrono');
        endif;
        if (($feature == 'rezerwacja') || ($feature == 'rezerwacja_online')){
            $id_category = array();
            $id_categories = DB::table('h5_cattrain')->where('trainingID', $id)->pluck('categoryID')->toArray();
            foreach($id_categories as $category) {
                $id_category[] = $category;
            }
            $getSzkolenie = DB::table('h5_training')->where('h5_training.trainingID',$id)->first();

            $rezerwacje = DB::table('h5_training')->join('h5_terms', 'h5_terms.trainingID', 'h5_training.trainingID')->where([['h5_terms.term_start', '>=', date('Y-m-d')], ['h5_training.training_price', '!=', '0'], ['h5_terms.trainingID', 'h5_training.trainingID']])->OrderBy('h5_training.training_title', 'asc')->get();

            // Jesli szkolenie jest z kategori online
            if(in_array("21", $id_category)) {
        $szkolenia = DB::table('h5_training')->join('h5_cattrain','h5_cattrain.trainingID','h5_training.trainingID')->join('h5_terms', 'h5_terms.trainingID', 'h5_training.trainingID')->where([['h5_terms.term_start', '>=', date('Y-m-d')], ['h5_training.modul_programu', '!=', '1'], ['h5_terms.term_closed', '!=', 'y'],['h5_cattrain.categoryID','21']])
        ->orderBy('h5_training.training_title', 'asc')
        ->pluck('h5_training.training_title', 'h5_training.trainingID')
        ->toArray();

        $terminy = DB::table('h5_training')->join('h5_cattrain','h5_cattrain.trainingID','h5_training.trainingID')->join('h5_terms', 'h5_terms.trainingID', 'h5_training.trainingID')->where([['h5_terms.trainingID', $id], ['h5_terms.term_start', '>=', date('Y-m-d')], ['h5_terms.term_closed', '!=', 'y'],['h5_cattrain.categoryID','21']])->whereRaw('(h5_terms.termParentID = "0" OR h5_terms.termParentID = h5_terms.termID)')->orderBy('h5_terms.term_start')->pluck('h5_terms.term_start', 'h5_terms.termID')->toArray();
            } else {
                // Kazde inne szkolenie
                // Tu w momencie gdy typ szkolenia to szkolenie online
                if($feature == 'rezerwacja_online') {

                    $szkolenia = DB::table('h5_training')->join('h5_terms', 'h5_terms.trainingID', 'h5_training.trainingID')->where([['h5_terms.term_start', '>=', date('Y-m-d')], ['h5_training.modul_programu', '!=', '1'], ['h5_terms.term_closed', '!=', 'y'],['h5_training.training_type','!=','1']])
        ->orderBy('h5_training.training_title', 'asc')
        ->pluck('h5_training.training_title', 'h5_training.trainingID')
        ->toArray();

        $terminy = DB::table('h5_training')->join('h5_terms', 'h5_terms.trainingID', 'h5_training.trainingID')->where([['h5_terms.trainingID', $id], ['h5_terms.term_start', '>=', date('Y-m-d')], ['h5_terms.term_closed', '!=', 'y']])->whereRaw('((h5_terms.termParentID = "0" OR h5_terms.termParentID = h5_terms.termID) AND (h5_terms.term_training_type = "2"))')->orderBy('h5_terms.term_start')->pluck('h5_terms.term_start', 'h5_terms.termID')->toArray();

                } else {
                    // Kazdy inny wariant
        $szkolenia = DB::table('h5_training')->join('h5_terms', 'h5_terms.trainingID', 'h5_training.trainingID')->where([['h5_terms.term_start', '>=', date('Y-m-d')], ['h5_training.modul_programu', '!=', '1'], ['h5_terms.term_closed', '!=', 'y']])
        ->orderBy('h5_training.training_title', 'asc')
        ->pluck('h5_training.training_title', 'h5_training.trainingID')
        ->toArray();

        $terminy = DB::table('h5_training')->join('h5_terms', 'h5_terms.trainingID', 'h5_training.trainingID')->where([['h5_terms.trainingID', $id], ['h5_terms.term_start', '>=', date('Y-m-d')], ['h5_terms.term_closed', '!=', 'y']])->whereRaw('(h5_terms.termParentID = "0" OR h5_terms.termParentID = h5_terms.termID)')->orderBy('h5_terms.term_start')->pluck('h5_terms.term_start', 'h5_terms.termID')->toArray();
                }
            }

            
            if($feature == 'rezerwacja_online') {
                return view('frontend.cms.terminy.rezerwacja.index_online', compact('getSzkolenie','rezerwacje', 'szkolenia', 'id', 'terminy', 'getTerms', 'getSzkolenia', 'status', 'podsumowanie'))->with('meta', $meta->MetaCore(2, null))->with('id_category', $id_category);
            } else{
                return view('frontend.cms.terminy.rezerwacja.index', compact('getSzkolenie','rezerwacje', 'szkolenia', 'id', 'terminy', 'getTerms', 'getSzkolenia', 'status', 'podsumowanie'))->with('meta', $meta->MetaCore(2, null))->with('id_category', $id_category);
            }
        }
    }

    // LOGI
    public function SaveLogs($lastIdKlienci, $terminOzEmail, $terminUczImie)
    {
        // Dodanie logu zapisu
        // #nr | data | klientID | ZglaszajacyID |UczestnikID (forach) | <przegladarka> | URL
        $log = Storage::get('h5_zapisy_log.txt');

        $log .= "//#############################################################\n";
        $log .= "Data|KlientID|ZglaszajacyMail|Przegladarka|URL|IP|UczestnikName\n";
        $log .= "//#############################################################\n";
        $log .= date('d/m/Y H:i:s').'|'.$lastIdKlienci.'|'.$terminOzEmail.'|'.$_SERVER['HTTP_USER_AGENT'].'|'.$_SERVER['REQUEST_URI'].'|'.$_SERVER['REMOTE_ADDR']."\n";
        for ($dlog = 0; $dlog < count($terminUczImie); ++$dlog) {
            $log .= ($dlog + 1).')'.$terminUczImie[$dlog];
        }
        $log .= "\n//------ \n";

        Storage::disk('local')->put('h5_zapisy_log.txt', $log);
    }

    // TERMINY
    public function index()
    {
        return $this->Terminy(null, 'home', null);
    }

    // TERMINY > SORT > CHRONO > ZAKRES DNI
    public function week_range($date)
    {
        $miesiace2 = [
            '01' => 'Styczeń',
            '02' => 'Luty',
            '03' => 'Marzec',
            '04' => 'Kwiecień',
            '05' => 'Maj',
            '06' => 'Czerwiec',
            '07' => 'Lipiec',
            '08' => 'Sierpień',
            '09' => 'Wrzesień',
            '10' => 'Październik',
            '11' => 'Listopad',
            '12' => 'Grudzień',
        ];

        $ts = strtotime($date);
        $start = strtotime('monday this week', $ts);
        $end = strtotime('sunday this week-1', $ts);
        $mstart = $miesiace2[date('m', $start)];
        $mend = $miesiace2[date('m', $end)];

        if ($mstart != $mend) {
            $dateRange = array(date('d', $start).' '.$mstart, ' - '.date('d', $end).' '.$mend);

            return $dateRange;
        } else {
            $dateRange = array(date('d', $start), ' - '.date('d', $end).' '.$mend);

            return $dateRange;
        }
    }

    // TERMINY > SORTOWOANIE > CHRONO
    public function chrono()
    {
        return $this->Terminy(null, 'chrono', null);
    }

    // TERMINY > REZERWACJA
    public function rezerwacja($id)
    {
        return $this->Terminy(null, 'rezerwacja', $id);
    }
    // TERMINY > REZERWACJA ONLINE
    public function rezerwacja_online($id)
    {
        return $this->Terminy(null, 'rezerwacja_online', $id);
    }

    // TERMINY > REZERWACJA > ZAPISZ
    public function zapisz(CreateTerminyRequest $request)
    {
        return $this->Terminy($request, 'ZapiszTermin', null);
    }

    // TERMINY > KATEGORIA
    public function kategoria($id)
    {
        return $this->Terminy(null, 'category', $id);
    }

    // TERMINY > SORTOWANIE (WG KATEGORII LUB CHRONOLOGICZNIE)
    public function sort($sort)
    {
        if ($sort == 'cat') :
            return $this->index(); else :
            return $this->chrono();
        endif;
    }

    // TERMINY > POTWIERDZENIE
    public function potwierdzenie($id)
    {
        return redirect()->route('homepage');
    }

    //TERMINY > TERMINY API (Pobieranie terminow wg szkolenia)
    public function api_terminy($miasto)
    {
        $latestTerms = DB::table('h5_training')->
                        join('h5_terms', 'h5_training.trainingID', 'h5_terms.trainingID')
                        ->where([['h5_terms.term_start', '>', date('Y-m-d H:i:s')], ['h5_terms.term_type', '1'], ['h5_terms.term_place', 'LIKE', '%Warszawa%']])->orderBy('h5_terms.term_start', 'desc')->limit(10)->get();

        return $latestTerms->toArray();
    }

    // TERMINY > NIP API (sprawdzanie nipu w bazie)
    public function api_nip($nip)
    {
        $check_nip = DB::table('crm_klienci')->where('crm_klienci.nip', $nip)->get();

        return $check_nip->toArray();
    }

    // TERMINY > KATALOG SZKOLEŃ
    public function katalog()
    {
        return $this->Terminy(null, 'katalog_szkolen', null);
    }

    //Tworzenie XML do MS Sharepoint
    public function callendar_xml()
    {
        header('Content-Type: application/xml');

        $return = '';
        $return .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        $return .= "<urlset xmlns=\"http://www.w3.org/TR/html4/\">\n";

        $data_start = date('Y-m-d', strtotime('last Monday'));

        $query_rsTerms = DB::table('h5_terms')->join('h5_training', 'h5_training.trainingID', '=', 'h5_terms.trainingID')->where([['h5_terms.term_start', '>=', $data_start], ['h5_terms.term_state', '=', '2']])->OrderBy('h5_terms.term_start', 'asc')->get();

        foreach ($query_rsTerms as $reTerm) :
            $return .= "<url>\n";
        $return .= '<termID>'.$reTerm->termID."</termID>\n";
        $return .= '<startdate>'.$reTerm->term_start."</startdate>\n";
        $return .= '<enddate>'.$reTerm->term_end."</enddate>\n";
        $return .= '<training>'.$reTerm->training_title."</training>\n";
        $return .= '<termID>'.$reTerm->termID."</termID>\n";

        $getLudzie = DB::table('crm_szkolenia')->where('termID', '=', $reTerm->termID)->count();

        $return .= '<peoples>'.$getLudzie."</peoples>\n";

        $return .= '<place>'.$reTerm->term_place."</place>\n";
        $return .= '<type>'.$reTerm->term_type."</type>\n";
        $return .= '<week>'.date('W', strtotime($reTerm->term_start))."</week>\n";

        $return .= "</url>\n";

        endforeach;

        $return .= "</urlset>\n";
        echo $return;
    }

    public function zgoda_dane($email)
    {
        // jezeli email istnieje w bazie to aktualizuj zgode jesli nie to przekieruj na glowna

        $checkMail = DB::table('crm_osoby')->where('email', $email)->count();

        if ($checkMail > 0) :

                DB::table('crm_osoby')->where('email', $email)->update(['marketing_zgoda' => '1']);
        DB::table('crm_osoby')->where('email', $email)->update(['newsletter' => '1']);

        echo 'Dziękujemy za wyrażenie zgody na przetwarzanie danych w celach marketingowych. Za chwile nastąpi przekierowanie...';
        sleep(3);

        return redirect()->route('homepage'); else:
            echo 'Podany email:'.$email.' nie istnieje w naszej bazie. Za chwile nastąpi przekierowanie...';
        sleep(3);

        return redirect()->route('homepage');
        endif;
    }

    //Sprawdzanie połączeń terminów
    public function checkTermParent($id)
    {
        $checkTermParent = DB::table('h5_terms')->where('termParentID', $id)->count();

        if ($checkTermParent > 1) {
            $getTermParent = DB::table('h5_terms')->where('termParentID', $id)->orderBy('term_start', 'asc')->get();
            $dataTerm = '';
            foreach ($getTermParent as $parentTerm) {
                $dataTerm .= $parentTerm->term_start.' - '.$parentTerm->term_end.', '.$parentTerm->term_place.'<br/> ';
            }

            return 'To szkolenie jest w formie sesji, odbędzie się w terminach:<br/>'.$dataTerm;
        } else {
            // return 'Brak dodatkowych terminów'.$checkTermParent;
        }
    }
}
