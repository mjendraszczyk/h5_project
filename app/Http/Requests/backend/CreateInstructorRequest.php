<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateInstructorRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
              'instructor_photo' => 'image|mimes:jpg,png,jpeg|max:5000',
              'instructor_name' => 'required',
            'instructor_lid' => 'required',
            'instructor_desc' => 'required',
        ];
    }
}
