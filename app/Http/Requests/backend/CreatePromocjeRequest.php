<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class CreatePromocjeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
         'news_title' => 'required',
            'news_lid' => 'required',
            'news_link' => 'required',
            'news_picture' => 'image|mimes:jpg,png,jpeg|max:5000',
            //'aktywna' => 'required',
        ];
    }
}
