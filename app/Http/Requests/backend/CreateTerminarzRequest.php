<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateTerminarzRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
         'training_title' => 'required',
            'term_start' => 'required',
            'term_end' => 'required',
            'term_place' => 'required',
            'term_instructor' => 'required',
            'term_fdays' => 'required',
            'term_state' => 'required',
            //'aktywna' => 'required',
        ];
    }
}
