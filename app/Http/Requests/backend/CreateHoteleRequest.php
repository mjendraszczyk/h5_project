<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateHoteleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'zdjecie1' => 'image|mimes:jpg,png,jpeg|max:5000',
            'nazwa' => 'required',
            'miasto' => 'required',
            'kod' => 'required',
            'ulica' => 'required',
            'telefon' => 'required',
            'odleglosc_od' => 'required',
            'cena_pokoj_jedno' => 'required',
            'uwagi' => 'required',
        ];
    }
}
