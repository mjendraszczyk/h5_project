<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateSzkoleniaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
              'training_title' => 'required',
              'training_lid' => 'required',
            'training_advantage' => 'required',
            'training_program' => 'required',
            'training_price' => 'required',
            'training_hours' => 'required',
        ];
    }
}
