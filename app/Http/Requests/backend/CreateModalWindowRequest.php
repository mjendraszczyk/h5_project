<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateModalWindowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'modal_url' => 'required',
            'modal_img' => 'image|mimes:jpg,png,jpeg|max:10000',
            'modal_name' => 'required',
            'modal_date_from' => 'required',
            'modal_date_to' => 'required',
        ];
    }
}
