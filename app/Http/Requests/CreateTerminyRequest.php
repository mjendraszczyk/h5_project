<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTerminyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:64',
            'uwagi' => 'max:64',
            'email' => 'required|email',
            'nip' => 'required|min:9',
            'city' => 'max:64',
            'zip' => 'max:6',
            'phone' => 'max:15',
            'street' => 'max:64',
            'company' => 'max:255',
            'user_mail' => 'max:128',
            'user_phone' => 'max:15',
            'akceptacja' => 'accepted',
        ];
    }
}
