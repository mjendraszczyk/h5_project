<?php 
// app/lang/pl/date.php
return array(
	"month"	=> array(
		1 => "stycznia",
		2 => "lutego",
		3 => "marca",
		4 => "kwietnia",
		5 => "maja",
		6 => "czerwca",
        7 => "lipca",
        8 => "sierpnia",
		9 => "września",
		10 => "października",
		11 => "listopada",
		12 => "grudnia"
	),
	'day' => array(
		1 => 'Pn',
		2 => 'Wt',
		3 => 'Śr',
		4 => 'Czw',
		5 => 'Pt',
		6 => 'Sob',
		7 => 'Nd',
	)
);