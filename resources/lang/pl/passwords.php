<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Hasło musi zawierac 6 znaków i polu potwierdzenie musi byc zgodne z podanym.',
    'reset' => 'Hasło zresetowano!',
    'sent' => 'Otrzymasz e-mail z linkiem do resetowania hasła!',
    'token' => 'TOKEN do resetowania hasła jest niepoprawny.',
    'user' => "Nie znaleziono uzytkownika z podanym adresem e-mail.",

];
