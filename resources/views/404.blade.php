 <link href="{{asset('css/app.css')}}" rel="stylesheet">
 <link href="{{asset('css/backend/global.css')}}" rel="stylesheet">
<style>
.error { 
    
    display: flex;
    justify-content: center;
    align-items: center;
    flex-flow: column;
    height: 100vh;
}
.error h1 { 
    font-weight:900;
    font-size:7.5rem;

}
.error h2 { 
    font-weight:200;
    text-transform:uppercase;
}
.error img {
        background: #d14141;
    padding: 20px 33%;
}
</style>
<div class="error">
    <img src="{{asset('img/backend/logo-white.png')}}" alt="" />
<h1>Error 404</h1> 
<h2>Przykro mi ale nic tutaj się nie znajduje</h2>
<p>Wróc do strony głównej</p>
<a  class="btn btn-success" href="{{URL::to('/')}}">Strona głowna High5</a>
</div>