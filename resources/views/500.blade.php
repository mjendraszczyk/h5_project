 <link href="{{asset('/css/app.css')}}" rel="stylesheet">
 <link href="{{asset('/css/backend/global.css?v=1540339096')}}" rel="stylesheet">
<style>
.error { 
    
    display: flex;
    justify-content: center;
    align-items: center;
    flex-flow: column;
    height: 100vh;
}
.error h1 { 
    font-weight:900;
    font-size:7.5rem;

}
.error h2 { 
    font-weight:200;
    text-transform:uppercase;
}
.error img {
        background: #d14141;
    padding: 20px 33%;
}
</style>
<div class="error">
    <img src="{{asset('img/backend/logo-white.png')}}" alt="" />
<h1>Error 500</h1> 
<h2>Opss, jesli tu trafiłes to cos działa nie tak.</h2>
<h3>Ale bez obaw, jesli uwazasz ze w tym miejscu powinno cos byc daj nam znac, a czym prędzej się tym zajmiemy:</h3>
<br/>
<h3>{{env('MAIL_USERNAME')}} </h1>
<br/>
Spróbuj tez wrócic do strony głównej
<p>Wróc do strony głównej</p>
<a  class="btn btn-success" href="{{URL::to('/')}}">Strona głowna High5</a>
</div>