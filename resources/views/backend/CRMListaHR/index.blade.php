@extends('layouts.app')
@section('title')
<div class="row">
    <div class="col-md-6">
        Lista HR
    </div>
    <div class="col-md-6">

        {!!Form::open(["method"=>"post",'url'=>route('getHrForOpiekun'),"class"=>"form"])!!}

        @if(Auth::user()->id_role == '2')
        <div class="input-group">

            @if(Route::CurrentRouteName() == 'GETgetHrForOpiekun')
            <span class="input-group-btn">
                <a href="{{route('getListaHR',['opiekunID'=>$opiekunID])}}" class="pull-right btn btn-success"><i
                        class="fa fa-file-excel-o"></i> Pobierz listę HR</a>
            </span>
            {!! Form::select('opiekun',$opiekun,$opiekunID,['class'=>'form-control']) !!}
            @else
            <span class="input-group-btn">
                <a href="{{route('getListaHR',['opiekunID'=>'null'])}}" class="pull-right btn btn-success"><i
                        class="fa fa-file-excel-o"></i> Pobierz listę HR</a>
            </span>
            {!! Form::select('opiekun',$opiekun,null,['class'=>'form-control']) !!}
            @endif
            <span class="input-group-btn">
                {!! Form::submit('Szukaj',['class'=>'btn btn-default'])!!}
                <a href="{{route('indexCRMListaHR')}}" class="btn btn-default"><i class="fa fa-times"
                        aria-hidden="true"></i></a>
            </span>

        </div>
        @endif
        {!! Form::close() !!}

    </div>

</div>
@endsection

@section('content')


<table id="tableSort" class="table table-responsive table-hover" table-data-sort="0" table-data-sort-type="desc"
    data-toggle="table" data-sort-name="date" data-sort-order="desc">
    <thead>
        <th>Stan</th>
        <th>Imię i nazwisko</th>
        <th>Firma</th>
        <th>Mail</th>
        <th>Telefon</th>

        <th>Ostatni kontakt</th>
        <th>Następny kontakt</th>
    </thead>
    <tbody>
        @foreach($getListaHR as $listaHR)
        <tr>
            <td>
                <span style="display:none;">{{$listaHR->nowy}}</span>
                @if($listaHR->nowy == '1')
                <span
                    style="float:right;background: #0dc073;color:  #fff;padding:  3px;border-radius:  5px;text-align:  center;">Nowy</span>
                @else
                @endif
            </td>
            <td class="col-sm-2">
                <a class="label-link"
                    href="{{route('editCRMOsoby',['klientID'=>$listaHR->klientID,'id'=>$listaHR->uczestnikID])}}">
                    <i class="fa fa-user"></i> {{$listaHR->imie_nazwisko}}</a>
            </td>
            <td class="col-sm-2"><a class="label-link" href="{{route('editCRMFirmy',['id'=>$listaHR->klientID])}}">
                    <i class="fa fa-suitcase"></i>
                    {{$listaHR->nazwa}}</a></td>
            <td class="col-sm-2">{{$listaHR->email}}</td>
            <td class="col-sm-2">{{$listaHR->tel}}</td>

            <td class="col-sm-2 text-right">
                <span>
                    <span class="modal_open" style="cursor:pointer;" data-element=".zdarzenia_lista"
                        data-uczestnik="{{$listaHR->uczestnikID}}"
                        data-url="{{route('getZdarzeniaFromHR',['id'=>$listaHR->uczestnikID])}}">{{$listaHR->ostatni_kontakt}}</span>


                    <span data-url="{{route('createCRMZdarzenie',['id'=>$listaHR->uczestnikID,'method'=>'modal'])}}"
                        data-element=".form-container" class="modal_open btn btn-warning">
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </span>
            </td>
            <td class="col-sm-2 text-right">

                {{$listaHR->nastepny_kontakt}}

                <span data-url="{{route('createCRMZdarzenie',['id'=>$listaHR->uczestnikID,'method'=>'modal'])}}"
                    data-element=".form-container" class="modal_open btn btn-success">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </span>

                {{--<div class="btn btn-success" style="display:inline-block;">
<span class="nastepny_kontakt_ui" style="cursor:pointer;">
<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
</span>
<input type="date" style="display:none;color:#000;"   name="nastepny_kontakt"  data-respond="html" data-url="{{route('updateAjaxHROsoba',['id'=>$listaHR->uczestnikID])}}"
                class="updateAjax kontener_nastepny_kontakt_ui"/>
                </div>--}}

            </td>
        </tr>

        @endforeach

    </tbody>
</table>


@endsection