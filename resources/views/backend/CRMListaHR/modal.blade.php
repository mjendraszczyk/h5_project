<div id="" class="zdarzenia_lista modal-open modal fade bd-example-modal-lg hidden-sm-down" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
    <div class="modal-content">
      
      <table class="table table-responsive" style="width:500px;">
      <thead>
  
      <th>Data dodania</th>
      <th>Typ</th>
      <th>Komentarz</th>
      </thead>
      <tbody>
 @foreach($hrZdarzenia as $hrZdarzenie)
 <tr>
  
 <td>
 {{$hrZdarzenie->data_dodania}}
 </td>
 <td>
 {{$typy_zdarzenia[$hrZdarzenie->typ]}}
 </td>
 <td>
 {{$hrZdarzenie->komentarz}}
 </td>
 </tr>
 @endforeach
 </tbody>
 </table>
 @if(count($hrZdarzenia) == 0)
<div class="alert alert-info">Brak zdarzeń</div>
 @endif
</div>
</div>
</div>