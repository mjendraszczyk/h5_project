@extends('layouts.app')

@section('title')
Dodaj uczestnika
@endsection

@section('content')
  
 <div class="panel-shadow">

 {!! Form::open(['url' => route("storeCRMOsoby",['klientID'=>$klientID]),'id'=>'OsobaForm','name'=>'OsobaForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.CRMOsoby.form')
 

{!! Form::close() !!} 
</div>
      

@endsection
