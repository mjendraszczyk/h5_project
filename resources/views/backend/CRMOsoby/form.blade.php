@if(Session::has('success')) 
<div class="alert alert-success">
    {{Session::get('success')}}
 </div>
@endif

@if(Session::has('errors')) 
<div class="alert alert-danger">
<ul>
 
@foreach(Session::get('errors')->all() as $error)
  <li>{{$error}}</li>
  @endforeach
  </ul>
</div>
@endif
 

<div class="form-group">
    {!! Form::label('imie_nazwisko','Imię nazwisko',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMOsoby')
    {!! Form::text('imie_nazwisko',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('imie_nazwisko',$osoba->imie_nazwisko,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
 
 <div class="form-group">
    {!! Form::label('email','Email',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMOsoby')
    {!! Form::text('email',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('email',$osoba->email,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('user_phone','Telefon',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMOsoby')
    {!! Form::text('user_phone',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('user_phone',$osoba->user_phone,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('user_cell','Komórka',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMOsoby')
    {!! Form::text('user_cell',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('user_cell',$osoba->user_cell,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('stanowisko','Stanowisko',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMOsoby')
    {!! Form::text('stanowisko',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('stanowisko',$osoba->stanowisko,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>



<div class="form-group">
<label class="control-label"> 
  @if(Route::CurrentRouteName() == 'createCRMOsoby')
    {!! Form::checkbox('hr', 1, null, ['class' => 'field']) !!} 
    @else 
    @if($osoba->hr == '1')
    {!! Form::checkbox('hr', 1, true, ['class' => 'field']) !!}  
    @else 
    {!! Form::checkbox('hr', 0, false, ['class' => 'field']) !!}  
    @endif
    @endif
    HR
</label>
 
    </div>

<div class="form-group">
<label class="control-label"> 
  @if(Route::CurrentRouteName() == 'createCRMOsoby')
    {!! Form::checkbox('newsletter', 1, null, ['class' => 'field']) !!} 
    @else 
    @if($osoba->newsletter == '1')
    {!! Form::checkbox('newsletter', 1, true, ['class' => 'field']) !!}  
    @else 
    {!! Form::checkbox('newsletter', 0, false, ['class' => 'field']) !!}  
    @endif
    @endif
    Newsletter
</label>
</div>




<div class="form-group">
    {!! Form::label('uwagi','Uwagi',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMOsoby')
    {!! Form::textarea('uwagi',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::textarea('uwagi',$osoba->uwagi,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('zainteresowania','Zaintereowania',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMOsoby')
    {!! Form::textarea('zainteresowania',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::textarea('zainteresowania',$osoba->zainteresowania,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    <div class="col-sm-12 text-right save-row">
    <a href="{{route('editCRMFirmy',['id'=>$klientID])}}" class="btn btn-default">Powrót</a>
        @if(Route::CurrentRouteName() == 'createCRMFirmy') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!} @else {!!
        Form::submit('Zapisz', ['class'=>'btn btn-success']) !!} @endif
    </div>