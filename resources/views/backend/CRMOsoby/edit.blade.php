@extends('layouts.app') 
@section('title') Edycja uczestnika  <div class="pull-right">
    <a href="{{route('editCRMFirmy',['id'=>$klientID])}}" class="btn btn-default">
        <i class="fa fa-suitcase"></i> 
      @foreach( \App\Http\Controllers\backend\CRMFirmyController::getFirma($klientID) as $firma)
{{$firma->nazwa}}
    @endforeach  
</a> </div>
@endsection
 
@section('content')

<div class="panel-heading">
     
 
@foreach($osoby as $k=>$osoba) 
    @if($k == 0)
<h1>{{$osoba->imie_nazwisko}}</h1>
 
</div> 
<div class="row dashboard">
    <div class="row">
<div class="col-md-6">

<h4 class="form-sector">Uczestnik</h4>

<div class="category-panel">

    {!! Form::open(['method'=>'PATCH','url' => route("updateCRMOsoby",['klientID'=>$klientID,'id'=>$osoba->uczestnikID]),'enctype' => 'multipart/form-data','id'=>'OsobaForm','name'=>'OsobaForm','class'=>'form-vertical','style'=>'width:100%'])
    !!}
     @include('backend.CRMOsoby.form')  
    
    {!! Form::close() !!} 
    @endif
    @endforeach
     
</div>
 
</div>
</div>
 
 
<div class="col-md-6">
<h4 class="form-sector">Szkolenia uczestnika </h4>
 
 <div class="category-panel"> 
<table table-data-sort='0' table-data-sort-type='desc' class="table table-responsive table-hover">
    <thead>
<th>Szkolenie</th>    
<th>Data</th>
<th>Stan</th>
<th>Komentarz</th>
    </thead>
    <tbody>

@foreach($szkolenia as $szkolenie) 
<tr>
<td>
<a href="{{route('showCRMSzkolenie',['termID'=>$szkolenie->termID])}}">{{$szkolenie->training_title}}</a>
</td>
<td>
{{$szkolenie->data_zgloszenia}}
</td>
<td>
{{$szkolenie->stan}}
</td>
<td>
{{$szkolenie->komentarz}}
</td>
</tr>

@endforeach
</tbody>
</table>
 </div>
  
  <h4 class="form-sector">Zdarzenia</h4>
 <div class="category-panel">
 
          {!!App\Http\Controllers\backend\CRMZdarzeniaController::create($osoba->uczestnikID,'standard')!!}
 <hr/>
 <table table-data-sort='0' table-data-sort-type='desc' class="table table-responsive table-hover">
    <thead>
<th>Typ</th>    
<th>Data dodania</th>
<th>Komentarz</th>
    </thead>
    <tbody>

@foreach($zdarzenia as $zdarzenie) 
<tr>
<td>
{{$typy_zdarzenia[$zdarzenie->typ]}}
</td>
<td>
{{$zdarzenie->data_dodania}}
</td>
<td>
{{$zdarzenie->komentarz}}
</td>
</tr>

@endforeach
</tbody>
</table>
 </div>
</div> 
    
    
      
@endsection