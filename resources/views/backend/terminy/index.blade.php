@extends('layouts.app') 
@section('title') Terminarz
@endsection
 
@section('content')
<div class="row text-right no-margin">
    <a href="{{route('createTerminarz')}}" class="btn btn-success"><i class="fa fa-plus"></i> Dodaj</a>
</div> 

<div class="panel-shadow">
 {!! Form::open(['url' => route("filtrTerminarz"), 'id'=>'filtrTerminarzForm','name'=>'filtrTerminarzForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

 {{-- 'url' => route("filtrTerminarz") --}}
 
 <div class="row">
<div class="col-md-6">
<div class="form-group">
    {!! Form::label('term_state','Pokaz szkolenia pewne',['class'=>'col-sm-12 control-label','style'=>'padding:0;']) !!} 
  
    {!! Form::select('term_state', $typy_szkolenia,null, ['class'=>'form-control']) !!}
      
     </div>

</div>
 <div class="col-md-5">
<div class="form-group">
    {!! Form::label('term_place','Pokaz szkolenia wg miejscowosci',['class'=>'col-sm-12 control-label','style'=>'padding:0;']) !!} 
  
    {!! Form::select('term_place', $miejscowosci_szkolenia,2, ['class'=>'form-control']) !!}
      
     </div>

</div>
<div class="col-md-1">
<div class="form-group">
    {!! Form::label('filtr','&nbsp;',['class'=>'col-sm-12 control-label']) !!} 
    
    <input type="submit" class="btn btn-primary" value="Filtr">
 
</div>
</div>

</div>
{!! Form::close() !!} 
</div>

<div class="clearfix"></div>

 
 <div class="panel-shadow">
   {!! $calendar->calendar() !!}  
   
 {!! $calendar->script() !!}       
    </div>
     

@endsection