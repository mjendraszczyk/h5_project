@if(Session::has('success'))
<div class="alert alert-success">
    {{Session::get('success')}}
</div>
@endif

@if(Session::has('errors'))
<div class="alert alert-danger">
    <ul>
        <!-- {{Session::get('errors')}} -->
        @foreach(Session::get('errors')->all() as $error)

        <!-- {{ Session::get('errors')}}  -->
        <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
@endif

<!-- Nazwa kursu:	
Start:	
2018-06-09
	Koniec:	
2018-06-10

Miejsce:	
Miejsce po nowemu:	
Trener:	
Typ:	
First - ilość dni:	
First - rabat:	
Stan:	
Kolor	 -->
{{Form::hidden('api_url_term',route('getAPITerminarz',''))}}


<div class="form-group">
    {!! Form::label('training_title','Nazwa kursu',['class'=>'col-sm-12 control-label']) !!}

    @if(Route::CurrentRouteName() == 'createTerminarz')

    {!! Form::select('training_title', $kursy,null, ['class'=>'form-control']) !!}

    @else
    {!! Form::select('training_title', $kursy,$termin->trainingID, ['class'=>'form-control']) !!}

    @endif
</div>
<div id="box_termin">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('term_start','Start',['class'=>'col-sm-12 control-label']) !!}
                @if(Route::CurrentRouteName() == 'createTerminarz')
                {!! Form::date('term_start[]',null,['class'=>'col-sm-12 form-control','required']) !!} @else {!!
                Form::date('term_start[]',$termin->term_start,['class'=>'col-sm-12
                form-control','required']) !!} @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('term_end','Koniec',['class'=>'col-sm-12 control-label']) !!}
                @if(Route::CurrentRouteName() == 'createTerminarz')
                {!! Form::date('term_end[]',null,['class'=>'col-sm-12 form-control','required']) !!} @else {!!
                Form::date('term_end[]',$termin->term_end,['class'=>'col-sm-12
                form-control','required']) !!} @endif
            </div>
        </div>
    </div>
</div>
@if(Route::CurrentRouteName() == 'createTerminarz')
<div class="clearfix"></div>
<div class="panel panel-default">
    <div class="panel-header">Dodaj kolejne terminy dla tego samego szkolenia</div>
    <div class="panel-body">

        <div class="row termin_admin_box">

        </div>
        <span class="btn btn-success dodawanie_terminow_admin">Dodaj termin</span>
        <span class='btn btn-default trash'>Usun</span>
    </div>
</div>
@else

<div class="clearfix"></div>
<div class="panel panel-default">
    <div class="panel-header">Edytuj powiązane terminy z tym szkoleniem</div>
    <div class="panel-body">

        @if($tabParentID != null)
        @foreach($tabParentID as $tpID)
        {{--@if($tpID->termParentID != $tpID->termID)--}}

        <div class="row" style="border-bottom:1px solid #eee;padding:5px 0;">
            <div class="col-md-5">
                {{$tpID->training_title}}
                @if($tpID->termParentID == $tpID->termID)
                <span class="primaryTerm">Termin podstawowy</span>
                @endif
            </div>
            <div class="col-md-6">
                <div class="col-md-6"><i class="fa fa-clock-o" aria-hidden="true"></i>
                    {{$tpID->term_start}}</div>
                <div class="col-md-6"><i class="fa fa-clock-o" aria-hidden="true"></i>
                    {{$tpID->term_end}}</div>
            </div>
            <div class="col-md-1">
                <a href="{{route('editTerminarz',['id'=>$tpID->termID])}}" class="btn gui btn-default"><i
                        class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
            </div>
        </div>
        {{--@else --}}
        @if(count($tabParentID) < 1) <div class="alert alert-danger">Brak dodatkowych terminów dla tego szkolenia</div>
    @endif
    {{--@endif--}}
    @endforeach

    @else
    <div class="alert alert-danger">Brak dodatkowych terminów dla tego szkolenia</div>
    @endif

    <div class="row termin_admin_box">

    </div>
    <span class="btn btn-success dodawanie_terminow_admin"><i class="fa fa-plus"></i> Dodaj termin</span>
    <span class='btn btn-default trash'><i class="fa fa-minus"></i> Usun</span>
</div>
</div>

@endif

<div class="form-group">
    {!! Form::label('term_place','Miejsce',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createTerminarz')
    {!! Form::text('term_place',null,['class'=>'col-sm-12 form-control','required']) !!} @else {!!
    Form::text('term_place',$termin->term_place,['class'=>'col-sm-12
    form-control','required']) !!} @endif
</div>


<div class="form-group">
    {!! Form::label('term_placeID','Miejsce po nowemu',['class'=>'col-sm-12 control-label']) !!}

    @if(Route::CurrentRouteName() == 'createTerminarz')

    {!! Form::select('term_placeID', $miejsca,2, ['class'=>'form-control']) !!}

    @else
    {!! Form::select('term_placeID', $miejsca,$termin->term_placeID, ['class'=>'form-control']) !!}

    @endif
</div>



<div class="form-group">
    {!! Form::label('term_instructor','Trener',['class'=>'col-sm-12 control-label']) !!}

    @if(Route::CurrentRouteName() == 'createTerminarz')

    {!! Form::select('term_instructor', $trenerzy,null, ['class'=>'form-control']) !!}

    @else
    {!! Form::select('term_instructor', $trenerzy,$termin->term_instructor, ['class'=>'form-control']) !!}

    @endif
</div>




<div class="form-group">
    {!! Form::label('term_type','Typ',['class'=>'col-sm-12 control-label']) !!}

    @if(Route::CurrentRouteName() == 'createTerminarz')

    {!! Form::select('term_type', $typy,null, ['class'=>'form-control']) !!}

    @else
    {!! Form::select('term_type', $typy,$termin->term_type, ['class'=>'form-control']) !!}

    @endif
</div>


<div class="form-group">
    {!! Form::label('term_training_type','Typ szkolenia',['class'=>'col-sm-12 control-label']) !!}

    @if(Route::CurrentRouteName() == 'createTerminarz')

    {!! Form::select('term_training_type', $termin_typ_szkolenia,null, ['class'=>'form-control']) !!}

    @else
    {!! Form::select('term_training_type', $termin_typ_szkolenia,$termin->term_training_type, ['class'=>'form-control'])
    !!}

    @endif
</div>


<div class="form-group">
    {!! Form::label('term_fdays','Ilosc dni',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createTerminarz')
    {!! Form::text('term_fdays',null,['class'=>'col-sm-12 form-control','required']) !!} @else {!!
    Form::text('term_fdays',$termin->term_fdays,['class'=>'col-sm-12
    form-control','required']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('term_frabat','Rabat',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createTerminarz')
    {!! Form::text('term_frabat',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::text('term_frabat',$termin->term_frabat,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    {!! Form::label('term_color','Kolor etykiety',['class'=>'col-sm-12 control-label']) !!}
    @if(Route::CurrentRouteName() == 'createTerminarz')
    {!! Form::color('term_color',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::color('term_color',$termin->term_color,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    {!! Form::label('term_state','Stan',['class'=>'col-sm-12 control-label']) !!}

    @if(Route::CurrentRouteName() == 'createTerminarz')

    {!! Form::select('term_state', $term_state,null, ['class'=>'form-control']) !!}

    @else
    {!! Form::select('term_state', $term_state,$termin->term_state, ['class'=>'form-control']) !!}

    @endif
</div>

<div class="form-group">
    @if(Route::CurrentRouteName() == 'createTerminarz')
    <label>{!! Form::checkbox('term_closed', 'y', 0, ['class'=>'']) !!} Brak miejsc</label>
    @else
    @if ($termin->term_closed == 'y')
    <label>{!! Form::checkbox('term_closed', 'y', 1, ['class'=>'']) !!} Brak miejsc</label>
    @else
    <label>{!! Form::checkbox('term_closed', 'y', 0, ['class'=>'']) !!} Brak miejsc</label>

    @endif
    @endif
</div>

<div class="form-group">
    <div class="text-right save-row" style="display:inline-block;">
        @if(Route::CurrentRouteName() == 'createTerminarz') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!}
        @else {!! Form::submit('Zapisz',
        ['class'=>'btn btn-success']) !!}

        @endif
    </div>