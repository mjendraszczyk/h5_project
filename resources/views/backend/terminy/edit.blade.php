@extends('layouts.app') 
@section('title') Edycja terminarzu  
@endsection
 
@section('content')
 
<div class="panel-shadow">
    @foreach($getTerminy as $key => $termin) {!! Form::open(['method'=>'PATCH','url' => route("updateTerminarz",['id'=>$termin->termID]),'enctype' => 'multipart/form-data','id'=>'TerminarzForm','name'=>'TerminarzForm','class'=>'form-vertical','style'=>'width:100%;'])
    !!}
    @if($key == 0) 
    @include('backend.terminy.form') 
    @endif
    {!! Form::close() !!}

 
    {!! Form::open(['method'=>'DELETE', 'style'=>'display:inline-block','action'=>['backend\TerminarzController@destroy',$termin->termID]]) !!}
                    <button class="btn btn-default"> 
 Usuń termin
</button> {!! Form::close() !!} 

    {!! Form::open(['method'=>'GET', 'style'=>'display:inline-block','action'=>['backend\TerminarzController@odepnij',$termin->termID]]) !!}
                    <button class="btn btn-default"> <i class="fa fa-chain-broken" aria-hidden="true"></i>

Odepnij powiązanie
</button> {!! Form::close() !!} 
 

     @endforeach
</div>
@endsection


