@extends('layouts.app')

@section('title')
Dodaj Termin
@endsection

@section('content')
  
 <div class="panel-shadow">

 {!! Form::open(['url' => route("storeTerminarz"),'id'=>'TerminarzForm','name'=>'TerminarzForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.terminy.form')
 

{!! Form::close() !!} 
</div>
      

@endsection
