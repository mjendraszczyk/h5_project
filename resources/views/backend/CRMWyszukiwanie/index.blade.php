@extends('layouts.app') 
@section('title') Wyszukiwanie
@endsection
 
@section('content')
 
  
 
<div class="category-panel">
    <div class="row form-row">
    <div class="col-lg-4">
      
    {!!Form::open(["url"=>route('searchZainteresowania'),"class"=>"form"])!!}   
      <div class="input-group">
{!! Form::text('zainteresowania','',['placeholder'=>'Szukaj wg zainteresowań / uwag','class'=>'form-control']) !!}
<span class="input-group-btn">
{!! Form::submit('Szukaj',['class'=>'btn btn-default'])!!}
</span>
</div>
{!! Form::close() !!}

</div>
<div class="col-lg-8">
 <div class="text-right">

    <span data-url="{{route('mailCRMWyszukiwanie',['fraza'=>$fraza])}}" data-element=".form-container" class="modal_open btn btn-success">
 Wyslij wiadomosc HR
 </span>  
 </div>
</div>
</div>
{!!Form::open(['url'=>route('searchOsoby')])!!}
<div class="row form-row">
    <div class="col-lg-4">
<div class="input-group">
{!! Form::text('osoby','',['class'=>'form-control','placeholder'=>'Szukaj osób']) !!}
<span class="input-group-btn">
{!! Form::submit('Szukaj',['class'=>'btn btn-default'])!!}
</span>
</div>
</div>
{!! Form::close() !!}
</div>


<div class="row form-row">
    
{!!Form::open(['url'=>route('searchSzkolenia'),'class'=>'form-inline-middle'])!!}
    <div class="col-lg-5">
        <div class="input-group">
        <div class="autocomplete-container">
{!! Form::text('szkolenia','',['required','id'=>'autocompleteSzkolenia','class'=>'form-control','placeholder'=>'Wyszukaj wg. szkoleń']) !!}

{!! Form::hidden('trainingID','',['id'=>'getTrainingID']) !!}
</div>
<span class="input-group-btn">
{!! Form::submit('Szukaj',['class'=>'btn btn-default'])!!}
</span>
</div>
</div>
<div class="col-lg-2">
    <label>
{!! Form::checkbox('ankiety','ankiety')!!} 
Ankiety
</label>
</div>
<div class="col-lg-5">
 <div class="row">
     <div class="col-md-6">
    {!! Form::date('od',date('Y-m-d',strtotime('-730 days')),['class'=>'form-control']) !!}
    </div>
    <div class="col-md-6">
    {!! Form::date('do',date('Y-m-d',strtotime('+7 days')),['class'=>'form-control']) !!}
    </div>
 </div>

    </div>
{!! Form::close() !!}
 
</div>

 

 </div>

<div class="row">
 <div class="pull-right">
 
 
 
 </div>
  </div>
 
<table id="tableSort" class="table table-responsive table-hover" data-toggle="table" data-sort-name="date" data-sort-order="desc">
    <thead>
        <th>Imię i nazwisko</th>
        <th>Firma</th>
        <th>Mail</th>         
    </thead>
    <tbody>
     @foreach($wyszukiwanieWynik as $wynik)
     @if($wynik->imie_nazwisko != null)
     
     <tr>
     <td>
 <a class="label-link" href="{{route('editCRMOsoby',['klientID'=>$wynik->klientID,'id'=>$wynik->uczestnikID])}}"><i class="fa fa-user"></i> {{$wynik->imie_nazwisko}}</a>
 </td>
 <td>
 <a class="label-link" href="{{route('editCRMFirmy',['id'=>$wynik->klientID])}}">
      @foreach(App\Http\Controllers\backend\CRMFirmyController::getFirma($wynik->klientID) as $firma)
<a class="label-link" href="{{route('editCRMFirmy',['id'=>$firma->klientID])}}"><i class="fa fa-suitcase"></i> {{$firma->nazwa}}</a>
 @endforeach
     </a>
 </td>
 <td>
 {{$wynik->email}}
 </td>
  
 </tr>
 @endif
 @endforeach
    </tbody>
</table>

 
@endsection