@if(Session::has('success')) 
<div class="alert alert-success">
    {{Session::get('success')}}
 </div>
@endif

@if(Session::has('errors')) 
<div class="alert alert-danger">
<ul>
 
@foreach(Session::get('errors')->all() as $error)
  <li>{{$error}}</li>
  @endforeach
  </ul>
</div>
@endif
 

<div class="form-group">
    {!! Form::label('odbiorcy','Odbiorcy',['class'=>'col-sm-12 control-label']) !!} 
    {!! Form::textarea('odbiorcy',$listaMaili,['required','placeholder'=>'adres@domena1.pl,adres@domena2.pl,adres@domena3.pl...','rows'=>'5','class'=>'col-sm-12 form-control']) !!}  
</div>

<div class="form-group">
    {!! Form::label('tytul','Tytuł',['class'=>'col-sm-12 control-label']) !!} 
    {!! Form::text('tytul',null,['required','placeholder'=>'Tytuł wiadomosci','class'=>'col-sm-12 form-control']) !!}  
</div>

<div class="form-group">
    {!! Form::label('tresc','Tresc',['class'=>'col-sm-12 control-label']) !!} 
    {!! Form::textarea('tresc',null,['required','placeholder'=>'Tresc wiadomosci','class'=>'col-sm-12 form-control']) !!}  
</div>

<div class="form-group">
 
 
     {{-- {!! Form::button('Wyslij',['data-url'=>route('SendmailCRMWyszukiwanie',['typ'=>'all']),'id'=>'mailSendForm','class'=>'sendAjax btn-success btn']) !!}  --}}
    {!! Form::submit('Wyślij',['class'=>'btn-success btn']) !!}
</div>