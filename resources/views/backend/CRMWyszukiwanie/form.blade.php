@if(Session::has('success')) 
<div class="alert alert-success">
    {{Session::get('success')}}
 </div>
@endif

@if(Session::has('errors')) 
<div class="alert alert-danger">
<ul>
<!-- {{Session::get('errors')}} -->
@foreach(Session::get('errors')->all() as $error)

  <!-- {{ Session::get('errors')}}  -->
  <li>{{$error}}</li>
  @endforeach
  </ul>
</div>
@endif
<div class="form-group">
    {!! Form::label('picture','Obrazek',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createHeadline')
    @else
    <img src="{{Config('url')}}/img/frontend/headlines/{{$headline->picture}}" alt="" class="thumbnail" /> @endif {!! Form::file('image')
    !!}
</div>
<div class="form-group">
    {!! Form::label('picture','Tytuł',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createHeadline')
    {!! Form::text('title',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('title',$headline->title,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
<div class="form-group">
    {!! Form::label('content','Tresc',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createHeadline')
    {!! Form::textarea('content',null,['class'=>'editor col-sm-12 form-control']) !!} @else {!! Form::textarea('content',$headline->content,['class'=>'editor col-sm-12
    form-control']) !!} @endif
</div>
<div class="form-group">
    {!! Form::label('link','Link',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createHeadline')
    {!! Form::text('link',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('link',$headline->link,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
<div class="form-group">
    {!! Form::label('position','Pozycja',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createHeadline')
    {!! Form::text('position',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('position',$headline->position,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() == 'createHeadline') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!} @else {!!
        Form::submit('Zapisz', ['class'=>'btn btn-success']) !!} @endif
    </div>