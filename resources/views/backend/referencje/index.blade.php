@extends('layouts.app')

@section('title')
Referencje
@endsection

@section('content')
 
  <div class="row text-right no-margin">
    <a href="{{route('createReferencje')}}" class="btn btn-success"><i class="fa fa-plus"></i> Dodaj</a>
</div>
 


<table id="tableSort" table-data-sort="0" table-data-sort-type="desc" class="table table-responsive table-hover">
 <thead>
 <th>ID</th>
        <th>Firma</th>
        <th>Osoba</th>
<th>Tresc</th>
<th>Status</th>
<th></th>
    </thead>
 
 
<tbody>
    @foreach($referencje as $ref)
<tr>
<td>
{{$ref->refID}}
</td>
<td>
{{$ref->firma}}
</td>
<td>
{{$ref->imie}}
</td>
<td>
{{$ref->tresc}}
</td>
 <td>
     {{$ref->type}}
 </td>

<td class="text-right">
                    <a href="{{route('editReferencje',['id'=>$ref->refID])}}" class="btn gui btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    {!!
                    Form::open(['method'=>'DELETE','action'=>['backend\ReferencjeController@destroy',$ref->refID]])
                    !!}
                    <button class="btn gui btn-default"> 
 <i class="fa fa-times" aria-hidden="true"></i>
</button> {!! Form::close() !!}


                </td>
</tr>
@endforeach
    </tbody>
    </table>


@endsection
