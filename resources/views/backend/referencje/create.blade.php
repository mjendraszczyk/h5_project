@extends('layouts.app')

@section('title')
Dodaj referencje
@endsection

@section('content')
  
 <div class="panel-shadow">

 {!! Form::open(['url' => route("storeReferencje"),'id'=>'ReferencjeForm','name'=>'ReferencjeForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.referencje.form')
 

{!! Form::close() !!} 
</div>
      

@endsection
