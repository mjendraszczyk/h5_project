@extends('layouts.app') 
@section('title') Edycja referencji
@endsection
 
@section('content')

<div class="panel-shadow">
    @foreach($referencje as $ref) {!! Form::open(['method'=>'PATCH','url' => route("updateReferencje",['id'=>$ref->refID]),'enctype'
    => 'multipart/form-data','id'=>'ReferencjeForm','name'=>'ReferencjeForm','class'=>'form-vertical','style'=>'width:100%;'])
    !!}
    @include('backend.referencje.form') {!! Form::close() !!} @endforeach
</div>
@endsection