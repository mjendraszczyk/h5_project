@extends('layouts.app')

@section('title')
Dodaj Newsa
@endsection

@section('content')
  
 <div class="panel-shadow">

 {!! Form::open(['url' => route("storeHistory"),'id'=>'HistoryForm','name'=>'HistoryForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.history.form')
 

{!! Form::close() !!} 
</div>
      

@endsection
