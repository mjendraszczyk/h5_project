@extends('layouts.app')

@section('title')
Artykuly
@endsection

@section('content')
 
    <div class="row text-right no-margin">
    <a href="{{route('createHistory')}}" class="btn btn-success"><i class="fa fa-plus"></i> Dodaj</a>
</div>

<table id="tableSort" table-data-sort="0" table-data-sort-type="desc" class="table table-responsive table-hover">
    <thead>
<th>ID</th>    
<th>Tytul</th>
<th>Wstep</th>
<th>Data</th>
 <th></th>
    </thead>
    <tbody>
 
@foreach($history as $hist)
<tr>
         <td>
                    {{$hist->newsID}}
                 </td>
             <td>
                    {{$hist->news_title}}
                 </td>
                 <td>
                        {{str_limit($hist->news_lid,128)}}
                     </td>
                      
                         <td>
                                {{$hist->news_data}}
                             </td>
                             <td class="text-right">

                    <a href="{{route('editHistory',['id'=>$hist->newsID])}}" class="btn gui btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>{!!
                    Form::open(['method'=>'DELETE','action'=>['backend\HistoryController@destroy',$hist->newsID]])
                    !!}
                    <button class="btn gui btn-default"> 
 <i class="fa fa-times" aria-hidden="true"></i>
</button> {!! Form::close() !!}


                </td>
</tr>
@endforeach
 
    </tbody>
</table>

 

@endsection
