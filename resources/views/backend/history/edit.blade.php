@extends('layouts.app') 
@section('title') Edycja Artykułu
@endsection
 
@section('content')

<div class="panel-shadow">
    @foreach($history as $hist) {!! Form::open(['method'=>'PATCH','url' => route("updateHistory",['id'=>$hist->newsID]),'enctype'
    => 'multipart/form-data','id'=>'HistoryForm','name'=>'HistoryForm','class'=>'form-vertical','style'=>'width:100%;'])
    !!}
    @include('backend.history.form') {!! Form::close() !!} @endforeach
</div>
@endsection