@if(Session::has('success')) 
<div class="alert alert-success">
    {{Session::get('success')}}
 </div>
@endif

@if(Session::has('errors')) 
<div class="alert alert-danger">
<ul>
<!-- {{Session::get('errors')}} -->
@foreach(Session::get('errors')->all() as $error)

  <!-- {{ Session::get('errors')}}  -->
  <li>{{$error}}</li>
  @endforeach
  </ul>
</div>
@endif

<div class="form-group">
{!! Form::label('news_place','Dział',['class'=>'col-sm-12 control-label']) !!}
@if(Route::CurrentRouteName() == 'createHistory')
{!! Form::select('news_place', array('1' => 'Na wszystkie', '2' => 'Tylko główna', '3' => 'Tylko sekretarki'), '1',['class'=>'form-control']) !!}
@else 
{!! Form::select('news_place', array('1' => 'Na wszystkie', '2' => 'Tylko główna', '3' => 'Tylko sekretarki'), $hist->news_place,['class'=>'form-control']) !!}
@endif 
</div>

<div class="form-group">
    {!! Form::label('news_title','Tytuł',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createHistory')
    {!! Form::text('news_title',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('news_title',$hist->news_title,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
<div class="form-group">
    {!! Form::label('news_lid','Wstęp',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createHistory')
    {!! Form::textarea('news_lid',null,['class'=>'editor col-sm-12 form-control']) !!} @else {!! Form::textarea('news_lid',$hist->news_lid,['class'=>'editor col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    {!! Form::label('news_text','Tekst',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createHistory')
    {!! Form::textarea('news_text',null,['class'=>'editor col-sm-12 form-control']) !!} @else {!! Form::textarea('news_text',$hist->news_text,['class'=>'editor col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    {!! Form::label('news_link','Link',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createHistory')
    {!! Form::text('news_link',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('news_link',$hist->news_link,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    {!! Form::label('news_pictur_pos','Pozycja obrazka',['class'=>'col-sm-12 control-label']) !!} 
    @if(Route::CurrentRouteName() == 'createHistory')
    <label>{!! Form::radio('news_pictur_pos','left', false, ['class'=>'radio-inline']) !!} Po lewej</label>
    <label>{!! Form::radio('news_pictur_pos','right', false, ['class'=>'radio-inline']) !!} Po prawej</label>
    
    @else 

    @if($hist->news_pictur_pos == 'left')
        
        <label>{!! Form::radio('news_pictur_pos','left', true, ['class'=>'radio-inline']) !!} Po lewej</label>
        <label>{!! Form::radio('news_pictur_pos','right', false, ['class'=>'radio-inline']) !!} Po prawej</label>

    @elseif ($hist->news_pictur_pos == 'right')
    
        <label>{!! Form::radio('news_pictur_pos','left', false, ['class'=>'radio-inline']) !!} Po lewej</label>
        <label>{!! Form::radio('news_pictur_pos','right', true, ['class'=>'radio-inline']) !!} Po prawej</label>
    
    @else 
        
        <label>{!! Form::radio('news_pictur_pos','left', false, ['class'=>'radio-inline']) !!} Po lewej</label>
        <label>{!! Form::radio('news_pictur_pos','right', false, ['class'=>'radio-inline']) !!} Po prawej</label>
    
    @endif

     @endif
</div>

<div class="form-group">
    {!! Form::label('picture','Obrazek',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createHistory')
    @else
    <img src="{{Config('url')}}/img/frontend/news/{{$hist->news_picture}}" alt="" class="thumbnail" /> @endif {!! Form::file('image')
    !!}
</div>

<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() == 'createHistory') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!} @else {!!
        Form::submit('Zapisz', ['class'=>'btn btn-success']) !!} @endif
    </div>