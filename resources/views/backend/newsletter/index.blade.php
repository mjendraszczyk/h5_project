@extends('layouts.app')

@section('title')
Przygotowanie newslettera
@endsection

@section('content')

@if(Session::has('panelInfo')) 
<div class="alert alert-success">
    {{Session::get('panelInfo')}}
 </div>
@endif
 
@include('backend.newsletter.progress')
 

<div class="panel-shadow">
 {!! Form::open(['url' => 'cmr/backoffice/newsletter/podglad', 'action' => 'backend\NewsletterController@showNewsletter']) !!}

 {!! Form::textarea('opis','',['class'=>'col-sm-12 form-control']) !!} 

<table class="table table-responsive table-hover">
 <thead>
        <th></th>
        <th>Data</th>
<th>Szkolenie</th>
<th>Cena</th>
<th></th>
    </thead>
  
<tbody>
@foreach($newsletter as $newslett)
<tr>
<td>
{!! Form::checkbox($newslett->termID,'1',true,[]) !!} 
</td>
<td>
@php
    $start_day=date ('j', strtotime($newslett->term_start));
	$end_day=date ('j', strtotime($newslett->term_end));
	$month=Lang::get('date.month.'.date('n', strtotime($newslett->term_start)));
	
     @endphp

	@if ($start_day !=$end_day)
	{{$start_day}} - {{$end_day}} {{$month}}
	@else
		{{$start_day}} {{$month}}
	@endif
<br/>
{{$newslett->term_place}}
</td>
<td>
{{$newslett->training_title}}
<br/>
{{$newslett->training_lid}}
</td>
<td class="col-sm-2">
{{number_format($newslett->training_price,2,'.',' ')}} {{Config::get('app.currency')}}
</td>
<td>
{!! Form::text('price_'.$newslett->termID,'',['class'=>'form-control']) !!} 
</td>
</tr>
@endforeach
<tr>

</tr>
</tbody>
	</table>
	
 
<!-- <a href="{{route('showNewsletter')}}" class="btn btn-success">Obejrzyj</a> -->
{!! Form::submit('Podgląd',['class'=>'col-sm-12 btn btn-success']) !!}
{!! Form::close() !!}
</div>

@endsection
