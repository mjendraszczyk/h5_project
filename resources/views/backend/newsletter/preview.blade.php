@extends('layouts.app')

@section('title')
Podgląd newslettera
@endsection

@section('content')

@include('backend.newsletter.progress')

<a href="{{ redirect()->getUrlGenerator()->previous() }}" class="btn btn-default">Powrót do edycji</a> 


<div class="panel-shadow">
{!! Form::open(['url' => route("NewsletterSend"),'id'=>'WyslijNewsletterForm','name'=>'WyslijNewsletterForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}


<div class="gui-window"> 
@include('backend.emails.newsletter')
</div>

{!! Form::submit('Testuj',['class'=>'btn btn-default','name'=>'wyslij','style'=>'margin:5px 0;width:100%;display:block;','value'=>'test']) !!}
{!! Form::submit('Wyślij',['class'=>'btn btn-success','name'=>'wyslij','value'=>'all','style'=>'width:100%;display:block;']) !!}
{!! Form::close() !!}
</div>
@endsection
