@if(Session::has('success')) 
<div class="alert alert-success">
    {{Session::get('success')}}
 </div>
@endif

@if(Session::has('errors')) 
<div class="alert alert-danger">
<ul>
<!-- {{Session::get('errors')}} -->
@foreach(Session::get('errors')->all() as $error)

  <!-- {{ Session::get('errors')}}  -->
  <li>{{$error}}</li>
  @endforeach
  </ul>
</div>
@endif

 

<div class="form-group">
    {!! Form::label('firma','Firma',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createReferencje')
    {!! Form::text('firma',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('firma',$ref->firma,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
  
 <div class="form-group">
    {!! Form::label('imie','Imię',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createReferencje')
    {!! Form::text('imie',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('imie',$ref->imie,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
  
<div class="form-group">
    {!! Form::label('funkcja','Funkcja',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createReferencje')
    {!! Form::text('funkcja',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('funkcja',$ref->funkcja,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
  

  <div class="form-group">
    {!! Form::label('tresc','Tresc',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createReferencje')
    {!! Form::textarea('tresc',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::textarea('firma',$ref->tresc,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
  



<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() == 'createReferencje') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!} @else {!!
        Form::submit('Zapisz', ['class'=>'btn btn-success']) !!} @endif
    </div>