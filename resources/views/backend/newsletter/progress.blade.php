<div class="row">

<div class="col-md-4">
<div class="row-progress active"><span>Przygotowanie</span></div>
</div>
<div class="col-md-4">
    <div class="row-progress @if(Route::current()->getName() == 'showNewsletter') active @endif"><span>Podgląd</span></div>
</div>
<div class="col-md-4">
	<div class="row-progress @if(Session::has('panelInfo')) active @endif"><span>Wysyłka</span></div>
</div>
</div>