@extends('layouts.app')

@section('title')
Lista Szkoleń
@endsection

@section('content')
  
 <div class="row text-right no-margin">
<a href="{{route('createASzkolenie',['idAkademia'=>$idAkademia])}}" class="btn btn-success"><i class="fa fa-plus"></i> Dodaj</a>
</div>
 

                  
<table id="tableSort" class="table table-responsive table-hover">
    <thead>
        <tr>
<th>ID</th>
<th>Nazwa</th>
<th></th>
</tr>
    </thead>
    <tbody>
  
@foreach($listSzkolenia as $listS)
<tr>
   <td class="col-sm-2"> 
                            {{$listS->akademia_szkolenieID}}
                         </td>
        <td>
            {{$listS->training_title}}
         </td>
 
                  
                  <td class="text-right"> 
                
<a href="{{route('deleteASzkolenie',['id'=>$listS->akademia_szkolenieID,'idAkademia'=>$listS->akademiaID])}}" class="btn btn-default"><i class="fa fa-times" aria-hidden="true"></i></a>

                </td>
</tr>
@endforeach
 
    </tbody>
</table>
 
      

@endsection
