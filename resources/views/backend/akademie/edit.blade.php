@extends('layouts.app') 
@section('title') Edycja akademii
@endsection
 
@section('content')

<div class="panel-shadow">
    @foreach($akademie as $akademia) {!! Form::open(['method'=>'PATCH','url' => route("updateAkademie",['id'=>$akademia->akademiaID]),'enctype' => 'multipart/form-data','id'=>'AkademiaForm','name'=>'AkademiaForm','class'=>'form-vertical','style'=>'width:100%;'])
    !!}
    @include('backend.akademie.form') {!! Form::close() !!} @endforeach
</div>
@endsection