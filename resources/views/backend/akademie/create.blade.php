@extends('layouts.app')

@section('title')
Dodaj akademie
@endsection

@section('content')
  
 <div class="panel-shadow">

 {!! Form::open(['url' => route("storeAkademie"),'id'=>'AkademiaForm','name'=>'AkademiaForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.akademie.form')
 

{!! Form::close() !!} 
</div>
      

@endsection
