@extends('layouts.app')

@section('title')
Akademie
@endsection

@section('content')
 <div class="row text-right no-margin">
    <a href="{{route('createAkademie')}}" class="btn btn-success"><i class="fa fa-plus"></i> Dodaj</a>
</div>

                  
<table id="tableSort" class="table table-responsive table-hover">
    <thead>
        <tr>
<th>ID</th>
<th>Nazwa</th>
<th>Opis</th>
<th>Aktywna</th>
<th></th>
<th></th>
</tr>
    </thead>
    <tbody>
  
@foreach($akademie as $akademia)
<tr>
   <td class="col-sm-2"> 
                            {{$akademia->akademiaID}}
                         </td>
        <td>
            {{$akademia->nazwa}} 
         </td>
             <td>
             {{$akademia->opis_krotki}}
                        
                 </td>
                 <td>
                        
                        {{$akademia->aktywna}}    
                     </td>
                  <td>
                  <a href="{{route('listSzkolenia',['idAkademia'=>$akademia->akademiaID])}}" class="btn btn-info">Lista Szkoleń</a>
                  </td>
                  <td class="text-right"> 
                            <a href="{{route('editAkademie',['id'=>$akademia->akademiaID])}}" class="btn gui btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    {!!
                    Form::open(['method'=>'DELETE','action'=>['backend\AkademieController@destroy',$akademia->akademiaID]])
                    !!}
                    <button class="btn gui btn-default"> 
 <i class="fa fa-times" aria-hidden="true"></i>
</button> {!! Form::close() !!}


                </td>
</tr>
@endforeach
 
    </tbody>
</table>
 
      

@endsection
