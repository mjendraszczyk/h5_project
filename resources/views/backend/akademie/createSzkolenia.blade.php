@extends('layouts.app')

@section('title')
Dodaj akademie
@endsection

@section('content')
  
 <div class="panel-shadow">

 {!! Form::open(['url' => route("storeSzkolenie",['idAkademia'=>$idAkademia]),'id'=>'AkademiaSzkolenieForm','name'=>'AkademiaSzkolenieForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

<div class="form-group">
<label>Szkolenie</label>
{!! Form::select('trainingID', $trainingID,null, ['class'=>'form-control']) !!}

 {!! Form::hidden('akademiaID',$idAkademia) !!}
 </div>
 <div class="form-group">
 {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!} 
 </div>

{!! Form::close() !!} 
</div>
      

@endsection
