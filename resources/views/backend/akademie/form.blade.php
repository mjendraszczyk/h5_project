@if(Session::has('success')) 
<div class="alert alert-success">
    {{Session::get('success')}}
 </div>
@endif

@if(Session::has('errors')) 
<div class="alert alert-danger">
<ul>
<!-- {{Session::get('errors')}} -->
@foreach(Session::get('errors')->all() as $error)

  <!-- {{ Session::get('errors')}}  -->
  <li>{{$error}}</li>
  @endforeach
  </ul>
</div>
@endif


 
<div class="form-group">
    {!! Form::label('nazwa','Nazwa',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createAkademie')
    {!! Form::text('nazwa',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('nazwa',$akademia->nazwa,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

 
<div class="form-group">
    {!! Form::label('opis_krotki','Opis krótki',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createAkademie')
    {!! Form::textarea('opis_krotki',null,['class'=>'editor col-sm-12 form-control']) !!} @else {!! Form::textarea('opis_krotki',$akademia->opis_krotki,['class'=>'editor col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('opis','Opis',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createAkademie')
    {!! Form::textarea('opis',null,['class'=>'editor col-sm-12 form-control']) !!} @else {!! Form::textarea('opis',$akademia->opis,['class'=>'editor col-sm-12
    form-control']) !!} @endif
</div>
 
<div class="form-group">

  {!! Form::label('aktywna','Aktywna',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createAkademie')
    {!! Form::checkbox('aktywna', 1, null, ['class' => 'field']) !!} @else 

    @if($akademia->aktywna == '1')
    {!! Form::checkbox('aktywna', 1, true, ['class' => 'field']) !!}  
    @else 
    {!! Form::checkbox('aktywna', 0, false, ['class' => 'field']) !!}  
    @endif
    @endif

 
    </div>


<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() == 'createAkademie') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!} @else {!! Form::submit('Zapisz',
        ['class'=>'btn btn-success']) !!} @endif
    </div>