@extends('layouts.app') 
@section('title') Gry szkoleniowe
@endsection
 
@section('content')
<div class="row text-right no-margin">
    <a href="{{route('createGry')}}" class="btn btn-success"><i class="fa fa-plus"></i> Dodaj</a>
</div>

<table id="tableSort" class="table table-responsive table-hover">
    <thead>
        <th>ID</th>
        <th>Nazwa</th>
        <th>Podtytuł</th>
        <th></th>
    </thead>
    <tbody>
         
             
            @foreach($gry as $gra)
            <tr>
                 
                <td>
                    {{$gra->graID}}
                </td>
                <td>
                    {{$gra->nazwa}}
                </td>
                <td>
                    {{$gra->podtytul}}
                </td>
                 
                <td class="text-right">
                    <a href="{{route('editGry',['id'=>$gra->graID])}}" class="btn gui btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>{!!
                    Form::open(['method'=>'DELETE','action'=>['backend\GryController@destroy',$gra->graID]])
                    !!}
                    <button class="btn gui btn-default"> 
 <i class="fa fa-times" aria-hidden="true"></i>
</button> {!! Form::close() !!}


                </td>
            </tr>
            @endforeach

    </tbody>
</table>
@endsection