@extends('layouts.app')

@section('title')
Dodaj Gry szkoleniowe
@endsection

@section('content')
  
 <div class="panel-shadow">

 {!! Form::open(['url' => route("storeGry"),'id'=>'GryForm','name'=>'GryForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.gry.form')
 

{!! Form::close() !!} 
</div>
      

@endsection
