@if(Session::has('success')) 
<div class="alert alert-success">
    {{Session::get('success')}}
 </div>
@endif

@if(Session::has('errors')) 
<div class="alert alert-danger">
<ul>
<!-- {{Session::get('errors')}} -->
@foreach(Session::get('errors')->all() as $error)

  <!-- {{ Session::get('errors')}}  -->
  <li>{{$error}}</li>
  @endforeach
  </ul>
</div>
@endif
<div class="form-group">
    {!! Form::label('nazwa','Nazwa',['class'=>'col-sm-12 control-label']) !!} 
    
    @if(Route::CurrentRouteName() == 'createGry')

    {!! Form::text('nazwa',null,['class'=>'col-sm-12 form-control']) !!}

     @else 
     {!! Form::text('nazwa',$gra->nazwa,['class'=>'col-sm-12 form-control']) !!} 
    @endif
</div>
<div class="form-group">
    {!! Form::label('podtytul','Podtytuł',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createGry')
    {!! Form::text('podtytul',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('podtytul',$gra->podtytul,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('opis','Opis',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createGry') {!!
    Form::textarea('opis',null,['class'=>'editor col-sm-12 form-control']) !!} @else {!! Form::textarea('opis',$gra->opis,['class'=>'editor col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    {!! Form::label('dla_kogo','Dla kogo',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createGry') {!!
    Form::textarea('dla_kogo',null,['class'=>'editor col-sm-12 form-control']) !!} @else {!! Form::textarea('dla_kogo',$gra->dla_kogo,['class'=>'editor col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('organizacja','Organizacja',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createGry') {!!
    Form::textarea('organizacja',null,['class'=>'editor col-sm-12 form-control']) !!} @else {!! Form::textarea('organizacja',$gra->organizacja,['class'=>'editor col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('korzysci','Korzysci',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createGry') {!!
    Form::textarea('korzysci',null,['class'=>'editor col-sm-12 form-control']) !!} @else {!! Form::textarea('korzysci',$gra->korzysci,['class'=>'editor col-sm-12
    form-control']) !!} @endif
</div>

<!-- <div class="form-group">
    <div class="thumbnail">
    @if(Route::CurrentRouteName() == 'createGry')
        @else
        <img src="{{asset('img/frontend/')}}/{{$gra->zdjecie}}" class="thumbnail" />
        @endif
        </div>
    </div> -->
    <div class="form-group">
    {!! Form::label('picture','Obrazek',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createGry')
    @else
    <img src="{{Config('url')}}/img/frontend/{{$gra->zdjecie}}" alt="" class="img-responsive thumbnail" /> @endif {!! Form::file('image')
    !!}
</div>
 
<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() == 'createGry') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!} @else {!! Form::submit('Zapisz',
        ['class'=>'btn btn-success']) !!} @endif
    </div>