@extends('layouts.app') 
@section('title') Edycja Gry szkoleniowe
@endsection
 
@section('content')

<div class="panel-shadow">
    @foreach($gry as $gra) {!! Form::open(['method'=>'PATCH','url' => route("updateGry",['id'=>$gra->graID]),'enctype' => 'multipart/form-data','id'=>'GryForm','name'=>'GryForm','class'=>'form-vertical','style'=>'width:100%;'])
    !!}
    @include('backend.gry.form') {!! Form::close() !!} @endforeach
</div>
@endsection