@extends('layouts.app')
@section('title') Edycja Hotelu
@endsection

@section('content')

<div class="panel-shadow">
    @foreach($hotele as $hotel) {!! Form::open(['method'=>'PATCH','url' =>
    route("updateHotele",['id'=>$hotel->id_hotele]),'enctype'
    => 'multipart/form-data','id'=>'HoteleForm','name'=>'HoteleForm','class'=>'form-vertical','style'=>'width:100%;'])
    !!}
    @include('backend.hotele.form') {!! Form::close() !!} @endforeach
</div>
@endsection