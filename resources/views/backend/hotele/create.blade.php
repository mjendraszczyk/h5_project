@extends('layouts.app')

@section('title')
Dodaj Hotel
@endsection

@section('content')
  
 <div class="panel-shadow">

 {!! Form::open(['url' => route("storeHotele"),'id'=>'HoteleForm','name'=>'HoteleForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.hotele.form')
 

{!! Form::close() !!} 
</div>
      

@endsection
