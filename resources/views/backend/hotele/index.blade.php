@extends('layouts.app')

@section('title')
Hotele
@endsection

@section('content')

<div class="row text-right no-margin">
    <a href="{{route('createHotele')}}" class="btn btn-success"><i class="fa fa-plus"></i> Dodaj</a>
</div>

<table id="tableSort" table-data-sort="0" table-data-sort-type="asc" class="table table-responsive table-hover">
    <thead>
        <th>ID</th>
        <th>Nazwa</th>
        <th>Parking</th>
        <th>Wazne</th>
        <th></th>
    </thead>
    <tbody>

        @foreach($hotele as $hotel)
        <tr>
            <td>
                {{$hotel->id_hotele}}
            </td>
            <td>
                {{$hotel->nazwa}}
            </td>
            <td>
                @if($hotel->parking != '')
                {{$hotel->parking}}
                @else
                Brak
                @endif
            </td>

            <td>
                @if($hotel->parking != '')
                {{$hotel->wazne}}
                @else
                Nie
                @endif
            </td>
            <td class="text-right">

                <a href="{{route('editHotele',['id'=>$hotel->id_hotele])}}" class="btn gui btn-default"><i
                        class="fa fa-pencil-square-o" aria-hidden="true"></i></a>{!!
                Form::open(['method'=>'DELETE','action'=>['backend\HoteleController@destroy',$hotel->id_hotele]])
                !!}
                <button class="btn gui btn-default">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button> {!! Form::close() !!}


            </td>
        </tr>
        @endforeach

    </tbody>
</table>



@endsection