@if(Session::has('success'))
<div class="alert alert-success">
    {{Session::get('success')}}
</div>
@endif

@if(Session::has('errors'))
<div class="alert alert-danger">
    <ul>
        <!-- {{Session::get('errors')}} -->
        @foreach(Session::get('errors')->all() as $error)

        <!-- {{ Session::get('errors')}}  -->
        <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
@endif



<div class="form-group">
    {!! Form::label('nazwa','Nazwa',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createHotele')
    {!! Form::text('nazwa',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::text('nazwa',$hotel->nazwa,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
<div class="form-group">
    {!! Form::label('odleglosc_od','Odległosc od',['class'=>'col-sm-12 control-label']) !!}
    @if(Route::CurrentRouteName() ==
    'createHotele')
    {!! Form::text('odleglosc_od',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::text('odleglosc_od',$hotel->odleglosc_od,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    {!! Form::label('kod','Kod',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createHotele')
    {!! Form::text('kod',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::text('kod',$hotel->kod,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
<div class="form-group">
    {!! Form::label('miasto','Miasto',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createHotele')
    {!! Form::text('miasto',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::text('miasto',$hotel->miasto,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('ulica','Ulica',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createHotele')
    {!! Form::text('ulica',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::text('ulica',$hotel->ulica,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    {!! Form::label('telefon','Telefon',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createHotele')
    {!! Form::text('telefon',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::text('telefon',$hotel->telefon,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>



<div class="form-group">
    {!! Form::label('cena_pokoj_jedno','Cena pokoj jednooosbowy',['class'=>'col-sm-12 control-label']) !!}
    @if(Route::CurrentRouteName() ==
    'createHotele')
    {!! Form::text('cena_pokoj_jedno',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::text('cena_pokoj_jedno',$hotel->cena_pokoj_jedno,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    {!! Form::label('uwagi','Uwagi',['class'=>'col-sm-12 control-label']) !!}
    @if(Route::CurrentRouteName() ==
    'createHotele')
    {!! Form::textarea('uwagi',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::textarea('uwagi',$hotel->uwagi,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    @if(Route::CurrentRouteName() == 'createHotele')
    <label>{!! Form::checkbox('parking','1', false, ['class'=>'checkbox-inline']) !!} Parking</label>
    @else
    @if($hotel->parking == '1')
    <label>{!! Form::checkbox('parking','1', true, ['class'=>'checkbox-inline']) !!} Parking</label>
    @else
    <label>{!! Form::checkbox('parking','1', false, ['class'=>'checkbox-inline']) !!} Parking</label>
    @endif
    @endif
</div>

<div class="form-group">
    {!! Form::label('cena_parking','Cena parking',['class'=>'col-sm-12 control-label']) !!}
    @if(Route::CurrentRouteName() ==
    'createHotele')
    {!! Form::text('cena_parking',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::text('cena_parking',$hotel->cena_parking,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    @if(Route::CurrentRouteName() == 'createHotele')
    <label>{!! Form::checkbox('wazne','1', false, ['class'=>'checkbox-inline']) !!} Wazne</label>
    @else
    @if($hotel->wazne == '1')
    <label>{!! Form::checkbox('wazne','1', true, ['class'=>'checkbox-inline']) !!} Wazne</label>
    @else
    <label>{!! Form::checkbox('wazne','1', false, ['class'=>'checkbox-inline']) !!} Wazne</label>
    @endif
    @endif
</div>

<div class="form-group">
    {!! Form::label('zdjecie1','Zdjęcie 1',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createHotele')
    @else
    <img src="{{public_path('/img/frontend/hotele/')}}{{$hotel->zdjecie1}}" alt="" class="thumbnail" /> @endif {!!
    Form::file('zdjecie1')
    !!}
</div>

<div class="form-group">
    {!! Form::label('zdjecie2','Zdjęcie 2',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createHotele')
    @else
    <img src="{{Config('url')}}{{public_path('/img/frontend/hotele/')}}{{$hotel->zdjecie2}}" alt="" class="thumbnail" />
    @endif {!!
    Form::file('zdjecie2')
    !!}
</div>

<div class="form-group">
    {!! Form::label('term_placeID','Miejsce',['class'=>'col-sm-12 control-label']) !!}
    @if(Route::CurrentRouteName() ==
    'createHotele')
    {!! Form::select('term_placeID',$term_placeID,'',['class'=>'col-sm-12
    form-control'])
    !!}
    @else

    {!! Form::select('term_placeID',$term_placeID,$hotel->term_placeID,['class'=>'col-sm-12
    form-control'])
    !!}
    @endif
</div>
<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() == 'createHotele') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!}
        @else {!!
        Form::submit('Zapisz', ['class'=>'btn btn-success']) !!} @endif
    </div>