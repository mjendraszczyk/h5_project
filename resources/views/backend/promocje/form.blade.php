@if(Session::has('success')) 
<div class="alert alert-success">
    {{Session::get('success')}}
 </div>
@endif

@if(Session::has('errors')) 
<div class="alert alert-danger">
<ul>
<!-- {{Session::get('errors')}} -->
@foreach(Session::get('errors')->all() as $error)

  <!-- {{ Session::get('errors')}}  -->
  <li>{{$error}}</li>
  @endforeach
  </ul>
</div>
@endif

 

<div class="form-group">
    {!! Form::label('news_title','Tytuł',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createPromocje')
    {!! Form::text('news_title',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('news_title',$promocja->news_title,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
  
  <div class="form-group">
    {!! Form::label('news_lid','Lid',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createPromocje')
    {!! Form::textarea('news_lid',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::textarea('news_lid',$promocja->news_lid,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
  
  <div class="form-group">
    {!! Form::label('news_text','Tekst',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createPromocje')
    {!! Form::textarea('news_text',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::textarea('news_text',$promocja->news_text,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
  
<div class="form-group">
    {!! Form::label('news_link','Link dodatkowy',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createPromocje')
    {!! Form::text('news_link',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('news_link',$promocja->news_link,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
 polozenie radio 
 
<div class="form-group">
    {!! Form::label('news_picture','Zdjęcie',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createPromocje')
    @else
    <img src="{{Config('url')}}/img/frontend/news/{{$promocja->news_picture}}" alt="" class="thumbnail" /> @endif {!! Form::file('news_picture')
    !!}
</div>

<div class="form-group">
    {!! Form::label('polozenie_zdj','Połozenie zdjęcia',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createPromocje')
    {!!Form::radio('news_pictur_pos', 'left', false) !!} Lewo
    {!!Form::radio('news_pictur_pos', 'right', false) !!} Prawo
    @else
    @if($promocja->news_pictur_pos == 'left')
    {!!Form::radio('news_pictur_pos', 'left', true) !!} Lewo
    {!!Form::radio('news_pictur_pos', 'right', false) !!} Prawo
    @elseif($promocja->news_pictur_pos == 'right')
    {!!Form::radio('news_pictur_pos', 'left', false) !!} Lewo
    {!!Form::radio('news_pictur_pos', 'right', true) !!} Prawo
    @else
    {!!Form::radio('news_pictur_pos', 'left', false) !!} Lewo
    {!!Form::radio('news_pictur_pos', 'right', false) !!} Prawo
    @endif

    @endif 
</div>





<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() == 'createPromocje') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!} @else {!!
        Form::submit('Zapisz', ['class'=>'btn btn-success']) !!} @endif
    </div>