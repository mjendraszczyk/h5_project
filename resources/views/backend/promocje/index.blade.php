@extends('layouts.app')

@section('title')
Promocje
@endsection

@section('content')
 
 <div class="row text-right no-margin">
    <a href="{{route('createPromocje')}}" class="btn btn-success"><i class="fa fa-plus"></i> Dodaj</a>
</div>




<table id="tableSort" class="table table-responsive table-hover">
 <thead>
        <th>ID</th>
        <th>Data</th>
<th>Tytuł</th>
<th>Miejsce</th>

<th></th>
    </thead>
  
<tbody>
    @foreach($promocje as $promocja)
<tr>
<td>
{{$promocja->newsID}}
</td>
<td>
{{$promocja->news_data}}
</td>
<td>
{{$promocja->news_title}}
</td>
<td>
{{$promocja->news_place}}
</td>

<td class="text-right">
                    <a href="{{route('editPromocje',['id'=>$promocja->newsID])}}" class="btn gui btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    {!!
                    Form::open(['method'=>'DELETE','action'=>['backend\PromocjeController@destroy',$promocja->newsID]])
                    !!}
                    <button class="btn gui btn-default"> 
 <i class="fa fa-times" aria-hidden="true"></i>
</button> {!! Form::close() !!}


                </td>
</tr>
@endforeach
    </tbody>
    </table>


@endsection
