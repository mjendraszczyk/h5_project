@extends('layouts.app')

@section('title')
Dodaj promocje
@endsection

@section('content')
  
<div class="panel-shadow">

 {!! Form::open(['url' => route("storePromocje"),'id'=>'PromocjeForm','name'=>'PromocjeForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.promocje.form')
 

{!! Form::close() !!} 
</div>
      

@endsection
