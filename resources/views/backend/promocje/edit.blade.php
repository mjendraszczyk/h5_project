@extends('layouts.app') 
@section('title') Edycja promocji
@endsection
 
@section('content')

<div class="panel-shadow">
    @foreach($promocje as $promocja) {!! Form::open(['method'=>'PATCH','url' => route("updatePromocje",['id'=>$promocja->newsID]),'enctype'
    => 'multipart/form-data','id'=>'PromocjeForm','name'=>'PromocjeForm','class'=>'form-vertical','style'=>'width:100%;'])
    !!}
    @include('backend.promocje.form') {!! Form::close() !!} @endforeach
</div>
@endsection