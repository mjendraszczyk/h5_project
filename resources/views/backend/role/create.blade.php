@extends('layouts.app')

@section('title')
Role
@endsection

@section('content')

<div class="panel-shadow">

    {!! Form::open(['url' => route("storeRole"),'id'=>'RoleForm','name'=>'RoleForm','enctype' =>
    'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

    @include('backend.role.form')


    {!! Form::close() !!}
</div>


@endsection