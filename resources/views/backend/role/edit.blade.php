@extends('layouts.app')
@section('title') Edycja roli
@endsection

@section('content')

<div class="panel-shadow">
    @foreach($role as $rola) {!! Form::open(['method'=>'PATCH','url' =>
    route("updateRole",['id'=>$rola->id_role]),'enctype'
    => 'multipart/form-data','id'=>'RoleForm','name'=>'RoleForm','class'=>'form-vertical','style'=>'width:100%;'])
    !!}
    @include('backend.role.form') {!! Form::close() !!} @endforeach
</div>
@endsection