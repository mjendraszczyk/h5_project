@if(Session::has('success'))
<div class="alert alert-success">
    {{Session::get('success')}}
</div>
@endif

@if(Session::has('errors'))
<div class="alert alert-danger">
    <ul>

        @foreach(Session::get('errors')->all() as $error)


        <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="form-group">
    {!! Form::label('name','Nazwa',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createRole')
    {!! Form::text('name',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::text('name',$rola->name,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() == 'createRole') {!! Form::submit('Dodaj', ['class'=>'btn btn-success'])
        !!} @else {!!
        Form::submit('Zapisz', ['class'=>'btn btn-success']) !!} @endif
    </div>