@if(Session::has('success'))
<div class="alert alert-success">
    {{Session::get('success')}}
</div>
@endif

@if(Session::has('errors'))
<div class="alert alert-danger">
    <ul>

        @foreach(Session::get('errors')->all() as $error)


        <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="form-group">
    {!! Form::label('name','Imie',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createUzytkownicy')
    {!! Form::text('name',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::text('name',$uzytkownik->name,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('email','E-mail',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createUzytkownicy')
    {!! Form::text('email',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::text('email',$uzytkownik->email,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('password','Hasło',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createUzytkownicy')
    {!! Form::password('password',['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::password('password',['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('id_role','Rola',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createUzytkownicy')
    {!! Form::select('id_role',$role, 1, ['class'=>'form-control']) !!}
    @else
    {!! Form::select('id_role',$role, $uzytkownik->id_role, ['class'=>'form-control']) !!}
    @endif
</div>


<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() == 'createUzytkownicy') {!! Form::submit('Dodaj', ['class'=>'btn btn-success'])
        !!} @else {!!
        Form::submit('Zapisz', ['class'=>'btn btn-success']) !!} @endif
    </div>