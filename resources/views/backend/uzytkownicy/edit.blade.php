@extends('layouts.app') 
@section('title') Edycja uzytkownika
@endsection
 
@section('content')

<div class="panel-shadow">
    @foreach($uzytkownicy as $uzytkownik) {!! Form::open(['method'=>'PATCH','url' => route("updateUzytkownicy",['id'=>$uzytkownik->id]),'enctype'
    => 'multipart/form-data','id'=>'UzytkownicyForm','name'=>'UzytkownicyForm','class'=>'form-vertical','style'=>'width:100%;'])
    !!}
    @include('backend.uzytkownicy.form') {!! Form::close() !!} @endforeach
</div>
@endsection