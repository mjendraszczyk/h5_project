@extends('layouts.app')

@section('title')
Uzytkownicy
@endsection

@section('content')
  
 <div class="panel-shadow">

 {!! Form::open(['url' => route("storeUzytkownicy"),'id'=>'UzytkownicyForm','name'=>'UzytkownicyForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.uzytkownicy.form')
 

{!! Form::close() !!} 
</div>
      

@endsection
