@extends('layouts.app')
@section('title') Szkolenia
@endsection

@section('content')
<div class="row text-right no-margin">
    <a href="{{route('createSzkolenia')}}" class="btn btn-success"><i class="fa fa-plus"></i> Dodaj</a>
</div>
<div class="text-center category-panel">
    @foreach($kategorie as $kcat => $kategoria)

    <span class="kategoria_{{$kcat}} navKategorie btn btn-default"
        style="margin:5px;">{{$kategoria->category_title}}</span>
    @endforeach
    <div class="clearfix"></div>
    <label style="float: left">Typ szkolenia:</label>
    <select name="training_type" class="form-control" style="float: left;width:auto">
        <option value="1">Otwarte</option>
        <option value="2">Online</option>
        <option value="3">Otwarte/Online</option>
    </select>
</div>

@foreach($kategorie as $kcatb => $kategoria)

<div class="box{{$kcatb}} boxKategorie category-panel">
    <h1> {{$kategoria->category_title}}</h1>
    <div class="row headline-row">
        <div class="col-sm-2">Pozycja
        </div>
        <div class="col-sm-3">Szkolenie
        </div>
        <div class="col-sm-2">Kategoria
        </div>
        <div class="col-sm-2">Cena
        </div>
        <div class="col-sm-1">Typ
        </div>
        <div class="col-sm-1">
        </div>
        <div class="col-sm-1">
        </div>
    </div>


    <ul class="sortable">

        @foreach($szkolenia as $szkolenie)

        @if($szkolenie->categoryID == $kategoria->categoryID)
        <li data-id="{{$szkolenie->cattrainID}}" data-pos="{{$szkolenie->position}}"
            data-url="{{route('change_position',['id'=>$szkolenie->cattrainID])}}"
            data-training_type="{{$szkolenie->training_type}}">
            <div class="col-sm-2">
                <div class="col-sm-6">
                    <div style="margin:25px 0;">
                        <i class="fa fa-arrows" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="col-sm-6">
                    <span class="position_box">
                        {!! Form::text('position',$szkolenie->position,['class'=>'position_input
                        form-control','readonly']) !!}
                    </span>
                </div>
            </div>
            <div class="col-sm-3">
                {{$szkolenie->training_title}}
            </div>
            <div class="col-sm-2">
                {{$kategoria->category_title}}
            </div>
            <div class="col-sm-2">

                {{number_format($szkolenie->training_price,2,'.',' ')}} {{Config::get('app.currency')}}
            </div>

            <div class="col-sm-1">
                {{$szkolenie->training_type}}
            </div>
            <div class="col-sm-1">
                <a class="opinie" href="{{route('indexSzkoleniaOpinia',['id'=>$szkolenie->trainingID])}}"
                    style="margin:0 10px;">

                    {{App\Http\Controllers\backend\SzkoleniaController::opinieQty($szkolenie->trainingID)}}

                    <i class="fa fa-star-o" aria-hidden="true"></i></a>

            </div>

            <div class="col-sm-1 text-right">
                <a href="{{route('editSzkolenia',['id'=>$szkolenie->trainingID])}}" class="btn gui btn-default"><i
                        class="fa fa-pencil-square-o" aria-hidden="true"></i></a>{!!
                Form::open(['method'=>'DELETE','action'=>['backend\SzkoleniaController@destroy',$szkolenie->trainingID]])
                !!}
                <button class="btn gui btn-default">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button> {!! Form::close() !!}


            </div>
        </li>
        @endif

        @endforeach

    </ul>



</div>



@endforeach
<div class="category-panel">
    <h1>Sieroty (bez kategorii)</h1>
    <div class="row headline-row">
        <div class="col-sm-2">Pozycja
        </div>
        <div class="col-sm-3">Szkolenie
        </div>
        <div class="col-sm-2">Kategoria
        </div>
        <div class="col-sm-2">Cena
        </div>
        <div class="col-sm-1">Typ</div>
        <div class="col-sm-1"></div>
        <div class="col-sm-1"></div>
    </div>


    <ul class="block">
        @foreach($getSieroty as $sierota)

        <li style="list-style:none;" data-id="0" data-pos="0" data-url="#">
            <div class="col-sm-2">
                <div class="col-sm-6">
                    <div style="margin:25px 0;">
                        <i class="fa fa-eye-slash" aria-hidden="true"></i>

                    </div>
                </div>
                <div class="col-sm-6">

                </div>
            </div>
            <div class="col-sm-3">
                {{$sierota->training_title}}
            </div>
            <div class="col-sm-2">

            </div>
            <div class="col-sm-2">

                {{number_format($sierota->training_price,2,'.',' ')}} {{Config::get('app.currency')}}
            </div>
            <div class="col-sm-1">
                {{$sierota->training_type}}
            </div>
            <div class="col-sm-1">
                <a class="opinie" href="{{route('indexSzkoleniaOpinia',['id'=>$sierota->trainingID])}}"
                    style="margin:0 10px;">

                    {{App\Http\Controllers\backend\SzkoleniaController::opinieQty($sierota->trainingID)}}

                    <i class="fa fa-star-o" aria-hidden="true"></i></a>

            </div>

            <div class="col-sm-1 text-right">
                <a href="{{route('editSzkolenia',['id'=>$sierota->trainingID])}}" class="btn gui btn-default"><i
                        class="fa fa-pencil-square-o" aria-hidden="true"></i></a>{!!
                Form::open(['method'=>'DELETE','action'=>['backend\SzkoleniaController@destroy',$sierota->trainingID]])
                !!}
                <button class="btn gui btn-default">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </button> {!! Form::close() !!}


            </div>
        </li>
        @endforeach
    </ul>
</div>
@endsection