@extends('layouts.app')

@section('title')
Dodaj Szkolenie
@endsection

@section('content')
  
 <div class="panel-shadow">

 {!! Form::open(['url' => route("storeSzkolenia"),'id'=>'SzkoleniaForm','name'=>'SzkoleniaForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.szkolenia.form')
 

{!! Form::close() !!} 
</div>
      

@endsection
