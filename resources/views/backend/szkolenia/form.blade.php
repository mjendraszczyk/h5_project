@if(Session::has('success'))
<div class="alert alert-success">
    {{Session::get('success')}}
</div>
@endif

@if(Session::has('errors'))
<div class="alert alert-danger">
    <ul>
        <!-- {{Session::get('errors')}} -->
        @foreach(Session::get('errors')->all() as $error)

        <!-- {{ Session::get('errors')}}  -->
        <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="form-group">
    {!! Form::label('categoryID','Kategoria',['class'=>'col-sm-12 control-label']) !!}

    @if(Route::CurrentRouteName() == 'createSzkolenia')

    @foreach($kategorie as $kategoria)

    <label>{!! Form::checkbox('categoryID[]', $kategoria->categoryID,false, ['class'=>'']) !!}
        {{$kategoria->category_title}}
    </label>
    @endforeach

    @else


    @foreach($kategorie as $kategoria)
    <label>
        {{--if in_array item then set val 1  --}}
        @if(in_array($kategoria->categoryID,$cattrain))
        {!! Form::checkbox('categoryID[]', $kategoria->categoryID,true, ['class'=>'']) !!}
        {{$kategoria->category_title}}
        @else
        {!! Form::checkbox('categoryID[]', $kategoria->categoryID,false, ['class'=>'']) !!}
        {{$kategoria->category_title}}
        @endif
        {{-- else val 0   --}}

    </label>
    @endforeach



    @endif
</div>

<div class="form-group">
    {!! Form::label('training_title','Tytuł',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createSzkolenia')
    {!! Form::text('training_title',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::text('training_title',$szkolenie->training_title,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('training_lid','Wstep',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createSzkolenia')
    {!! Form::textarea('training_lid',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::textarea('training_lid',$szkolenie->training_lid,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    {!! Form::label('training_cite','Cytat',['class'=>'col-sm-12 control-label']) !!}

    @if(Route::CurrentRouteName() == 'createSzkolenia')

    {!! Form::select('training_cite', $cytaty,null, ['class'=>'form-control']) !!}

    @else
    {!! Form::select('training_cite', $cytaty,$szkolenie->training_cite, ['class'=>'form-control']) !!}

    @endif
</div>

<div class="form-group">
    {!! Form::label('training_tag','Tagi',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createSzkolenia')
    {!! Form::text('training_tag',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::text('training_tag',implode(", ",$tagi) ,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('training_advantage','Korzysci',['class'=>'col-sm-12 control-label']) !!}
    @if(Route::CurrentRouteName() == 'createSzkolenia')
    {!! Form::textarea('training_advantage',null,['class'=>'editor col-sm-12 form-control']) !!} @else {!!
    Form::textarea('training_advantage',$szkolenie->training_advantage,['class'=>'editor col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    {!! Form::label('training_program','Program',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName()
    == 'createSzkolenia')
    {!! Form::textarea('training_program',null,['class'=>'editor col-sm-12 form-control']) !!} @else {!!
    Form::textarea('training_program',$szkolenie->training_program,['class'=>'editor col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    {!! Form::label('training_demand','Wymagania',['class'=>'col-sm-12 control-label']) !!}
    @if(Route::CurrentRouteName() == 'createSzkolenia')
    {!! Form::textarea('training_demand',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::textarea('training_demand',$szkolenie->training_demand,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('training_type','Typ szkolenia',['class'=>'col-sm-12 control-label']) !!}
    @if(Route::CurrentRouteName() ==
    'createSzkolenia')
    @foreach($typy_szkolenia as $key => $typ_szkolenia)
    @if(($key+1) == 1)
    <label>
        {!! Form::radio('training_type',($key+1), true, ['data-training'=>str_slug($typ_szkolenia),
        'class'=>'radio-inline']) !!}
        {{$typ_szkolenia}}</label>
    @else
    <label>
        {!! Form::radio('training_type',($key+1), false,
        ['data-training'=>str_slug($typ_szkolenia),'class'=>'radio-inline']) !!}
        {{$typ_szkolenia}}</label>
    @endif
    @endforeach
    @else
    @foreach($typy_szkolenia as $key=> $typ_szkolenia)
    @if($szkolenie->training_type == ($key+1))
    <label>{!! Form::radio('training_type',($key+1), true,
        ['data-training'=>str_slug($typ_szkolenia),'class'=>'radio-inline']) !!}
        {{$typ_szkolenia}}</label>
    @else
    <label>{!! Form::radio('training_type',($key+1), false,
        ['data-training'=>str_slug($typ_szkolenia),'class'=>'radio-inline']) !!}
        {{$typ_szkolenia}}</label>
    @endif
    @endforeach
    @endif
</div>
<div class="col-md-6 training_type otwarte-domyslnie" @if(Route::CurrentRouteName() !='createSzkolenia' ) @if (
    ($szkolenie->training_type == '2')) style="display:none;" @endif @endif>
    <div class="form-group">
        {!! Form::label('training_price','Cena',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName()
        ==
        'createSzkolenia')
        {!! Form::text('training_price',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
        Form::text('training_price',$szkolenie->training_price,['class'=>'col-sm-12
        form-control']) !!} @endif
    </div>

    <div class="form-group">
        {!! Form::label('training_hours','Godziny',['class'=>'col-sm-12 control-label']) !!}
        @if(Route::CurrentRouteName()
        == 'createSzkolenia')
        {!! Form::text('training_hours',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
        Form::text('training_hours',$szkolenie->training_hours,['class'=>'col-sm-12
        form-control']) !!} @endif
    </div>

    @if(Route::CurrentRouteName() != 'createSzkolenia')
    @php
    $tab_activities = explode(";",$szkolenie->training_activities);
    array_pop($tab_activities);
    @endphp
    @endif

    <div class="form-group">
        @for($i=0;$i<=11;$i++) <label class="col-sm-12">

            @if(Route::CurrentRouteName() == 'createSzkolenia')

            {!! Form::checkbox('training_activities[]', $i,null, ['class'=>'']) !!}

            @else
            @if(in_array($i,$tab_activities))
            {!! Form::checkbox('training_activities[]', $i,1, ['class'=>'']) !!}

            @else
            {!! Form::checkbox('training_activities[]', $i,0, ['class'=>'']) !!}
            @endif
            @endif
            {{$aktywnosci_tab[$i]}}
            </label>
            @endfor
    </div>
</div>

<div class="col-md-6 training_type online" @if(Route::CurrentRouteName() !='createSzkolenia' ) @if (($szkolenie->
    training_type == '1')) style="display:none;" @endif @endif>
    <div class="form-group">
        {!! Form::label('training_price1','Cena',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName()
        ==
        'createSzkolenia')
        {!! Form::text('training_price1',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
        Form::text('training_price1',$szkolenie->training_price1,['class'=>'col-sm-12
        form-control']) !!} @endif
    </div>

    <div class="form-group">
        {!! Form::label('training_hours1','Godziny',['class'=>'col-sm-12 control-label']) !!}
        @if(Route::CurrentRouteName()
        == 'createSzkolenia')
        {!! Form::text('training_hours1',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
        Form::text('training_hours1',$szkolenie->training_hours1,['class'=>'col-sm-12
        form-control']) !!} @endif
    </div>

    @if(Route::CurrentRouteName() != 'createSzkolenia')
    @php
    $tab_activities1 = explode(";",$szkolenie->training_activities1);
    array_pop($tab_activities1);
    @endphp
    @endif

    <div class="form-group">
        @for($i=0;$i<=11;$i++) <label class="col-sm-12">

            @if(Route::CurrentRouteName() == 'createSzkolenia')

            {!! Form::checkbox('training_activities1[]', $i,null, ['class'=>'']) !!}

            @else
            @if(in_array($i,$tab_activities1))
            {!! Form::checkbox('training_activities1[]', $i,1, ['class'=>'']) !!}

            @else
            {!! Form::checkbox('training_activities1[]', $i,0, ['class'=>'']) !!}
            @endif
            @endif
            {{$aktywnosci_tab[$i]}}
            </label>
            @endfor
    </div>
</div>


<div class="form-group">
    {!! Form::label('training_tr1','Trener 1',['class'=>'col-sm-12 control-label']) !!}

    @if(Route::CurrentRouteName() == 'createSzkolenia')

    {!! Form::select('training_tr1', $trenerzy,null, ['class'=>'form-control']) !!}

    @else
    {!! Form::select('training_tr1', $trenerzy,$szkolenie->training_tr1, ['class'=>'form-control']) !!}

    @endif
</div>

<div class="form-group">
    {!! Form::label('training_tr2','Trener 2',['class'=>'col-sm-12 control-label']) !!}

    @if(Route::CurrentRouteName() == 'createSzkolenia')

    {!! Form::select('training_tr2', $trenerzy,null, ['class'=>'form-control']) !!}

    @else
    {!! Form::select('training_tr2', $trenerzy,$szkolenie->training_tr2, ['class'=>'form-control']) !!}

    @endif
</div>

<div class="form-group">
    {!! Form::label('training_tr3','Trener 3',['class'=>'col-sm-12 control-label']) !!}

    @if(Route::CurrentRouteName() == 'createSzkolenia')

    {!! Form::select('training_tr3', $trenerzy,null, ['class'=>'form-control']) !!}

    @else
    {!! Form::select('training_tr3', $trenerzy,$szkolenie->training_tr3, ['class'=>'form-control']) !!}

    @endif
</div>

<div class="form-group">
    <label>

        @if(Route::CurrentRouteName() == 'createSzkolenia')

        {!! Form::checkbox('training_news', '1',null, ['class'=>'']) !!}

        @else

        @if($szkolenie->training_news == '1')
        {!! Form::checkbox('training_news', '0',1, ['class'=>'']) !!}
        @else
        {!! Form::checkbox('training_news', '1',0, ['class'=>'']) !!}
        @endif
        @endif
        Nowosc
    </label>
</div>

<div class="form-group">
    <label>

        @if(Route::CurrentRouteName() == 'createSzkolenia')

        {!! Form::checkbox('modul_programu', '1',null, ['class'=>'']) !!}

        @else
        @if($szkolenie->modul_programu == '1')
        {!! Form::checkbox('modul_programu', '0',1, ['class'=>'']) !!}
        @else
        {!! Form::checkbox('modul_programu', '1',0, ['class'=>'']) !!}
        @endif
        @endif
        Szkolenie jest modulem programu
    </label>
</div>




<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() == 'createLink') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!} @else
        {!! Form::submit('Zapisz',
        ['class'=>'btn btn-success']) !!} @endif
    </div>