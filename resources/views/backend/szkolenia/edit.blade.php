@extends('layouts.app') 
@section('title') Edycja Szkolenia  
@endsection
 
@section('content')
 
<div class="panel-shadow">
  {{-- {{print_r($szkolenia)}} --}}
      @foreach($szkolenia as $key => $szkolenie)  
    {!! Form::open(['method'=>'PATCH','url' => route("updateSzkolenia",['id'=>$szkolenie->trainingID]),'enctype' => 'multipart/form-data','id'=>'SzkoleniaForm','name'=>'SzkoleniaForm','class'=>'form-vertical','style'=>'width:100%;'])
    !!}
     
       @if($key == 0)   
     @include('backend.szkolenia.form')  
      @endif     
    {!! Form::close() !!} 
    
     @endforeach  
</div>
@endsection