@extends('layouts.app')

@section('title')
Dodaj Program
@endsection

@section('content')
  <div class="panel-shadow">

 {!! Form::open(['url' => route("storeProgramy"),'id'=>'ProgramyForm','name'=>'ProgramyForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.programy.form')
 

{!! Form::close() !!} 
</div>
      

@endsection
