@if(Session::has('success')) 
<div class="alert alert-success">
    {{Session::get('success')}}
 </div>
@endif

@if(Session::has('errors')) 
<div class="alert alert-danger">
<ul>
<!-- {{Session::get('errors')}} -->
@foreach(Session::get('errors')->all() as $error)

  <!-- {{ Session::get('errors')}}  -->
  <li>{{$error}}</li>
  @endforeach
  </ul>
</div>
@endif

 

<div class="form-group">
    {!! Form::label('nezwa','Nazwa',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createProgramy')
    {!! Form::text('nazwa',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('nazwa',$program->nazwa,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('podtytul','Podtytuł',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createProgramy')
    {!! Form::text('podtytul',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('podtytul',$program->podtytul,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('korzysc','Korzysc',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createProgramy')
    {!! Form::textarea('korzysc',null,['class'=>'editor col-sm-12 form-control']) !!} @else {!! Form::textarea('korzysc',$program->korzysc,['class'=>'editor col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('dla_kogo','Dla kogo',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createProgramy')
    {!! Form::textarea('dla_kogo',null,['class'=>'editor col-sm-12 form-control']) !!} @else {!! Form::textarea('dla_kogo',$program->dla_kogo,['class'=>'editor col-sm-12
    form-control']) !!} @endif
</div>



<div class="form-group">
    {!! Form::label('cena','Cena',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createProgramy')
    {!! Form::text('cena',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('cena',$program->cena,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

 



 

 
<div class="form-group">
    @if(Route::CurrentRouteName() == 'createProgramy')
    <label>{!! Form::checkbox('aktywny','1', false, ['class'=>'radio-inline']) !!} Aktywny</label>
     
    @else 

    @if($program->aktywny == '1')
        
        <label>{!! Form::checkbox('aktywny','1', true, ['class'=>'radio-inline']) !!} Aktywny</label>
         

    @elseif ($program->aktywny == '0')
    
        <label>{!! Form::checkbox('aktywny','1', false, ['class'=>'radio-inline']) !!} Aktywny</label>
    
    @else 
        
        <label>{!! Form::checkbox('aktywny','1', true, ['class'=>'radio-inline']) !!} Aktywny</label>
    
    @endif

     @endif
</div>  

 


<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() == 'createProgramy') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!} @else {!!
        Form::submit('Zapisz', ['class'=>'btn btn-success']) !!} @endif
    </div>