
@extends('layouts.app')

@section('title')
Programy
@endsection

@section('content')
<div class="category-panel">
<h1> Dodaj szkolenie do programu</h1>
<h4>{{(json_decode(json_encode($getProgramName[0]->nazwa)))}}</h4>
 <div class="row headline-row">
      {!! Form::open(['url' => route("storeModulyProgramy",['id'=>$id]),'id'=>'storeModulyProgramy','name'=>'storeModulyProgramy','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

     <div class="form-group">
     {!! Form::label('Szkolenie','Szkolenie',['class'=>'col-sm-12 control-label']) !!}
     {!! Form::select('trainingID',$getSzkoleniaDependProgram,null,['class'=>'form-control']) !!}
     </div>
     <div class="form-group">
 {!! Form::submit('Dodaj',['class'=>'btn btn-success']) !!}
 </div>
 </div>
 
   {!! Form::close() !!}
    </div>
    @endsection