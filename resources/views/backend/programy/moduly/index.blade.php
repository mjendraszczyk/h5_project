
@extends('layouts.app')

@section('title')
Programy
@endsection

@section('content')
<div class="category-panel">
<h1> Moduły programów szkoleniowych</h1>
 <div class="row headline-row">
     <div class="col-sm-2">Pozycja
</div>
     <div class="col-sm-10">Szkolenie
</div>
 
 </div>
 {{--{{print_r($getSzkoleniaDependProgram)}} --}}
    <ul class="sortable">
 @foreach($getSzkoleniaDependProgram as $program)
<li data-id="{{$program->programID}}" data-pos="{{$program->kolejnosc}}" data-url="{{route('change_position_program',['id'=>$program->program_modulID,'kolejnosc'=>$program->kolejnosc])}}">


<div class="col-sm-2">
<div class="col-sm-6">
<div style="margin: 25px 0px;"><i aria-hidden="true" class="fa fa-arrows"></i></div>
</div> <div class="col-sm-6"><span class="position_box">
<input readonly="readonly" name="position" type="text" class="position_input form-control" value="{{$program->kolejnosc}}">
</span>
</div></div>


<div class="col-sm-10">
<div class="col-sm-11">
{{$program->training_title}}
</div>
<div class="col-sm-1">
 {!!
                    Form::open(['method'=>'DELETE','action'=>['backend\ProgramyController@destroyModulyProgramy','id'=>$program->programID,'idSzkolenie'=>$program->program_modulID]])
                    !!}
                    <button class="btn gui btn-default"> 
 <i class="fa fa-times" aria-hidden="true"></i>
</button> {!! Form::close() !!}
</div></li>
 @endforeach

  
</ul>

    </div>
    @endsection