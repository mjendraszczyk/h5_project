@extends('layouts.app') 
@section('title') Edycja Programu
@endsection
 
@section('content')

<div class="panel-shadow">
    @foreach($programy as $program) {!! Form::open(['method'=>'PATCH','url' => route("updateProgramy",['id'=>$program->programID]),'enctype'
    => 'multipart/form-data','id'=>'ProgramyForm','name'=>'ProgramyForm','class'=>'form-vertical','style'=>'width:100%;'])
    !!}
    @include('backend.programy.form') {!! Form::close() !!} @endforeach
</div>
@endsection