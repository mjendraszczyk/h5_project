@extends('layouts.app')

@section('title')
Programy
@endsection

@section('content')
 <div class="row text-right no-margin">
    <a href="{{route('createProgramy')}}" class="btn btn-success"><i class="fa fa-plus"></i> Dodaj</a>
</div>


      
<table id="tableSort" class="table table-responsive table-hover">
    
<thead>
<tr>         
        <th>ID</th>
<th>Nazwa</th>
<th>Podtytul</th>
<th>Aktywny</th>
<th>Cena</th>
<th></th>
</tr>
    </thead>
 
    <tbody>
 
@foreach($programy as $program)
<tr>
             <td>
                    {{$program->programID}}
                 </td>        
             <td>
                    {{$program->nazwa}}
                 </td>
                 <td>
                        {{str_limit($program->podtytul,128)}}
                     </td>
                      
                             <td>
                                {{($program->aktywny)}}
                                 </td>
                                 <td class="col-sm-1">
                                    {{number_format($program->cena,2,'.',' ')}} {{Config::get('app.currency')}}
                                     </td>

                                     <td class="text-right">
                                     <a href="{{route('createModulyProgramy',['id'=>$program->programID])}}" class="btn gui btn-success"> <i class="fa fa-plus"></i></a>
                                     <a href="{{route('indexModulyProgramy',['id'=>$program->programID])}}" class="btn gui btn-info"> <i class="fa fa-list"></i></a>
                    <a href="{{route('editProgramy',['id'=>$program->programID])}}" class="btn gui btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    {!!
                    Form::open(['method'=>'DELETE','action'=>['backend\ProgramyController@destroy',$program->programID]])
                    !!}
                    <button class="btn gui btn-default"> 
 <i class="fa fa-times" aria-hidden="true"></i>
</button> {!! Form::close() !!}


                </td>
                                      
</tr>
@endforeach
 
    </tbody>
</table>
 

@endsection
