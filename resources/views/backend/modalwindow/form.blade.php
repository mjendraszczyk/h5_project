@if(Session::has('success')) 
<div class="alert alert-success">
    {{Session::get('success')}}
 </div>
@endif

@if(Session::has('errors')) 
<div class="alert alert-danger">
<ul>
 
@foreach(Session::get('errors')->all() as $error)

  
  <li>{{$error}}</li>
  @endforeach
  </ul>
</div>
@endif

  

<div class="form-group">
    {!! Form::label('modal_img','Obrazek',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createModalWindow')
    {!! Form::file('modal_img') !!}
     @else      
     <div class="clearfix"></div>
  <div class="thumbnail"><img src="{{asset('/img/frontend/modal')}}/{{$modal->modal_img}}" alt="" /></div>

        {!! Form::file('modal_img') !!} @endif
</div>

<div class="form-group">
    {!! Form::label('modal_name','Nazwa',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createModalWindow')
    {!! Form::text('modal_name',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('modal_name',$modal->modal_name,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('modal_desc','Krótki opis',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createModalWindow')
    {!! Form::text('modal_desc',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('modal_desc',$modal->modal_desc,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="row">
    <div class="col-md-6">
<div class="form-group">
    {!! Form::label('modal_date_from','Wazny od',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createModalWindow')
    {!! Form::date('modal_date_from',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::date('modal_date_from',$modal->modal_date_from,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
 </div>
 <div class="col-md-6">
 <div class="form-group">
    {!! Form::label('modal_date_to','Wazny do',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createModalWindow')
    {!! Form::date('modal_date_to',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::date('modal_date_to',$modal->modal_date_to,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
</div>
  </div> 

<div class="form-group">
    {!! Form::label('modal_url','Link',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createModalWindow')
    {!! Form::text('modal_url',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('modal_url',$modal->modal_url,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>


  
<div class="row"> 
    <div class="col-sm-6">
<div class="form-group" style="border: 1px solid #eee;padding:20px;">

<label class="control-label"> 
  @if(Route::CurrentRouteName() == 'createModalWindow')
    {!! Form::checkbox('modal_active', 1, null, ['class' => 'field']) !!} @else 

    @if($modal->modal_active == '1')
    {!! Form::checkbox('modal_active', 1, true, ['class' => 'field']) !!}  
    @else 
    {!! Form::checkbox('modal_active', 1, false, ['class' => 'field']) !!}  
    @endif
    @endif
    Pokaz na stronie głównej
</label>
 
    </div>
</div>


<div class="col-sm-6">
<div class="form-group" style="border: 1px solid #eee;padding:20px;">

<label class="control-label"> 
  @if(Route::CurrentRouteName() == 'createModalWindow')
    {!! Form::checkbox('modal_timer', 1, null, ['class' => 'field']) !!} @else 

    @if($modal->modal_timer == '1')
    {!! Form::checkbox('modal_timer', 1, true, ['class' => 'field']) !!}  
    @else 
    {!! Form::checkbox('modal_timer', 1, false, ['class' => 'field']) !!}  
    @endif
    @endif
    Włącz odliczanie czasu na modalu
</label>
 
    </div>
</div>

</div>

 

<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() == 'createModalWindow') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!} @else <span data-url="{{route('showModalWindow',['id'=>$modal->modal_windowID])}}" data-element="#h5_banner" class="modal_open btn btn-default">Podgląd</span> {!!
        Form::submit('Zapisz', ['class'=>'btn btn-success']) !!} @endif
    </div>