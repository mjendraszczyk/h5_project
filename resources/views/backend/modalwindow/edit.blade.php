@extends('layouts.app') 
@section('title') Edycja okienka modalnego
@endsection
 
@section('content')

<div class="panel-shadow">
    @foreach($ModalWindow as $modal) {!! Form::open(['method'=>'PATCH','url' => route("updateModalWindow",['id'=>$modal->modal_windowID]),'enctype'
    => 'multipart/form-data','id'=>'ModalWindowForm','name'=>'ModalWindowForm','class'=>'form-vertical','style'=>'width:100%;'])
    !!}
    @include('backend.modalwindow.form') {!! Form::close() !!} @endforeach
</div>
@endsection