@extends('layouts.app')

@section('title')
Dodaj okienko modalne
@endsection

@section('content')
  
 <div class="panel-shadow">

 {!! Form::open(['url' => route("storeModalWindow"),'id'=>'ModalWindowForm','name'=>'ModalWindowForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.modalwindow.form')
 

{!! Form::close() !!} 
</div>
      

@endsection
