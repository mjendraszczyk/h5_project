@extends('layouts.app') 
@section('title') Okienka modalne
@endsection
 
@section('content')
<div class="row text-right no-margin">
    <a href="{{route('createModalWindow')}}" class="btn btn-success"><i class="fa fa-plus"></i> Dodaj</a>
</div>
  

<table class="tableSort table table-responsive table-hover" data-toggle="table" data-sort-name="date" data-sort-order="desc">
    <thead>
    <th>
        ID
        </th>
    <th>Obrazek</th>
        <th>Nazwa</th>
        <th>Status</th>
        <th></th>
    
    </thead>
    <tbody>


@foreach($ModalWindow as $modal) 

        <tr>
        <td>
        {{$modal->modal_windowID}}
        </td>
        <td>
        <img src="{{asset('/img/frontend/modal')}}/{{$modal->modal_img}}" style="max-width:150px;" class="thumbnail" />
        </td>
<td>
        {{$modal->modal_name}}
        </td>
        <td>
        {{$modal->modal_active}}
        </td>
      

 <td class="text-right">
                    <a href="{{route('editModalWindow',['id'=>$modal->modal_windowID])}}" class="btn gui btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                      
                    {!!        
                     Form::open(['method'=>'DELETE','action'=>['backend\ModalWindowController@destroy','id'=>$modal->modal_windowID]])
                    !!}
                    <button class="btn gui btn-default"> 
 <i class="fa fa-times" aria-hidden="true"></i>
</button> {!! Form::close() !!}


                 

                </td>

           </tr>


            @endforeach
</tbody>
</table>
    
@endsection