@if(Session::has('success'))
<div class="alert alert-success">
    {{Session::get('success')}}
</div>
@endif

@if(Session::has('errors'))
<div class="alert alert-danger">
    <ul>
        <!-- {{Session::get('errors')}} -->
        @foreach(Session::get('errors')->all() as $error)

        <!-- {{ Session::get('errors')}}  -->
        <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
@endif

@if((Route::CurrentRouteName() == 'admin') || (Route::CurrentRouteName() == 'filtr_dash_miasto'))
<div class="form-group">
    <div class="autocomplete-container">

        {!! Form::label('uczestnik','Uczestnik',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName()
        != 'editCRMZdarzenia')
        {!! Form::text('uczestnik',null,['required','id'=>'dashboardFindUczestnik','placeholder'=>'Znadź
        osobe','class'=>'col-sm-12 form-control']) !!}

        @else {!! Form::text('uczestnik',null,['required','id'=>'dashboardFindUczestnik','placeholder'=>'Znadź
        osobe','class'=>'col-sm-12
        form-control']) !!} @endif
    </div>
</div>
@endif
<div class="form-group">

    @if(Route::CurrentRouteName() != 'editCRMZdarzenia')
    {!! Form::hidden('uczestnikID',$uczestnikID,['id'=>'getUczestnikID','class'=>'col-sm-12 form-control']) !!}
    @else {!! Form::hidden('uczestnikID',$uczestnikID,['id'=>'getUczestnikID','class'=>'col-sm-12
    form-control']) !!} @endif


</div>


<div class="form-group">
    <div class="row">
        <div class="col-md-6">
            {!! Form::label('typ','Typ',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() !=
            'editCRMZdarzenia')
            {!! Form::select('typ',$typy_zdarzenia,null,['class'=>'col-sm-12 form-control']) !!} @else {!!
            Form::select('typ',$typy_zdarzenia,$zdarzenie->typ,['class'=>'col-sm-12
            form-control']) !!} @endif
        </div>
        <div class="col-md-6">
            {!! Form::label('data_przypomnienia','Data przypomnienia',['class'=>'col-sm-12 control-label']) !!}
            {{-- {!! Form::label('data_dodania','Data dodania',['class'=>'col-sm-12 control-label']) !!}   --}}
            {!! Form::hidden('data_dodania',date('Y-m-d'),['class'=>'form-control']) !!}
            {!! Form::date('data_przypomnienia',date('Y-m-d'),['class'=>'form-control']) !!}
            {!! Form::checkbox('przypomnij','1',['class'=>'form-control']) !!} Przypomnij o zdarzeniu

        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::hidden('priorytet','0',['class'=>'form-control']) !!}
    {!! Form::hidden('opiekunID',Auth::user()->id,['class'=>'form-control']) !!}

    {!! Form::label('komentarz','Komentarz',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() !=
    'editCRMZdarzenia')
    {!! Form::textarea('komentarz',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::textarea('komentarz',$zdarzenie->komentarz,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() != 'editCRMZdarzenia') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!}
        @else {!!
        Form::submit('Zapisz', ['class'=>'btn btn-success']) !!} @endif
    </div>
</div>