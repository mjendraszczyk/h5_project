<div class="form-container modal-open modal fade bd-example-modal-lg hidden-sm-down" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            {!! Form::open(['url' =>
            route("storeCRMZdarzeniaNew"),'id'=>'ZdarzeniaForm','name'=>'ZdarzeniaForm','enctype' =>
            'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

            @include('backend.CRMZdarzenia.form')


            {!! Form::close() !!}

        </div>
    </div>
</div>