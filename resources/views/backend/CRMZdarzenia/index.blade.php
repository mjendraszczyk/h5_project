@extends('layouts.app')

@section('title')
<div class="row">
    <div class="col-md-6">
        Zdarzenia archiwalne
    </div>
    <div class="col-md-6 text-right">
        <div class="col-md-12">

            {!! Form::open(['name'=>'filtr_zdarzenia','url'=>route('filtrZdarzenia'),'class'=>'form-inline']) !!}
            <div class="input-group">
                @if(Route::CurrentRouteName() == 'getResultsZdarzenia')
                <div class="input-group-btn">
                    {!!
                    Form::select('typ_zdarzenia',$typy_zdarzenia,$typID,['class'=>'form-control','style'=>'width:100%;'])
                    !!}
                </div>
                @if(Auth::user()->id_role == '3')
                <div class="input-group-btn">
                    {!! Form::hidden('opiekun',Auth::user()->id)!!}
                </div>
                @else
                <div class="input-group-btn">
                    {!! Form::select('opiekun',$opiekun,$opiekunID,['class'=>'form-control','style'=>'width:100%;']) !!}
                </div>
                @endif

                @else
                <div class="input-group-btn">
                    {!!
                    Form::select('typ_zdarzenia',$typy_zdarzenia,null,['class'=>'form-control','style'=>'width:100%;'])
                    !!}
                </div>
                @if(Auth::user()->id_role == '3')
                <div class="input-group-btn">
                    {!! Form::hidden('opiekun',Auth::user()->id)!!}
                </div>
                @else
                <div class="input-group-btn">
                    {!! Form::select('opiekun',$opiekun,null,['class'=>'form-control','style'=>'width:100%;']) !!}
                </div>
                @endif
                {{-- <div class="input-group-btn">
                    {!! Form::select('opiekun',$opiekun,null,['class'=>'form-control','style'=>'width:100%;']) !!}
                </div> --}}
                @endif
                <div class="input-group-btn">
                    {!! Form::submit('Filtr',['class'=>'btn btn-default']) !!}
                    <a href="{{route('indexCRMZdarzenia')}}" class="btn btn-default"><i class="fa fa-times"
                            aria-hidden="true"></i></a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

@section('content')


<table id="tableSort" table-data-sort='0' table-data-sort-type='desc' class="table table-responsive table-hover">
    <thead>
        <th>Data zdarzenia</th>
        <th>Klient</th>
        <th>Typ zdarzenia</th>
        <th>Komentarz</th>
        <th>Firma</th>
        <th>Opiekun</th>
        <th>HR</th>
        {{--<th></th> --}}
    </thead>
    <tbody>

        @foreach($zdarzenia as $zdarzenie)
        <tr>
            <td class="col-md-1">
                {{$zdarzenie->data_dodania}}



            </td>
            <td class="col-md-2">

                @foreach(json_decode(App\Http\Controllers\backend\CRMOsobyController::getUczestnikName($zdarzenie->uczestnikID))
                as $osoba)
                <a class="label-link"
                    href="{{route('editCRMOsoby',['klientID'=>$osoba->klientID,'id'=>$osoba->uczestnikID])}}">{{$osoba->imie_nazwisko}}</a>
                @endforeach






                {{-- @if($zdarzenie->imie_nazwisko == '') 
                              <span style="padding:5px;border-radius:10px;color:#fff;background: #528acb;"> Nieokreslono</span>
                               @else 
                               <a class="label-link" href="{{route('editCRMOsoby',['klientID'=>$zdarzenie->klientID,'id'=>$zdarzenie->uczestnikID])}}">{{$zdarzenie->imie_nazwisko}}</a>
                @endif --}}
            </td>
            <td class="col-md-1">
                {{$typy_zdarzenia[$zdarzenie->typ]}}

            </td>
            <td class="col-md-3">
                {{$zdarzenie->komentarz}}

            </td>
            <td class="col-md-2">


                @foreach(json_decode(App\Http\Controllers\backend\CRMOsobyController::getUczestnikName($zdarzenie->uczestnikID))
                as $osoba)

                @foreach(App\Http\Controllers\backend\CRMFirmyController::getFirma($osoba->klientID) as $firma)
                <a class="label-link" href="{{route('editCRMFirmy',['id'=>$firma->klientID])}}">{{$firma->nazwa}}</a>
                @endforeach

                @endforeach

            </td>
            <td class="col-md-2">



                <div class="form-group">

                    @foreach(App\Http\Controllers\backend\CRMOsobyController::getUczestnikName($zdarzenie->uczestnikID)
                    as $osoba)


                    @foreach(App\Http\Controllers\backend\CRMFirmyController::getFirma($osoba->klientID) as $firma)


                    @foreach(App\Http\Controllers\backend\UzytkownicyController::getUzytkownik($firma->opiekunID) as
                    $opiekunName)


                    {{$opiekunName->name}}

                    {{--   {!! Form::select('opiekun', $opiekun, $opiekunName->id, ['class'=>'form-control']) !!}  --}}

                    @endforeach

                    @endforeach

                    @endforeach





                </div>

            </td>
            <td class="col-md-1">
                <div class="form-group">

                    <span class="hidden">{{$zdarzenie->hr}}</span>
                    {!! Form::select('hr', $hr, $zdarzenie->hr, ['class'=>'updateAjax
                    form-control','data-url'=>route('updateAjaxZdarzenie',['id'=>$zdarzenie->zdarzenieID])]) !!}

                </div>
            </td>
        </tr>
        @endforeach

    </tbody>
</table>



@endsection