<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'High5') }}</title>
    <!-- Styles -->


    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.17/css/dataTables.bootstrap4.min.css" />
    <link media="none" onload="if(media!='all')media='all'" href="{{asset('css/frontend/font-awesome.css')}}" rel="stylesheet"
        type="text/css">
                <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/><link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.css" rel="stylesheet"/><table data-toggle="table" data-url="https://api.github.com/users/wenzhixin/repos?type=owner&sort=full_name&direction=asc&per_page=100&page=1" data-sort-name="stargazers_count" data-sort-order="desc"> <thead> <tr> <th data-field="name" data-sortable="true"> Name </th> <th data-field="stargazers_count" data-sortable="true"> Stars </th> <th data-field="forks_count" data-sortable="true"> Forks </th> <th data-field="description" data-sortable="true"> Description </th> </tr></thead></table><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.js"></script> -->

    <link href="{{ asset('css/backend/global.css?v=') }}{{time()}}" rel="stylesheet">

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>


    <link href="{{ asset('css/backend/sidebar.css') }}?v=1.0" rel="stylesheet">
          <script src="{{asset('js/backend/tinymce/js/tinymce/tinymce.min.js')}}"></script>

<script src="{{asset('js/backend/jquery.js')}}"></script>

</head>