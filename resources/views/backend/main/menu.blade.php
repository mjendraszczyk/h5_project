@if(Auth::user())
<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
        @if((Auth::user()->id_role == '2') || (Auth::user()->id_role == '1'))
        <li class="menu--category">
            <span @if(trim(str_limit(Request::route()->getPrefix(),4,''),"/") == 'cms') class="active" @else class=""
                @endif >
                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                Zarządzanie trescia
            </span>
            <ul class="submenu" @if(trim(str_limit(Request::route()->getPrefix(),4,''),"/") == 'cms')
                style="display:block;" @else style="display:none;" @endif >
                <li>
                    <a href="{{route('indexSzkolenia')}}"><i class="fa fa-graduation-cap" aria-hidden="true"></i>
                        Szkolenia</a>
                </li>
                <li>
                    <a href="{{route('indexKonsultacje')}}"><i class="fa fa-comments-o" aria-hidden="true"></i>
                        Konsultacje</a>
                </li>
                <li>
                    <a href="{{route('indexTerminarz')}}"><i class="fa fa-calendar-o" aria-hidden="true"></i>
                        Terminarz</a>
                </li>
                <li>
                    <a href="{{route('indexLink')}}">
                        <i class="fa fa-random" aria-hidden="true"></i>
                        Połączenia
                    </a>
                </li>
                <li>
                    <a href="{{route('indexTop10')}}">
                        <i class="fa fa-trophy" aria-hidden="true"></i>TOP10
                    </a>
                </li>
                <li>
                    <a href="{{route('indexHotele')}}">
                        <i class="fa fa-bed" aria-hidden="true"></i>Hotele
                    </a>
                </li>
                <li>
                    <a href="{{route('indexHeadline')}}"><i class="fa fa-picture-o" aria-hidden="true"></i>
                        Headline</a>
                </li>

                <li>
                    <a href="{{route('indexPromocje')}}"><i class="fa fa-tags" aria-hidden="true"></i>

                        Promocje</a>
                </li>
                <li>
                    <a href="{{route('indexHistory')}}"><i class="fa fa-newspaper-o" aria-hidden="true"></i>
                        Aktualnosci</a>
                </li>
                <li>
                    <a href="{{route('indexAkademie')}}"><i class="fa fa-university" aria-hidden="true"></i>
                        Akademie</a>
                </li>
                <li>
                    <a href="{{route('indexModalWindow')}}"><i class="fa fa-th-large" aria-hidden="true"></i>
                        Okna modalne
                    </a>
                </li>

                <li>
                    <a href="{{route('indexProgramy')}}"><i class="fa fa-desktop" aria-hidden="true"></i>
                        Programy</a>
                </li>
                <li>
                    <a href="{{route('indexGry')}}"><i class="fa fa-gamepad" aria-hidden="true"></i>
                        Gry szkoleniowe</a>
                </li>
                <li>
                    <a href="{{route('indexReferencje')}}"><i class="fa fa-list" aria-hidden="true"></i>Referencje</a>
                </li>
                <li>
                    <a href="{{route('indexBlog')}}"><i class="fa fa-book" aria-hidden="true"></i>Blog</a>
                </li>

                <li>
                    <a href="{{route('indexInstructor')}}"><i class="fa fa-users" aria-hidden="true"></i>Trenerzy</a>
                </li>


            </ul>
        </li>
        @endif
        @if((Auth::user()->id_role == '2') || (Auth::user()->id_role == '3'))
        <li class="menu--category">
            <span @if(trim(str_limit(Request::route()->getPrefix(),4,''),"/") == 'cms') class="" @else class="active"
                @endif >
                <i class="fa fa-users" aria-hidden="true"></i>

                Zarządzanie klientami
            </span>
            <ul class="submenu" @if(trim(str_limit(Request::route()->getPrefix(),4,''),"/") == 'cms')
                style="display:none;" @else style="display:block;" @endif >
                <li>
                    <a href="{{route('indexCRMFirmy')}}"><i class="fa fa-suitcase" aria-hidden="true"></i>

                        Firmy</a>
                </li>
                @if(Auth::user()->id_role == '2')
                <li>
                    <a href="{{route('indexNewsletter')}}"><i class="fa fa-envelope-o" aria-hidden="true"></i>
                        Newsletter</a>
                </li>
                @endif
                <li>
                    <a href="{{route('indexCRMWyszukiwanie')}}"><i class="fa fa-search" aria-hidden="true"></i>
                        Wyszukiwanie</a>
                </li>
                <li>
                    <a href="{{route('indexCRMListaHR')}}"><i class="fa fa-user" aria-hidden="true"></i>
                        Lista HR</a>
                </li>
                <li>
                    <a href="{{route('indexCRMFaktury')}}"><i class="fa fa-credit-card" aria-hidden="true"></i>

                        Faktury</a>
                </li>



                <li>
                    <a href="{{route('indexCRMZdarzeniaNew')}}"><i class="fa fa-star" aria-hidden="true"></i>
                        Zdarzenia</a>
                </li>

                <li>
                    <a href="{{route('indexCRMZdarzenia')}}"><i class="fa fa-sitemap" aria-hidden="true"></i>
                        Zdarzenia archiwalne</a>
                </li>
                @if(Auth::user()->id_role == '2')
                <li>
                    <a href="{{route('indexUzytkownicy')}}"><i class="fa fa-users" aria-hidden="true"></i>

                        Uzytkownicy</a>
                </li>
                <li>
                    <a href="{{route('indexRole')}}"><i class="fa fa-sitemap" aria-hidden="true"></i>

                        Role</a>
                </li>
                @endif
            </ul>
        </li>
        @endif
        @endif
    </ul>
    <p class="copyH5">
        &copy;{{date('Y')}} High5 v 1.0
    </p>
</div>