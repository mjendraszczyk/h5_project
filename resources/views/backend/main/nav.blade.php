<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target="#app-navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ route('admin') }}">
                <img src="{{asset('img/backend/logo-white.png')}}" alt="" />
            </a>
            <a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle">
                <i class="fa fa-bars"></i>Menu
            </a>
        </div>
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
                <li>
                </li>
            </ul>
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @guest
                <li><a href="{{ route('login') }}">Zaloguj</a></li>
                {{--<li><a href="{{ route('register') }}">Stwórz konto</a></li>--}}
                @else

                @if((Auth::user()->id_role == '2'))
                <li>
                    <a href="{{ route('admin') }}">Szkolenia</a>
                </li>
                <li>
                    <a href="https://www.high5.pl/admin/index.php" target="_blank"
                        style="background: #7c7a77;color: #fff;">Stare CRM</a>
                </li>
                <li>
                    <a href="{{route('indexNewsletter')}}">Newsletter</a>
                </li>
                <li>
                    <a href="{{route('indexReferencje')}}">Inne</a>
                </li>
                <li>
                    <a href="{{ route('indexInstructor') }}">Trenerzy</a>
                </li>
                @endif
                {{--<li>
                <a href="#"><i class="fa fa-lock" style="padding: 0 5px 0 0px;display:  inline;"></i>Ankiety</a>
                </li>--}}
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"
                        aria-haspopup="true">
                        <span class="fa fa-user" style="fa fa-user"></span> {{ Auth::user()->name }}
                        <span class="badge">
                            {{App\Http\Controllers\Controller::getRola(Auth::user()->id_role)}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Wyloguj
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>