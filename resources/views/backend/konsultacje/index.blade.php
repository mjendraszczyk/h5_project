@extends('layouts.app')

@section('title')
Konsultacje
@endsection

@section('content')
 <div class="row text-right no-margin">
    <a href="{{route('createKonsultacje')}}" class="btn btn-success"><i class="fa fa-plus"></i> Dodaj</a>
</div>
 

<table id="tableSort" class="table table-responsive table-hover">
 <thead>
     <tr>
     <td>
ID
</td>
        <th>Kategoria</th>
        <th>Temat</th>
<th></th>
</tr>
    </thead>
<tbody>
    @foreach($konsultacje as $konsultacja)
<tr>
<td>
{{$konsultacja->consultingID}}
</td>
<td>
{{$konsultacja->category_title}}
</td>
<td>
{{$konsultacja->consulting_subject}}
</td>
 
<td class="text-right"> 
                    <a href="{{route('editKonsultacje',['id'=>$konsultacja->consultingID])}}" class="btn gui btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    {!!
                    Form::open(['method'=>'DELETE','action'=>['backend\KonsultacjeController@destroy',$konsultacja->consultingID]])
                    !!}
                    <button class="btn gui btn-default"> 
 <i class="fa fa-times" aria-hidden="true"></i>
</button> {!! Form::close() !!}


                </td>
</tr>
@endforeach
    </tbody>
    </table>


@endsection
