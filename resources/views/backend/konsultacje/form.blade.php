@if(Session::has('success')) 
<div class="alert alert-success">
    {{Session::get('success')}}
 </div>
@endif

@if(Session::has('errors')) 
<div class="alert alert-danger">
<ul>
<!-- {{Session::get('errors')}} -->
@foreach(Session::get('errors')->all() as $error)

  <!-- {{ Session::get('errors')}}  -->
  <li>{{$error}}</li>
  @endforeach
  </ul>
</div>
@endif

 

<div class="form-group">
    {!! Form::label('consulting_subject','Temat',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createKonsultacje')
    {!! Form::text('consulting_subject',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('consulting_subject',$konsultacja->consulting_subject,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
  
  
<div class="form-group">
    {!! Form::label('consulting_problems','Problem',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createKonsultacje')
    {!! Form::textarea('consulting_problems',null,['class'=>'editor col-sm-12 form-control']) !!} @else {!! Form::textarea('consulting_problems',$konsultacja->consulting_problems,['class'=>'editor col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    {!! Form::label('consulting_category','Kategoria',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createKonsultacje')
     
     
    {!! Form::select('consulting_category', $konsultacje_tab,null, ['class'=>'form-control']) !!}

     @else 
    {!! Form::select('consulting_category', $konsultacje_tab,$konsultacja->categoryID, ['class'=>'form-control']) !!}
      @endif
</div>
   
<div class="form-group">
    {!! Form::label('consulting_price','Cena',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createKonsultacje')
    {!! Form::text('consulting_price',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('consulting_price',$konsultacja->consulting_price,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() == 'createKonsultacje') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!} @else {!!
        Form::submit('Zapisz', ['class'=>'btn btn-success']) !!} @endif
    </div>