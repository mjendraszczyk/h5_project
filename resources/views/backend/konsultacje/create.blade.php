@extends('layouts.app')

@section('title')
Dodaj konsultacje
@endsection

@section('content')
  
 <div class="panel-shadow">

 {!! Form::open(['url' => route("storeKonsultacje"),'id'=>'KonsultacjeForm','name'=>'KonsultacjeForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.konsultacje.form')
 

{!! Form::close() !!} 
</div>
      

@endsection
