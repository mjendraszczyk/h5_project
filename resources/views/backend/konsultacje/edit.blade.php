@extends('layouts.app') 
@section('title') Edycja konsultacji
@endsection
 
@section('content')

<div class="panel-shadow">
    @foreach($konsultacje as $konsultacja) {!! Form::open(['method'=>'PATCH','url' => route("updateKonsultacje",['id'=>$konsultacja->consultingID]),'enctype'
    => 'multipart/form-data','id'=>'KonsutlacjeForm','name'=>'KonsutlacjeForm','class'=>'form-vertical','style'=>'width:100%;'])
    !!}
    @include('backend.konsultacje.form') {!! Form::close() !!} @endforeach
</div>
@endsection