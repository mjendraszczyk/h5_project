@extends('layouts.app')

@section('title')
Dodaj Połączenia
@endsection

@section('content')
  
 <div class="panel-shadow">

 {!! Form::open(['url' => route("storeLink"),'id'=>'LinkForm','name'=>'LinkForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.link.form')
 

{!! Form::close() !!} 
</div>
      

@endsection
