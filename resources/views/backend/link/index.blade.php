@extends('layouts.app') 
@section('title') Połączenia
@endsection
 
@section('content')
<div class="row text-right no-margin">
    <a href="{{route('createLink')}}" class="btn btn-success"><i class="fa fa-plus"></i> Dodaj</a>
</div>

 
<table id="tableSort" class="table table-responsive table-hover" data-toggle="table" data-sort-name="date" data-sort-order="desc">
    <thead>
    <th>ID</th>
        <th>Szkolenie</th>
         
        <th></th>
    </thead>
    <tbody>
         
            
            @foreach($links as $link)
            <tr>
                <td>
                    {{$link->linkID}}
                </td>       
                <td>
                    {{$link->training_title}}
                </td>
                 
                 
                <td class="text-right">
                    <a href="{{route('editLink',['id'=>$link->linkID])}}" class="btn gui btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>{!!
                    Form::open(['method'=>'DELETE','action'=>['backend\LinkController@destroy',$link->linkID]])
                    !!}
                    <button class="btn gui btn-default"> 
 <i class="fa fa-times" aria-hidden="true"></i>
</button> {!! Form::close() !!}


                </td>
            </tr>
            @endforeach

    </tbody>
</table>
@endsection