@if(Session::has('success')) 
<div class="alert alert-success">
    {{Session::get('success')}}
 </div>
@endif

@if(Session::has('errors')) 
<div class="alert alert-danger">
<ul>
<!-- {{Session::get('errors')}} -->
@foreach(Session::get('errors')->all() as $error)

  <!-- {{ Session::get('errors')}}  -->
  <li>{{$error}}</li>
  @endforeach
  </ul>
</div>
@endif
<div class="form-group">
    {!! Form::label('primaryID','Kurs podstawowy',['class'=>'col-sm-12 control-label']) !!} 
    
    @if(Route::CurrentRouteName() == 'createLink')

    {!! Form::select('primaryID', $link_tab,null, ['class'=>'form-control']) !!}

     @else 
    {!! Form::select('primaryID', $link_tab,$link->primaryID, ['class'=>'form-control']) !!}
      
    @endif
</div>
 
<div class="form-group">
    {!! Form::label('link1ID','Link1',['class'=>'col-sm-12 control-label']) !!} 
    
    @if(Route::CurrentRouteName() == 'createLink')

    {!! Form::select('link1ID', $link_tab,null, ['class'=>'form-control']) !!}

     @else 
    {!! Form::select('link1ID', $link_tab,$link->link1ID, ['class'=>'form-control']) !!}
      
    @endif
</div>

<div class="form-group">
    {!! Form::label('link2ID','Link2',['class'=>'col-sm-12 control-label']) !!} 
    
    @if(Route::CurrentRouteName() == 'createLink')

    {!! Form::select('link2ID', $link_tab,null, ['class'=>'form-control']) !!}

     @else 
    {!! Form::select('link2ID', $link_tab,$link->link2ID, ['class'=>'form-control']) !!}
      
    @endif
</div>

<div class="form-group">
    {!! Form::label('link3ID','Link3',['class'=>'col-sm-12 control-label']) !!} 
    
    @if(Route::CurrentRouteName() == 'createLink')

    {!! Form::select('link3ID', $link_tab,null, ['class'=>'form-control']) !!}

     @else 
    {!! Form::select('link3ID', $link_tab,$link->link3ID, ['class'=>'form-control']) !!}
      
    @endif
</div>


<div class="form-group">
    {!! Form::label('link4ID','Link4',['class'=>'col-sm-12 control-label']) !!} 
    
    @if(Route::CurrentRouteName() == 'createLink')

    {!! Form::select('link4ID', $link_tab,null, ['class'=>'form-control']) !!}

     @else 
    {!! Form::select('link4ID', $link_tab,$link->link4ID, ['class'=>'form-control']) !!}
      
    @endif
</div>
 

<div class="form-group">
    {!! Form::label('link5ID','Link5',['class'=>'col-sm-12 control-label']) !!} 
    
    @if(Route::CurrentRouteName() == 'createLink')

    {!! Form::select('link5ID', $link_tab,null, ['class'=>'form-control']) !!}

     @else 
    {!! Form::select('link5ID', $link_tab,$link->link5ID, ['class'=>'form-control']) !!}
      
    @endif
</div>

  
<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() == 'createLink') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!} @else {!! Form::submit('Zapisz',
        ['class'=>'btn btn-success']) !!} @endif
    </div>