@extends('layouts.app') 
@section('title') Edycja Połączenia  
@endsection
 
@section('content')
 
<div class="panel-shadow">
    @foreach($links as $link) {!! Form::open(['method'=>'PATCH','url' => route("updateLink",['id'=>$link->linkID]),'enctype' => 'multipart/form-data','id'=>'LinkForm','name'=>'LinkForm','class'=>'form-vertical','style'=>'width:100%;'])
    !!}
    @include('backend.link.form') {!! Form::close() !!} @endforeach
</div>
@endsection