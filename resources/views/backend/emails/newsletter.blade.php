
<table style="width:100%;">
    <tr>
        <td style="width: 100%;background: #000;padding: 20px;text-align: center;">
 <img src="{{asset('img/frontend/high5l.png')}}" alt="" />
 </td>
 </tr>
</table>
<h1 style="color:#000000;font-size:32px;text-align:center;display:block;">Szczegóły oferty</h1>
<table style="width:100%;">
    <tr>
        <td style="padding:20px 0;border: 1px solid #eee;text-align: center;width: 90%;display: block;margin: 10px auto 50px auto;">
@if(Route::currentRouteName() == 'NewsletterSend')

 {{-- {{print_r($widok)}} --}}
 {{$widok[0]['opis']}}
        @else 
{{$widok[0]['opis']}}
        @endif
            
 {{-- {{$widok[0]['opis']}} --}}
 </td>
 </tr>
</table>
<table class="table table-responsive table-hover" align="center" style="border-collapse:collapse;">
  
 <thead>
     <tr style="border-bottom:3px solid #eee;">
    <td style="font-weight:700;padding: 25px;">
        Termin szkolenia
</td>
    <td style="font-weight:700;padding: 25px;">
         
        Temat
    </td>
        <td style="font-weight:700;padding: 25px;">
        Cena
    </td>
</tr> 
 </thead>
<tbody>


@for($preview=1;$preview<sizeof($widok);$preview++) 
<tr style="border-bottom:1px solid #eee;">
    <td>
    @php
    $start_day=date ('j', strtotime($widok[$preview]['term_start']));
	$end_day=date ('j', strtotime($widok[$preview]['term_end']));
	$month=Lang::get('date.month.'.date('n', strtotime($widok[$preview]['term_start'])));
	
     @endphp

	@if ($start_day !=$end_day)
	{{$start_day}} - {{$end_day}} {{$month}}
	@else
		{{$start_day}} {{$month}}
    @endif
    <br/>
    {{$widok[$preview]['term_place']}}
    </td>
    <td style="padding: 15px 0;">
       <a href="{{route('rezerwacja_szkolenia',['id'=>$widok[$preview]['trainingID']])}}"> {{$widok[$preview]['training_title']}}</a>
    </td>
    <td>
        {{number_format($widok[$preview]['trainig_price'],2,'.',' ')}} {{Config::get('app.currency')}}
    </td>
</tr>


@endfor
 


<tr>

</tr>
</tbody>
    </table>
    <table>
        <tr>
            <td>


     Otrzymujesz ten newsletter ponieważ wyraziłaś(eś) zgodę na otrzymywanie materiałów od High5. Jeśli chcesz wypisać się 
       
     @if(Route::currentRouteName() == 'NewsletterSend')
   <a href="{{route('NewsletterDelete')}}">klikij tutaj</a>   
      

 
     @else 
     <a href="{{route('NewsletterDelete')}}">klikij tutaj</a> 
 @endif
</td>
</tr>
</table>