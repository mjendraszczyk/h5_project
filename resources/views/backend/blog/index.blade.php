@extends('layouts.app')

@section('title')
Blog
@endsection

@section('content')
 <div class="row text-right no-margin">
    <a href="{{route('createBlog')}}" class="btn btn-success"><i class="fa fa-plus"></i> Dodaj</a>
</div>

                  
<table id="tableSort" table-data-sort="0" table-data-sort-type="desc"  class="table table-responsive table-hover">
    <thead>
<th>Data</th>
<th>Tytuł</th>
<th>Autor</th>
<th>Kategoria</th>
<th></th>
    </thead>
    <tbody>
 
@foreach($blogs as $blog)
<tr>
   <td class="col-sm-2"> 
                            {{$blog->created}}
                         </td>
        <td>
            {{$blog->title}} 
         </td>
             <td>
                        {{$blog->instructor_name}}    
                 </td>
                 <td>
                        
                        {{$blog->category_title}}    
                     </td>
                  
                  <td class="text-right">
                            <a href="{{route('editBlog',['id'=>$blog->blogID])}}" class="btn gui btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    {!!
                    Form::open(['method'=>'DELETE','action'=>['backend\BlogController@destroy',$blog->blogID]])
                    !!}
                    <button class="btn gui btn-default"> 
 <i class="fa fa-times" aria-hidden="true"></i>
</button> {!! Form::close() !!}


                </td>
</tr>
@endforeach
 
    </tbody>
</table>
 
 


@endsection
