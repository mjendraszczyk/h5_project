@extends('layouts.app')

@section('title')
Dodaj wpis na blogu
@endsection

@section('content')
  
 <div class="panel-shadow">

 {!! Form::open(['url' => route("storeBlog"),'id'=>'BlogForm','name'=>'BlogForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.blog.form')
 

{!! Form::close() !!} 
</div>
      

@endsection
