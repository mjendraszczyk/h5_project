@extends('layouts.app') 
@section('title') Edycja wpisu na blogu
@endsection
 
@section('content')

<div class="panel-shadow">
    @foreach($blogs as $blog) {!! Form::open(['method'=>'PATCH','url' => route("updateBlog",['id'=>$blog->blogID]),'enctype' => 'multipart/form-data','id'=>'BlogForm','name'=>'BlogForm','class'=>'form-vertical','style'=>'width:100%;'])
    !!}
    @include('backend.blog.form') {!! Form::close() !!} @endforeach
</div>
@endsection