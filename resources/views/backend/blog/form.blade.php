@if(Session::has('success'))
<div class="alert alert-success">
    {{Session::get('success')}}
</div>
@endif

@if(Session::has('errors'))
<div class="alert alert-danger">
    <ul>
        <!-- {{Session::get('errors')}} -->
        @foreach(Session::get('errors')->all() as $error)

        <!-- {{ Session::get('errors')}}  -->
        <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
@endif



<div class="form-group">
    {!! Form::label('category','Kategoria',['class'=>'col-sm-12 control-label']) !!}

    @if(Route::CurrentRouteName() == 'createBlog')

    {!! Form::select('category', $category,null, ['class'=>'form-control']) !!}

    @else
    {!! Form::select('category', $category,$blog->categoryID, ['class'=>'form-control']) !!}

    @endif
</div>


<div class="form-group">
    {!! Form::label('author','Autor',['class'=>'col-sm-12 control-label']) !!}

    @if(Route::CurrentRouteName() == 'createBlog')

    {!! Form::select('author', $instructor,null,['class'=>'form-control']) !!}

    @else
    {!! Form::select('author', $instructor,$blog->instructorID,['class'=>'form-control']) !!}

    @endif
</div>

<div class="form-group">
    {!! Form::label('tytul','Tytuł',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createBlog')
    {!! Form::text('tytul',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::text('tytul',$blog->title,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('lid','Wstęp',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createBlog') {!!
    Form::textarea('lid',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::textarea('lid',$blog->lid,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    {!! Form::label('tekst','Tekst',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createBlog') {!!
    Form::textarea('tekst',null,['class'=>'editor col-sm-12 form-control']) !!} @else {!!
    Form::textarea('tekst',$blog->tekst,['class'=>'editor col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('tagi','Tagi',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createBlog')
    {!! Form::text('tagi',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::text('tagi',$blog->tagi,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('picture','Obrazek',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() ==
    'createBlog')
    @else
    <img src="{{Config('url')}}/public/img/frontend/blog/{{$blog->picture}}" alt="" class="thumbnail" /> @endif {!!
    Form::file('image')
    !!}
</div>


<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() == 'createBlog') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!} @else
        {!! Form::submit('Zapisz',
        ['class'=>'btn btn-success']) !!} @endif
    </div>