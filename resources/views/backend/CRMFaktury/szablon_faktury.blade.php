<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
        @font-face {
            font-family: 'DejaVu Sans' !important;
            src: url('{{asset("font")}}/DejaVuSans.woff') format('woff'),
            url('{{asset("font")}}/DejaVuSans.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }

        body {
            font-family: "DejaVu Sans";
            /* font-family:Arial; */
            font-size: 12px;
        }

        .row {
            display: block;
            clear: both;
            width: 100%;
        }

        .col-md-12,
        .col-md-11,
        .col-md-10,
        .col-md-9,
        .col-md-8,
        .col-md-7,
        .col-md-6,
        .col-md-5,
        .col-md-4,
        .col-md-3,
        .col-md-2,
        .col-md-1 {
            float: left;
        }

        .text-center {
            text-align: center;
        }

        .col-md-12 {
            width: 100%;
        }

        .col-md-11 {
            width: 91.66%;
        }

        .col-md-10 {
            width: 80%;
        }

        .col-md-8 {
            width: 75%;
        }

        .col-md-7 {
            width: 58.33%;
        }

        .col-md-5 {
            width: 41.66%;
        }

        .col-md-6 {
            width: 50%;
        }

        .col-md-4 {
            width: 25%;
        }

        .col-md-3 {
            width: 33%;
        }

        .col-md-2 {
            width: 20%;
        }

        .col-md-1 {
            width: 8.33%;
        }

        table {
            border-collapse: collapse;
        }

        td,
        th {
            border: 1px solid #000;
            padding: 5px;
        }

        h1,
        h3 {
            margin: 0px;
            padding: 0;
        }

        h1 {
            font-size: 18px;
        }

        .text-right {
            text-align: right;
        }
    </style>
</head>

<body>
    <div class="row">
        <div class="col-md-7">
            <h1>Faktura {{$typy}} nr {{$data['nr_faktury']}} / {{date('m',strtotime($data['data_wystawienia']))}} -
                {{date('Y',strtotime($data['data_wystawienia']))}} </h1>
            <h3>ORYGINAŁ / KOPIA</h3>
        </div>

        <div class="col-md-5 text-right">
            Data wystawienia: {{$data['data_wystawienia']}}<br />Data sprzedaży: {{$data['data_sprzedazy']}}
        </div>
    </div>

    <div class="row">
        <br /><br /><br />
        <div class="col-md-6">
            <strong>Sprzedawca</strong>
            <br />
            {{Config::get('app.firma')}}
            <br />
            {{Config::get('app.kod')}} {{Config::get('app.miasto')}}
            <br />
            ul. {{Config::get('app.adres')}}
            <br />
            tel.: {{Config::get('app.tel')}}
            <br />
            fax: {{Config::get('app.fax')}}
            <br /><br />
            NIP: {{Config::get('app.nip')}}
            <br /><br />
        </div>
        <div class="col-md-6">
            <strong>Nabywca</strong>
            <br />
            {{$data['nazwa_firmy']}}
            <br />
            {{$data['kod']}} {{$data['miasto']}}
            <br />
            {{$data['adres']}}
            <br />
            NIP: {{$data['nip']}}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            Konto: {{Config::get('app.bank')}}

        </div>
    </div>
    <div class="row">
        <br /><br />
        <strong>Zamówienie</strong><br />
        <table style="width:100%;font-size:10px;">
            <thead>
                <tr>
                    <th style="width:3%;">
                        Lp.
                    </th>
                    <th style="width:20%;">
                        Nazwa
                    </th>
                    <th style="width:5%;">
                        GTU
                    </th>
                    <th style="width:3%;">
                        Szt.
                    </th>
                    <th style="width:3%;">
                        J.m.
                    </th>
                    <th>
                        Netto
                    </th>
                    <th>
                        Cena
                    </th>
                    <th>
                        % VAT
                    </th>
                    <th>
                        VAT
                    </th>
                    <th>
                        Brutto
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        1
                    </td>
                    <td style="width:25%;">
                        {{$data['opis']}}
                    </td>
                    <td>
                        GTU_12
                    </td>
                    <td>
                        {{$data['ilosc']}}
                    </td>
                    <td>
                        {{$data['jm']}}
                    </td>
                    <td>
                        {{number_format($data['cena_jednostkowa'],2,'.','')}} {{Config::get('app.currency')}}
                    </td>
                    <td>
                        {{number_format($data['kwota'],2,'.','')}} {{Config::get('app.currency')}}
                    </td>
                    <td>
                        @if($data['VAT'] == '0')
                        ZW
                        @else
                        {{number_format($data['VAT'],2,'.','')}}%
                        @endif
                    </td>
                    <td>
                        {{number_format($data['kwota_VAT'],2,'.','')}} {{Config::get('app.currency')}}
                    </td>
                    <td>
                        {{number_format($data['kwota_brutto'],2,'.','')}} {{Config::get('app.currency')}}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>


    <div class="row">
        <br /><br /><br />
        <div class="col-md-12">
            <table align="right" style="font-size:11px;">
                <thead>
                    <tr>
                        <th>Wg stawki VAT</th>
                        <th>Netto</th>
                        <th>VAT</th>
                        <th>Brutto</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            @if($data['VAT'] == '0')
                            ZW
                            @else
                            {{number_format($data['VAT'],2,'.',' ')}}%
                            @endif
                        </td>
                        <td>
                            {{number_format($data['kwota'],2,'.',' ')}} {{Config::get('app.currency')}}
                        </td>
                        <td>
                            {{number_format($data['kwota_VAT'],2,'.',' ')}}
                        </td>
                        <td>
                            {{number_format($data['kwota_brutto'],2,'.',' ')}} {{Config::get('app.currency')}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Razem
                        </td>
                        <td>
                            {{number_format($data['kwota'],2,'.',' ')}} {{Config::get('app.currency')}}
                        </td>
                        <td>

                            {{number_format($data['kwota_VAT'],2,'.',' ')}}
                        </td>
                        <td>
                            {{number_format($data['kwota_brutto'],2,'.',' ')}} {{Config::get('app.currency')}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        {{-- 
            @if($data['rabat'] == '')
            <strong>Rabat: 0%</strong>
            @else 
            <strong>Rabat: {{$data['rabat']}}%</strong>
        @endif
        <br /> --}}

        Forma: {{$forma_platnosci}}
        <br />
        Termin: {{$termin_platnosci}}
        <br />
        <br />
        <br />
        <strong>Razem do zapłaty:
            {{number_format($data['kwota_brutto'],2,'.',' ')}} {{Config::get('app.currency')}}
        </strong>
        <br />
        Słownie:
        {{$slowa_cyfry}} złotych {{substr(strstr ( number_format($data['kwota_brutto'],2,'.',' '), '.' ),1)}} / 100
        groszy
        <br /> <br />
		Mechanizm podzielonej płatności
		<br />

    </div>
    <div class="row">
        @if(($data['typ'] == '3') || ($data['typ'] == '4'))

        <strong>Wpłaty</strong><br />
        <table class="col-md-12" style="font-size:11px;">
            <thead>
                <tr>
                    <th>
                        Lp.
                    </th>
                    <th>
                        Nazwa
                    </th>

                    <th>
                        Data
                    </th>
                    <th>
                        Netto
                    </th>
                    <th>
                        % VAT
                    </th>
                    <th>
                        VAT
                    </th>
                    <th>
                        Brutto
                    </th>
                </tr>
            </thead>
            <tbody>

                @foreach($listaZaliczek as $key => $zaliczka)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>
                        @if($zaliczka->typ == '4')
                        Rozliczenie {{$zaliczka->nr_faktury}} / {{date('m',strtotime($data['data_wystawienia']))}} -
                        {{date('Y',strtotime($data['data_wystawienia']))}}
                        @else
                        Zaliczka {{$zaliczka->nr_faktury}} / {{date('m',strtotime($zaliczka->data_wystawienia))}} -
                        {{date('Y',strtotime($zaliczka->data_wystawienia))}}
                        @endif
                    </td>
                    <td>{{$zaliczka->data_wystawienia}}</td>
                    <td>{{number_format($zaliczka->zaplacono/(1+$zaliczka->VAT/100),2,'.',' ')}}
                        {{Config::get('app.currency')}}</td>
                    <td>
                        @if($zaliczka->VAT == '0')
                        ZW
                        @else
                        {{number_format($zaliczka->VAT,2,'.',' ')}}%
                        @endif
                    <td>{{number_format(($zaliczka->VAT / 100) * $zaliczka->zaplacono/(1+$zaliczka->VAT/100),2,'.',' ')}}
                        {{Config::get('app.currency')}}</td>
                    <td>{{number_format($zaliczka->zaplacono,2,'.',' ')}} {{Config::get('app.currency')}}</td>
                </tr>
                @endforeach

                {{-- 
                    <tr>
                        <td>
                            1
                        </td>
                        <td>
                            Zaliczka {{$data['nr_faktury']}} - {{date('m/Y')}}
                </td>

                <td>
                    {{number_format($data['zaplacono']/(1+$data['VAT']/100),2,'.',' ')}} {{Config::get('app.currency')}}
                </td>
                <td>
                    @if($data['VAT'] == '0')
                    ZW
                    @else
                    {{number_format($data['VAT'],2,'.',' ')}}%
                    @endif
                </td>
                <td>
                    {{number_format(($data['VAT'] / 100) * $data['zaplacono']/(1+$data['VAT']/100),2,'.',' ')}}
                    {{Config::get('app.currency')}}
                </td>
                <td>
                    {{number_format($data['zaplacono'],2,'.',' ')}} {{Config::get('app.currency')}}
                </td>
                </tr>
                --}}
            </tbody>
        </table>
        <br />
        Pozostało do zapłaty:
        {{($data['kwota_brutto'])-(App\Http\Controllers\backend\CRMFakturyController::getZaplacono($data['FZID']))}}
        {{Config::get('app.currency')}}
        @endif
    </div>


    <div class="row" style="position:absolute;bottom:100px;">
        <br /><br /><br /><br />
        <div class="col-md-6 text-center">
            Wystawił
            <br /> <br />
            Renata Jerzowska
            <br /><br />
            Podpis osoby upoważnionej
            <br />
            do wystawienia faktury VAT
        </div>

        <div class="col-md-6 text-center">
            Odebrał
            <br /><br />
            <br /><br />
            Podpis osoby upoważnionej
            <br />
            do odbioru faktury VAT

        </div>
    </div>

</body>

</html>