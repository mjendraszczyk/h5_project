@php echo '<'.'?xml version="1.0" encoding="UTF-8"?>'; @endphp

<tns:JPK xmlns:tns="http://jpk.mf.gov.pl/wzor/2017/11/13/1113/">
<tns:Naglowek>
        <tns:KodFormularza kodSystemowy="JPK_VAT (3)" wersjaSchemy="1-1">JPK_VAT</tns:KodFormularza>
        <tns:WariantFormularza>3</tns:WariantFormularza>
        <tns:CelZlozenia>0</tns:CelZlozenia> {{-- Pole zawiera warianty: 0 – Plik JPK za okres; 1-Pierwsza korekta 2- druga korekta itd. --}}
        <tns:DataWytworzeniaJPK>{{date('Y-m-d H:i:s')}}</tns:DataWytworzeniaJPK>
        <tns:DataOd>{{date('Y-m',strtotime($rok.'-'.$miesiac))}}-01</tns:DataOd>
        <tns:DataDo>{{date('Y-m-t',strtotime($rok.'-'.$miesiac))}}</tns:DataDo>
        <tns:NazwaSystemu>High5</tns:NazwaSystemu>
</tns:Naglowek>
    <tns:Podmiot1>
        <tns:NIP>{{Config::get('app.nip')}}</tns:NIP>
        <tns:PelnaNazwa>{{Config::get('app.firma')}}</tns:PelnaNazwa>
        <tns:Email>{{env('MAIL_USERNAME')}}</tns:Email>
    </tns:Podmiot1>
 @foreach($jpk as $faKey => $fa)
<tns:SprzedazWiersz>
        <tns:LpSprzedazy>{{$faKey+1}}</tns:LpSprzedazy>
        <tns:NrKontrahenta>{{$fa->klientID}}</tns:NrKontrahenta>
        <tns:NazwaKontrahenta>{{$fa->nazwa}}</tns:NazwaKontrahenta>
        <tns:AdresKontrahenta>{{$fa->adres}}, {{$fa->kod}} {{$fa->miasto}}</tns:AdresKontrahenta>
        <tns:DowodSprzedazy>{{$fa->nr_faktury}}/{{$miesiac}}-{{$rok}}</tns:DowodSprzedazy>
        <tns:DataWystawienia>{{$fa->data_wystawienia}}</tns:DataWystawienia>
        <tns:DataSprzedazy>{{$fa->data_sprzedazy}}</tns:DataSprzedazy>
        @if($fa->typ =='3' || $fa->typ == '4')
        <tns:K_19>{{number_format(($fa->zaplacono/((100+$fa->VAT)/100)),2,"."," ")}}</tns:K_19> {{--  To kwota netto przy sprzedazy na kraj --}}
        <tns:K_20>{{number_format($fa->zaplacono-($fa->zaplacono/((100+$fa->VAT)/100)),2,"."," ")}}</tns:K_20>  {{--  To kwota vat przy sprzedazy na kraj --}}
        @else 
        <tns:K_19>{{number_format($fa->kwota,2,"."," ")}}</tns:K_19> {{--  To kwota netto przy sprzedazy na kraj --}}
        <tns:K_20>{{number_format($fa->kwota_VAT,2,"."," ")}}</tns:K_20>  {{--  To kwota vat przy sprzedazy na kraj --}}
        @endif
</tns:SprzedazWiersz>
@endforeach
<tns:SprzedazCtrl>
        <tns:LiczbaWierszySprzedazy>{{$ilosc_sprzedazy}}</tns:LiczbaWierszySprzedazy>
        <tns:PodatekNaliczony>{{number_format($podatekNalezny,2,'.',' ')}}</tns:PodatekNaliczony>
</tns:SprzedazCtrl>
</tns:JPK>