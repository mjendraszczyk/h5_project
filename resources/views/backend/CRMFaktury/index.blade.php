@extends('layouts.app')

@section('title')
<div class="row">
    <div class="col-md-6">
        Faktury z
        @if(Route::CurrentRouteName() == 'indexCRMFaktury')
        {{Lang::get('date.month.'.date('n'))}}
        <span class="label-button">{{date('Y')}}</span>
        @else
        {{Lang::get('date.month.'.date('n',strtotime('1-'.$miesiac.'-'.$rok)))}}
        <span class="label-button">{{$rok}}</span>
        @endif
    </div>



    <div class="col-md-6 text-right">
        <div class="col-md-9">
            @if(Route::CurrentRouteName() == 'indexCRMFaktury')
            {!!
            Form::open(['url'=>route('filtrFaktury',['miesiac'=>date('m'),'rok'=>date('Y')]),'name'=>'filtrFaktury','class'=>'form-inline'])
            !!}
            @else
            {!!
            Form::open(['url'=>route('filtrFaktury',['miesiac'=>$miesiac,'rok'=>$rok]),'name'=>'filtrFaktury','class'=>'form-inline'])
            !!}
            @endif
            <div class="col-md-10">
                {!! Form::select('typ_faktury',$typy,null,['class'=>'form-control','style'=>'width:100%;']) !!}
            </div>
            <div class="col-md-2">
                {!! Form::submit('Filtr',['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
        <div class="col-md-3">
            @if(Route::CurrentRouteName() == 'indexCRMFaktury')
            {{-- <a href="{{route('getFakturyFromRange',['miesiac'=>(date('m')-1),'rok'=>date('Y')])}}" class="btn
            btn-default"><</a> --}} <a
                href="{{route('getFakturyFromRange',['miesiac'=>date('m',strtotime('-1 month',strtotime(date('Y-m')))),'rok'=>date('Y',strtotime('-1 month',strtotime(date('Y-m'))))])}}"
                class="btn btn-default">
                <</a> <a href="#" class="btn btn-default">></a>
                    @else
                    <a href="{{route('getFakturyFromRange',['miesiac'=>date('m',strtotime('-1 month',strtotime($rok.'-'.$miesiac))),'rok'=>date('Y',strtotime('-1 month',strtotime($rok.'-'.$miesiac)))])}}"
                        class="btn btn-default">
                        <</a> <a
                            href="{{route('getFakturyFromRange',['miesiac'=>date('m',strtotime('+1 month',strtotime($rok.'-'.$miesiac))),'rok'=>date('Y',strtotime('+1 month',strtotime($rok.'-'.$miesiac)))])}}"
                            class="btn btn-default">>
                    </a>
                    @endif
        </div>
    </div>
</div>
@endsection






@section('content')


<div class="row text-right no-margin">
    @if(Route::CurrentRouteName() == 'indexCRMFaktury')
    <a href="{{route('getJPKFromRange',['miesiac'=>date('m'),'rok'=>date('Y')])}}" class="btn btn-info"><i
            class="fa fa-cog" aria-hidden="true"></i>
        Generuj JPK
        {{date('m/Y')}}
    </a>
    @else
    @if(Auth::user()->id_role == '2')
    <a href="{{route('getJPKFromRange',['miesiac'=>$miesiac,'rok'=>$rok])}}" class="btn btn-info"><i class="fa fa-cog"
            aria-hidden="true"></i>
        Generuj JPK
        {{$miesiac}}/{{$rok}}
    </a>
    <a href="{{route('createZestawienieFaktur',['miesiac'=>$miesiac,'rok'=>$rok])}}" class="btn btn-default"><i
            class="fa fa-file-excel-o" aria-hidden="true"></i>
        Zestawienie faktur</a>
    @endif
    @endif


    {{-- 
          <a href="{{route('createCRMFaktury')}}" class="btn btn-success"><i class="fa fa-plus"></i> Dodaj</a> --}}

</div>

<div class="row text-right no-margin">
    {{-- <a href="{{route('createHistory')}}" class="btn btn-success"><i class="fa fa-plus"></i> Dodaj</a>--}}
</div>

<table id="tableSort" table-data-sort="1" table-data-sort-type="desc" class="table table-responsive table-hover"
    table-data-sort="1" table-data-sort-type="desc">
    <thead>
        <th>Numer faktury</th>
        <th>Data wystawienia</th>
        <th>Data sprzedazy</th>
        <th>Klient</th>
        <th>Kwota</th>
        <th>Stan</th>
        <th></th>
    </thead>
    <tbody>

        @foreach($faktury as $faktura)
        <tr>
            <td class="col-sm-1">
                {{$typy[$faktura->typ]}}/{{$faktura->nr_faktury}}
            </td>
            <td class="col-sm-1">
                {{$faktura->data_wystawienia}}
            </td>
            <td class="col-sm-1">
                {{$faktura->data_sprzedazy}}
            </td>
            <td class="col-sm-3">
                @if($faktura->nazwa == '')
                Nieokreslono
                @else
                <a class="label-link" href="{{route('editCRMFirmy',['klientID'=>$faktura->klientID])}}"><i
                        class="fa fa-suitcase"></i> {{$faktura->nazwa}}</a>
                @endif

            </td>
            <td class="col-sm-1">
                {{number_format($faktura->kwota_brutto,2,'.',' ')}} {{Config::get('app.currency')}}
            </td>
            <td class="col-sm-4">

                <div class="col-sm-2">
                    @if($faktura->oplacona == '1')
                    <span class="btn btn-success kontener_input">
                        @else
                        <span class="btn btn-danger kontener_input">
                            @endif
                            <i class="fa fa-usd"></i> <span
                                data-url="{{route('updateAjaxWysylka',['id'=>$faktura->fakturaID])}}"
                                data-value="{{$faktura->oplacona}}" class="btnUpdateAjax fv_zaplacone btn">
                                {!! Form::hidden('oplacona',$faktura->oplacona) !!}

                            </span>
                        </span>


                </div>


                <div class="col-sm-6" style="padding:0px;">
                    {!! Form::date('data_wysylki',$faktura->data_wysylki,['class'=>'updateAjax
                    form-control','data-url'=>route('updateAjaxWysylka',['id'=>$faktura->fakturaID])]) !!}
                </div>
                <div class="col-sm-4" style="padding:0;">

                    {!! Form::select('forma', $forma_wysylki ,$faktura->forma,['class'=>'updateAjax
                    form-control','data-url'=>route('updateAjaxWysylka',['id'=>$faktura->fakturaID])]) !!}
                </div>

            </td>
            <td class="col-sm-1 text-right">

                <a href="{{asset('admin/faktury/')}}/{{$faktura->fakturaID}}_{{$faktura->nr_faktury}}.pdf"
                    target="_blank" class="btn gui btn-default" style="color:red;"><i class="fa fa-file-pdf-o"
                        aria-hidden="true"></i>
                </a>

                <a href="{{route('editCRMFaktury',['id'=>$faktura->fakturaID])}}" class="btn gui btn-default"><i
                        class="fa fa-pencil-square-o" aria-hidden="true"></i></a>




            </td>
        </tr>
        @endforeach

    </tbody>
</table>



@endsection