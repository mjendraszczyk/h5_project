@extends('layouts.app')

@section('title')
Edytuj fakturę nr # 
@foreach($faktury as $faktura)
<span id="stan_nr_faktury">{{$faktura->nr_faktury}}</span>
@endforeach
@endsection

@section('content')
  
  
{{-- @foreach($faktury as $faktura) --}}
 {!! Form::open(['method'=>'PATCH','url' => route("updateCRMFaktury",['id'=>$faktura->fakturaID]),'id'=>'FakturyForm','name'=>'FakturyForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.CRMFaktury.form')
 

{!! Form::close() !!} 
{{-- @endforeach --}}
 
      

@endsection
