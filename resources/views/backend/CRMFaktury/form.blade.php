@if(Session::has('success'))
<div class="alert alert-success">
    {{Session::get('success')}}
</div>
@endif @if(Session::has('errors'))
<div class="alert alert-danger">
    <ul>
        @foreach(Session::get('errors')->all() as $error)
        <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="category-panel">
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                {!! Form::label('nr_faktury','Numer faktury',['class'=>'col-sm-12 control-label']) !!} 
                {{-- TEST: 
                {!! App\Http\Controllers\backend\CRMFakturyController::getNextNrFakturyStatic(3) !!} --}}
                @if(Route::CurrentRouteName() == 'createCRMFaktury')
                    {!! Form::text('nr_faktury',null,['class'=>'col-sm-12 form-control']) !!} 
                    @if($typ == '3') 
                        {!! @Form::hidden('nr_faktury_nowy',App\Http\Controllers\backend\CRMFakturyController::getNextNrFakturyStatic(3),['class'=>'col-sm-12
                        form-control']) !!} 

                        {!! @Form::hidden('FZID',App\Http\Controllers\backend\CRMFakturyController::getNextNrFakturyStatic(3),['class'=>'col-sm-12
                        form-control']) !!} 
                    @endif 
                @else 
                    {!! @Form::text('nr_faktury',$faktura->nr_faktury,['readonly','class'=>'col-sm-12 form-control']) !!} 

                    @if(($faktura->typ == '3') || ($faktura->typ == '2') || ($faktura->typ == '4')) 
                        {!! @Form::hidden('nr_faktury_nowy',App\Http\Controllers\backend\CRMFakturyController::getNextNrFakturyStatic(3),['class'=>'col-sm-12 form-control']) !!} 
                        
                        @if($faktura->typ == '2') 
                            {!! @Form::hidden('FZID',App\Http\Controllers\backend\CRMFakturyController::getNextNrFakturyStatic(3),['class'=>'col-sm-12 form-control']) !!} 
                        @else 
                        {!! @Form::hidden('FZID',$faktura->FZID,['class'=>'col-sm-12 form-control']) !!}
                        @endif 
                    @endif 
                @endif
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
                {!! Form::label('typ','Typ faktury',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFaktury')
                {{-- disabled dla edycji fv --}} {!! Form::select('typ',$typy,null,['class'=>'col-sm-12 form-control','data-url'=>route('getLastNrFaktury',['typ'=>'1'])])
                !!} @else {!! Form::select('typ',$typy,$faktura->typ,['readonly','class'=>'col-sm-12 form-control','data-url'=>route('getLastNrFaktury',['typ'=>$faktura->typ])])
                !!} @endif
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('data_sprzedazy','Data sprzedazy',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName()
                == 'createCRMFaktury') {!! Form::date('data_sprzedazy',date('Y-m-d'),['class'=>'col-sm-12 form-control'])
                !!} @else {!! Form::date('data_sprzedazy',$faktura->data_sprzedazy,['class'=>'col-sm-12 form-control']) !!}
                @endif
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('data_wystawienia','Data wystawienia',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName()
                == 'createCRMFaktury') {!! Form::date('data_wystawienia',date('Y-m-d'),['class'=>'col-sm-12 form-control'])
                !!} @else {!! Form::date('data_wystawienia',$faktura->data_wystawienia,['class'=>'col-sm-12 form-control'])
                !!} @endif
            </div>
        </div>
    </div>

</div>

<div class="row dashboard">
    <div class="row">
        <div class="col-md-6">
            <div class="panel-header">Sprzedawca</div>
            <div class="category-panel">


                <div class="form-group">
                    {!! Form::label('nazwa_sprzedawcy','Nazwa firmy',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName()
                    == 'createCRMFaktury') {!! Form::text('nazwa_sprzedawcy',Config::get('app.firma'),['disabled','class'=>'col-sm-12
                    form-control']) !!} @else {!! Form::text('nazwa_sprzedawcy',Config::get('app.firma'),['disabled','class'=>'col-sm-12
                    form-control']) !!} @endif
                </div>
                <div class="row">
                    <div class="col-md-3">
                        {!! Form::label('kod_sprzedawcy','Kod',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFaktury')
                        {!! Form::text('kod_sprzedawcy',Config::get('app.kod'),['disabled','class'=>'col-sm-12 form-control'])
                        !!} @else {!! Form::text('kod_sprzedawcy',Config::get('app.kod'),['disabled','class'=>'col-sm-12
                        form-control']) !!} @endif
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            {!! Form::label('miasto_sprzedawcy','Miasto',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFaktury')
                            {!! Form::text('miasto_sprzedawcy',Config::get('app.miasto'),['disabled','class'=>'col-sm-12
                            form-control']) !!} @else {!! Form::text('miasto_sprzedawcy',Config::get('app.miasto'),['disabled','class'=>'col-sm-12
                            form-control']) !!} @endif
                        </div>

                    </div>
                </div>


                <div class="row">
                    <div class="col-md-5">
                        {!! Form::label('nip_sprzedawcy','NIP',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFaktury')
                        {!! Form::text('nip_sprzedawcy',Config::get('app.nip'),['disabled','class'=>'col-sm-12 form-control'])
                        !!} @else {!! Form::text('nip_sprzedawcy',Config::get('app.nip'),['disabled','class'=>'col-sm-12
                        form-control']) !!} @endif
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            {!! Form::label('adres_sprzedawcy','Ulica',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFaktury')
                            {!! Form::text('adres_sprzedawcy',Config::get('app.adres'),['disabled','class'=>'col-sm-12 form-control'])
                            !!} @else {!! Form::text('adres_sprzedawcy',Config::get('app.adres'),['disabled','class'=>'col-sm-12
                            form-control']) !!} @endif
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel-header">Nabywca</div>
            <div class="category-panel">
                <div id="KlientForm">
                 @if(Route::CurrentRouteName() == 'createCRMFaktury')
                    {!! Form::hidden('uczestnikID',$uczestnikID) !!} 
                    {!! Form::hidden('termID',$termID) !!} 
                    @else 
                    {!! Form::hidden('uczestnikID',$faktura->uczestnikID) !!}
                    {!! Form::hidden('termID',$faktura->termID) !!} 
                    @endif
                    @if(count(App\Http\Controllers\backend\CRMFakturyController::NabywcaAPI($klientID)) == 0 )

                    <div class="alert alert-danger">Nabywca usunięty</div>
                    @else @foreach(App\Http\Controllers\backend\CRMFakturyController::NabywcaAPI($klientID) as $nabywca) {!! Form::hidden('klientID',$nabywca->klientID)
                    !!}


                    <div class="form-group">
                        {!! Form::label('nazwa_firmy','Nazwa firmy',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFaktury')
                        {!! Form::text('nazwa_firmy',$nabywca->nazwa,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('nazwa_firmy',$nabywca->nazwa,['class'=>'col-sm-12
                        form-control']) !!} @endif
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            {!! Form::label('kod','Kod',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFaktury')
                            {!! Form::text('kod',$nabywca->kod,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('kod',$nabywca->kod,['class'=>'col-sm-12
                            form-control']) !!} @endif
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                {!! Form::label('miasto','Miasto',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFaktury')
                                {!! Form::text('miasto',$nabywca->miasto,['class'=>'col-sm-12 form-control']) !!} @else {!!
                                Form::text('miasto',$nabywca->miasto,['class'=>'col-sm-12 form-control']) !!} @endif
                            </div>

                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-5">
                        {!! Form::label('nip','NIP',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFaktury')
                        {!! Form::text('nip',$nabywca->nip,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('nip',$nabywca->nip,['class'=>'col-sm-12
                        form-control']) !!} @endif
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            {!! Form::label('adres','Ulica',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFaktury')
                            {!! Form::text('adres',$nabywca->adres,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('adres',$nabywca->adres,['class'=>'col-sm-12
                            form-control']) !!} @endif
                        </div>

                    </div>
                </div>

                @endforeach @endif
            </div>
        </div>
    </div>
</div>


<div class="category-panel">
    <table id="fv_table" class="table table-responsive table-hover">
        <thead>
            <th>
                Lp.
            </th>
            <th>
                Nazwa
            </th>

            <th>
                Ilość
            </th>
            <th>
                j.m.
            </th>
            <th>
                Cena jedn.
            </th>
            <th>
                Wartość netto
            </th>
            <th>
                VAT [%]
            </th>
            <th>
                Wartość VAT
            </th>
            <th>
                Wartość brutto
            </th>
            </tr>
            <tbody>
                <tr>
                    <td>
                        1
                    </td>
                    <td>
                        @if(Route::CurrentRouteName() == 'createCRMFaktury') @if($szkolenieTerm_title == '0') 
                        {!! Form::textarea('opis',null,['required','rows'=>'3','class'=>'col-sm-12 form-control']) !!} 
                        {!! Form::hidden('opis_org',null,['required','rows'=>'3','class'=>'col-sm-12 form-control']) !!} 
                        @else 
                        {!! Form::textarea('opis','Szkolenie: '.$szkolenieTerm_title->training_title.' PKWiU: 85.59.B',['required','rows'=>'3','class'=>'col-sm-12 form-control']) !!} 
                        {!! Form::hidden('opis_org','Szkolenie: '.$szkolenieTerm_title->training_title.' PKWiU: 85.59.B',['required','rows'=>'3','class'=>'col-sm-12 form-control']) !!} 
                        @endif 
                        @else 
                        {!! Form::textarea('opis',$faktura->opis,['required','rows'=>'3','class'=>'col-sm-12 form-control']) !!} 
                        {!! Form::hidden('opis_org',$faktura->opis,['required','rows'=>'3','class'=>'col-sm-12 form-control']) !!} 
                        @endif
                    </td>
                    <td class="col-xs-1">
                        @if(Route::CurrentRouteName() == 'createCRMFaktury') {!! Form::text('ilosc',1,['class'=>'col-sm-12 form-control']) !!} @else
                        {!! Form::text('ilosc',$faktura->ilosc,['class'=>'col-sm-12 form-control']) !!} @endif
                    </td>
                    <td class="col-xs-1">
                        @if(Route::CurrentRouteName() == 'createCRMFaktury') {!! Form::select('jm',$jm,null,['class'=>'col-sm-12 form-control'])
                        !!} @else {!! Form::select('jm',$jm,$faktura->jm,['class'=>'col-sm-12 form-control']) !!} @endif
                    </td>
                    <td>
                        @if(Route::CurrentRouteName() == 'createCRMFaktury') 
                        @if($szkolenieTerm_title == '0') 

                        {!! Form::text('cena_jednostkowa',null,['class'=>'col-sm-12 form-control']) !!}
                        {!! Form::hidden('cena_jednostkowa_org',null,['class'=>'col-sm-12 form-control']) !!}

                        @else 
                         {!! Form::text('cena_jednostkowa',$szkolenieTerm_price->training_price,['class'=>'col-sm-12 form-control']) !!}
                         {!! Form::hidden('cena_jednostkowa_org',$szkolenieTerm_price->training_price,['class'=>'col-sm-12 form-control']) !!}
                        @endif 
                         @else 
                         {!! Form::text('cena_jednostkowa',$faktura->cena_jednostkowa,['class'=>'col-sm-12 form-control']) !!} 
                         @if($faktura->cena_jednostkowa_org != 0)
                         {!! Form::hidden('cena_jednostkowa_org',$faktura->cena_jednostkowa_org,['class'=>'col-sm-12 form-control']) !!}
                         @else 
                         {!! Form::hidden('cena_jednostkowa_org',$faktura->cena_jednostkowa,['class'=>'col-sm-12 form-control']) !!}
                         @endif
                         @endif
                    </td>

                    @if(Route::CurrentRouteName() == 'createCRMFaktury') {!! Form::hidden('cena_jednostkowa_normalna',null,['class'=>'col-sm-12
                    form-control']) !!} @else {!! Form::hidden('cena_jednostkowa_normalna',$faktura->cena_jednostkowa,['class'=>'col-sm-12
                    form-control']) !!} @endif
                    </td>



                    <td>
                        @if(Route::CurrentRouteName() == 'createCRMFaktury') {!! Form::text('kwota',null,['class'=>'col-sm-12 form-control']) !!}
                        @else {!! Form::text('kwota',$faktura->kwota,['class'=>'col-sm-12 form-control']) !!} @endif
                    </td>

                    <td class="col-xs-1">
                        @if(Route::CurrentRouteName() == 'createCRMFaktury') {!! Form::select('VAT',$vat,'23',['class'=>'col-sm-12 form-control'])
                        !!} @else {!! Form::select('VAT',$vat,$faktura->VAT,['class'=>'col-sm-12 form-control']) !!} @endif
                    </td>
                    <td>
                        @if(Route::CurrentRouteName() == 'createCRMFaktury') {!! Form::text('kwota_VAT',null,['class'=>'col-sm-12 form-control'])
                        !!} @else {!! Form::text('kwota_VAT',$faktura->kwota_VAT,['class'=>'col-sm-12 form-control']) !!}
                        @endif
                    </td>
                    <td>
                        @if(Route::CurrentRouteName() == 'createCRMFaktury') {!! Form::text('kwota_brutto',null,['class'=>'col-sm-12 form-control'])
                        !!} @else {!! Form::text('kwota_brutto',$faktura->kwota_brutto,['class'=>'col-sm-12 form-control'])
                        !!} @endif
                    </td>

                </tr>
            </tbody>
    </table>

    <div class="row">
        <div class="col-md-6">
            @if(Route::CurrentRouteName() == 'editCRMFaktury') @if(($faktura->typ == '3') || ($faktura->typ == '4'))
            <h4>Powiązane faktury</h4>
            <ul class="list">
                @foreach($fzaliczkowe as $zaliczka) @if($faktura->FZID == $zaliczka->FZID)
                <li>
                    Faktura {{$zaliczka->nr_faktury}} / {{$zaliczka->rok}} <span class="mark alert-success price" style="position:relative;right:0;">{{$zaliczka->zaplacono}}  {{Config::get('app.currency')}}</span>
                    <div class="pull-right">
                        <a href="{{route('editCRMFaktury',['id'=>$zaliczka->fakturaID])}}" class="btn btn-default">Edytuj</a>
                        <a href="{{route('GETdestroyCRMFaktury',['id'=>$zaliczka->fakturaID])}}" class="btn btn-default"><i class="fa fa-times" aria-hidden="true"></i></a>                        {{-- {!! Form::open(['method'=>'DELETE','action'=>['backend\CRMFakturyController@destroy',$zaliczka->fakturaID],'style'=>'display:inline-block;'])
                        !!}
                        <button class="btn gui btn-default"> 
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button> {!! Form::close() !!} --}}
                    </div>

                </li>
                @endif @endforeach
            </ul>
            @endif @endif
        </div>
        @if(Route::CurrentRouteName() == 'editCRMFaktury') @if(($faktura->typ == '3') || ($faktura->typ == '4'))
        <div class="col-md-6">
            <div class="col-md-6">
                <label for="zaplacono">Zapłacono w zaliczce</label> @if(Route::CurrentRouteName() == 'editCRMFaktury') {!!
                Form::text('zaplacono',$faktura->zaplacono,['required','class'=>'form-control']) !!} @else {!! Form::text('zaplacono',null,['class'=>'form-control'])
                !!} @endif
            </div>
            <div class="col-md-6">
                <label for="zaplacono">Podsumowanie</label>


                <span class="row" style="display:block;">
                    <div class="col-md-6">
                        Pozostało: 
                    </div>
                    @if(Route::CurrentRouteName() == 'editCRMFaktury')
                    <div class="col-md-6">
                        {!! Form::text('pozostalozFV',round(($faktura->kwota_brutto-$zaplacono),2),['readonly','class'=>'naked__input text-right']) !!} {{Config::get('app.currency')}}
                    </div>
                    @else
                    <div class="col-md-6">
                        {!! Form::text('pozostalozFV',0,['readonly','class'=>'naked__input text-right']) !!} {{Config::get('app.currency')}}
                    </div>
                    @endif
                </span>
                <span class="row" style="display:none;">
                    <div class="col-md-6">
                        Pozostanie:
                    </div>
                    @if(Route::CurrentRouteName() == 'editCRMFaktury')
                    <div class="col-md-6">
                        {!! Form::text('pozostalo',$faktura->pozostalo,['readonly','class'=>'naked__input text-right']) !!} {{Config::get('app.currency')}}
                    </div>
                    @else
                    <div class="col-md-6">
                        {!! Form::text('pozostalo',null,['readonly','class'=>'naked__input text-right']) !!} {{Config::get('app.currency')}}
                    </div>
                    @endif
                </span>

            </div>
            @if(Route::CurrentRouteName() == 'editCRMFaktury')
            <div class="col-sm-12">
                <label for="zaplacono">Zapłacono łącznie</label>
                <span class="row">
                    {!! Form::text('zaplacono_lacznie',round($zaplacono,2),['readonly','class'=>'naked__input']) !!}
                    {!!Form::hidden('zaplacono_lacznie_licz',$zaplacono)!!} {{Config::get('app.currency')}}
                </span>
            </div>
            @endif
        </div>
        @endif @endif
    </div>

</div>


<div class="category-panel">
    <div class="row">
        <div class="col-sm-4">

            {!! Form::label('rabat','Rabat [%]',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFaktury')
            {!! Form::text('rabat', null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('rabat',$faktura->rabat_f,['class'=>'col-sm-12
            form-control']) !!} @endif

        </div>


        <div class="col-md-4">
            {!! Form::label('termin','Termin płatnosci',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFaktury')
            {!! Form::select('termin',$termin,null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::select('termin',$termin,$faktura->termin,['class'=>'col-sm-12
            form-control']) !!} @endif
        </div>
        <div class="col-md-4">

            {!! Form::label('forma','Typ płatnosci',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFaktury')
            {!! Form::select('forma',$forma,null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::select('forma',$forma,$faktura->forma,['class'=>'col-sm-12
            form-control']) !!} @endif
        </div>


    </div>





    <div class="form-group">
        <div class="col-sm-12 text-right save-row">
            @if(Route::CurrentRouteName() == 'createCRMFaktury') {!! Form::submit('Utwórz', ['class'=>'btn btn-success']) !!} @else @if($sprawdzPDF
            == '1')
            <a href="{{asset('admin/faktury/')}}/{{$faktura->fakturaID}}_{{$faktura->nr_faktury}}.pdf" target="_blank" class="btn btn-default"
                @else <a href="{{asset('admin/faktury/')}}/{{$faktura->fakturaID}}_{{$faktura->nr_faktury}}.pdf" target="_blank"
                class="disabled btn btn-default hidden" @endif style="color:red;">
                <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                Zobacz fakturę PDF 
            </a> @if(($faktura->typ == '3') || ($faktura->typ == '2')) @if(Route::CurrentRouteName() == 'editCRMFaktury')
            {!! Form::button('Utwórz fakturę zaliczkową', ['class'=>'btn btn-default','name'=>'submitBtn','value'=>'faktura-zaliczkowa','type'=>'submit'])
            !!} @if($faktura->typ == '3') {!! Form::button('Utwórz fakturę rozliczeniową', ['class'=>'btn btn-info','name'=>'submitBtn','value'=>'faktura-koncowa','type'=>'submit'])
            !!} @endif @endif @endif @if(count(App\Http\Controllers\backend\CRMFakturyController::NabywcaAPI($klientID))
            == 0 ) {!! Form::submit('Zapisz', ['disabled','class'=>'btn btn-success']) !!} @else {!! Form::button('Generuj
            PDF', ['class'=>'btn btn-primary','name'=>'submitBtn','value'=>'update','type'=>'submit']) !!} {!! Form::button('Zapisz',
            ['class'=>'btn btn-success','name'=>'submitBtn','value'=>'save-faktura','type'=>'submit']) !!} @endif @endif
        </div>
    </div>
</div>