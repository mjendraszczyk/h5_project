
@extends('layouts.app')

@section('title')
Dodaj fakturę
<span id="stan_nr_faktury"></span>
@endsection

@section('content')
   
 {!! Form::open(['url' => route("storeCRMFaktury"),'id'=>'FakturyForm','name'=>'FakturyForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.CRMFaktury.form')
 

{!! Form::close() !!} 
 
     
@endsection
