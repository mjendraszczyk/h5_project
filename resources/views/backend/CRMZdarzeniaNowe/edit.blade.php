<div class="form-container modal-open modal fade bd-example-modal-lg hidden-sm-down" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            {!! Form::open(['url' =>
            route("updateCRMZdarzeniaNew",['id'=>$zdarzenie->zdarzenieID]),'id'=>'ZdarzeniaForm','method'=>'POST','name'=>'ZdarzeniaForm','enctype'
            =>
            'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}
            {{-- @method('POST') --}}

            @include('backend.CRMZdarzeniaNowe.form')


            {!! Form::close() !!}

        </div>
    </div>
</div>