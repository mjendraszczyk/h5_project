@extends('layouts.app')

@section('title')
<div class="row">
    <div class="col-md-6">
        Zdarzenia
    </div>
    <div class="col-md-6 text-right">
        <div class="col-md-12">

            {!! Form::open(['name'=>'filtr_zdarzenia','url'=>route('filtrZdarzeniaNew'),'class'=>'form-inline']) !!}
            <div class="input-group">
                @if(Route::CurrentRouteName() == 'getResultsZdarzeniaNew')
                <div class="input-group-btn">
                    {!!
                    Form::select('typ_zdarzenia',$typy_zdarzenia,$typID,['class'=>'form-control','style'=>'width:100%;'])
                    !!}
                </div>
                @if(Auth::user()->id_role == '3')
                <div class="input-group-btn">
                    {!! Form::hidden('opiekun',Auth::user()->id)!!}
                </div>
                @else
                <div class="input-group-btn">
                    {!! Form::select('opiekun',$opiekun,$opiekunID,['class'=>'form-control','style'=>'width:100%;']) !!}
                </div>
                @endif

                @else
                <div class="input-group-btn">
                    {!!
                    Form::select('typ_zdarzenia',$typy_zdarzenia,null,['class'=>'form-control','style'=>'width:100%;'])
                    !!}
                </div>
                @if(Auth::user()->id_role == '3')
                <div class="input-group-btn">
                    {!! Form::hidden('opiekun',Auth::user()->id)!!}
                </div>
                @else
                <div class="input-group-btn">
                    {!! Form::select('opiekun',$opiekun,null,['class'=>'form-control','style'=>'width:100%;']) !!}
                </div>
                @endif
                {{-- <div class="input-group-btn">
                    {!! Form::select('opiekun',$opiekun,null,['class'=>'form-control','style'=>'width:100%;']) !!}
                </div> --}}
                @endif
                <div class="input-group-btn">
                    {!! Form::submit('Filtr',['class'=>'btn btn-default']) !!}
                    <a href="{{route('indexCRMZdarzeniaNew')}}" class="btn btn-default"><i class="fa fa-times"
                            aria-hidden="true"></i></a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

@section('content')


<table id="tableSort" table-data-sort='0' table-data-sort-type='desc' class="table table-responsive">
    <thead>
        <th>Data zdarzenia</th>
        <th>Typ zdarzenia</th>
        <th>Pozostało</th>
        <th>Komentarz</th>
        <th>Firma</th>
        <th>Przypisany</th>
        <th>Stan</th>
        <th>Kwota</th>
        <th>Priorytet</th>
        <th></th>
        {{--<th></th> --}}
    </thead>
    <tbody>

        @foreach($zdarzenia as $zdarzenie)
        <tr @if($zdarzenie->stan == '2') class="wygrana" @endif @if($zdarzenie->stan == '3') class="odrzucona" @endif
            @if($zdarzenie->stan == '4') class="zrealizowana" @endif>
            <td class="col-md-1">
                {!! Form::date('dzialanie_data', $zdarzenie->dzialanie_data,
                ['class'=>'form-control updateAjax dzialanieData',
                'data-url'=>route('updateAjaxZdarzenieData',['id'=>$zdarzenie->zdarzenieID])]) !!}
                <span style="display:none;">{{$zdarzenie->dzialanie_data}} </span>
            </td>

            <td class="col-md-1">

                {{$typy_zdarzenia[$zdarzenie->typ]}}

            </td>
            <td class="col-md-1">
                <span class="fa fa-clock-o"></span>
                <span class="pozostalo">
                    @php
                    $earlier = new DateTime(date('Y-m-d'));
                    $later = new DateTime($zdarzenie->dzialanie_data);

                    $diff = $later->diff($earlier)->format("%a");
                    
                    if($diff <= 0) {
                        echo "Na dziś";
                    }
                    elseif(($diff > 0) && ($diff <= 7))  {
                        echo 'Ten tydzień';
                    } else {
                        echo $diff. ' dni';
                    }
                
                    @endphp
                </span>
                </div>
            <td class="col-md-2">
                {{-- {{$zdarzenie->komentarz}} --}}
                @if(empty($zdarzenie->dzialanie_komentarz))
                {!! Form::textarea('komentarz',$zdarzenie->komentarz,['class'=>'updateAjax col-sm-12
                form-control','rows'=>'1',
                'data-url'=>route('updateAjaxZdarzenieKomentarz',['id'=>$zdarzenie->zdarzenieID])])
                !!}
                @else
                {!! Form::textarea('komentarz',$zdarzenie->dzialanie_komentarz,['class'=>'updateAjax col-sm-12
                form-control','rows'=>'1',
                'data-url'=>route('updateAjaxZdarzenieKomentarz',['id'=>$zdarzenie->zdarzenieID])])
                !!}
                @endif
            </td>
            <td class="col-md-2">


                @foreach(App\Http\Controllers\backend\CRMFirmyController::getFirma($zdarzenie->klientID) as $firma)
                <a class="label-link" href="{{route('editCRMFirmy',['id'=>$firma->klientID])}}">{{$firma->nazwa}}</a>
                @endforeach



            </td>
            <td class="col-md-2">



                <div class="form-group">
                    <span class="fa fa-user" style="border: 1px solid #bfbfbf;
                        border-radius: 50%;
                        padding: 3px 5px;">

                    </span>
                    @foreach(App\Http\Controllers\backend\UzytkownicyController::getUzytkownik($zdarzenie->opiekunID) as
                    $opiekunName)
                    @if(Auth::user()->id_role == '2')
                    {!! Form::select('opiekun', $opiekun, $opiekunName->id,
                    ['class'=>'updateAjax form-control','style'=>'display:inline-block !important;width: 75%
                    !important;','data-url'=>route('updateAjaxZdarzenieOpiekun',['id'=>$zdarzenie->zdarzenieID])])
                    !!}
                    @else


                    {{$opiekunName->name}}



                    @endif
                    @endforeach
                </div>

            </td>
            <td class="col-md-2">
                {{-- <span class="alert-success" style="padding: 3px 5px;"> --}}

                {{-- {{$stany_zdarzenia[$zdarzenie->stan]}} --}}
                {!! Form::select('stan', $stany_zdarzenia, $zdarzenie->stan, ['class'=>'updateAjax
                form-control','data-url'=>route('updateAjaxZdarzenieStatus',['id'=>$zdarzenie->zdarzenieID])]) !!}
                {{-- </span> --}}
            </td>
            <td>{{$zdarzenie->kwota}} zł</td>
            <td>
                @if($zdarzenie->priorytet == '1')
                <span class="alert-danger" style="padding: 5px;">
                    <span class="fa fa-warning"></span>
                    {{$priorytet[$zdarzenie->priorytet]}}
                    {{-- {{print_r($priorytet)}} --}}
                </span>
                @else
                <span class="alert-info" style="padding: 5px;">
                    <span class="fa fa-warning"></span>
                    {{$priorytet[$zdarzenie->priorytet]}}
                    {{-- {{print_r($priorytet)}} --}}
                </span>
                @endif
            </td>
            <td class="col-md-1">
                <div class="form-group">
                    @if(Auth::user()->id_role == '2')

                    <span data-url="{{route('editCRMZdarzeniaNew',['id'=>$zdarzenie->zdarzenieID])}}"
                        data-element=".form-container" class="modal_open btn btn-default"><i label="Edytuj zdarzenie"
                            class="fa fa-edit"></i></span>

                    {{-- <a href="{{route('editCRMZdarzeniaNew',['id'=>$zdarzenie->zdarzenieID])}}"
                    class="btn btn-default"><span class="fa fa-edit"></span></a> --}}
                    @endif
                    {{-- <span class="hidden">{{$zdarzenie->hr}}</span>
                    {!! Form::select('hr', $hr, $zdarzenie->hr, ['class'=>'updateAjax
                    form-control','data-url'=>route('updateAjaxZdarzenie',['id'=>$zdarzenie->zdarzenieID])]) !!} --}}

                </div>
            </td>
        </tr>
        @endforeach

    </tbody>
</table>



@endsection