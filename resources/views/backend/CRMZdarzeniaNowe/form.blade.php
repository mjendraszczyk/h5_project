@if(Session::has('success'))
<div class="alert alert-success">
    {{Session::get('success')}}
</div>
@endif

@if(Session::has('errors'))
<div class="alert alert-danger">
    <ul>
        <!-- {{Session::get('errors')}} -->
        @foreach(Session::get('errors')->all() as $error)

        <!-- {{ Session::get('errors')}}  -->
        <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
@endif

@if((Route::CurrentRouteName() == 'admin') || (Route::CurrentRouteName() == 'filtr_dash_miasto') ||
(Route::CurrentRouteName() == 'createCRMZdarzeniaNew'))
<div class="form-group">
    <div class="autocomplete-container">

        {!! Form::label('uczestnikID','Uczestnik',['class'=>'col-sm-12 control-label']) !!}
        @if(Route::CurrentRouteName()
        != 'editCRMZdarzeniaNew')
        {!! Form::text('uczestnikID',null,['required','id'=>'dashboardFindUczestnik','placeholder'=>'Znadź
        osobe','class'=>'col-sm-12 form-control']) !!}

        @else {!! Form::text('uczestnikID',null,['required','id'=>'dashboardFindUczestnik','placeholder'=>'Znadź
        osobe','class'=>'col-sm-12
        form-control']) !!} @endif
    </div>
</div>
@endif
<div class="form-group">

    {{-- @if(Route::CurrentRouteName() == 'createCRMZdarzeniaNew')
    {!! Form::hidden('uczestnikID',null,['id'=>'getUczestnikID','class'=>'col-sm-12 form-control'])
    !!}
    @else {!! Form::hidden('uczestnikID',$zdarzenie->uczestnikID,['id'=>'getUczestnikID','class'=>'col-sm-12
    form-control']) !!} @endif --}}


</div>

<div class="form-group">
    <div class="row">
        <div class="col-md-6">
            {!! Form::label('typ','Typ',['class'=>'col-sm-12 control-label']) !!} @if((Route::CurrentRouteName() ==
            'createCRMZdarzeniaNew') || (Route::CurrentRouteName() == 'admin') || (Route::CurrentRouteName() ==
            'filtr_dash_miasto'))
            {!! Form::select('typ',$typy_zdarzenia,null,['class'=>'col-sm-12 form-control']) !!} @else {!!
            Form::select('typ',$typy_zdarzenia,$zdarzenie->typ,['class'=>'col-sm-12
            form-control']) !!} @endif
        </div>
        <div class="col-md-6">
            {!! Form::label('data_przypomnienia','Data przypomnienia',['class'=>'col-sm-12 control-label']) !!}
            {{-- {!! Form::label('data_dodania','Data dodania',['class'=>'col-sm-12 control-label']) !!}   --}}
            {!! Form::hidden('data_dodania',date('Y-m-d'),['class'=>'form-control']) !!}
            {!! Form::date('data_przypomnienia',date('Y-m-d'),['class'=>'form-control']) !!}
            {!! Form::checkbox('przypomnij','1',['class'=>'form-control']) !!} Przypomnij o zdarzeniu

        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('temat','Temat',['class'=>'col-sm-12 control-label']) !!} @if((Route::CurrentRouteName() ==
    'createCRMZdarzeniaNew') || (Route::CurrentRouteName() == 'admin') || (Route::CurrentRouteName() ==
    'filtr_dash_miasto'))
    {!! Form::text('temat',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::text('temat',$zdarzenie->temat,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('komentarz','Opis',['class'=>'col-sm-12 control-label']) !!}
    @if((Route::CurrentRouteName() ==
    'createCRMZdarzeniaNew') || (Route::CurrentRouteName() == 'admin') || (Route::CurrentRouteName() ==
    'filtr_dash_miasto'))
    {!! Form::textarea('komentarz',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::textarea('komentarz',$zdarzenie->komentarz,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

@if((Route::CurrentRouteName() !=
'createCRMZdarzeniaNew') && (Route::CurrentRouteName() !=
'admin'))
<div class="form-group">
    {!! Form::label('dzialanie_komentarz','Komentarz',['class'=>'col-sm-12 control-label']) !!}
    @if((Route::CurrentRouteName() !=
    'editCRMZdarzeniaNew') || (Route::CurrentRouteName() == 'admin') || (Route::CurrentRouteName() ==
    'filtr_dash_miasto'))
    {!! Form::textarea('dzialanie_komentarz',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::textarea('dzialanie_komentarz',$zdarzenie->dzialanie_komentarz,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
@endif

<div class="form-group">
    {!! Form::label('priorytet','Priorytet',['class'=>'col-sm-12 control-label']) !!} @if((Route::CurrentRouteName() !=
    'editCRMZdarzeniaNew') || (Route::CurrentRouteName() == 'admin') || (Route::CurrentRouteName() ==
    'filtr_dash_miasto'))
    {!! Form::select('priorytet',$priorytet, null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::select('priorytet',$priorytet,$zdarzenie->priorytet,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    {!! Form::label('kwota','Kwota',['class'=>'col-sm-12 control-label']) !!} @if((Route::CurrentRouteName() ==
    'createCRMZdarzeniaNew') || (Route::CurrentRouteName() == 'admin') || (Route::CurrentRouteName() ==
    'filtr_dash_miasto'))
    {!! Form::text('kwota',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::text('kwota',$zdarzenie->kwota,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

@if((Route::CurrentRouteName() != 'admin') &&(Route::CurrentRouteName() != 'filtr_dash_miasto') &&
(Route::CurrentRouteName() != 'createCRMZdarzeniaNew'))

<div class="form-group">
    {!! Form::label('klientID','Klient',['class'=>'col-sm-12 control-label']) !!} @if((Route::CurrentRouteName() ==
    'createCRMZdarzeniaNew') || (Route::CurrentRouteName() == 'admin') || (Route::CurrentRouteName() ==
    'filtr_dash_miasto'))
    {!! Form::select('klientID',$klienci, null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::select('klientID',$klienci, $zdarzenie->klientID,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
@endif

<div class="form-group">
    {!! Form::label('stan','Stan',['class'=>'col-sm-12 control-label']) !!} @if((Route::CurrentRouteName() ==
    'createCRMZdarzeniaNew') || (Route::CurrentRouteName() == 'admin') || (Route::CurrentRouteName() ==
    'filtr_dash_miasto'))
    {!! Form::select('stan',$stany_zdarzenia, null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::select('stan',$stany_zdarzenia, $zdarzenie->stan,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">

    @if((Route::CurrentRouteName() == 'createCRMZdarzeniaNew') || (Route::CurrentRouteName() == 'admin') ||
    (Route::CurrentRouteName() == 'filtr_dash_miasto'))
    {!! Form::hidden('uczestnikID',null,['id'=>'getUczestnikID','class'=>'col-sm-12 form-control']) !!}
    @else {!! Form::hidden('uczestnikID',$uczestnikID,['id'=>'getUczestnikID','class'=>'col-sm-12
    form-control']) !!} @endif


</div>


<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() != 'editCRMZdarzenia') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!}
        @else {!!
        Form::submit('Zapisz', ['class'=>'btn btn-success']) !!} @endif
    </div>
</div>
<script>
    $(document).ready(function() {
    autocompeteFnc("{{route('osobyAutocomplete')}}","#dashboardFindUczestnik","#getUczestnikID");
    autocompeteFnc("{{route('szkoleniaAutocomplete')}}","#autocompleteSzkolenia","#getTrainingID");
    
    });
</script>