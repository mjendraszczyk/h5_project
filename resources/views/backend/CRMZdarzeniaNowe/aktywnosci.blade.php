<h4 class="smallTitleBox">Zdarzenia</h4>
<ul class="list">
@foreach($zdarzenia as $zdarzenie) 
 <li>
<span class="label success">{{$zdarzenie->data_dodania}} </span>
{{$zdarzenie->komentarz}}  
 </li>
@endforeach
@if(count($zdarzenia) == 0)  
Brak zdarzeń
@endif
</ul>
<h4 class="smallTitleBox">Szkolenia</h4>
<ul class="list">
@foreach($szkolenia as $szkolenie) 
<li>
    <span class="label success">{{$szkolenie->term_start}} </span>
<a href="{{route('showCRMSzkolenie',['termID'=>$szkolenie->termID])}}">{{$szkolenie->training_title}} </a>

</li>
@endforeach
@if(count($szkolenia) == 0)  
Brak szkoleń
@endif
</ul>