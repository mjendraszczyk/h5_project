@extends('layouts.app') 
@section('title') Edycja Headline
@endsection
 
@section('content')

<div class="panel-shadow">
    @foreach($headlines as $headline) {!! Form::open(['method'=>'PATCH','url' => route("updateHeadline",['id'=>$headline->headlineID]),'enctype'
    => 'multipart/form-data','id'=>'HeadlineForm','name'=>'HeadlineForm','class'=>'form-vertical','style'=>'width:100%;'])
    !!}
    @include('backend.headlines.form') {!! Form::close() !!} @endforeach
</div>
@endsection