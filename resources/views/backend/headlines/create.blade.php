@extends('layouts.app')

@section('title')
Dodaj Headline
@endsection

@section('content')
  
 <div class="panel-shadow">

 {!! Form::open(['url' => route("storeHeadline"),'id'=>'HeadlineForm','name'=>'HeadlineForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.headlines.form')
 

{!! Form::close() !!} 
</div>
      

@endsection
