@extends('layouts.app') 
@section('title') Headline
@endsection
 
@section('content')
<div class="row text-right no-margin">
    <a href="{{route('createHeadline')}}" class="btn btn-success"><i class="fa fa-plus"></i> Dodaj</a>
</div>

<table id="tableSort" class="table table-responsive table-hover" data-toggle="table" data-sort-name="date" data-sort-order="desc">
    <thead>
        <th>Obrazek</th>
        <th>Tytul</th>
        <th>Tresc</th>
        <th>Link</th>
        <th>Pozycja</th>
        <th></th>
    </thead>
    <tbody>
         
            @foreach($headlines as $headline)
            <tr>
                <td>
                    <img src="{{Config('url')}}/img/frontend/headlines/{{$headline->picture}}" alt="" class="thumbinail min" />
                </td>
                <td>
                    {{$headline->title}}
                </td>
                <td>
                    {{$headline->content}}
                </td>
                <td>
                    {{$headline->link}}
                </td>
                <td>
                    {{$headline->position}}
                </td>
                <td class="text-right">
                    <a href="{{route('editHeadline',['id'=>$headline->headlineID])}}" class="btn gui btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>{!!
                    Form::open(['method'=>'DELETE','action'=>['backend\HeadlinesController@destroy',$headline->headlineID]])
                    !!}
                    <button class="btn gui btn-default"> 
 <i class="fa fa-times" aria-hidden="true"></i>
</button> {!! Form::close() !!}


                </td>
            </tr>
            @endforeach

    </tbody>
</table>
@endsection