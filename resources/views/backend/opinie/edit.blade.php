@extends('layouts.app') 
@section('title') Edycja Opini
@endsection
 
@section('content')

<div class="panel-shadow">
    @foreach($opinie as $opinia) {!! Form::open(['method'=>'PATCH','url' => route("updateSzkoleniaOpinia",['szkolenieID'=>$szkolenieID,'id'=>$opinia->referencePrivID]),'enctype'
    => 'multipart/form-data','id'=>'SzkoleniaOpiniaForm','name'=>'SzkoleniaOpiniaForm','class'=>'form-vertical','style'=>'width:100%;'])
    !!}
    @include('backend.opinie.form') {!! Form::close() !!} @endforeach
</div>
@endsection