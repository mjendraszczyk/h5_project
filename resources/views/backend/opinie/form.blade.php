@if(Session::has('success')) 
<div class="alert alert-success">
    {{Session::get('success')}}
 </div>
@endif

@if(Session::has('errors')) 
<div class="alert alert-danger">
<ul>
<!-- {{Session::get('errors')}} -->
@foreach(Session::get('errors')->all() as $error)

  <!-- {{ Session::get('errors')}}  -->
  <li>{{$error}}</li>
  @endforeach
  </ul>
</div>
@endif

 

<div class="form-group">
    {!! Form::label('tresc','Tresc',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createSzkoleniaOpinia')
    {!! Form::textarea('tresc',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::textarea('tresc',$opinia->tresc,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('imie','Imię',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createSzkoleniaOpinia')
    {!! Form::text('imie',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('imie',$opinia->imie,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
 
 <div class="form-group">
    {!! Form::label('firma','Firma',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createSzkoleniaOpinia')
    {!! Form::text('firma',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('firma',$opinia->firma,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
 
  
 
<div class="form-group">
    {!! Form::label('ocena','Ocena',['class'=>'col-sm-12 control-label']) !!} 
    
    @if(Route::CurrentRouteName() == 'createSzkoleniaOpinia')

    {!! Form::select('ocena', $oceny,null, ['class'=>'form-control']) !!}

     @else 
    {!! Form::select('ocena', $oceny,$opinia->ocena, ['class'=>'form-control']) !!}
      
    @endif
</div>



<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() == 'createSzkoleniaOpinia') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!} @else {!!
        Form::submit('Zapisz', ['class'=>'btn btn-success']) !!} @endif
    </div>