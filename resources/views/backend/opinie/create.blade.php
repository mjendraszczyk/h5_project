@extends('layouts.app')

@section('title')
Dodaj Ocene
@endsection

@section('content')
  
 <div class="panel-shadow">

 {!! Form::open(['url' => route("storeSzkoleniaOpinia",['szkolenieID'=>$szkolenieID]),'id'=>'SzkoleniaOpiniaForm','name'=>'SzkoleniaOpiniaForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.opinie.form')
 

{!! Form::close() !!} 
</div>
      

@endsection
