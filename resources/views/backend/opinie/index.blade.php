@extends('layouts.app') 
@section('title') Oceny szkolenia
@foreach($trening as $t) 
 
{{$t->training_title}}
 
@endforeach
@endsection
 
@section('content')
<div class="row text-right no-margin">
    <a href="{{route('createSzkoleniaOpinia',['szkolenieID'=>$szkolenieID])}}" class="btn btn-success"><i class="fa fa-plus"></i> Dodaj</a>
</div>
  

<table class="tableSort table table-responsive table-hover" data-toggle="table" data-sort-name="date" data-sort-order="desc">
    <thead>
    <th>
        ID
        </th>
    <th>Imię</th>
        <th>Firma</th>
        <th>Ocena</th>
        <th></th>
    
    </thead>
    <tbody>


@foreach($opinie as $opinia) 

        <tr>
        <td>
        {{$opinia->referencePrivID}}
        </td>
        <td>
        {{$opinia->imie}}
        </td>
<td>
        {{$opinia->firma}}
        </td>
        <td>
        {{$opinia->ocena}}
        </td>
      

 <td class="text-right">
                    <a href="{{route('editSzkoleniaOpinia',['szkolenieID'=>$szkolenieID,'id'=>$opinia->referencePrivID])}}" class="btn gui btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    

 
                     
                    {!!
                     
                     Form::open(['method'=>'DELETE','action'=>['backend\OpinieController@destroy','szkolenieID'=>$szkolenieID,'id'=>$opinia->referencePrivID]])
                    !!}
                    <button class="btn gui btn-default"> 
 <i class="fa fa-times" aria-hidden="true"></i>
</button> {!! Form::close() !!}


                 

                </td>

           </tr>


            @endforeach
</tbody>
</table>
    
@endsection