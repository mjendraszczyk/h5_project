@extends('layouts.app')
@section('title') Edycja klienta
<div class="pull-right">
    <a href="{{route('createCRMFirmy')}}" class="btn btn-success">Dodaj firmę</a>
    <span data-url="{{route('konsolidujCRMFirmy',['klientID'=>$id,'method'=>'modal'])}}" data-element=".form-container"
        class="modal_open btn btn-info">Konsoliduj</span>
    @if(Auth::user()->id_role == '2')
    {!!Form::open(['method'=>'DELETE','action'=>['backend\CRMFirmyController@destroy',$klientID],'style'=>'display:inline-block;'])!!}
    <button class="btn btn-danger"> Usuń firmę </button> {!! Form::close() !!}
    @endif

</div>
@endsection

@section('content')

<div class="panel-heading">
    @foreach($CRMFirmy as $k=>$firma)
    @if($k == 0)
    <h1 style="display:inline-block;">{{$firma->nazwa}}
        <span>
            @for($di=0;$di<$firma->ranga;$di++)
                <i class="fa fa-star yellow"></i>
                @endfor
        </span>
    </h1>
    <span style="border-left:1px solid #eee; padding:0 10px; display: inline-block;">
        Rabat {{$firma->rabat}} {{Config::get('app.percent_symbol')}}
    </span>
</div>
<div class="row dashboard">
    @if($firma->wazne != '')
    <div class="alert alert-danger">
        {{$firma->wazne}}
    </div>
    @endif
    <div class="row">
        <div class="col-md-6">

            <h4 class="form-sector">Firma</h4>
            <div class="category-panel">

                {!! Form::open(['method'=>'PATCH','url' => route("updateCRMFirmy",['id'=>$firma->klientID]),'enctype' =>
                'multipart/form-data','id'=>'FirmaForm','name'=>'FirmaForm','class'=>'form-vertical','style'=>'width:100%'])
                !!}
                @include('backend.CRMFirmy.form')

                {!! Form::close() !!}
                @endif
                @endforeach

            </div>
        </div>

    </div>
    <div class="col-md-6">

        <h4 class="form-sector">Osoby z HR<a href="{{route('createCRMOsoby',['klientID'=>$id])}}"
                class="btn btn-default btn-inline"><i class="fa fa-plus"></i> Nowa osoba</a></h4>

        <div class="category-panel scroll-box">


            @foreach($hr as $h)
            <div class="row panel-list">
                <div class="col-sm-12"><i class="fa fa-user"></i> <a class="label-link"
                        href="{{route('editCRMOsoby',['klientID'=>$klientID,'id'=>$h->uczestnikID])}}">{{$h->imie_nazwisko}}</a>
                </div>
                <div class="col-sm-12" style="word-break:break-word;"><i aria-hidden="true"
                        class="fa fa-envelope-o"></i> {{$h->email}}</div>
                <div class="col-sm-12" style="padding:  10px;margin:  20px 0 0 0;background:  #fafafa;">
                    <div class="row">
                        <div class="col-md-6 text-left">

                            <span data-url="{{route('getZdarzeniaFromUczestnik',['uczestnikID'=>$h->uczestnikID])}}"
                                class="aktywnosci btn btn-default">
                                <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>
                                Aktywnosci
                            </span>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{route('editCRMOsoby',['klientID'=>$id,'id'=>$h->uczestnikID])}}"
                                class="btn btn-default"><i label="Edytuj osobę" class="fa fa-pencil-square-o"></i> </a>

                            <span data-url="{{route('ModalcreateCRMSzkolenie',['klientID'=>$h->klientID])}}"
                                data-element=".form-container" class="modal_open btn btn-default"><i
                                    label="Dodaj szkolenie" class="fa fa-book"></i></span>

                            <span
                                data-url="{{route('createCRMZdarzeniaNew',['id'=>$h->uczestnikID,'method'=>'modal'])}}"
                                data-element=".form-container" class="modal_open btn btn-default"><i
                                    label="Dodaj zdarzenie" class="fa fa-comments"></i></span>

                            {!!
                            Form::open(['method'=>'DELETE','url'=>route('destroyCRMOsoby',['klientID'=>$id,'id'=>$h->uczestnikID]),'style'=>'display:inline;'])
                            !!}
                            <button class="btn gui btn-default">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button> {!! Form::close() !!}

                        </div>
                        <div class="contentAjaxAktywnosci"></div>
                    </div>
                </div>
            </div>
            @endforeach


        </div>
    </div>


    <div class="col-md-6">

        <h4 class="form-sector">Lista osób <a href="{{route('createCRMOsoby',['klientID'=>$id])}}"
                class="btn btn-default btn-inline"><i class="fa fa-plus"></i> Nowa osoba</a></h4>

        <div class="category-panel scroll-box">

            @foreach($osoby as $osoba)
            <div class="row panel-list">
                <div class="col-sm-12"><a class="label-link"
                        href="{{route('editCRMOsoby',['klientID'=>$klientID,'id'=>$osoba->uczestnikID])}}"><i
                            class="fa fa-user"></i> {{$osoba->imie_nazwisko}}</a></div>
                <div class="col-sm-12" style="word-break:break-word;">
                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                    {{$osoba->email}}</div>
                <div class="col-sm-12" style="
    padding:  10px;
    margin:  20px 0 0 0;
    background:  #fafafa;
">
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <span data-url="{{route('getZdarzeniaFromUczestnik',['uczestnikID'=>$osoba->uczestnikID])}}"
                                class="aktywnosci btn btn-default">
                                <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>
                                Aktywnosci
                            </span>

                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{route('editCRMOsoby',['klientID'=>$id,'id'=>$osoba->uczestnikID])}}"
                                class="btn btn-default"><i label="Edytuj osobę" class="fa fa-pencil-square-o"></i></a>
                            <span data-url="{{route('ModalcreateCRMSzkolenie',['klientID'=>$osoba->klientID])}}"
                                data-element=".form-container" class="modal_open btn btn-default"><i
                                    label="Dodaj szkolenie" class="fa fa-book"></i> </span>
                            <span
                                data-url="{{route('createCRMZdarzeniaNew',['id'=>$osoba->uczestnikID,'method'=>'modal'])}}"
                                data-element=".form-container" class="modal_open btn btn-default"><i
                                    label="Dodaj zdarzenie" class="fa fa-comments"></i></span>

                            {!!
                            Form::open(['method'=>'DELETE','url'=>route('destroyCRMOsoby',['klientID'=>$id,'id'=>$osoba->uczestnikID]),'style'=>'display:inline;'])
                            !!}
                            <button class="btn gui btn-default">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button> {!! Form::close() !!}

                            {{-- <a href="{{route('destroyCRMOsoby',['klientID'=>$id,'id'=>$osoba->uczestnikID])}}"
                            class="btn btn-default"><i label="Usuń osobę" class="fa fa-trash-o"></i></a> --}}
                        </div>
                        <div class="contentAjaxAktywnosci"></div>
                    </div>
                </div>
            </div>
            @endforeach



        </div>

    </div>

</div>

{{-- 
<div class="form-group">
    <div class="autocomplete-container">

        {!! Form::label('uczestnikID','Uczestnik',['class'=>'col-sm-12 control-label']) !!}
        @if(Route::CurrentRouteName()
        != 'editCRMZdarzeniaNew')
        {!! Form::text('uczestnikID',null,['required','id'=>'dashboardFindUczestnik','placeholder'=>'Znadź
        osobe','class'=>'col-sm-12 form-control']) !!}

        @else {!! Form::text('uczestnikID',null,['required','id'=>'dashboardFindUczestnik','placeholder'=>'Znadź
        osobe','class'=>'col-sm-12
        form-control']) !!} @endif
    </div>
</div> --}}


<div class="col-sm-12">
    <div class="panel-heading">Oferty
        <div class="dropdown show pull-right" style="color:#fff;">
            @if(Auth::user()->id_role == '2')

            @endif
            <span style="color:#fff;" data-url="{{route('createCRMZdarzeniaNew')}}" data-element=".form-container"
                class="modal_open btn btn-success">
                Tworzenie nowej oferty
            </span>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                {{-- <a class="dropdown-item" href="{{route('createCRMZdarzeniaNew')}}">Tworzenie
                nowej oferty</a> --}}
                <span style="color:#000;" data-url=" {{route('createCRMZdarzeniaNew')}}" data-element=".form-container"
                    class="modal_open dropdown-item">
                    Tworzenie nowej oferty
                </span>

            </div>

        </div>

        {{--<a href="{{route('createCRMFaktury',['klientID'=>$klientID,'szkolenieTermID'=>'0'])}}" class="btn
        btn-success pull-right">Nowa faktura</a>--}}
    </div>

    <table id="tableSort" table-data-sort="1" table-data-sort-type="desc" class="table table-responsive">
        <thead>
            <th>Temat</th>
            <th>Data</th>
            <th>Komentarz</th>
            <th>Kwota</th>
            <th>Stan</th>
            <th>Powiązany</th>
            <th>Priorytet</th>
            <th></th>

        </thead>


        <tbody>
            @foreach($getOferty as $oferta)
            <tr @if($oferta->stan == '2') class="wygrana" @endif @if($oferta->stan == '3') class="odrzucona" @endif
                @if($oferta->stan == '4') class="zrealizowana" @endif>
                <td>
                    {{$oferta->temat}}
                </td>
                <td>
                    {{$oferta->dzialanie_data}}
                </td>
                <td>
                    {{$oferta->dzialanie_komentarz}}
                </td>
                <td>
                    {{$oferta->kwota}}
                </td>
                <td>
                    @if(isset($stany_zdarzenia[$oferta->stan]))
                    {{$stany_zdarzenia[$oferta->stan]}}
                    @else
                    @endif
                   {{-- {{$stany_zdarzenia[$oferta->stan]}} --}}
                </td>
                <td>
                    <span class="fa fa-user" style="border: 1px solid #bfbfbf;
                        border-radius: 50%;
                        padding: 3px 5px;">

                    </span>
                    {{$opiekun[$oferta->opiekunID]}}
                </td>
                <td>
                    @if($oferta->priorytet == '1')
                    <span class="alert-danger" style="padding: 5px;">
                        <span class="fa fa-warning"></span>
                        {{$priorytet[$oferta->priorytet]}}
                        {{-- {{print_r($priorytet)}} --}}
                    </span>
                    @else
                    <span class="alert-info" style="padding: 5px;">
                        <span class="fa fa-warning"></span>
                        {{$priorytet[$oferta->priorytet]}}
                        {{-- {{print_r($priorytet)}} --}}
                    </span>
                    @endif
                </td>
                <td>

                    <div class="col-sm-6" style="padding:0px;">
                        {{-- {!! Form::date('data_wysylki',$faktura->data_wysylki,['class'=>'updateAjax
                        form-control','data-url'=>route('updateAjaxWysylka',['id'=>$faktura->fakturaID])]) !!} --}}
                    </div>
                    <div class="col-sm-3" style="padding:0;">

                        {{-- {!! Form::select('forma', $forma_wysylki ,$faktura->forma,['class'=>'updateAjax
                        form-control','data-url'=>route('updateAjaxWysylka',['id'=>$faktura->fakturaID])]) !!} --}}

                    </div>
                    <div class="col-sm-1" style="padding:0;">
                        {{-- <a
                            href="{{route('editCRMZdarzeniaNew',['id'=>$oferta->zdarzenieID])}}"
                        class="btn btn-default"><i class="fa fa-edit"></i></a>
                        --}}
                        <span data-url="{{route('editCRMZdarzeniaNew',['id'=>$oferta->zdarzenieID])}}"
                            data-element=".form-container" class="modal_open btn btn-default"><i
                                label="Edytuj zdarzenie" class="fa fa-edit"></i></span>
                    </div>





                </td>



            </tr>

            @endforeach
        </tbody>
    </table>

</div>


<div class="col-sm-12">
    <div class="panel-heading">Faktury
        <div class="dropdown show pull-right" style="color:#fff;">
            @if(Auth::user()->id_role == '2')
            <a class="btn btn-success dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Utwórz fakturę
            </a>
            @endif
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item"
                    href="{{route('createCRMFaktury',['klientID'=>$klientID,'uczestnikID'=>'0','szkolenieTermID'=>'0','typ'=>'1'])}}">Faktura
                    VAT</a>
                <a class="dropdown-item"
                    href="{{route('createCRMFaktury',['klientID'=>$klientID,'uczestnikID'=>'0','szkolenieTermID'=>'0','typ'=>'2'])}}">Faktura
                    Proforma</a>
                <a class="dropdown-item"
                    href="{{route('createCRMFaktury',['klientID'=>$klientID,'uczestnikID'=>'0','szkolenieTermID'=>'0','typ'=>'3'])}}">Faktura
                    zaliczkowa</a>
            </div>

        </div>

        {{--<a href="{{route('createCRMFaktury',['klientID'=>$klientID,'szkolenieTermID'=>'0'])}}" class="btn
        btn-success pull-right">Nowa faktura</a>--}}
    </div>
 

    <table id="tableSort" table-data-sort="1" table-data-sort-type="desc" class="table table-responsive table-hover">
        <thead>
            <th>Nr</th>
            <th>Data</th>
            <th>Kurs</th>
            <th>Netto</th>
            <th>Brutto</th>
            <th>Stan</th>

        </thead>


        <tbody>
            @foreach($faktury as $faktura)
            <tr>
                <td>
                    {{$typyFaktury[$faktura->typ]}} / {{$faktura->nr_faktury}}
                </td>
                <td>
                    {{$faktura->data_wystawienia}}
                </td>
                <td>
                    {{$faktura->opis}}
                </td>
                <td>
                    {{$faktura->kwota}}
                </td>
                <td>
                    {{$faktura->kwota_brutto}}
                </td>
                <td class="col-sm-4">


                    {!! Form::open(['method'=>'PATCH','url' => route('updateWysylka',['id'=>$faktura->fakturaID])]) !!}


                    <div class="col-sm-2">

                        @if($faktura->oplacona == '1')
                        <span class="btn btn-success kontener_input">
                            @else
                            <span class="btn btn-danger kontener_input">
                                @endif
                                <i class="fa fa-usd"></i> <span
                                    data-url="{{route('updateAjaxWysylka',['id'=>$faktura->fakturaID])}}"
                                    data-value="{{$faktura->oplacona}}" class="btnUpdateAjax fv_zaplacone btn">
                                    {!! Form::hidden('oplacona',$faktura->oplacona) !!}

                                </span>
                            </span>
                    </div>
                    <div class="col-sm-6" style="padding:0px;">
                        {!! Form::date('data_wysylki',$faktura->data_wysylki,['class'=>'updateAjax
                        form-control','data-url'=>route('updateAjaxWysylka',['id'=>$faktura->fakturaID])]) !!}
                    </div>
                    <div class="col-sm-3" style="padding:0;">

                        {!! Form::select('forma', $forma_wysylki ,$faktura->forma,['class'=>'updateAjax
                        form-control','data-url'=>route('updateAjaxWysylka',['id'=>$faktura->fakturaID])]) !!}

                    </div>
                    <div class="col-sm-1" style="padding:0;"><a
                            href="{{route('editCRMFaktury',['id'=>$faktura->fakturaID])}}" class="btn btn-default"><i
                                class="fa fa-edit"></i></a></div>





                </td>



            </tr>

            @endforeach
        </tbody>
    </table>
    

</div>
<script>

</script>
@endsection