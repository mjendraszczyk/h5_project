@extends('layouts.app') 
@section('title') Firmy
@endsection
 
@section('content')
<div class="row text-right no-margin">
    <a href="{{route('createCRMFirmy')}}" class="btn btn-success"><i class="fa fa-plus"></i> Dodaj</a>
</div>


<div class="row panel-shadow" style="    justify-content: flex-start;"> 
         <div style="margin: 10px 0;">
     <form action="{{route('filtrCRMFirmyPhone')}}" method="post">
<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<input type="text" placeholder="Wyszukaj firmę po nr tel" name="phone" value="{{Session::get('tel')}}" class="form-control" style="min-width:250px;display: inline-block;width:auto;"/>
<input type="text" placeholder="Wyszukaj firmę po nazwie" name="nazwa" value="{{Session::get('nazwa')}}" class="form-control" style="min-width:250px;display: inline-block;width:auto;"/>
<input type="submit" name="filtr" value="Szukaj" class="btn btn-primary"/>
</form>

</div>
<div>
         @foreach($letters as $key => $letter) 
  <a href="{{route('filtrujCRMFirmy',['char'=>$letters[$key]])}}" class="btn btn-default">{{$letters[$key]}}</a> 
         @endforeach
</div>
         </div>

<table id="tableSort" class="table table-responsive table-hover" data-toggle="table" data-sort-name="date" data-sort-order="desc">
    <thead>
        <th>Nazwa</th>
        <th>Liczba pracowników</th>
        <th>Miejscowosc</th>
        <th>Telefon</th>
        <th>Opiekun</th>
        <th></th>
    </thead>
    <tbody>
     

  
            @foreach($firmy as $firma)
            <tr>
                <td>
                    @if($firma->referencje != 0) 
	 <i class="fa fa-star-o large"></i>
 @else 
&nbsp;
    @endif
                    <a class="label-link" href="{{route('editCRMFirmy',['id'=>$firma->klientID])}}">
                       <i class="fa fa-suitcase"></i> {{$firma->nazwa}} </a>
                    <span>
                    @for($di=0;$di<$firma->ranga;$di++)
                    <i class="fa fa-star yellow"></i>
                    @endfor
                    <span class="hidden">{{$firma->ranga}}</span>
                    </span>
                </td>
                <td>
                    <i class="fa fa-users"></i>
 {{App\Http\Controllers\backend\CRMFirmyController::getQtyEmployers($firma->klientID)}}

                </td>
                <td>
                <i class="fa fa-map-marker" aria-hidden="true"></i>
    
                {{$firma->miasto}}
                </td>
                <td>
    
                {{$firma->tel}}
                </td>
  
                <td>
                    
                @if(\app\Http\Controllers\Controller::getOpiekunName($firma->opiekunID) != '') 
                {{\app\Http\Controllers\Controller::getOpiekunName($firma->opiekunID)}}
                @else
                <a href="{{route('przypiszOpiekuna',['klientID' => $firma->klientID])}}" class="btn btn-primary"><i class="fa fa-user"></i> Przypisz do siebie</a>
                @endif
                  
                    {{-- {{$users[$firma->opiekunID]}} --}}
                </td>
                <td class="text-right">
                    <a href="{{route('editCRMFirmy',['id'=>$firma->klientID])}}" class="btn gui btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>{!!
                    Form::open(['method'=>'DELETE','action'=>['backend\CRMFirmyController@destroy',$firma->klientID]])
                    !!}
                    <button class="btn gui btn-default"> 
 <i class="fa fa-times" aria-hidden="true"></i>
</button> {!! Form::close() !!}


                </td>
            </tr>
            @endforeach

    </tbody>
</table>
@endsection