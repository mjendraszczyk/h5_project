<div class="form-container modal-open modal fade bd-example-modal-lg hidden-sm-down" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
    <div class="modal-content">
      
{!! Form::open(['url' => route('storekonsolidujCRMFirmy',['klientID'=>$klientID]),'id'=>'KonsolidujForm','name'=>'KonsolidujForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}
 <span class="alert alert-danger" style="display:block;">  
Pamiętaj, że ta operacja nie może zostać cofnięta. Przeniesione zostaną wszystkie faktury oraz uczestnicy szkoleń.
Podaj identyfikatory klientów, których chcesz przenieść do klienta
</span>
Przenies dane klientów do firmy:
<br/> 
<strong>
    @foreach(App\Http\Controllers\backend\CRMFirmyController::getFirma($klientID) as $firma)
<div class="clearfix"></div>
{{$firma->nazwa}}
    @endforeach
    </strong>

{!! Form::text('konsoliduj_klientID',null,['required','class'=>'form-control']) !!}
<br/>
<div class="text-center">
{!! Form::submit('Wykonaj',['class'=>'btn btn-success']) !!}
</div>
{!! Form::close() !!} 
 
</div>
</div>
</div>