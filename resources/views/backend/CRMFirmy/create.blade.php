@extends('layouts.app')

@section('title')
Dodaj klienta

@endsection

@section('content')
  
 <div class="panel-shadow">

 {!! Form::open(['url' => route("storeCRMFirmy"),'id'=>'FirmaForm','name'=>'FirmaForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.CRMFirmy.form')
 

{!! Form::close() !!} 
</div>
      

@endsection
