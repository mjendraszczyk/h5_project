@if(Session::has('success')) 
<div class="alert alert-success">
    {{Session::get('success')}}
 </div>
@endif

@if(Session::has('errors')) 
<div class="alert alert-danger">
<ul>
 
@foreach(Session::get('errors')->all() as $error)
  <li>{{$error}}</li>
  @endforeach
  </ul>
</div>
@endif
 

<div class="form-group">
    {!! Form::label('nazwa','Nazwa',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFirmy')
    {!! Form::text('nazwa',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('nazwa',$firma->nazwa,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    {!! Form::label('adres','Adres',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFirmy')
    {!! Form::text('adres',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('adres',$firma->adres,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="row">
<div class="col-md-4">
<div class="form-group">
    {!! Form::label('kod','Kod',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFirmy')
    {!! Form::text('kod',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('kod',$firma->kod,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
</div>
<div class="col-md-8">
<div class="form-group">
    {!! Form::label('miasto','Miasto',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFirmy')
    {!! Form::text('miasto',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('miasto',$firma->miasto,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
</div>
</div>

<div class="row"> 
<div class="col-md-6">
<div class="form-group">
    {!! Form::label('nip','NIP',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFirmy')
    {!! Form::text('nip',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('nip',$firma->nip,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
</div>

<div class="col-md-6">
<div class="form-group">
    {!! Form::label('tel','Telefon',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFirmy')
    {!! Form::text('tel',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('tel',$firma->tel,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
</div>
</div>


 
<div class="form-group">
    {!! Form::label('opiekun','Opiekun',['class'=>'col-sm-12 control-label']) !!} 
    
    @if(Route::CurrentRouteName() == 'createCRMFirmy')

    {!! Form::select('opiekun', $opiekun,null, ['class'=>'form-control']) !!}

     @else 
    {!! Form::select('opiekun', $opiekun,$firma->opiekunID, ['class'=>'form-control']) !!}
      
    @endif
</div>


<div class="form-group">
    {!! Form::label('ranga','Ranga',['class'=>'col-sm-12 control-label']) !!} 
    
    <span class="ratings">
    @for($dt=0;$dt<5;$dt++)
    @if(Route::CurrentRouteName() == 'createCRMFirmy')
    <i class="fa fa-star"></i> 
    @else
    @if($dt<$firma->ranga)
    <i class="fa fa-star yellow"></i> 
    @else 
    <i class="fa fa-star"></i> 
    @endif
    @endif
    @endfor
    </span>

    @if(Route::CurrentRouteName() == 'createCRMFirmy')
    {!! Form::hidden('ranga',0,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::hidden('ranga',$firma->ranga,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('branza','Branza',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFirmy')
    {!! Form::select('branza',$branza,null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::select('branza',$branza,$firma->branza,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
<label class="control-label"> 
  @if(Route::CurrentRouteName() == 'createCRMFirmy')
    {!! Form::checkbox('referencje', 1, null, ['class' => 'field']) !!} @else 

    @if($firma->referencje == '1')
    {!! Form::checkbox('referencje', 1, true, ['class' => 'field']) !!}  
    @else 
    {!! Form::checkbox('referencje', 0, false, ['class' => 'field']) !!}  
    @endif
    @endif
    Na stronę referencji
</label>
 
    </div>



<div class="form-group">
    {!! Form::label('rabat','Rabat',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFirmy')
    {!! Form::text('rabat',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('rabat',$firma->rabat,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>


<div class="row">


<div class="col-md-6">
<div class="form-group">
<label class="control-label"> 
  @if(Route::CurrentRouteName() == 'createCRMFirmy')
    {!! Form::checkbox('vip', 1, null, ['class' => 'field']) !!} @else 

    @if($firma->vip == '1')
    {!! Form::checkbox('vip', 1, true, ['class' => 'field']) !!}  
    @else 
    {!! Form::checkbox('vip', 0, false, ['class' => 'field']) !!}  
    @endif
    @endif
    HR VIP
</label>
 
    </div>
    </div>
    <div class="col-md-6">
    <div class="form-group">
<label class="control-label"> 
  @if(Route::CurrentRouteName() == 'createCRMFirmy')
    {!! Form::checkbox('last_minute', 1, null, ['class' => 'field']) !!} @else 

    @if($firma->last_minute == '1')
    {!! Form::checkbox('last_minute', 1, true, ['class' => 'field']) !!}  
    @else 
    {!! Form::checkbox('last_minute', 0, false, ['class' => 'field']) !!}  
    @endif
    @endif
    Last Minute
</label>
 
    </div>
    </div>
    </div>

<div class="form-group">
    {!! Form::label('wazne','Wazne',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createCRMFirmy')
    {!! Form::textarea('wazne',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::textarea('wazne',$firma->wazne,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() == 'createCRMFirmy') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!} @else {!!
        Form::submit('Zapisz', ['class'=>'btn btn-success']) !!} @endif
    </div>