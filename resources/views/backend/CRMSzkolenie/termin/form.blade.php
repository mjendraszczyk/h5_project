<div class="form-container modal-open modal fade bd-example-modal-lg hidden-sm-down" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
    <div class="modal-content text-center" style="float:none;">
      
{!! Form::open(['url' => route("zmienTerminCRMSzkolenie",['crmszkolenieID'=>$crmszkolenieID]),'id'=>'SzkoleniaForm','name'=>'SzkoleniaForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

<h3>Dostępne terminy dla szkolenia</h3>

{!! Form::select('termID',$terminy,$getTermin,['class'=>'form-control']) !!}
 <br/>
 {!! Form::submit('Zmien',['class'=>'btn btn-success']) !!}

{!! Form::close() !!} 
 
</div>
</div>
</div>