@extends('layouts.app')
@section('title') Szkolenie
@foreach($getSzkoleniaTermDetail as $termDetail)
{{$termDetail->training_title}}

@endforeach
@endsection

@section('content')
<div class="category-panel">
  <div class="row no-margin">
    <div class="text-left">
      @foreach($getSzkoleniaTermDetail as $termDetail)
      <span>
        <h2 style="padding:0;margin:0;font-weight:900;"> {{$termDetail->instructor_name}}</h2>
        Data: {{date('d',strtotime($termDetail->term_start))}} - {{date('d-m-Y',strtotime($termDetail->term_end))}}
        <br />
        <div class="col-sm-4" style="padding:0;">
          {!!
          Form::select('term_state',$term_state,$termDetail->term_state,['data-url'=>route('ajaxUpdateSzkolenie',['id'=>$termDetail->termID,'field'=>'term_state']),'class'=>'updateAjax
          form-control','style'=>'width:auto;display:inline-block;']) !!}
          <span style="display:inline-block;" data-element=".form-container"
            data-url="{{route('mailCRMSzkolenie',['termID'=>$termDetail->termID])}}"
            class="modal_open btn btn-default">Mail potwierdzający</span>

        </div>
      </span>
      @endforeach
    </div>
    <div class="text-right">
      <a href="{{route('generateListaObecnosciCRMSzkolenie',['termID'=>$termID])}}" class="btn btn-success"><i
          class="fa fa-file-excel-o" aria-hidden="true"></i>
        Lista obecnoci</a>

      <a href="{{route('generateKartaTreneraCRMSzkolenie',['termID'=>$termID])}}" class="btn btn-default"><i
          class="fa fa-file-excel-o" aria-hidden="true"></i>
        Karta trenera</a>
    </div>
  </div>
</div>


<div class="category-panel">
  <table class="table table-responsive table-hover" data-toggle="table" data-sort-name="date" data-sort-order="desc">
    <thead>
      <th>Uczestnik</th>
      <th>Firma</th>
      <th>Stanowisko</th>
      <th>Termin</th>
      <th>Komentarz</th>
      <th>Status</th>
    </thead>
    <tbody>



      @foreach($getSzkoleniaTermUsers as $termUsers)
      <tr>
        <td class="col-sm-2"><a class="label-link"
            href="{{route('editCRMOsoby',['klientID'=>$termUsers->klientID,'id'=>$termUsers->uczestnikID])}}"><i
              class="fa fa-user"></i> {{$termUsers->imie_nazwisko}}</a>
          Z:
          @if($termUsers->parentID == '0')
          <a
            href="mailto:{{(App\Http\Controllers\backend\CRMSzkolenieController::getMailOZ($termUsers->uczestnikID,1))}}">
            {{json_decode(App\Http\Controllers\backend\CRMOsobyController::getUczestnikName($termUsers->uczestnikID))[0]->imie_nazwisko}}
          </a>
          {{--         @elseif(($termUsers->userID != '') && ($termUsers->parentID != ''))
pracownik (H5)
 @foreach(App\Http\Controllers\backend\UzytkownicyController::getUzytkownik($termUsers->userID) as $opiekunName) 
            {{$opiekunName->name}}

          @endforeach
          --}}
          @elseif($termUsers->parentID == '')

          Dodany przez moderatora

          @else
          <a href="mailto:{{(App\Http\Controllers\backend\CRMSzkolenieController::getMailOZ($termUsers->parentID,1))}}">
            {{json_decode(App\Http\Controllers\backend\CRMOsobyController::getUczestnikName($termUsers->parentID))[0]->imie_nazwisko}}
          </a>
          @endif

        </td>
        <td class="col-sm-1"><a class="label-link" href="{{route('editCRMFirmy',['id'=>$termUsers->klientID])}}"><i
              class="fa fa-suitcase"></i> {{$termUsers->nazwa}}</a></td>
        <td class="col-sm-1">{{$termUsers->stanowisko}}</td>
        <td class="col-sm-1">{{$termUsers->data_zgloszenia}}</td>
        <td class="col-sm-1">
          {!!
          Form::textarea('komentarz',$termUsers->komentarz,['data-url'=>route('ajaxUpdateSzkolenie',['id'=>$termUsers->crmszkolenieID,'field'=>'komentarz']),'class'=>'updateAjax
          form-control','rows'=>'2','style'=>'min-height:75px;height:75px;width:250px']) !!}

        </td>
        <td class="col-sm-6 text-right">

          @if(app\Http\Controllers\backend\CRMSzkolenieController::checkIfHaveZaliczka($termUsers->uczestnikID,$termUsers->termID)
          > 0 )
          <i class="fa fa-file-o" aria-hidden="true"></i>
          <span style="color:red;">
            <i class="fa fa-exclamation" aria-hidden="true"></i>
          </span>

          @else
          @endif



          {!! Form::select('stan',$term_state,$termUsers->stan,['class'=>'updateAjax
          form-control','data-url'=>route('ajaxUpdateSzkolenie',['id'=>$termUsers->crmszkolenieID,'field'=>'stan']),'style'=>'width:auto;display:inline-block;'])
          !!}
          {{-- <a href="#" class="btn btn-info">Ankieta</a> --}}
          <a href="{{route('createCRMAnkieta',['klientID'=>$termUsers->klientID,'uczestnikID'=>$termUsers->uczestnikID,'termID'=>$termID])}}"
            class="btn btn-info"><i class="fa fa-list" aria-hidden="true"></i></a>
          {!!
          Form::open(['method'=>'DELETE','action'=>['backend\CRMSzkolenieController@destroyRezerwacja',$termUsers->crmszkolenieID],'style'=>'display:inline-block;'])
          !!}
          <button class="btn gui btn-default">
            <i class="fa fa-trash" aria-hidden="true"></i>
          </button> {!! Form::close() !!}
          <span data-url="{{route('zmienTerminModal',['uczestnikID'=>$termUsers->crmszkolenieID])}}"
            data-element=".form-container" class="modal_open btn btn-default">Inny termin</span>


          <div class="dropdown show">
            @if(Auth::user()->id_role == '2')
            <a class="btn btn-success dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
              data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Faktura
            </a>

            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
              <a class="dropdown-item"
                href="{{route('createCRMFaktury',['klientID'=>$termUsers->klientID,'uczestnikID'=>$termUsers->uczestnikID,'szkolenieTermID'=>$termDetail->termID,'typ'=>'1'])}}">Faktura
                VAT</a>
              <a class="dropdown-item"
                href="{{route('createCRMFaktury',['klientID'=>$termUsers->klientID,'uczestnikID'=>$termUsers->uczestnikID,'szkolenieTermID'=>$termDetail->termID,'typ'=>'2'])}}">Faktura
                Proforma</a>
              <a class="dropdown-item"
                href="{{route('createCRMFaktury',['klientID'=>$termUsers->klientID,'uczestnikID'=>$termUsers->uczestnikID,'szkolenieTermID'=>$termDetail->termID,'typ'=>'3'])}}">Faktura
                zaliczkowa</a>
            </div>
            @endif
          </div>

          {{-- <a href="{{route('createCRMFaktury',['klientID'=>$termUsers->klientID,'szkolenieTermID'=>$termDetail->termID])}}"
          class="btn btn-success"><i class="fa fa-plus"></i> Faktura</a> --}}


        </td>
      </tr>
      @endforeach

    </tbody>
  </table>
</div>
@endsection