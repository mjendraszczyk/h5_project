@if(Session::has('success'))
<div class="alert alert-success">
    {{Session::get('success')}}
</div>
@endif

@if(Session::has('errors'))
<div class="alert alert-danger">
    <ul>
        <!-- {{Session::get('errors')}} -->
        @foreach(Session::get('errors')->all() as $error)

        <!-- {{ Session::get('errors')}}  -->
        <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="form-group">
    <div class="autocomplete-container">
        {!! Form::label('trainingID','Szkolenie',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName()
        != 'editCRMSzkolenia')
        {!! Form::select('trainingID',$trainingID,null,['required','class'=>'col-sm-12 form-control']) !!}

        @else
        {!! Form::select('trainingID',$trainingID,null,['class'=>'col-sm-12 form-control']) !!}
        @endif
    </div>
</div>

<div class="form-group">
    <div class="autocomplete-container">
        {!! Form::label('termID','Termin',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() !=
        'editCRMSzkolenia')
        {!! Form::select('termID',$terminy,null,['required','id'=>'termin','class'=>'col-sm-12 form-control']) !!}

        @else
        {!! Form::select('termID',$terminy,null,['required','id'=>'termin','class'=>'col-sm-12 form-control']) !!}
        @endif
    </div>
</div>


<div class="form-group">
    {!! Form::label('komentarz','Komentarz',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() !=
    'editCRMSzkolenia')
    {!! Form::textarea('komentarz',null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::textarea('komentarz',null,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('stan','Stan',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() !=
    'editCRMSzkolenia')
    {!! Form::select('stan',$stan,null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::select('stan',$stan,null,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    {!! Form::label('parentID','Zgłaszający',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() !=
    'editCRMSzkolenia')
    {!! Form::select('parentID',$parentID,null,['class'=>'col-sm-12 form-control']) !!} @else {!!
    Form::select('parentID',$parentID,null,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('uczestnikID','Uczestnik',['class'=>'col-sm-12 control-label']) !!}

    @if(Route::CurrentRouteName() != 'editCRMSzkolenia')

    {!! Form::select('uczestnikID',$parentID,null,['class'=>'col-sm-12 form-control']) !!}

    @else
    {!! Form::select('uczestnikID',$parentID,null,['class'=>'col-sm-12 form-control']) !!}

    @endif
</div>


<div class="form-group">
    {!! Form::hidden('priorytet','0',['class'=>'col-sm-12 form-control']) !!}
    {!! Form::hidden('opiekunID',Auth::user()->id,['class'=>'col-sm-12 form-control']) !!}
</div>

<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() != 'editCRMSzkolenia') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!}
        @else {!!
        Form::submit('Zapisz', ['class'=>'btn btn-success']) !!} @endif
    </div>
</div>