 
 @if(Session::has('success')) 
<div class="alert alert-success">
    {{Session::get('success')}}
 </div>
@endif

@if(Session::has('errors')) 
<div class="alert alert-danger">
<ul>

@foreach(Session::get('errors')->all() as $error)


  <li>{{$error}}</li>
  @endforeach
  </ul>
</div>
@endif
<h3>Zainteresowanie szkoleniami</h3>
<h6>Wybierz interesujące Cię szkolenia</h6>
<div class="row">
<div class="col-md-12">
 <div class="form-group">
 

{!! Form::select('duallistbox_demo1[]',$getSzkolenia,null,['id'=>'bootstrap-duallistbox-nonselected-list_duallistbox_demo1[]','multiple'=>'multiple','style'=>'min-height:300px;','class'=>'select_multiselect col-sm-12 form-control']) !!} 
{!! Form::hidden('klientID',$klientID) !!}  
{!! Form::hidden('uczestnikID',$uczestnikID) !!}
</div>

</div>
 
</div>

<div class="form-group">
<div class="autocomplete-container">
    {!! Form::label('uwagi','Uwagi',['class'=>'col-sm-12 control-label']) !!}  
    {!! Form::textarea('uwagi',null,['id'=>'uwagi','class'=>'col-sm-12 form-control']) !!} 

     
</div>
</div>
  

<div class="form-group">
    {!! Form::hidden('uczestnikID',$uczestnikID,['class'=>'col-sm-12 form-control']) !!} 
    <div class="col-sm-12 text-right save-row">
    <a href="{{redirect()->back()->getTargetUrl()}}" class="btn btn-default">Powrót</a>
          {!! Form::submit('Zapisz', ['class'=>'btn btn-success']) !!}  
    </div>
    </div>