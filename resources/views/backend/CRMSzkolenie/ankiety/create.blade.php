@extends('layouts.app')

@section('title')
Dodaj ankiete dla 
@foreach(App\Http\Controllers\backend\CRMOsobyController::getUczestnikName($uczestnikID) as $uczestnik)
{{$uczestnik->imie_nazwisko}}
@endforeach
@endsection

@section('content')
  
 <div class="panel-shadow">
      
{!! Form::open(['url' => route("storeCRMAnkieta",['klientID'=>$klientID,'uczestnikID'=>$uczestnikID,'termID'=>$termID]),'id'=>'AnkietaForm','name'=>'AnkietaForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.CRMSzkolenie.ankiety.form')
 

{!! Form::close() !!} 
 
</div>
       

@endsection