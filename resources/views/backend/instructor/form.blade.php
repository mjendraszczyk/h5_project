@if(Session::has('success')) 
<div class="alert alert-success">
    {{Session::get('success')}}
 </div>
@endif

@if(Session::has('errors')) 
<div class="alert alert-danger">
<ul>
<!-- {{Session::get('errors')}} -->
@foreach(Session::get('errors')->all() as $error)

  <!-- {{ Session::get('errors')}}  -->
  <li>{{$error}}</li>
  @endforeach
  </ul>
</div>
@endif

 

<div class="form-group">
    {!! Form::label('instructor_name','Imię i nazwisko',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createInstructor')
    {!! Form::text('instructor_name',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('instructor_name',$inst->instructor_name,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
  
  <div class="form-group">
    {!! Form::label('instructor_mail','E-mail',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createInstructor')
    {!! Form::text('instructor_mail',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('instructor_mail',$inst->instructor_mail,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
  
  <div class="form-group">
    {!! Form::label('instructor_phone','Telefon',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createInstructor')
    {!! Form::text('instructor_phone',null,['class'=>'col-sm-12 form-control']) !!} @else {!! Form::text('instructor_phone',$inst->instructor_phone,['class'=>'col-sm-12
    form-control']) !!} @endif
</div>
  
<div class="form-group">
    {!! Form::label('instructor_lid','Wstęp',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createInstructor')
    {!! Form::textarea('instructor_lid',null,['class'=>'editor col-sm-12 form-control']) !!} @else {!! Form::textarea('instructor_lid',$inst->instructor_lid,['class'=>'editor col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    {!! Form::label('instructor_desc','Opis',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createInstructor')
    {!! Form::textarea('instructor_desc',null,['class'=>'editor col-sm-12 form-control']) !!} @else {!! Form::textarea('instructor_desc',$inst->instructor_desc,['class'=>'editor col-sm-12
    form-control']) !!} @endif
</div>


<div class="form-group">
    {!! Form::label('instructor_refs','Referencje',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createInstructor')
    {!! Form::textarea('instructor_refs',null,['class'=>'editor col-sm-12 form-control']) !!} @else {!! Form::textarea('instructor_refs',$inst->instructor_refs,['class'=>'editor col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('instructor_scope','Zakres',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createInstructor')
    {!! Form::textarea('instructor_scope',null,['class'=>'editor col-sm-12 form-control']) !!} @else {!! Form::textarea('instructor_scope',$inst->instructor_scope,['class'=>'editor col-sm-12
    form-control']) !!} @endif
</div>

<div class="form-group">
    {!! Form::label('instructor_photo','Zdjęcie',['class'=>'col-sm-12 control-label']) !!} @if(Route::CurrentRouteName() == 'createInstructor')
    @else
    <img src="{{Config('url')}}/img/frontend/trainers/big/{{$inst->instructor_photo}}" alt="" class="thumbnail" /> @endif {!! Form::file('instructor_photo')
    !!}
</div>


{{--<div class="form-group">
    @if(Route::CurrentRouteName() == 'createInstructor')
    {!! Form::checkbox('instructor_accepted','1',null,['class'=>'col-sm-12 form-control']) !!} @else 
    
    @if($inst->instructor_accepted == '1') 
    {!! Form::checkbox('instructor_accepted',$inst->instructor_accepted,true,['class'=>'col-sm-12 form-control']) !!} 
    @else 
{!! Form::checkbox('instructor_accepted',$inst->instructor_accepted,false,['class'=>'col-sm-12 form-control']) !!} 
    @endif

    @endif
         {!! Form::label('instructor_accepted','Na stronę',['class'=>'control-label']) !!}
</div> --}}



<div class="form-group">

<label class="control-label"> 
  @if(Route::CurrentRouteName() == 'createInstructor')
    {!! Form::checkbox('instructor_accepted', 1, null, ['class' => 'field']) !!} @else 

    @if($inst->instructor_accepted == '1')
    {!! Form::checkbox('instructor_accepted', 1, true, ['class' => 'field']) !!}  
    @else 
    {!! Form::checkbox('instructor_accepted', 0, false, ['class' => 'field']) !!}  
    @endif
    @endif
    Aktywna
</label>
 
    </div>



<div class="form-group">
    <div class="col-sm-12 text-right save-row">
        @if(Route::CurrentRouteName() == 'createInstructor') {!! Form::submit('Dodaj', ['class'=>'btn btn-success']) !!} @else {!!
        Form::submit('Zapisz', ['class'=>'btn btn-success']) !!} @endif
    </div>