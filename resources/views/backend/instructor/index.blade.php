@extends('layouts.app')

@section('title')
Trenerzy
@endsection

@section('content')
 <div class="row text-right no-margin">
    <a href="{{route('createInstructor')}}" class="btn btn-success"><i class="fa fa-plus"></i> Dodaj</a>
</div>
 

<table id="tableSort" class="table table-responsive table-hover">
 <thead>
        <th>ID</th>
        <th>Zdjęcie</th>
<th>Imię i nazwisko</th>
<th>Aktywny</th>

<th></th>
    </thead>
 
 
<tbody>
    @foreach($instructor as $inst)
<tr>
<td>
{{$inst->instructorID}}
</td>
<td>
<img src="{{Config('url')}}/img/frontend/trainers/big/{{$inst->instructor_photo}}" alt="" style="max-width:64px;height:64px;" class="thumbnail"/>
</td>
<td>
{{$inst->instructor_name}}
</td>
<td>
@if($inst->instructor_accepted == 1)
<i class="fa fa-check"></i>
@else
<i class="fa fa-close"></i>
@endif
</td>

<td class="text-right">
                    <a href="{{route('editInstructor',['id'=>$inst->instructorID])}}" class="btn gui btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    {!!
                    Form::open(['method'=>'DELETE','action'=>['backend\InstructorController@destroy',$inst->instructorID]])
                    !!}
                    <button class="btn gui btn-default"> 
 <i class="fa fa-times" aria-hidden="true"></i>
</button> {!! Form::close() !!}


                </td>
</tr>
@endforeach
    </tbody>
    </table>


@endsection
