@extends('layouts.app')

@section('title')
Dodaj trenera
@endsection

@section('content')
  
 <div class="panel-shadow">

 {!! Form::open(['url' => route("storeInstructor"),'id'=>'InstructorForm','name'=>'InstructorForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}

@include('backend.instructor.form')
 

{!! Form::close() !!} 
</div>
      

@endsection
