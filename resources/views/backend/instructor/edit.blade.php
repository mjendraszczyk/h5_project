@extends('layouts.app') 
@section('title') Edycja trenera
@endsection
 
@section('content')

<div class="panel-shadow">
    @foreach($instructor as $inst) {!! Form::open(['method'=>'PATCH','url' => route("updateInstructor",['id'=>$inst->instructorID]),'enctype'
    => 'multipart/form-data','id'=>'InstructorForm','name'=>'InstructorForm','class'=>'form-vertical','style'=>'width:100%;'])
    !!}
    @include('backend.instructor.form') {!! Form::close() !!} @endforeach
</div>
@endsection