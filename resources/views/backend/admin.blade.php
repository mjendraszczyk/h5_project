@extends('layouts.app')

@section('title')
@if(Route::CurrentRouteName() == 'indexInstructor')
Trenerzy
@else
Panel administratora
@endif
@endsection

@section('content')
<div class="row dashboard">

    <div class="row">
        <div class="col-md-12">

            <div class="panel-header">
                Zbliające się szkolenia
            </div>
            <div class="category-panel">
                <div class="col-md-12">
                    <div class="row" style="padding:10px;border-bottom:1px solid #eee;">
                        {!! Form::open(['url' => route('filtr_dash_miasto'),'enctype' =>
                        'multipart/form-data','id'=>'TerminyForm','name'=>'TerminyForm','class'=>'form-vertical','style'=>'width:100%'])
                        !!}
                        <div class="col-md-6" style="padding:0;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="submit" name="filtruj" value="prev" class="btn btn-default">
                                        <</button> <button type="submit" name="filtruj" value="next"
                                            class="btn btn-default">>
                                    </button>

                                </div>
                                {!! Form::select('filtruj_szkolenia',$miasto, 2, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-6" style="padding:0;">
                            <div class="col-md-5">

                                {!! Form::date('term_start', date( "Y-m-d", strtotime( "-7 days" ) ),
                                ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-md-5">

                                {!! Form::date('term_end', date( "Y-m-d", strtotime( "+7 days" ) ),
                                ['class'=>'form-control']) !!}

                            </div>

                            <div class="col-md-2">
                                {{-- {!! Form::submit('Filtr',['class'=>'btn btn-success']) !!} --}}
                                <button name="filtruj" value="filtr" class="btn btn-success">Filtr</button>
                            </div>

                            {!! Form::close() !!}
                        </div>


                    </div>


                </div>

                <div class="col-md-12">

                    @if(count($lastTerminy)>5)
                    <div style="overflow:hidden;height:350px;">
                        @endif

                        <table class="table table-responsive">
                            <thead>
                                <th>Termin</th>
                                <th>Nazwa</th>
                                <th>Miejsce</th>
                                <th>Typ</th>
                            </thead>
                            <tbody>
                                @foreach($lastTerminy as $KTermin => $termin)
                                <tr>
                                    <td class="col-md-2">
                                        {{date('d',strtotime($termin->term_start))}} -
                                        {{date('d-m-Y',strtotime($termin->term_end))}}
                                    </td>
                                    <td class="col-md-5">
                                        <a class="label-link"
                                            href="{{route('showCRMSzkolenie',['termID'=>$termin->termID])}}"> <i
                                                class="fa fa-graduation-cap"></i> {{$termin->training_title}} (
                                            {{App\Http\Controllers\backend\DashboardController::getQtyUczestnicy($termin->termID)}}
                                            )</a>
                                    </td>
                                    <td class="col-md-3">
                                        {{$termin->term_place}}
                                    </td>
                                    <td class="col-md-2">
                                        {{$term_state[$termin->term_type]}}
                                    </td>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                        @if(count($lastTerminy)>5)

                    </div>
                    <div class="text-center">
                        <div class="readmore btn btn-success">Więcej</div>
                    </div>
                    @endif
                </div>
            </div>
        </div>


    </div>





    <div class="row">

        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif

        @if(Route::CurrentRouteName() == 'indexInstructor')
        <table class="table table-responsive table-hover">
            <thead>
                <th>ID</th>
                <th>Imię i nazwisko</th>
                <th>e-mail</th>
                <th>telefon</th>
                <th></th>
            </thead>
            <tbody>
                @foreach($instructor as $inst)
                <tr>
                    <td>
                        {{$inst->instructorID}}
                    </td>
                    <td>
                        {{$inst->instructor_name}}
                    </td>
                    <td>
                        {!!$inst->instructor_fakemail!!}
                    </td>
                    <td>
                        {{$inst->instructor_phone}}
                    </td>
                    <td>
                        <a href="{{route('editInstructor',['id'=>$inst->instructorID])}}" class="btn gui btn-default"><i
                                class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        {!!
                        Form::open(['method'=>'DELETE','action'=>['backend\InstructorController@destroy',$inst->instructorID]])
                        !!}
                        <button class="btn gui btn-default">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button> {!! Form::close() !!}


                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else

        <div class="col-md-6">
            <div>
                <div class="panel-header">
                    Dodaj zdarzenie
                </div>
                <div class="category-panel">
                    {!!App\Http\Controllers\backend\CRMZdarzeniaController::createNew('createForm')!!}
                </div>
            </div>

            <div>

                <div class="panel-header">
                    Ostatnie zdarzenia
                </div>


                <div class="category-panel">
                    <ul class="list">
                        @foreach($lastZdarzenia as $zdarzenie)
                        <li>
                            <i class="fa fa-clock-o"></i> {{$zdarzenie->data_dodania}} <a class="label-link"
                                href="{{route('editCRMOsoby',['klientID'=>$zdarzenie->klientID,'id'=>$zdarzenie->uczestnikID])}}"><i
                                    class="fa fa-user"></i> {{$zdarzenie->imie_nazwisko}}</a>
                            z firmy <a class="label-link"
                                href="{{route('editCRMFirmy',['klientID'=>$zdarzenie->klientID])}}"><i
                                    class="fa fa-suitcase"></i> {{$zdarzenie->nazwa}}</a>

                            <p class="kontakt_box">
                                {{$zdarzenie->komentarz}}
                            </p>

                        </li>

                        @endforeach
                        <li class="text-center">
                            <a class="btn btn-success" href="{{route('indexCRMZdarzeniaNew')}}">Zobacz wszystkie
                                zdarzenia</a>
                        </li>
                    </ul>
                </div>

            </div>
            <div>

            </div>




        </div>

        <div class="col-md-6">
            <div>
                <div class="panel-header">
                    Znajdź firmę
                </div>
                <div class="category-panel">
                    {!! Form::open(['url' => route('findCRMFirmy'),'enctype' =>
                    'multipart/form-data','id'=>'FirmaForm','name'=>'FirmaForm','class'=>'form-vertical','style'=>'width:100%'])
                    !!}
                    <div class="input-group">
                        {!! Form::text('znajdz_firme','',['placeholder'=>'Wprowadź nazwę
                        firmy','class'=>'form-control']) !!}
                        <div class="input-group-btn">
                            {!! Form::submit('Szukaj',['class'=>'btn btn-default']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}



                </div>

            </div>

            <div class="panel-header">
                Ostatnie rezerwacje
            </div>

            <div class="category-panel">

                <ul class="list">

                    @foreach($lastRezerwacje as $rezerwacja)
                    <li>
                        <span class="fa fa-clock-o"></span> {{$rezerwacja->ostatni_kontakt}}
                        <a class="label-link"
                            href="{{route('editCRMOsoby',['klientID'=>$rezerwacja->klientID,'id'=>$rezerwacja->uczestnikID])}}">
                            <span class="fa fa-user"></span> {{$rezerwacja->imie_nazwisko}}
                        </a>
                        <br />
                        <h3> {{$rezerwacja->training_title}} </h3>
                        <span class="mark alert-success price">{{number_format($rezerwacja->training_price,2,'.',' ')}}
                            {{Config::get('app.currency')}}</span>
                        <p class="kontakt_box">
                            <span class="fa fa-phone"></span>
                            @if($rezerwacja->user_phone != null)
                            {{$rezerwacja->user_phone}}
                            @else
                            @if(@$getContactPhone[$rezerwacja->parentID] == null)
                            Brak informacji
                            @else
                            {{$getContactPhone[$rezerwacja->parentID]}}
                            @endif
                            @endif
                            <span class="fa fa-envelope-o
                              "></span>
                            @if($rezerwacja->email != null)
                            {{$rezerwacja->email}}
                            @else
                            <span class="mark alert-danger">HR</span>
                            {{@$getContactMail[$rezerwacja->parentID]}}
                            @endif
                        </p>
                    </li>

                    @endforeach
                </ul>
            </div>

        </div>
        @endif


    </div>

</div>


@endsection