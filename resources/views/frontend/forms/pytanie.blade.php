{!! Form::open(['url' => route("szkolenia_mail"),'id'=>'formularzKurs','name'=>'form1','class'=>'form-horizontal']) !!}
<div class="form-group">
    {!! Form::label('tresc','Pytanie',['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!!
        Form::textarea('tresc',null,['id'=>'tresc','class'=>'form-control','cols'=>'40','rows'=>'5','style'=>'resize:none;','required'])
        !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('name','Imię',['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('name', null, ['class' => 'form-control','size'=>'40','id'=>'name','required']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('email','e-mail',['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('email', null, ['class' => 'form-control','size'=>'40','id'=>'email','required']) !!}
        {!! Form::hidden('contactUrl', Request::url(), ['class' =>
        'form-control','size'=>'40','id'=>'contactUrl','required']) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-sm-2">
    </div>
    <div class="col-sm-10">
        <label>
            {!! Form::checkbox('agree_kontakt', 1, false, ['class' => '','required','size'=>'40','id'=>'name']) !!}
            <small>
                * Wyrażam zgodę na przetwarzanie podanych w formularzu danych osobowych w zakresie niezbędnym do
                udzielenia
                odpowiedzi
                na zapytanie, zgodnie z ustawą z dnia 18 lipca 2002 r. o świadczeniu usług drogą elektroniczną oraz
                ustawą z
                dnia 29
                sierpnia 1997 r. o ochronie danych osobowych.
                <br /><br />
                Administratorem danych jest HIGH5 Group Sp. z o. o. z siedzibą w Warszawie przy ul. Wroniej 45 lok.129,
                biuro@high5.pl,
                tel. 22-824-50-25.
                <br /><br />
                Informujemy, że mają Państwo prawo dostępu do swoich danych oraz możliwość ich poprawiania,
                sprostowania,
                usunięcia
                lub
                ograniczenia przetwarzania, prawo do wniesienia sprzeciwu wobec przetwarzania, a także o prawo do
                przenoszenia
                danych.
                Podanie danych ma charakter dobrowolny, jest jednak niezbędne do udzielenia odpowiedzi. Dane osobowe
                będą
                przechowywane
                przez okres niezbędny do udzielenia odpowiedzi na zapytanie.
            </small>
        </label>
    </div>
</div>
<div class="modal-footer">


    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            {!! Form::button('Wyślij', ['id'=>'action','class'=>'btn btn-aktywny']) !!}
        </div>
    </div>
</div>
</form>
{!! Form::close() !!}