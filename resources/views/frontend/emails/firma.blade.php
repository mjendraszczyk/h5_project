<table style="text-align:  center;width:  100%;background: #101010;padding:  25px;">
    <tbody>
        <tr>
        <td style="color:#fff;">
        <img src="{{asset('img/frontend/high5l.png')}}" style="width: 150px;" alt="{{config('app.name')}}">    
        </td>    
        </tr>
    </tbody>
</table>
<table style="width:100%;text-align:center;" cellpadding="10">
<tbody>
    <tr>
        <td>
                <h3 style="display:  table;padding:  0 0 10px;margin:  auto;font-size: 20px;margin: 20px auto;border-bottom: 2px solid #ea3f33;"> 
                        Klient zmienił dane podczas składania rezerwacji
                    </h3> 
        </td>
    </tr>
    
</tbody>
</table>
<h3 style="font-weight:bold;font-family:arial;font-size:20px;">
  Obecne dane klienta
</h3>
<table style="margin:  auto;padding: 15px;width:  100%;text-align:  center;border:1px solid #eee;">
    <tbody><tr style="text-align:  center;">
<td><table style="width:  100%;" cellpadding="8" cellspacing="10">
    <tbody>
        <tr>
            <td style="border:  1px solid #eee;font-weight: 700;">
                Nazwa firmy
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                NIP
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                Adres
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                Kod pocztowy
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                Miasto
            </td>
        </tr>
             <tr>


            <td>
            {{$OldNazwaFirmy}}
            </td>
            <td>
            {{$OldNipFirmy}}
            </td>
            <td>
            {{$OldAdresFirmy}}
        </td>
            <td>
            {{$OldKodFirmy}}
        </td>
            <td>
            {{$OldMiastoFirmy}}
        </td>
        </tr>
</tbody>
</table>

 
    </td></tr>

</tbody></table>


<h3 style="font-weight:bold;font-family:arial;font-size:20px;">Dane podane przez klienta</h3>

<table style="margin:  auto;padding: 15px;width:  100%;text-align:  center;border:1px solid #eee;">
    <tbody><tr style="text-align:  center;">
<td><table style="width:  100%;" cellpadding="8" cellspacing="10">
    <tbody>
        <tr>
            <td style="border:  1px solid #eee;font-weight: 700;">
                Nazwa firmy
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                NIP
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                Adres
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                Kod pocztowy
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                Miasto
            </td>
        </tr>
           <tr>


            <td>
            {{$NewNazwaFirmy}}
            </td>
            <td>
            {{$NewNipFirmy}}
            </td>
            <td>
            {{$NewAdresFirmy}}
        </td>
            <td>
            {{$NewKodFirmy}}
        </td>
            <td>
            {{$NewMiastoFirmy}}
        </td>
        </tr>
</tbody>
</table>

 
    </td></tr>

</tbody></table>


<h3 style="font-weight:bold;font-family:arial;font-size:20px;">Dane pobrane z GUS</h3>

<table style="margin:  auto;padding: 15px;width:  100%;text-align:  center;border:1px solid #eee;">
    <tbody><tr style="text-align:  center;">
<td><table style="width:  100%;" cellpadding="8" cellspacing="10">
    <tbody>
        <tr>
            <td style="border:  1px solid #eee;font-weight: 700;">
                Nazwa firmy
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                NIP
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                Adres
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                Kod pocztowy
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                Miasto
            </td>
        </tr>
        <tr>


            <td>
            {{$GusNazwaFirmy}}
            </td>
            <td>
            {{$GusNipFirmy}}
            </td>
            <td>
            {{$GusAdresFirmy}}
            </td>
            <td>
            {{$GusKodFirmy}}
            </td>
            <td>
            {{$GusMiastoFirmy}}
        </td>
        </tr>
</tbody>
</table>

 
    </td></tr>

</tbody></table>
 

<table style="width:  100%;text-align:  center;background: #f9f9f9;padding:  15px;">
<tbody>
<tr>
    <td style="color: #b3b3b3;">copyright &copy; {{date('Y')}} {{config('app.name')}}</td>
</tr>
</tbody>
</table>