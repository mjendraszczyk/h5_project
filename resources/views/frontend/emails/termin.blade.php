<table style="text-align:  center;width:  100%;background: #101010;padding:  25px;">
    <tbody>
        <tr>
        <td style="color:#fff;">
        <img src="{{asset('img/frontend/high5l.png')}}" style="width: 150px;" alt="{{config('app.name')}}">    
        </td>    
        </tr>
    </tbody>
</table>
<table style="width:100%;text-align:center;" cellpadding="10">
<tbody>
    <tr>
        <td>
                <h3 style="display:  table;padding:  0 0 10px;margin:  auto;font-size: 20px;margin: 20px auto;border-bottom: 2px solid #ea3f33;"> 
                        @if(Route::currentRouteName() == 'zapisz_terminy')
                       Rezerwacja szkolenia High5 | Szkolenie
                        @else
                        Rezerwacja programu High5 | Program rozwojowy
                        @endif
                    </h3> 
        </td>
    </tr>
    
</tbody>
</table>
<h3 style="font-weight:bold;font-family:arial;font-size:20px;">
      @if(Route::currentRouteName() == 'zapisz_terminy')
                Szkolenie
                @else
                Program
                @endif
</h3>
<table style="margin:  auto;padding: 15px;width:  100%;text-align:  center;border:1px solid #eee;">
    <tbody><tr style="text-align:  center;">
<td><table style="width:  100%;" cellpadding="8" cellspacing="10">
    <tbody>
        <tr>
            <td style="border:  1px solid #eee;font-weight: 700;">
                @if(Route::currentRouteName() == 'zapisz_terminy')
                Szkolenie
                @else
                Program
                @endif
            </td>
            @if(Route::currentRouteName() == 'zapisz_terminy')
            <td style="border: 1px solid #eee;font-weight:  700;">
                Termin
            </td>
            @endif
            <td style="border: 1px solid #eee;font-weight:  700;">
                Cena
            </td>
        </tr>
        <tr>
            <td>
                    {{$terminSzkolenie}}
            </td>
            @if(Route::currentRouteName() == 'zapisz_terminy')
            <td>
                    
@if($terminSesja == null) 
{{$terminTermin}}
@else 
{!! $terminSesja!!}
@endif
       
 
 
            </td>
            @endif
            <td>
                {{$terminCena}}
        </td>
        </tr>
</tbody>
</table>
<table style="width:  100%;" cellpadding="8" cellspacing="10">
    <tbody>
        <tr>
            <td style="font-weight:  700;border: 1px solid #eee;">
                Uwagi
            </td>
        </tr>
        <tr>
            <td style="padding:  20px;">
                    {{$terminUwagi}}
            </td>
        </tr>
    </tbody>
</table>
    </td></tr>

</tbody></table>

<h3 style="font-weight:bold;font-family:arial;font-size:20px;">Osoba zgłaszająca</h3>

<table style="width:100%;padding:15px;border:1px solid #eee;" cellpadding="8" cellspacing="10">
    <tbody>
        <tr>
            <td style="width: 50%;border:  1px solid #eee;font-weight: 700;">
                Imię
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                E-mail
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                Telefon
            </td>
            
        </tr>
        <tr>
            <td>
                    {{$terminOzImie}}
            </td>
            <td>
                    {{$terminOzEmail}}
            </td>
            <td>
                {{$terminOzPhone}}
        </td>
        </tr>
</tbody>
</table>

<h3 style="font-weight:bold;font-family:arial;font-size:20px;">Uczestnicy</h3>
<table style="width:100%;padding:15px;border:1px solid #eee;" cellpadding="8" cellspacing="10">
    <tbody>
        <tr>
            <td style="width: 50%;border:  1px solid #eee;font-weight: 700;">
                Imię
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                E-mail
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                Telefon
            </td>
            
        </tr>
        <tr>
            <td>
                @foreach($terminUczImie as $imie)
                {{$imie}} <br/>
                @endforeach
            </td>
            <td>
                    @foreach($terminUczEmail as $email)
                    {{$email}} <br/>
                    @endforeach      
            </td>
            <td>
                    @foreach($terminUczPhone as $phone)
                    {{$phone}} <br/>
                    @endforeach
        </td>
        </tr>
</tbody>
</table>


<h3 style="font-weight:bold;font-family:arial;font-size:20px;">Dane firmy</h3>
<table style="width:  100%;padding:15px;border:1px solid #eee;" cellpadding="8" cellspacing="10">
    <tbody>
        <tr>
            <td style="width: 50%;border:  1px solid #eee;font-weight: 700;">
                Nazwa firmy
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                Ulica
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                Kod
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                Miasto
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                NIP
            </td>
            
        </tr>
        <tr>
                <td>
                        {{$terminFirmaNazwa}}
                </td>
                <td>
                        {{$terminFirmaUlica}}
                </td>

            <td>
                    {{$terminFirmaKod}}
            </td>
            <td>
                    {{$terminFirmaMiasto}}
            </td>
            <td>
                {{$terminFirmaNip}}
        </td>
        </tr>
</tbody>
</table>

<table style="width:  100%;text-align:  center;background: #f9f9f9;padding:  15px;">
<tbody>
<tr>
    <td style="color: #b3b3b3;">ccopyright &copy; {{date('Y')}} {{config('app.name')}}</td>
</tr>
</tbody>
</table>