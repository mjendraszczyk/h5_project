<table style="text-align:  center;width:  100%;background: #101010;padding:  25px;">
    <tbody>
        <tr>
            <td style="color:#fff;">
                <img src="{{asset('img/frontend/high5l.png')}}" style="width: 150px;" alt="{{config('app.name')}}">
            </td>
        </tr>
    </tbody>
</table>
<table style="width:100%;text-align:center;" cellpadding="10">
    <tbody>
        <tr>
            <td>
                <h3
                    style="display:  table;padding:  0 0 10px;margin:  auto;font-size: 20px;margin: 20px auto;border-bottom: 2px solid #ea3f33;">
                    Przypomnienie z strony High5 | Kontakt</h3>
            </td>
        </tr>

    </tbody>
</table>
<table style="margin:  auto;padding: 15px;width:  50%;text-align:  center;border:1px solid #eee;">
    <tbody>
        <tr>

            Z strony: {{$contactUrl}}

        </tr>
        <tr style="text-align:  center;">

            <td>
                <table style="width:  100%;" cellpadding="8" cellspacing="10">
                    <tbody>

                        <tr>
                            <td style="width: 50%;border:  1px solid #eee;font-weight: 700;">
                                Klient
                            </td>
                            <td style="border: 1px solid #eee;font-weight:  700;">
                                E-mail
                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{$contactName}}
                            </td>
                            <td>
                                {{$contactEmail}}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table style="width:  100%;border:1px solid #eee;" cellpadding="8" cellspacing="10">
                    <tbody>
                        <tr>
                            <td style="font-weight:  700;border: 1px solid #eee;">
                                Wiadomosc
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:  20px;">
                                {!!$contactMessage!!}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
<table style="width:  100%;text-align:  center;background: #f9f9f9;padding:  15px;">
    <tbody>
        <tr>
            <td style="color: #b3b3b3;">ccopyright &copy; {{date('Y')}} {{config('app.name')}}</td>
        </tr>
    </tbody>
</table>