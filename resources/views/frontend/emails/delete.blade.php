@include('frontend.main.header')
<div class="container">
 <div style="padding:70px 0 0 0">
<h1>
    Rezygnacja z otrzymywania newslettera
</h1>
<p>
    Wprowadź swój adres e-mail w celu anulowania subskrypcji
</p>

@if(Session::has('panelInfo')) 
<div class="alert alert-danger">
    {{Session::get('panelInfo')}}
 </div>
@endif

{!! Form::open(['url'=>route('NewsletterWypisz'),'id'=>'WyslijNewsletterForm','name'=>'WyslijNewsletterForm','enctype' => 'multipart/form-data','class'=>'form-vertical','style'=>'width:100%;']) !!}
<div style="margin:20px 0;">
{!! Form::text('email',null,['class'=>'form-control', 'required']) !!}
</div>

{!! Form::submit("Wyslij",['class'=>'btn btn-success','style'=>'width:100%;display:block;']) !!}
{!! Form::close() !!}
</div> 
</div>
@include('frontend.main.footer')
