@foreach($terminy as $key => $termin)
@if($key == 0)
@section('title')
HIGH5: Terminy szkoleń w kategorii {{$termin->category_title}} Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
Szkolenia realizowane w kategorii {{$termin->training_title}}  Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('keywords')
Szkolenia otwarte, szkolenia zamknięte, coaching, rozwój, zarządzanie, warszawa, projekty, procesy, HR, negocjacje, sprzedaż
@endsection
@endif 

@if($lastCategory != $termin->category_title)
<h3 class="page-header red">{{$termin->category_title}}</h3> 
@php($lastCategory=$termin->category_title)
@endif
@if($lastTraining != $termin->training_title)
<h4>
<a href="{{route('szkolenia_strona',['id'=>$termin->categoryID,'title'=>str_slug($termin->category_title),'id_kurs'=>$termin->trainingID,'title_kurs'=>str_slug($termin->training_title)])}}" title="" rel="nofollow">
{{$termin->training_title}}
</a></h4>
@php($lastTraining=$termin->training_title)
@endif
  <div class="termin">  
    <p>{{date('d',strtotime($termin->term_start))}}-{{date('d',strtotime($termin->term_end))}}
        {{Lang::get('date.month.'.date('n',strtotime($termin->term_end)))}} 
        {{date('Y',strtotime($termin->term_end))}}, @if($termin->term_place != ''){{$termin->term_place}} @else Warszawa @endif
    @if($termin->term_state == 2)
    <img src="{{asset('img/frontend/pewny1.png')}}" alt="pewny" />
    @endif
    </p><a href="{{route('rezerwacja_szkolenia',['id'=>$termin->trainingID])}}" class="prawo" rel="nofollow">[..zarezerwuj..]</a>
</div>
  @endforeach