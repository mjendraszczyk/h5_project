<div class="podsumowanie">
@if (Session::has('termin_zapis_succ'))
<div class="row alert alert-success">{{Session::get('termin_zapis_succ')}}</div>
@endif

<table style="width:100%;text-align:center;" cellpadding="10">
<tbody>
    <tr>
        <td>
                <h3 style="display:  table;padding:  0 0 10px;margin:  auto;font-size: 20px;margin: 20px auto;border-bottom: 2px solid #ea3f33;"> 
                         
                       Podsumowanie rezerwacji szkolenia
                         
                        
                    </h3> 
        </td>
    </tr>
    
</tbody>
</table>
<h3 style="font-weight:bold;font-family:arial;font-size:20px;">
       
                Szkolenie
        
         
          
</h3>
<table style="margin:  auto;padding: 15px;width:  100%;text-align:  center;border:1px solid #eee;">
    <tbody><tr style="text-align:  center;">
<td><table style="width:  100%;" cellpadding="8" cellspacing="10">
    <tbody>
        <tr>
            <td style="border:  1px solid #eee;font-weight: 700;">
                 
                Szkolenie
                 
                 
                 
            </td>
             
            <td style="border: 1px solid #eee;font-weight:  700;">
                Termin
            </td>
             
            <td style="border: 1px solid #eee;font-weight:  700;">
                Cena
            </td>
        </tr>
        <tr>
            <td>
                    {{$terminSzkolenie}}
            </td>
             
            <td>
                    @if($terminSesja == null) 
{{$terminTermin}}
@else 
{!! $terminSesja!!}
@endif
            </td>
             
            <td>
                {{$terminCena}}
        </td>
        </tr>
</tbody>
</table>
<table style="width:  100%;" cellpadding="8" cellspacing="10">
    <tbody>
        <tr>
            <td style="font-weight:  700;border: 1px solid #eee;">
                Uwagi
            </td>
        </tr>
        <tr>
            <td style="padding:  20px;">
                    {{$terminUwagi}}
            </td>
        </tr>
    </tbody>
</table>
    </td></tr>

</tbody></table>

<h3 style="font-weight:bold;font-family:arial;font-size:20px;">Osoba zgłaszająca</h3>

<table style="width:100%;padding:15px;border:1px solid #eee;" cellpadding="8" cellspacing="10">
    <tbody>
        <tr>
            <td style="width: 50%;border:  1px solid #eee;font-weight: 700;">
                Imię
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                E-mail
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                Telefon
            </td>
            
        </tr>
        <tr>
            <td>
                    {{$terminOzImie}}
            </td>
            <td>
                    {{$terminOzEmail}}
            </td>
            <td>
                {{$terminOzPhone}}
        </td>
        </tr>
</tbody>
</table>

<h3 style="font-weight:bold;font-family:arial;font-size:20px;">Uczestnicy</h3>
<table style="width:100%;padding:15px;border:1px solid #eee;" cellpadding="8" cellspacing="10">
    <tbody>
        <tr>
            <td style="width: 50%;border:  1px solid #eee;font-weight: 700;">
                Imię
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                E-mail
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                Telefon
            </td>
            
        </tr>
        <tr>
            <td>
                @foreach($terminUczImie as $imie)
                {{$imie}} <br/>
                @endforeach
            </td>
            <td>
                    @foreach($terminUczEmail as $email)
                    {{$email}} <br/>
                    @endforeach      
            </td>
            <td>
                    @foreach($terminUczPhone as $phone)
                    {{$phone}} <br/>
                    @endforeach
        </td>
        </tr>
</tbody>
</table>


<h3 style="font-weight:bold;font-family:arial;font-size:20px;">Dane firmy</h3>
<table style="width:  100%;padding:15px;border:1px solid #eee;" cellpadding="8" cellspacing="10">
    <tbody>
        <tr>
            <td style="width: 50%;border:  1px solid #eee;font-weight: 700;">
                Nazwa firmy
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                Ulica
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                Kod
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                Miasto
            </td>
            <td style="border: 1px solid #eee;font-weight:  700;">
                NIP
            </td>
            
        </tr>
        <tr>
                <td>
                        {{$terminFirmaNazwa}}
                </td>
                <td>
                        {{$terminFirmaUlica}}
                </td>

            <td>
                    {{$terminFirmaKod}}
            </td>
            <td>
                    {{$terminFirmaMiasto}}
            </td>
            <td>
                {{$terminFirmaNip}}
        </td>
        </tr>
</tbody>
</table>

</div>