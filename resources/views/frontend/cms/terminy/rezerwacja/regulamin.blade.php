<ol class="regulamin">
<li> Zgłoszenie na szkolenie realizowane jest w formie elektronicznej poprzez wypełnienie formularza on-line lub/I przesłanie skanu wypełnionego formularza zgłoszenia.</li>
<li>Przesłanie formularza jest równoznaczne z zawarciem umowy zakupu szkolenia z firmą High5 Group Sp. z. o.o. (zwaną dalej Organizatorem) na zasadach określonych w niniejszym regulaminie i upoważnia firmę High5 Group Sp. z. o.o.  do wystawienia po szkoleniu faktury VAT bez składania podpisu przez osobę upoważnioną ze strony zgłaszającego.</li>
<li>Organizator potwierdza otrzymanie zgłoszenia i rezerwację miejsca na szkoleniu w terminie 3 dni od otrzymania zgłoszenia.</li>
<li>Organizator zastrzega sobie prawo odmowy przyjęcia zgłoszenia w przypadku, gdy udział w szkoleniu mógłby naruszyć interesy organizatora.</li>
<li>Organizator na 5 dni przed terminem szkolenia osobie zgłaszającej i uczestnikowi wysyła drogą elektroniczną  „Zaproszenie na szkolenie”, zawierające wszystkie informacje organizacyjne niezbędne do uczestnictwa w szkoleniu. Otrzymanie Zaproszenia jest podstawą do uczestnictwa w szkoleniu.</li>
<li>Rezygnacja z udziału w szkoleniu na 5 lub więcej dni przed planowanym terminem rozpoczęcia szkolenia nie pociąga za sobą żadnych obciążeń finansowych po stronie Zamawiającego, pod warunkiem przesłania przez Zamawiającego informacji o rezygnacji na adres: biuro@high5.pl. </li>
<li>W przypadku rezygnacji na mniej niż 5 dni przed terminem szkolenia Organizator zastrzega sobie prawo do wystawienia faktury na 50% ceny szkolenia tytułem kary umownej.</li>
<li>Nieobecność Uczestnika na szkoleniu nie zwalnia Zamawiającego od zapłaty pełnej ceny wynagrodzenia za jego udział w szkoleniu, chyba, że zachodzą sytuacje wskazane w punkcie 6 i 7</li>
<li>Możliwe jest zgłoszenie zastępstwa dla uczestnika.</li>
<li>Organizator zastrzega sobie prawo do odwołania szkolenia  lub zmiany jego terminu na 5 dni przed terminem szkolenia, o czym zobowiązany jest poinformować uczestników szkolenia i Zamawiającego.</li>
<li>Organizator nie ponosi odpowiedzialności za koszty poniesione przez Zamawiającego na rzecz podmiotów trzecich (np. z tytułu przedpłat dokonanych na rzecz hoteli, z tytułu dojazdu na szkolenie) jeśli zostały one poniesione przed otrzymaniem zaproszenia na szkolenie.</li>
<li>Gwarancja Jakości: Uczestnik do połowy pierwszego dnia szkolenia  może zrezygnować ze szkolenia, podając konkretny merytoryczny powód wraz z jego uzasadnieniem. Zamawiający nie będzie obciążony w takim przypadku  kosztem szkolenia.</li>
<li>Płatność za szkolenie następuje na podstawie wystawionej po szkoleniu faktury VAT.</li>
<li>W przypadku finansowania szkolenia w co najmniej 70% ze środków publicznych Zgłaszający  powinien poinformować Organizatora w momencie zgłaszania uczestnika.</li>
</ol>