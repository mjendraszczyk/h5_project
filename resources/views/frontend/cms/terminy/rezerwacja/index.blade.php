@extends('frontend.main.cms')
@section('col-left')
@if(Route::currentRouteName() == 'rezerwacja_szkolenia')
{{-- Szkolenie online --}}
{{-- @if(in_array("21",$id_category))
<h2 class="antonioSmall TgridLeft red">Regulamin zgłaszania uczestnika na szkolenie on-line</h2>
@include('frontend.cms.terminy.rezerwacja.regulamin_online')
@else
<h2 class="antonioSmall TgridLeft red">Regulamin zgłaszania uczestnika na szkolenie otwarte</h2>
@include('frontend.cms.terminy.rezerwacja.regulamin')
@endif --}}
<div class="regulamin-block regulamin1">
    <h1 class="antonioBig">Przeczytaj to</h1>
    <h2 class="antonioSmall TgridLeft red">Regulamin zgłaszania uczestnika na szkolenie otwarte</h2>
    @include('frontend.cms.terminy.rezerwacja.regulamin')
</div>
<div class="regulamin-block regulamin2">
    <h1 class="antonioBig">Przeczytaj to</h1>
    <h2 class="antonioSmall TgridLeft red">Regulamin zgłaszania uczestnika na szkolenie on-line</h2>
    @include('frontend.cms.terminy.rezerwacja.regulamin_online')
</div>

{{-- @include('frontend.cms.terminy.rezerwacja.regulamin') --}}
@else
@include('frontend.cms.uslugi.programy-rozwojowe.rezerwacja.regulamin')
@endif
<div id="fb-root"></div>
<div class="fb-like" data-href="https://www.facebook.com/pages/High5-Training-Group/146638568702913" data-width="130"
    data-layout="box_count" data-action="like" data-show-faces="true" data-share="true"
    style="width:100%; text-align: center;"></div>
<p class="polub white">Polub naszą stronę na Facebook-u i odbierz dodatkowe 10% rabatu na szkolenie</p>
@endsection
@section('content')

@if($podsumowanie == '1')
@include('frontend.cms.terminy.rezerwacja.podsumowanie')
@else
{{-- @if(in_array("21",$id_category))
@include('frontend.cms.terminy.rezerwacja.form_online')
@else --}}
@include('frontend.cms.terminy.rezerwacja.form')
{{-- @endif --}}
@endif

@endsection