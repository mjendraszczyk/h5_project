@section('title')
HIGH5: Terminy szkoleń. @if(array_key_exists($id,$szkolenia) == true) Szkolenie - {{$szkolenia[$id]}} @else Szkolenia
@endif - Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
Terminy szkoleń otwartych i zamkniętych, coaching. Rozwój osobisty, negocjacje, zarządzanie projektami i procesami
Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('keywords')
Szkolenia otwarte, szkolenia zamknięte, coaching, rozwój, zarządzanie, warszawa, projekty, procesy, HR, negocjacje,
sprzedaż
@endsection

<h1 class="antonioBig">Formularz zgłoszeniowy</h1>
@if(count($errors) >0)
<div class="row alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
        <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
@endif
@if (Session::has('termin_zapis_succ'))
<div class="row alert alert-success">{{Session::get('termin_zapis_succ')}}</div>
@endif
@if(Session::has('termin_zapis_fail'))
<div class="row alert alert-danger">{{Session::get('termin_zapis_fail')}}</div>
@endif
@if(Route::currentRouteName() == 'rezerwacja_szkolenia')
{!! Form::open(['url' =>
route("zapisz_terminy",["id"=>$id]),'id'=>'orderForm','name'=>'form','onsubmit'=>"YY_checkform('orderForm','proforma','#nip','2','Prosimy
o podanie numeru NIP','akceptacja','#q','1','Prosimy zaakceptować regulamin','name','#q','0','Prosimy o podanie danych
osoby zgłaszającej','email','#S','2','Prosimy o podanie poprawnego adresu email');return document.MM_returnValue'"]) !!}
@else
{!! Form::open(['url' =>
route("zapisz_program",["id"=>$id]),'id'=>'orderForm','name'=>'form','onsubmit'=>"YY_checkform('orderForm','proforma','#nip','2','Prosimy
o podanie numeru NIP','akceptacja','#q','1','Prosimy zaakceptować regulamin','name','#q','0','Prosimy o podanie danych
osoby zgłaszającej','email','#S','2','Prosimy o podanie poprawnego adresu email');return document.MM_returnValue'"]) !!}
@endif
<fieldset>
    <legend>
        @if(Route::currentRouteName() == 'rezerwacja_szkolenia')
        Szkolenie
        @else
        Program rozwojowy
        @endif
    </legend>

    @if(Route::currentRouteName() == 'rezerwacja_szkolenia')
    {!! Form::label('szkolenie', "Szkolenie", ['class' => 'control-label']) !!}
    @else
    {!! Form::label('szkolenie', "Nazwa programu", ['class' => 'control-label']) !!}
    @endif

    {!! Form::select('szkolenie', $szkolenia, $id,['id'=>'szkolenie']) !!}
    @if(Route::currentRouteName() == 'rezerwacja_szkolenia')
    {!! Form::label('termin', null, ['class' => 'control-label']) !!}
    <div id="select_kontener">
        {{-- {{print_r($terminy)}}
        {{head($terminy)}} --}}
        {{-- {!! Form::select('termin', $terminy, head($terminy),['id'=>'termin', 'required'=>'required']) !!} --}}
        <select id="termin" name="termin" required>
            <option value="" disabled>Wybierz termin</option>
            @foreach ($terminy as $keyt => $termin)
            <option value="{{$keyt}}">
                {{$termin}}
            </option>
            @endforeach
        </select>
    </div>
    @endif
    @if(Route::currentRouteName() == 'rezerwacja_programy')
    <div id="rezerwacja_program"></div>
    @else
    <div id="rezerwacja_szkolenie"></div>
    @endif
    <div id="cena" style="display: none">
    </div>

    <div id="terminy_sesja">
    </div>
    {!! Form::hidden('cena', null, ['class' => 'form-control', 'id'=>'input_cena']) !!}

    {!! Form::label('uwagi', null, ['class' => 'control-label']) !!}
    {!! Form::textarea('uwagi', null, ['class' => 'control-label','placeholder'=>'']) !!}
</fieldset>

<fieldset>
    <legend>
        Osoba zgłaszająca
    </legend>
    {!! Form::label('name', 'Imię i nazwisko *', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'inputLongText form-control','required']) !!}

    {!! Form::label('email', 'E-mail *', ['class' => 'control-label']) !!}
    {!! Form::text('email', null, ['class' => 'inputLongText
    form-control','required','pattern'=>'[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$']) !!}

    {!! Form::label('phone', 'Telefon ', ['class' => 'control-label']) !!}
    {!! Form::text('phone', null, ['class' => 'inputLongText form-control']) !!}

    <div class="col-sm-12">
        <div class="col-sm-1">
            @if(count($errors) >0)
            {!! Form::checkbox('uczestnik', "yes", null, ['id'=>'uczestnik','class'=>'checkbox']) !!}
            @else
            {!! Form::checkbox('uczestnik', "yes", null, ['id'=>'uczestnik','class'=>'checkbox']) !!}
            @endif

        </div>
        {!! Form::label('uczestnik', 'Zaznacz jeśli jesteś także uczestnikiem ', ['class' => 'checkboxLabel col-sm-11'])
        !!}
    </div>
</fieldset>

<fieldset>
    <legend>
        Uczestnicy
    </legend>

    {!! Form::label('username[0]', 'Imię i nazwisko', ['class' => 'control-label']) !!}

    {!! Form::text('username[0]', null, ['class' => 'inputLongText form-control uczestnik']) !!}


    {!! Form::label('user_mail[0]', 'E-mail', ['class' => 'control-label']) !!}

    {!! Form::text('user_mail[0]', null, ['class' => 'inputLongText form-control uczestnik']) !!}

    {!! Form::label('user_phone[0]', 'Telefon', ['class' => 'control-label']) !!}

    {!! Form::text('user_phone[0]', null, ['class' => 'inputLongText form-control uczestnik']) !!}

    <p id="parah"></p>
    {{--  <a href="javascript:addInput()" class="formAddField">Dodaj kolejnego uczestnika</a>
        <a href="javascript:deleteInput()" class="formRemoveField">Usuń ostatniego uczestnika</a>
          --}}
    <span id="dodaj_uczestnika" class="btn btn-default">Dodaj uczestnika</span>
    <span id="usun_uczestnika" class="btn btn-default">Usuń uczestnika</span>

</fieldset>

<fieldset>
    <legend>
        Dane teleadresowe firmy
    </legend>
    {!! Form::label('nip', 'NIP *', ['class' => 'control-label']) !!}
    {!! Form::text('nip', null, ['id'=>'nip', 'class' => 'inputLongText form-control','required']) !!}

    {!! Form::label('company', 'Firma', ['class' => 'control-label']) !!}
    {!! Form::text('company', null, ['id'=>'company', 'class' => 'inputLongText form-control']) !!}

    {!! Form::label('street', 'Ulica', ['class' => 'control-label']) !!}
    {!! Form::text('street', null, ['id'=>'street', 'class' => 'inputLongText form-control']) !!}

    <div class="zipContainer">
        {!! Form::label('zip', 'Kod', ['class' => 'control-label']) !!}
        {!! Form::text('zip', null, ['id'=>'zip', 'class' => 'inputLongText form-control']) !!}

    </div>

    <div class="cityContainer">
        {!! Form::label('city', 'Miasto', ['class' => 'control-label']) !!}
        {!! Form::text('city', null, ['id'=>'city', 'class' => 'inputLongText form-control']) !!}
    </div>

    @if(Route::currentRouteName() == 'rezerwacja_programy')
    {{-- <div class="col-sm-12">
                <div class="col-sm-1" style="margin-top: 5px;">
            
                    {!! Form::checkbox('proforma', "tak", null, ['id'=>'proforma','class'=>'checkbox']) !!}
                </div>
                 {!! Form::label('proforma', 'Proszę o wystawienie faktury pro-forma', ['class' => 'checkboxLabel col-sm-11']) !!}     
            </div> --}}
    @endif

    <div class="col-sm-12">
        <!-- <div class="col-sm-1" style="margin-top: 5px;">           
                 {!! Form::checkbox('publiczne', "yes", null, ['id'=>'publiczne','class'=>'checkbox']) !!}
                </div> -->

        <!-- {!! Form::label('publiczne', 'Szkolenie finansujemy ze środków publicznych i podlega
                zwolnieniu z VAT', ['class' => 'checkboxLabel col-sm-11']) !!}         -->
    </div>

</fieldset>
<div class="checkboxs">
    <div class="col-sm-12">
        <div class="col-sm-1" style="margin-top: 5px;">
            {!! Form::checkbox('zgody_all', "1", null, ['id'=>'zgody_all','class'=>'checkbox']) !!}

        </div>
        {!! Form::label('zgody_all', 'Zaznacz wszystkie zgody', ['class' => 'checkboxLabel col-sm-11','required']) !!}
    </div>

    <div class="col-sm-12">
        <div class="col-sm-1" style="margin-top: 5px;">
            {!! Form::checkbox('akceptacja', "yes", null, ['id'=>'akceptacja','class'=>'checkbox','required']) !!}

        </div>
        {!! Form::label('akceptacja', '* Przeczytałem „Regulamin zgłaszania uczestnika na szkolenie otwarte” i akceptuję
        go.', ['class' => 'checkboxLabel col-sm-11','required']) !!}
    </div>

    <div class="col-sm-12">
        <div class="col-sm-1" style="margin-top: 5px;">
            {!! Form::checkbox('dane_przetwarzanie', "1", null,
            ['id'=>'dane_przetwarzanie','class'=>'checkbox','required']) !!}

        </div>
        {!! Form::label('dane_przetwarzanie', '* Wyrażam zgodę na przetwarzanie podanych w formularzu danych osobowych w
        zakresie niezbędnym do obsługi logistycznej zgłoszenia, zgodnie z ustawą z dnia 18 lipca 2002 r. o świadczeniu
        usług drogą elektroniczną oraz ustawą z dnia 29 sierpnia 1997 r. o ochronie danych osobowych. ', ['class' =>
        'checkboxLabel col-sm-11','required']) !!}

        <span
            style="font-size: 11px;color: #9b9b9b;line-height: 16px !important;border: 1px solid #eee;padding: 10px;text-align:  justify;display:  block;clear:  both;">
            Administratorem danych jest HIGH5 Group Sp. z o. o. z siedzibą w Warszawie przy ul. Wroniej 45 lok.129,
            biuro@high5.pl, tel. 22-824-50-25.

            Informujemy, że mają Państwo prawo dostępu do swoich danych oraz możliwość ich poprawiania, sprostowania,
            usunięcia lub ograniczenia przetwarzania, prawo do wniesienia sprzeciwu wobec przetwarzania, a także o prawo
            do przenoszenia danych.

            Podanie danych ma charakter dobrowolny, jest jednak niezbędne, żeby móc zarejestrować Państwa zgłoszenie.

            Dane osobowe będą przechowywane przez okres niezbędny do realizacji usługi szkoleniowej oraz wynikających z
            przepisów prawa.
        </span>

    </div>


    <div class="col-sm-12">
        <div class="col-sm-1" style="margin-top: 5px;">
            {!! Form::checkbox('dane_marketing', "1", null, ['id'=>'dane_marketing','class'=>'checkbox']) !!}

        </div>
        {!! Form::label('dane_marketing', 'Wyrażam zgodę na przetwarzanie danych w celach informowania mnie o
        nowościach, promocjach i innych działaniach związanych z przedmiotem szkolenia, w tym na otrzymywanie informacji
        handlowych na podany w formularzu adres e-mail, zgodnie z ustawą z dnia 18 lipca 2002 r. o świadczeniu usług
        drogą elektroniczną oraz ustawą z dnia 29 sierpnia 1997 r. o ochronie danych osobowych.
        ', ['class' => 'checkboxLabel col-sm-11','required']) !!}



        <span
            style="font-size: 11px;color: #9b9b9b;line-height: 16px !important;border: 1px solid #eee;padding: 10px;text-align:  justify;display:  block;clear:  both;">
            Podanie danych ma charakter dobrowolny, bez wyrażenia zgody nie będziemy jednak mogli wysyłać do Państwa
            informacji o nowościach i aktualnych promocjach.
            Informujemy, że mają Państwo prawo dostępu do swoich danych oraz możliwość ich poprawiania, sprostowania,
            usunięcia lub ograniczenia przetwarzania, prawo do wniesienia sprzeciwu wobec przetwarzania, a także o prawo
            do przenoszenia danych.
            Informujemy, że mają Państwo prawo do cofnięcia zgody w dowolnym momencie bez wpływu na zgodność z prawem
            przetwarzania, którego dokonano na podstawie zgody przed jej cofnięciem.
            Dane osobowe będą przechowywane do czasu odwołania zgody, ale nie dłużej niż 3 lata
            Informujemy, że przysługuje Państwu prawo do złożenia na nas skargi do organu nadzorczego - szczegóły pod
            adresem: https://giodo.gov.pl/579
            Administratorem danych jest HIGH5 Group Sp. z o. o. z siedzibą w Warszawie przy ul. Wroniej 45 lok.129,
            biuro@high5.pl, tel. 22-824-50-25. 
        </span>
    </div>

</div>
<div id="buttonContainer">
    {!! Form::submit('Wyślij zgłoszenie', ['id'=>'submitButton', 'class'=>'btn btn-success']) !!}
</div>

{!! Form::close() !!}