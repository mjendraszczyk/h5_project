@extends('frontend.main.cms') 
@section('menu') 
@section('title_menu')
Katalog szkoleń
@endsection
@include('frontend.cms.terminy.menu')
@endsection 
@section('content')
<h1 class="antonioBig Lmargin red">Terminy: wszystkie </h1>
<div id="chrono" class="btn-group" style="float: right;">  
    @if($sort == 'cat')
    <a href="{{route('terminy_sort',['sort'=>'cat'])}}" class="btn btn-aktywny">Wg kategorii</a> 
    <a href="{{route('terminy_sort',['sort'=>'chrono'])}}" class="btn btn-default">Chronologicznie</a>
    @elseif($sort == 'chrono')
    <a href="{{route('terminy_sort',['sort'=>'cat'])}}" class="btn btn-default">Wg kategorii</a> 
    <a href="{{route('terminy_sort',['sort'=>'chrono'])}}" class="btn btn-aktywny">Chronologicznie</a>
    @else
    <a href="{{route('terminy_sort',['sort'=>'cat'])}}" class="btn btn-default">Wg kategorii</a> 
    <a href="{{route('terminy_sort',['sort'=>'chrono'])}}" class="btn btn-default">Chronologicznie</a>
    @endif 
    </div>
<div id="terminarz">
@if(Request::path() == 'terminy/chrono')
@include('frontend.cms.terminy.chrono')
@else  
@include('frontend.cms.terminy.cat')
@endif

</div>

@endsection