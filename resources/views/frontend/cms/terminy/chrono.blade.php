@section('title')
HIGH5: Terminy szkoleń. Szkolenia dla firm - szkolimy najlepszych -  Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
Terminy szkoleń otwartych i zamkniętych, coaching. Rozwój osobisty, negocjacje, zarządzanie projektami i procesami  Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('keywords')
Szkolenia otwarte, szkolenia zamknięte, coaching, rozwój, zarządzanie, warszawa, projekty, procesy, HR, negocjacje, sprzedaż
@endsection
@foreach($terminy as $key => $termin)
@php 
$currentWeek=date('W', strtotime($termin->term_start)); 
@endphp 
 
@if(($currentWeek != $lastWeek))

<h3 class="page-header red">Tydzień {{$currentWeek-1}}: 
    @inject('week', 'App\Http\Controllers\TerminyController')

    @foreach($week->week_range($termin->term_start) as $date)
{{$date}}
    @endforeach
 </h3>
 
@php $lastWeek=date('W', strtotime($termin->term_start)); @endphp
@endif

<h4>
        <a href="{{route('szkolenia_strona',['id'=>$termin->categoryID,'title'=>str_slug($termin->category_title),'id_kurs'=>$termin->trainingID,'title_kurs'=>str_slug($termin->training_title)])}}" title="" rel="nofollow">
        {{$termin->training_title}}
        </a>
        </h4>
  <div class="termin">  
    <p>{{date('d',strtotime($termin->term_start))}}-{{date('d',strtotime($termin->term_end))}}
            {{Lang::get('date.month.'.date('n',strtotime($termin->term_end)))}} 
            {{date('Y',strtotime($termin->term_end))}}, @if($termin->term_place != ''){{$termin->term_place}} @else Warszawa @endif
    @if($termin->term_state == 2)
    <img src="{{asset('img/frontend/pewny1.png')}}" alt="pewny" />
    @endif
    </p><a href="{{route('rezerwacja_szkolenia',['id'=>$termin->trainingID])}}" class="prawo" rel="nofollow">[..zarezerwuj..]</a>
</div>
 
  @endforeach