@extends('frontend.main.fullwidth')
@section('title')
HIGH5 Training Group: Kim jesteśmy, usługi i szkolenia, które realizujemy Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
High5 Training Group: firma szkoleniowo-doradcza. Szkolimy umiejętności, które są niezbędne w efektywnym funkcjonowaniu każdego przedsiębiorstwa. Tag:   @foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('content')
<div id="kfsHeader"></div>
<div class="col-sm-10">
<h1 class="antonioBig red">Jak uzyskać dofinansowane szkolenia?</h1>
	

<p>Obecnie istnieją dwie możliwości uzyskania dofinansowania:</p>
<ul>
  <li>Baza usług rozwojowych </li>
  <li>Krajowy Fundusz Szkoleniowy</li>
</ul>
<h2 class="antonioBig">Baza Usług Rozwojowych</h2>
<p><img src="./img/frontend/kfs_parp.gif" alt="logo PARP">Baza Usług Rozwojowych jest platformą, za pośrednictwem której osoba  indywidualna lub firma  może  zarejestrować się na wybraną usługę rozwojową, w <strong>tym szkolenie.</strong></p>
<p>Na stronie <a href="http://serwis-uslugirozwojowe.parp.gov.pl/">http://serwis-uslugirozwojowe.parp.gov.pl/</a> w  zakładce &quot;<strong>Dofinansowanie</strong>&quot; możesz znaleźć kluczowe informacje nt. możliwości  uzyskania dofinansowania na usługi rozwojowe w <strong>poszczególnych województwach</strong>, w tym szczegółowe <strong>zasady i procedury</strong> ubiegania się o środki oraz ich rozliczania.</p>
<h3 class="antonioSmall">Jak  otrzymać wsparcie? </h3>
<ul>
  <li>Skontaktuj  się z operatorem w Twoim województwie <br>
    (Będziesz musiał wypełnić 
  i wysłać do Operatora formularz zgłoszeniowy wraz z niezbędnymi załącznikami, a  po pozytywnym rozpatrzeniu dokumentacji   podpiszesz umowę z Operatorem).</li>
<li>Znajdź  w BUR nasze szkolenia <a href="http://bit.ly/BUR_high5">http://bit.ly/BUR_high5</a></li>
</ul>
<blockquote>
  <p>Uwaga! Szkolenia      dofinansowane <strong>nie są niestety      dostępne dla podmiotów z województwa      mazowieckiego oraz pomorskiego.</strong> Taką decyzję podjęły władze samorządowe.  </p>
</blockquote>
Zapisz się na wybrane szkolenie przez BUR a od nas otrzymasz  potwierdzenie przyjęcia zgłoszenia, <br>
a przed szkoleniem szczegóły organizacyjne.
<ul>
  <li><strong>Przyjdź do nas  na szkolenie </strong></li>
  <li>Po  szkoleniu wystawiamy fakturę, którą musisz przekazać Operatorowi a on dokona  płatności </li>
  <li>Ocen  szkolenie za pomocą elektronicznej ankiety 
    w BUR. Pamiętaj, że jest to <strong>wymagane</strong>, aby otrzymać dofinasowanie!<br clear="all">
  </li>
</ul>
<h2 class="antonioBig">Krajowy Fundusz Szkoleniowy</h2>
<p><img src="./img/frontend/kfs_logo.gif" alt="logo KFS"> KFS to instrument polityki rynku pracy wprowadzony w 2014 r. Celem KFS jest zapobiegać utracie zatrudnienia przez osoby pracujące,  z powodu kompetencji nieadekwatnych do wymagań rynkowych. Więcej informacji o Krajowym Funduszu Szkoleniowym znajdą Państwo na stronie <a href="http://psz.praca.gov.pl/-/55453-krajowy-fundusz-szkoleniowy">Ministerstwa Rodziny, Pracy i Polityki Społecznej</a>. </p>
    
<h3>Kto może wystąpić o dofinansowanie?</h3>
<p>O środki może wystąpić każdy pracodawca, czyli podmiot zatrudniający pracowników na podstawie umowy o pracę. Nie jest pracodawcą podmiot prowadzący działalność gospodarczą i niezatrudniający pracowników w ramach umowy o pracę.</p>
<p>Uwaga!</p>
<p>Każdy Urząd Pracy stosuje własne, odrębne zasady dotyczące składania wniosków i ich wzory. Przed złożeniem wniosku zaleca się sprawdzenie dostępność środków z KFS w Powiatowym Urzędzie Pracy.</p>

<div id="OfertySide">
<h2 class="antonioBig Lmargin white">Masz pytania ?</h2>
<p>(+4822) 824 50 25 <br>
    <a href="mailto:biuro@high5.pl">
        biuro@high5.pl</a></p>
<p>Zapytania o dofinansowanie szkoleń: <a href="mailto:patrycja.lichacy@high5.pl" class="antonioSmall">patrycja.lichacy@high5.pl</a>
</p>

</div><h2>Możesz liczyć na naszą pomoc w wypełnieniu wniosku!</h2>
<p>Firmom ubiegającym się o środki z KFS na szkolenia pomagamy w wypełnieniu wniosków składanych w Powiatowych Urzędach Pracy.</p>

    
    
<h3 class="antonioSmall">Jaki jest poziom dofinansowania?</h3>
<ul>
  <li>dla  mikroprzedsiębiorstw: 100% kosztów kształcenia*</li>
  <li>dla  pozostałych firm: 80% kosztów kształcenia*</li>
</ul>
<p>*jednak nie więcej jak 300% przeciętnego  wynagrodzenia w danym roku na jednego uczestnika.<br>
  Kwota jaką można przeznaczyć na osobę to około 11 800 zł.</p>
<h3 class="antonioSmall">Jak  otrzymać wsparcie?</h3>
<ul>
  <li>Złóż  wniosek wraz z załącznikami we właściwym dla siebie Powiatowym Urzędzie Pracy  (w miejscu siedziby lub wykonywania działalności). Wniosek 
  o dofinansowanie kształcenia ustawicznego ze środków KFS powinien znajdować się  na stronie internetowej Urzędu.</li>
  <li>Pomożemy  Ci w uzupełnieniu wniosku. Prześlij go na adres e-mail <script>document.write('<'+'a'+' '+'h'+'r'+'e'+'f'+'='+"'"+'m'+'a'+'i'+'l'+'t'+'o'+'&'+'#'+'5'+'8'+';'+'p'+'a'+'&'+'#'+'3'+
'7'+';'+'7'+'4'+'&'+'#'+'3'+'7'+';'+'7'+'2'+'y'+'c'+'&'+'#'+'1'+'0'+'6'+';'+'a'+'&'+'#'+'4'+'6'+';'+
'l'+'&'+'#'+'1'+'0'+'5'+';'+'%'+'6'+'3'+'h'+'%'+'&'+'#'+'5'+'4'+';'+'&'+'#'+'4'+'9'+';'+'&'+'#'+'9'+
'9'+';'+'y'+'&'+'#'+'6'+'4'+';'+'h'+'i'+'%'+'6'+'7'+'&'+'#'+'1'+'0'+'4'+';'+'&'+'#'+'5'+'3'+';'+'%'+
'2'+'E'+'%'+'7'+'0'+'&'+'#'+'1'+'0'+'8'+';'+"'"+'>'+'p'+'a'+'t'+'r'+'y'+'&'+'#'+'9'+'9'+';'+'j'+'&'+
'#'+'9'+'7'+';'+'&'+'#'+'4'+'6'+';'+'l'+'&'+'#'+'1'+'0'+'5'+';'+'c'+'h'+'a'+'c'+'&'+'#'+'1'+'2'+'1'+
';'+'&'+'#'+'6'+'4'+';'+'h'+'i'+'g'+'&'+'#'+'1'+'0'+'4'+';'+'5'+'&'+'#'+'4'+'6'+';'+'p'+'&'+'#'+'1'+
'0'+'8'+';'+'<'+'/'+'a'+'>');</script><noscript>[Turn on JavaScript to see the email address]</noscript> Napisz jeżeli potrzebujesz również innych dokumentów, aby otrzymać  dofinansowanie np. wzór certyfikatu ukończenia szkolenia (informacje otrzymasz  w Urzędzie Pracy)</li>
  <li>Podpisz  umowę z Urzędem Pracy i zapisz się na szkolenie przez nasz formularz on-line.  Od nas otrzymasz potwierdzenie zgłoszenia a potem szczegóły organizacyjne. </li>
  <li><strong>Przyjdź do nas na szkolenie </strong></li>
  <li>Po  szkoleniu otrzymasz fakturę oraz certyfikat, który dostarczysz do Urzędu Pracy 
    w celu rozliczenia usługi. </li>
</ul>
 </div>          
@endsection