@extends('frontend.main.cms')
@section('menu')
@include('frontend.cms.referencje.menu')
@endsection
@section('content')
@section('title')
HIGH5 Training Group Warszawa Referencje Tag: @foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('keywords')
Szkolenia otwarte, szkolenia zamknięte, coaching, rozwój, zarządzanie, warszawa, projekty, procesy, HR, negocjacje, sprzedaż
@endsection
@section('description')
Szkolenia otwarte i szkolenia zamknięte, coaching. Rozwój osobisty, negocjacje, zarządzanie projektami i procesami, Szkolenia dla managerów i dla działów Tag:   @foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
<h1 class="antonioBig Lmargin red">Firmy, dla których pracowaliśmy</h1>
<div id="RefList">     
    <ul class="FirmList">
    @foreach($grouped as $key => $group)
        <h3 class="branza">{{$branza[$key]}}</h3>
        @foreach($group as $k => $klient) 
            <li class="shortFirm">{{$group[$k]->nazwa}}</li>
        @endforeach    
    @endforeach
    </ul>
</div>
<div id="logos" class="logos">
    @foreach($loga as $key => $logo) 
        @if($key > 1)
            <a href="#"><img src="{{Config('url')}}/img/frontend/logo/{{$logo}}" width="80" height="45" alt="{{$logo}}"></a>
        @endif
    @endforeach
</div>      
@endsection