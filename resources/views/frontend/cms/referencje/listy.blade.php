@extends('frontend.main.cms')
@section('menu')
@include('frontend.cms.referencje.menu')
@endsection
@section('content')
@section('title')
HIGH5 Training Group Warszawa Listy referencyjne Tag: @foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('keywords')
Szkolenia otwarte, szkolenia zamknięte, coaching, rozwój, zarządzanie, warszawa, projekty, procesy, HR, negocjacje, sprzedaż
@endsection
@section('description')
Szkolenia otwarte i szkolenia zamknięte, coaching. Rozwój osobisty, negocjacje, zarządzanie projektami i procesami, Szkolenia dla managerów i dla działów Tag:   @foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
<h1 class="antonioBig Lmargin red">Listy referencyjne</h1>
<div class="LMargin40">
    @foreach($references_list as $key => $ref) 
        @if($ref->zawiera_pdf == '0')
			<div class="@if(($key%2) == 0) ref_boxLeft @else ref_boxRight @endif">	
            <img src="{{Config('url')}}/img/frontend/referencje/{{$ref->obrazek}}" alt="{{$ref->klient}}" class="@if(($key%2) == 0) ref_imgLeft @else ref_imgRight @endif">
            <p class="listyRef_body">
                {!!$ref->szczegoly!!}
            </p>
            <p class="listyRef_instytucja">{{$ref->klient}}</p>
            </div>
        @else   
            <div class="col-sm-3">
            <!--<a href="{{Config('url')}}/img/frontend/referencje/{{$ref->plik_pdf}}">-->
                <img src="{{Config('url')}}/img/frontend/referencje/{{$ref->obrazek}}"  width="155" height="200" alt="referencje"><br>   
            <!--</a>-->
            </div>
        @endif
    @endforeach
</div>
@endsection