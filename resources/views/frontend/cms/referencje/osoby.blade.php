@extends('frontend.main.cms')
@section('menu')
@include('frontend.cms.referencje.menu')
@endsection
@section('content')
@section('title')
HIGH5 Training Group Warszawa Opinie uczestników Tag: @foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('keywords')
Szkolenia otwarte, szkolenia zamknięte, coaching, rozwój, zarządzanie, warszawa, projekty, procesy, HR, negocjacje, sprzedaż
@endsection
@section('description')
Szkolenia otwarte i szkolenia zamknięte, coaching. Rozwój osobisty, negocjacje, zarządzanie projektami i procesami, Szkolenia dla managerów i dla działów Tag:   @foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
<h1 class="antonioBig Lmargin red">Opinie uczestników</h1>
<div class="LMargin40">
<p class="refTitle">Poniżej wybrane odpowiedzi z ankiet, które uczestnicy wypełniają na koniec zajęć. Na życzenie możemy udostępnić skany pełnych wersji</p>
<ul class="refOpinie">    
        @foreach($referencje as $referencja) 
            <li @if($referencja->wazne == 'Y') class="WaznaReferencja" @endif> 
  
              {{$referencja->tresc}}   
              <span class="refOsoba">{{$referencja->imie}} - {{$referencja->funkcja}}</span>
              <span class="refOsoba">{{$referencja->firma}}</span>
            </li>  
        @endforeach   
</ul>
</div>
@endsection