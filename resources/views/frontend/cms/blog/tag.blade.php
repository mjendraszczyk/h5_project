@extends('frontend.main.cms')
@section('menu')
@include('frontend.cms.blog.menu')
@endsection
@section('menu_extends')
@include('frontend.cms.blog.menu_extends')
@endsection
@section('content')
<h1 class="antonioBig topPage red">Tag: #{{$tag}}</h1>
@foreach($posts as $tag)
@section('title')
HIGH5 Training Group Blog: {{$tag->title}}
@endsection
@section('description')
Szkolenia otwarte i szkolenia zamknięte, coaching. Rozwój osobisty, negocjacje, zarządzanie projektami i procesami, Szkolenia dla managerów i dla działów Tag:   @foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('keywords')
Szkolenia otwarte, szkolenia zamknięte, coaching, rozwój, zarządzanie, warszawa, projekty, procesy, HR, negocjacje, sprzedaż
@endsection
<div class="blogOther">
        <a href="{{route('blog')}}/post/{{$tag->blogID}}/{{str_slug($tag->title)}}">
            <h3>{{$tag->title}}</h3></a>
        <div class="blogOther_author"><img src="{{Config('url')}}/img/frontend/trainers/{{str_slug($tag->instructor_name,'_')}}.gif" alt="{{$tag->instructor_name}}"><p>{{$tag->instructor_name}}</p></div>
        <p class="blog_data">{{$tag->created}}</p>
        <a href="{{route('blog')}}/post/{{$tag->blogID}}/{{str_slug($tag->title)}}">
            <p class="blog_lid">
            {!!$tag->lid!!}
            </p>
            </a>
        <p class="blog_tagi">
@foreach($tagi as $t)
           <a href="{{route('blog_tag',['tag'=>str_slug($t->tag)])}}">{{$t->tag}}</a>
           @if(!$loop->last),
           @endif 
@endforeach
         </p>
    </div>
@endforeach 
@endsection