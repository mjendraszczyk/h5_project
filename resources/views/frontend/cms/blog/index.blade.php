@extends('frontend.main.cms')
@section('menu')
@include('frontend.cms.blog.menu')
@endsection
@section('menu_extends')
@include('frontend.cms.blog.menu_extends')
@endsection
@section('content')

@section('title')
HIGH5 Training Group Warszawa : Blog Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
zkolenia otwarte i szkolenia zamknięte, coaching. Rozwój osobisty, negocjacje, zarządzanie projektami i procesami,
Szkolenia dla managerów i dla działów Tag: @foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('keywords')
Szkolenia otwarte, szkolenia zamknięte, coaching, rozwój, zarządzanie, warszawa, projekty, procesy, HR, negocjacje,
sprzedaż
@endsection

<div id="blog_sm_maincol" style="padding-bottom: 20px;">
    @foreach($blogs as $key => $blog)
    @if($key==0)
    @if(env('APP_ENV')=='production' )
    <div id="blog_first" class="col-sm-12"
        style="background-image:url({{Config('url')}}/public/img/frontend/blog/{{$blog->picture}})">
        @else
        <div id="blog_first" class="col-sm-12"
            style="background-image:url({{Config('url')}}/img/frontend/blog/{{$blog->picture}})">
            @endif
            <h1><a href="{{route('blog')}}/post/{{$blog->blogID}}/{{str_slug($blog->title)}}">{{$blog->title}}</a></h1>
            <p class="blog_data hidden-sm">{{$blog->created}}</p>
            <p class="blog_tagi">
                @foreach($tags as $klucz => $tag)
                @if($tag->blogID == $blog->blogID)
                <a href="{{route('blog_tag',['tag'=>str_slug($tag->tag)])}}">#{{$tag->tag}}</a>
                @endif
                @endforeach
            </p>
            {{--<img src="{{Config('url')}}/img/frontend/trainers/{{str_before($blog->instructor_photo,'.')}}.gif"
            alt="{{$blog->instructor_name}}"/>--}}
            @if(file_exists(Config('url').'img/frontend/trainers/'.str_before($blog->instructor_photo,".").'.gif') ==
            true)
            <img src="{{Config('url')}}/img/frontend/trainers/{{str_before($blog->instructor_photo,'.')}}.gif"
                alt="{{$blog->instructor_name}}" />
            @else
            <img src="{{Config('url')}}/img/frontend/trainers/big/{{str_before($blog->instructor_photo,'.')}}.jpg"
                style="height:42px;" alt="{{$blog->instructor_name}}" />
            @endif
            <p class="inst_name">{{$blog->instructor_name}}</p>
        </div>
        @endif
        @if(($key>=1) && ($key<5)) @if(env('APP_ENV')=='production' ) <div class="blog_box col-sm-6"
            style="background-image:url({{Config('url')}}/public/img/frontend/blog/{{$blog->picture}});background-size:cover;background-repeat:no-repeat; background-position:top; ">

            @else
            <div class="blog_box col-sm-6"
                style="background-image:url({{Config('url')}}/img/frontend/blog/{{$blog->picture}});background-size:cover;background-repeat:no-repeat; background-position:top; ">
                @endif
                <a href="{{route('blog')}}/post/{{$blog->blogID}}/{{str_slug($blog->title)}}">
                    <h2>{{$blog->title}}</h2>
                    <p class="blog_data">{{$blog->created}}</p>
                    <div class="blogT_info">
                        @if(file_exists(Config('url').'img/frontend/trainers/'.str_before($blog->instructor_photo,".").'.gif')
                        == true)
                        <img src="{{Config('url')}}/img/frontend/trainers/{{str_before($blog->instructor_photo,'.')}}.gif"
                            alt="{{$blog->instructor_name}}" />
                        @else
                        <img src="{{Config('url')}}/img/frontend/trainers/big/{{str_before($blog->instructor_photo,'.')}}.jpg"
                            style="height:42px;" alt="{{$blog->instructor_name}}" />
                        @endif
                        <p>{{$blog->instructor_name}}</p>
                    </div>
                    {{--<img src="{{Config('url')}}/img/frontend/trainers/{{str_before($blog->instructor_photo,'.')}}.gif"
                    alt="{{$blog->instructor_name}}"><p>{{$blog->instructor_name}}</p>
            </div>--}}
            </a>
    </div>
    @endif
    @if($key>=5)
    <div class="blogOther">
        <a href="{{route('blog')}}/post/{{$blog->blogID}}/{{str_slug($blog->title)}}">
            <h3>{{$blog->title}}</h3>
        </a>
        <div class="blogOther_author">
            {{--<img src="{{Config('url')}}/img/frontend/trainers/{{str_before($blog->instructor_photo,'.')}}.gif"
            alt="{{$blog->instructor_name}}">--}}
            @if(file_exists(Config('url').'img/frontend/trainers/'.str_before($blog->instructor_photo,".").'.gif') ==
            true)
            <img src="{{Config('url')}}/img/frontend/trainers/{{str_before($blog->instructor_photo,'.')}}.gif"
                alt="{{$blog->instructor_name}}" />
            @else
            <img src="{{Config('url')}}/img/frontend/trainers/big/{{str_before($blog->instructor_photo,'.')}}.jpg"
                style="height:42px;" alt="{{$blog->instructor_name}}" />
            @endif
            <p>{{$blog->instructor_name}}</p>
        </div>
        <p class="blog_data">{{$blog->created}}</p>
        <a href="{{route('blog')}}/post/{{$blog->blogID}}/{{str_slug($blog->title)}}">
            <p class="blog_lid">
                {!! $blog->lid !!}
            </p>
        </a>
        <p class="blog_tagi">
            {{--  {{$tags}} --}}
            @foreach($tags as $klucz => $tag)
            @if($tag->blogID == $blog->blogID)
            <a href="{{route('blog_tag',['tag'=>str_slug($tag->tag)])}}">#{{$tag->tag}}</a>
            @endif
            @endforeach
        </p>
    </div>
    @endif
    @endforeach
    {{--  {{var_dump($tags)}} --}}
</div>
@endsection