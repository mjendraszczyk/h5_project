@extends('frontend.main.cms')
@section('menu')
@include('frontend.cms.blog.menu')
@endsection
@section('menu_extends')
@include('frontend.cms.blog.menu_extends')
@endsection
@section('content')
@foreach($blogs as $blog)

@section('title')
HIGH5 Training Group Blog: {{$blog->title}}
@endsection
@section('description')
{{$blog->lid}}
@endsection
@section('keywords')
Szkolenia otwarte, szkolenia zamknięte, coaching, rozwój, zarządzanie, warszawa, projekty, procesy, HR, negocjacje, sprzedaż
@endsection

<h1 class="antonioBig topPage red">{{$blog->title}}</h1>
<p class="tagi">
@foreach($tags as $key=>$tag) 
<a href="{{route('blog_tagi')}}/{{str_slug($tag->tag)}}">{{$tag->tag}}</a>
@endforeach
</p>
<div class="blog_trainer hidden-xs">
    <img alt="{{$blog->instructor_name}}" src="{{Config('url')}}/img/frontend/trainers/big/{{$blog->instructor_photo}}">
    <img alt="{{$blog->instructor_name}}" src="{{Config('url')}}/img/frontend/trainers/big/{{str_before($blog->instructor_photo,'.')}}.gif">
</div>
<p class="lid">
{{$blog->lid}}
</p>
<div class="blog_tekst"> 
        {!! $blog->tekst !!}
                    </div>
         <p class="published">opublikowano: {{$blog->created}}<br>kontakt: {!!$blog->instructor_fakemail!!}</p>
            <p>&nbsp;</p>
            <div class="fb-comments" data-href="{{route('blog')}}/post/{{$blog->blogID}}/{{str_slug($blog->title)}}" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
            @endforeach
@endsection