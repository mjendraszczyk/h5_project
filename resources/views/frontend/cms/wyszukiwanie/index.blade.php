@extends('frontend.main.search') 
@section('content')
<h1 class="antonioBig Lmargin red">Wyniki wyszukiwania</h1>
<h2 class="antonioBig Lmargin">{{$fraza}}</h2>
{{--  {{$szukaj_szkolenia}}  --}}
<div class="toright">
    <ul id="trainingList">
        @if($szukaj_szkolenia != "0")
@foreach($szukaj_szkolenia as $szkolenie)
<li><a href="{{route('szkolenia_strona',['id'=>$szkolenie->categoryID,'title'=>str_slug($szkolenie->category_title),'id_kurs'=>$szkolenie->trainingID,'title_kurs'=>str_slug($szkolenie->training_title)])}}" title="{{$szkolenie->training_title}}">{{$szkolenie->training_title}}
        <span class="trainingLid">
        {{$szkolenie->training_lid}}
        </span>    
</a></li>
@endforeach
@else
<h3>
        Przykro nam, ale nie znaleźliśmy w naszych szkoleniach nic pasującego do Twojego zapytania
</h3>
@endif
</ul>
</div>
@endsection
