<ol>
    <li><span class="headerM">Tylko konkrety</span><br>
        Na szkoleniu dostaniesz to, czego potrzebujesz. Konkretnie i praktycznie. Trenerzy skoncentrowani są na rozwiązywaniu realnych problemów. Przykłady wzięte z życia, ćwiczenia, które utrwalają nową wiedzę, wsparcie dla Twojego rozwoju. Tylko skuteczne narzędzia.
        Unikalna metodologia Planu Działań Wdrożeniowych  i system double loop pomoże Ci zrealizować swoje cele po szkoleniu -  wtedy, kiedy najbardziej tego potrzebujesz.</li>
    <li><span class="headerM">Proces</span><br>
        Szkolenie nie kończy się na szkoleniu. To proces.
        Dbając o efektywność  szkoleń otwartych zapewniamy uczestnikom  udział  w całym procesie szkoleniowym.</li>
    <li><span class="headerM">Nie zapominamy</span><br>
        O nowo zdobytą wiedzę i umiejętności  musimy się troszczyć: regularnie  powtarzać i wykorzystywać w codziennej pracy. Chcemy Ci w tym pomóc. Przygotowaliśmy specjalną platformę on-linę, dzięki której będziemy z Tobą w kontakcie i  będziemy przysyłać Ci, Uczestniku  najważniejsze treści  szkolenia, inspiracje, casy.</li>
    <li><span class="headerM">Pewne</span><br>
        Robimy wszystko, żeby nie odwoływać  szkoleń
        Jeśli przy szkoleniu widzisz ikonę „pewny”  tzn., że szkolenie odbędzie się w miejscu i o czasie – tak jak zaplanowałaś. Tylko w sporadycznych, wyjątkowych  wypadkach, nie z naszej winy, szkolenie ze statusem pewny może być przeniesione na inny termin.<br>

        Jeśli natomiast Ty z  ważnych przyczyn nie możesz przyjść na zarezerwowane szkolenie, nie poniesiesz z tego powodu żadnych konsekwencji – zaproponujemy Ci inny, dogodny termin szkolenia.</li>
    <li><span class="headerM">Komfort</span><br>
        Jesteśmy w samym centrum Warszawy, w sąsiedztwie kompleksu Warsaw Spire.
        Tylko 5 minut od metra, tramwaju i tuż przy parking strzeżonym. Kreatywna i wygodna przestrzeń specjalnie dla Państwa została zaprojektowane przez pracownię Modelina i zyskała już uznanie na świecie ( projekt ukaże się w Modern Office Design wydawnictwa Images Publishing) Na lunch zapraszamy do restauracji Stixx.  Komfort to też liczba Uczestników ma szkoleniu. Możesz mieć pewność, że będziesz w grupie nie większej niż 12 osób, a to znaczy, że będziesz miał  przestrzeń dla siebie, czas na swoje pytania I rozwiązania tylko dla Ciebie.
    </li>
</ol>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>