@extends('frontend.main.cms')
@section('menu')
@section('title_menu')
Szkolenia otwarte
@endsection
@include('frontend.cms.szkolenia.menu')
@include('frontend.cms.szkolenia.tagCloud')
@endsection
@section('content')

<div class="toright" style="padding-top: 0;">
    <ul id="trainingList">
        @foreach($szkolenia as $key => $szkolenie)

        @if($key == 0)
        <h1 class="antonioBig Lmargin red">{{$szkolenie->category_title}}</h1>

        @section('title')
        Kategoria szkoleń: {{$szkolenie->category_title}} Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last)
        @else,@endif @endforeach
        @endsection
        @section('description')
        Szkolenia zamknięte w kategoriach: sprzedaż, negocjacje, zarządzanie zespołem, zarządzanie procesami, rozwój
        osobisty, zarządzanie projektami realizowane w formule zamkniętej (in-company)@endsection
        @section('keywords')
        Szkolenia otwarte, szkolenia zamknięte, coaching, rozwój, zarządzanie, warszawa, szkolenia dla firm, projekty,
        procesy, HR, negocjacje, sprzedaż
        @endsection
        @endif
        <li>
            @if($szkolenie->training_type == '3' || $szkolenie->training_type == '1')
            @if($szkolenie->training_news == 1)
            <img src="{{asset('/img/frontend/new.gif')}}" alt="nowość" class="trainingNews">
            @endif
            <a
                href="{{route('szkolenia_strona',['id'=>$szkolenie->categoryID,'title'=>str_slug($szkolenie->category_title),'id_kurs'=>$szkolenie->trainingID,'title_kurs'=>str_slug($szkolenie->training_title)])}}">
                <h4>{{$szkolenie->training_title}}</h4>
                <span class="trainingLid">{{$szkolenie->training_lid}}</span>
            </a>
            @endif
        </li>
        @endforeach
    </ul>
</div>
@endsection