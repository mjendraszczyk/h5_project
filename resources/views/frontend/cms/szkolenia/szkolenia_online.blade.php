@extends('frontend.main.cms')
@section('title_menu')
Szkolenia online
@endsection
@section('menu')
@include('frontend.cms.szkolenia.menu_online')
@endsection
@section('content')
@section('title')
HIGH5: Szkolenia online, konsultacje, coaching Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif
@endforeach
@endsection
@section('description')
Szkolenia online w kategoriach: sprzedaż, negocjacje, zarządzanie zespołem, zarządzanie procesami, rozwój osobisty
realizowane jako szkolenia indywidualne lub konsultacje Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last)
@else,@endif @endforeach
@endsection
@section('keywords')
szkolenia online, szkolenia indywidualne, konsultacje biznesowe, szkolenia biznesowe, program rozwoju, coaching
@endsection
<h1 class="antonioBig red">Szkolenia on-line</h1>
<div class="toright">
  <div id="OfertySide">
    <h2 class="antonioBig Lmargin white">Masz pytania ?</h2>
    <p>(+4822) 824 50 25 <br>
      <a href="mailto:biuro@high5.pl">
        biuro@high5.pl</a></p>
    <p>Zapytania o szkolenia on-line: <a href="mailto:renata.jerzowska@high5.pl"
        class="antonioSmall">renata.jerzowska@high5.pl</a>
    </p>
    <p class="antonioSmall">tel: 602 608 269</p>
  </div>
  <p>Nasze szkolenia on-line są w pełni interaktywne i angażujące, oparte na wieloletnim doświadczeniu naszych trenerów.
  </p>
  <p>Nie wymagają od Państwa żadnego specjalnego oprogramowania, wystarczy dostęp Uczestników do Internetu. </p>
  <p>Szkolenia on-line realizujemy w <strong>formie zamkniętej (in-company) i otwartej</strong>. </p>
  <p>Tematy szkoleń otwartych on-line znajdziesz w <a
      href="https://www.high5.pl/szkolenia/szkolenie/21/szkolenia-on-line">menu po lewej stronie.</a> </p>
  <p>Terminy szkoleń otwartych on-line znajdziesz bezpośrednio przy opisie szkolenia, lub w zakładce "<a
      href="https://www.high5.pl/terminy/21/szkolenia-on-line">Terminy</a>".</p>
  <p> Nasze rozwiązania realizujemy w formułach on-line: </p>
  <ul>
    <li> na wewnętrznej platformie klienta, </li>
    <li> na zewnętrznej dowolnie wybranej przez Klienta,</li>
    <li> przez naszą platformę do szkoleń on-line. </li>
  </ul>
  <p>Działania on-line realizujemy w zależności od Państwa potrzeb w różnorodnych formach: </p>
  <ul>
    <li>live onlin1e, </li>
    <li>webinary, </li>
    <li>podcasty i videocasty, </li>
    <li>wideo-konferencje, </li>
    <li>blanded learning. </li>
  </ul>
  <h2>Formy szkoleń on-line </h2>

  <img src="{{Config('url')}}/img/frontend/online1.png" alt="laptop" style="float: right">
  <h3 class="red">Szkolenie live on-line</h3>
  <p>Szkolenia i treningi grupowe przeprowadzamy w formie interaktywnych spotkań online w małych i średnich grupach (do
    25 os.), podczas których trener utrzymuje kontakt z uczestnikami za pomocą czatu i wideokonferencji. Uczestnicy
    angażowani są w różnego rodzaju ćwiczenia, dyskusje, ankiety, analizę filmów oraz case’ów, a nawet zadania na
    flipcharcie. </p>
  <p>Szkolenie może być również nagrane i w dowolnym momencie odtwarzane przez Uczestników. </p>
  <blockquote>Proces przygotowania szkolenia on-line czyli analiza potrzeb, zawartość merytoryczna, materiały jakie
    otrzymują Uczestnicy jest taka sama jak w przypadku szkoleń stacjonarnych. Dzięki krótkiej i angażującej formie
    (zwykle 2-3 h dziennie) są bardzo skondensowane i pozwalają uczestnikom zdobywać niezbędną wiedzę i umiejętności,
    bez zbytniego odrywania się od pracy. </blockquote>
  <h3 class="red">Webinarium </h3>
  <p>Szkolenie w dużej grupie (nawet do 1000 os.), nastawione na przekazanie najważniejszych treści i dzielenie się
    wiedzą uczestnikami. </p>
  <p>Takie rozwiązanie daje również możliwość interakcji z uczestnikami np. za pomocą czatu moderowanego. </p>
  <h3 class="red">Transmisje on-line </h3>
  <p>Nie masz możliwości skorzystania z naszego szkolenia stacjonarnego? </p>
  <p>Dzięki prowadzonej przez nas transmisji online dajemy możliwość czynnego udziału w naszych szkoleniach
    stacjonarnych. Każde szkolenie stacjonarne jest transmitowane przez nas online. Uczestnicy mogą aktywnie brać udział
    w realizowanych ćwiczeniach i prowadzonych dyskusjach lub wybrać inną formę: być wolnymi słuchaczami. </p>
  <h3 class="red">Podcast </h3>
  <p>Krótka i atrakcyjna forma wykładów lub wywiadów z zakresu prawa, zarządzanie, psychologii, HR, procesów, projektów,
    sprzedaży. Dzięki temu narzędziu słuchacz zdobywa nową wiedzę i inspiracje, które może wykorzystać w praktyce, bez
    wychodzenia z domu, powracać w dowolnym momencie i słuchać ich wielokrotnie. </p>
  <h3 class="red">Videocast </h3>
  <p>Sam dźwięk nie jest wystarczający? Nasze podcasty realizujemy również w wersji video, dzięki czemu przekazywane
    treści są jeszcze bardziej atrakcyjne dla odbiorcy. Nie tylko dźwięk, ale też wizerunek eksperta wpłynie na odbiór
    oraz poziom zaufania dla przekazywanych treści. </p>
  <h3 class="red">Blanded learning</h3>
  <p> Forma nauki łącząca w sobie tradycyjne podejście (bezpośredni kontakt z trenerem) z aktywnościami prowadzonymi
    zdalnie. </p>
  <h3 class="red">Szkolenia indywidualne on-line </h3>
  <p>Szkolenia indywidualne on-line umożliwiają: </p>
  <ul>
    <li>pracę on-line w dowolnym terminie i miejscu, </li>
    <li>precyzyjne dostosowanie programu i czasu szkolenia do oczekiwań Uczestnika, </li>
    <li>łączenie wielu zagadnień w jednym szkoleniu, </li>
    <li>okazję do bardzo intensywnego, praktycznego treningu umiejętności w relacji indywidualnej z trenerem, </li>
    <li>pracę z narzędziami diagnostyczno-rozwojowymi, wybór eksperta/trenera. </li>
  </ul>
  <h2>LAPTOP, SMARTFON lub TABLET </h2>
  <p>Dzięki stosowanym przez nas narzędziom, do realizacji szkoleń potrzebny jest jedynie dostęp do Internetu oraz
    głośnik i mikrofon (dla chętnych osób, które chcą wziąć udział w dyskusji). </p>
  <p>Przed szkoleniem każdy z Uczestników otrzymuje <strong>dokładną instrukcję korzystania z danego narzędzia</strong>.
  </p>
  <h2>Jak wygląda szkolenie on-line? </h2>
  <p>Podczas szkolenia on-line trener: - jest widziany i słyszany przez Uczestników -udostępniania swój pulpit
    Uczestnikom (z prezentacją, filmami, tabelami, narzędziami…) -wykorzystuje dodatkowo wiele nowatorskich narzędzi
    m.in.: </p>
  <ul>
    <li>tablicę interaktywną, która umożliwia kreatywną współpracę z Uczestnikami szkolenia, </li>
    <li>ankiety i testy – przeprowadzane w czasie rzeczywistym, z szybkimi wynikami,</li>
    <li>czat prywatny - dzięki, któremu możliwe są indywidualne rozmowy pomiędzy trenerem a uczestnikiem, </li>
    <li>czat moderowany, który umożliwia prowadzenie rozmów tekstowych z publicznością bez przerywania trwającej
      prezentacji. P</li>
  </ul>
  <p>Trener przekazuje uczestnikom przed szkoleniem materiały szkoleniowe i zestaw ćwiczeń. </p>
  <p>Proponowane narzędzia sprawią, że szkolenie on-line będzie bardziej interaktywne i zajmujące, a także pozwoli
    uczestnikom na bieżąco wyjaśniać wątpliwości oraz otrzymywać informację zwrotną i śledzić reakcje w czasie
    rzeczywistym. Dzięki tym rozwiązaniom efektywność szkolenia jest równie wysoka, jak szkolenia przeprowadzanego w
    trybie stacjonarnym. </p>
  <p>Dostęp do szkolenia on-line zabezpieczamy hasłem, a przed szkoleniem wykorzystujemy spersonalizowaną stronę
    rejestracji oraz wysyłamy zaproszenia dla Uczestników w formie powiadomienia e-mailowego. </p>
  <p><strong>Uczestnicy szkoleń on-line otrzymują certyfikaty ukończenia szkolenia. </strong></p>
  <p>&nbsp;</p>
</div>
@endsection