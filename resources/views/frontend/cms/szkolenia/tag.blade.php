@extends('frontend.main.cms')
@section('menu')
@section('title_menu')
Szkolenia otwarte
@endsection
@include('frontend.cms.szkolenia.menu')
@endsection
@section('content')


{{--  {{$tag}}  --}}

    @foreach($tag as $key => $t)
    @if($loop->first)
    <h1 class="antonioBig Lmargin red">{{$t->category_title}}</h1>
    <h2 class="antonioBig Lmargin">#{{$tag_training}}</h2>
    <ul id="trainingList">
    @endif
        <li>
            <a style="text-decoration: none; color: #333;" href="{{route('szkolenia_strona',['id'=>$t->categoryID,'title'=>str_slug($t->category_title),'id_kurs'=>$t->trainingID,'kurs_title'=>str_slug($t->training_title)])}}" title="Biuro Projektów -warsztaty">
            <h4>{{$t->training_title}}</h4>
            <span class="trainingLid">{{$t->training_lid}}</span></a></li>
               @if($loop->last)
</ul>
@endif
            @endforeach
         
@endsection