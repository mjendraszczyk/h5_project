@extends('frontend.main.cms')
@section('menu')
@section('title_menu')
Szkolenia otwarte
@endsection
@include('frontend.cms.szkolenia.menu')
@endsection
@section('content')
<h3>Szkolenia otwarte</h3>
<div class="toright">
        <h2 class="antonioSmall red">A oto kilka powodów, </h2>
        <p><strong>dla których już </strong><strong>13041*</strong><strong> osób  wybrało szkolenie otwarte w High5 Training Group:</strong></p>
		<ul>
			<li>dostarczymy Ci tylko skutecznych i sprawdzonych narzędzi, zainspirujemy  i rozwiążemy problem,</li>
			  <li>spotkasz u nas samych ekspertów i wybitnych trenerów,</li>
			  <li>uważamy, że szkolenie nie kończy się na samym szkoleniu - dajemy Ci dodatkowo: e-mentoring,  konsultacje i wsparcie,</li>
			  <li>nie odwołujemy szkoleń  - jeśli szkolenie ma status pewne tzn., że odbędzie się w zaplanowanym miejscu i czasie, </li>
			  <li>dbamy o Twój komfort  - szkolenia są w samym sercu Warszawy, w sąsiedztwie hotelu Hilton i  kompleksu Warsaw Spire (7 min. spacerem od Metra Daszyńskiego), w specjalnie zaprojektowanej  kreatywnej przestrzeni  (<em>przestrzeń wyróżniona w publikacji: Big  Design for Small Workspace (wyd. The Images Publishing Group)),</em></li>
		  <li>zapraszamy do restauracji - na lunch dostaniesz to, co tylko zechcesz, wybór należy do  Ciebie, </li>
		  <li>lubimy zaskakiwać: i tym, że powiemy Ci jak bajki Ezopa zwiększą Twoją  skuteczność  i tym, że uprasujemy Ci  koszulę (jeśli tylko zechcesz)</li>
		  <li>robimy naprawdę bardzo dobre szkolenia.</li>
    </ul>
	<p><strong>Sprawdź nas i  przekonaj się sam!    </strong></p>
        
        <div id="obrazekszkolenia">
          <p><img src="{{Config('url')}}/img/frontend/wejscie.jpg" width="100%" alt="High5 Warszawa wejscie"></p>
            <span id="cyttresc">
              <p>Dzisiejsze oczywistości zawsze stają się jutrzejszymi absurdami</p>
              <p id="cytpodpis"><em>Peter F. Drucker</em></p>
        </span>
        </div>
        <h2 class="antonioSmall red">Proces rozwoju&nbsp; uczestnika szkolenia otwartego</h2>
        <p>Dbając o efektywność&nbsp;  szkoleń  zapewniamy Uczestnikom&nbsp;  udział&nbsp; w całym procesie szkoleniowym.<br>
Przed  szkoleniem:</p>
    <ul>
      <li>przeprowadzimy indywidualną diagnozę potrzeb szkoleniowych,</li>
      <li>ustalimy Twoje cele na najbliższe szkolenie,</li>
      <li>przekażemy Ci pigułkę wiedzy i materiał,&nbsp;który pozwoli przygotować  się do szkolenia,</li>
    </ul>
    <p>Podczas  szkolenia:</p>
    <ul>
      <li>otrzymasz praktyczną wiedzę i nowe umiejętności,</li>
      <li>sprawdzisz i zastosujesz tylko skuteczne&nbsp; techniki i narzędzia,</li>
      <li>będziesz miał możliwość pracy na własnych przykładach,</li>
      <li>podejmiesz decyzję co i kiedy wprowadzisz w życie,</li>
    </ul>
    <p>Po  szkoleniu:</p>
    <ul>
      <li>możemy monitorować wdrożenie Twoich planów rozwojowych,</li>
      <li>zagwarantujemy 60 dniowy  e-mentoring  z trenerem,</li>
      <li>przekażemy one-page czyli szkolenie w pigułce,</li>
      <li>otrzymasz dodatkowe bloki wiedzy&nbsp; (nawiązujące do planów  rozwojowych&nbsp; i umiejętności rozwijanych na szkoleniu).</li>
    </ul>
    <p>Tematyka szkoleń otwartych  jest bardzo szeroka i podzielona została na kategorie, które znajdziesz <strong>w menu po  lewej stronie.<br>
      </strong> <br>
      Terminy szkoleń otwartych  znajdziesz bezpośrednio przy opisie szkolenia, lub w zakładce &quot;<u><a href="https://www.high5.pl/terminy">Terminy</a></u>&quot;.</p>
    </div>
@endsection