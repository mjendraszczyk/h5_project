@extends('frontend.main.cms')
@section('menu')
@section('title_menu')
Szkolenia zamknięte
@endsection
@include('frontend.cms.szkolenia.menu_zamkniete')
@endsection
@section('content')

 <div class="toright" style="padding-top: 20px;">
        <h1 class="antonioBig Lmargin">Szkolenia zamknięte</h1>
     @foreach($szkolenia as $key => $szkolenie)
     @if($key == 0)

@section('title')
Szkolenia zamknięte Szkolenia: {{$szkolenie->category_title}} Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
Strona firmy szkoleniowej: szkolenia dla firm otwarte i szkolenia zamknięte, coaching. Rozwój osobisty, negocjacje, zarządzanie projektami i procesami, Szkolenia dla managerów i dla działów HR Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('keywords')
Szkolenia otwarte, szkolenia zamknięte, coaching, rozwój, zarządzanie, warszawa, szkolenia dla firm, projekty, procesy, HR, negocjacje, sprzedaż
@endsection
     <h2 class="antonioBig red">{{$szkolenie->category_title}}</h2>

     <div id="OfertySide">
            <h2 class="antonioBig Lmargin white">Masz pytania ?</h2>
            <p>(+4822) 824 50 25 <br>
              <a href="mailto:biuro@high5.pl">biuro@high5.pl</a></p>
              <p>Zapytania o szkolenia zamknięte: <a href="mailto:renata.jerzowska@high5.pl" class="antonioSmall">renata.jerzowska@high5.pl</a></p>
              <p class="antonioSmall">tel: 602 608 269</p>
            </div>
     <p class="firstInfo">Poniżej przedstawiamy zakres tematów jakie realizujemy w formule zamkniętej. Szczegółowe programy szkoleń przygotowujemy po poznaniu Państwa potrzeb szkoleniowych.
            Jeśli chcielibyście Państwo otrzymać przykładowy program, prosimy o kontakt do koordynatora szkoleń zamkniętych .</p>
<ul id="trainingCList">
  @if(array_key_exists($szkolenie->categoryID,$lista))
    @foreach($lista[$szkolenie->categoryID] as $k => $l)
    <li>
       {{$lista[(int)$szkolenie->categoryID][$k]}}
      </li>
    @endforeach
    @else 
    @endif
 
@endif
     {{--  <li>
           {{$szkolenie->training_title}}
      </li>  --}}
     @endforeach

</ul>
</div>



@endsection