@foreach($cattrain as $cat)
@if(App\Http\Controllers\SzkoleniaController::qtySzkoleniaFromCat($cat->categoryID,'1') > 0)
<li>
    <a
        href="{{route('szkolenia_kategoria',['id'=>$cat->categoryID,'title'=>str_slug($cat->category_title)])}}">{{$cat->category_title}}</a>
</li>
@endif
@endforeach