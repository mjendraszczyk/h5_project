@extends('frontend.main.cms')
@section('menu')
@section('title_menu')
Szkolenia zamknięte
@endsection
@include('frontend.cms.szkolenia.menu_zamkniete')
@endsection
@section('content')
@section('title')
HIGH5: Szkolenia zamknięte dla firm: Warszawa Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
Szkolenia zamknięte w kategoriach: sprzedaż, negocjacje, zarządzanie zespołem, zarządzanie procesami, rozwój osobisty, zarządzanie projektami realizowane w formule zamkniętej (in-company)
@endsection
@section('keywords')
szkolenia zamknięte, szkolenia in-company, szkolenia biznesowe, program rozwoju
@endsection
<h3>Szkolenia zamknięte</h3>
<div class="toright">
        <h2 class="antonioBig red">Dedykowane, wewnętrzne, in-company</h2>
<h4 class="antonioSmall">Dlaczego szkolenia w High5?</h4>
<div id="OfertySide">
<h2 class="antonioBig Lmargin white">Masz pytania ?</h2>
<p>(+4822) 824 50 25 <br>
    <a href="mailto:biuro@high5.pl">
        biuro@high5.pl</a></p>
<p>Zapytania o szkolenia zamknięte: <a href="mailto:renata.jerzowska@high5.pl" class="antonioSmall">renata.jerzowska@high5.pl</a>
</p>
<p class="antonioSmall">tel: 602 608 269</p>
</div>
<p>Oferujemy Państwu  wsparcie&nbsp;w realizacji&nbsp; szkoleń zamkniętych oraz  długoterminowych projektów rozwojowych.</p>
<p> Wartość oferowanych przez nas rozwiązań  szkoleniowych opiera się na:</p>
<ul>
  <li>zapewnieniu&nbsp;<strong>rezultatów</strong>&nbsp;(osiąganiu celów biznesowych, wzroście  wskaźników wiodących, zmianie postaw i  motywacji&nbsp; wśród Uczestników),</li>
  <li>skoncentrowaniu na Państwa&nbsp;<strong>potrzebach</strong>&nbsp;biznesowych,</li>
  <li>kompleksowym&nbsp;<strong>wsparciu</strong>&nbsp; (możecie Państwo na nas liczyć  nie tylko przy realizacji szkoleń, ale i przy wdrożeniach nowych pracowników,  badaniu &nbsp;potencjału obecnych i przyszłych pracowników,&nbsp;  coachingu&nbsp; a także logistyce szkoleń),</li>
  <li>przywiązywaniu dużej wagi do<strong>&nbsp;transferu wiedzy i umiejętności</strong>,</li>
  <li>profesjonalnej i kompetentnej kadrze - ekspertach w swoich dziedzinach o  doskonałych umiejętnościach trenerskich,</li>
  <li><strong>gwarancję satysfakcji&nbsp;</strong>uczestników  szkoleń (średni&nbsp; wskaźnik Net Promoter Score w roku 2017 wynosił (NPS) 94%),</li>
  <li>innowacyjnych formach realizacji (prowadzimy projekty, które łączą w  sobie diagnostykę,  konsultacje, symulacje,  gry strategiczne i szkoleniowe, blended- learning, action learning, multimedialne  pigułki wiedzy, dedykowane platformy on-line)</li>
</ul>

<img class="img-responsive" src="{{asset('img/frontend/sala02.jpg')}}" width="100%" alt="lodka">
<h4 class="antonioBig red">Proces szkoleniowy </h4>
<p>Pomagamy na każdym etapie</p>

<ul id="zakladkiProces">
<li id="closedTabS_1" class="active"><a href="javascript:tabSwitch_2(1, 4, 'closedTabS_', 'closedTabSContent_');">Badanie<br>potrzeb</a>
</li>
<li id="closedTabS_2"><a href="javascript:tabSwitch_2(2, 4, 'closedTabS_', 'closedTabSContent_');">Budowanie<br>strategii</a>
</li>
<li id="closedTabS_3"><a href="javascript:tabSwitch_2(3, 4, 'closedTabS_', 'closedTabSContent_');">Realizacja<br>projektu</a>
</li>
<li id="closedTabS_4"><a href="javascript:tabSwitch_2(4, 4, 'closedTabS_', 'closedTabSContent_');">Ocena<br>wyników</a>
</li>
</ul>

<div id="closedTabSContent_1">

<h5 class="antonioSmall">Krok I</h5>
<h5 class="antonioSmall red">Diagnoza i analiza potrzeb szkoleniowych </h5>
<p>  W ramach diagnozy potrzeb  przeprowadzamy:</p>
<ul>
  <li>wywiady z uczestnikami, wywiady z opiekunami/sponsorami projektów,</li>
  <li>obserwacje uczestniczące,</li>
  <li>testy, ankiety, kwestionariusze i arkusze samooceny,</li>
  <li>grupy fokusowe,</li>
  <li>analizę heurystyczną,</li>
  <li>badania &bdquo;tajemniczy klient&rdquo;.</li>
</ul>
<p><strong><em>&bdquo;Czasem trzeba zrobić krok w  tył, aby z perspektywy zobaczyć pełniejszy obraz&rdquo;</em></strong></p>
<p>&nbsp;</p>
</div>
<div id="closedTabSContent_2">
<h5 class="antonioSmall">Krok II</h5>
<h5 class="antonioSmall red"> Formułowanie strategii szkoleniowej</h5>
<p>To czas w procesie  szkoleniowym, w którym:</p>
<ul>
  <li>definiujemy oczekiwane cele biznesowe, wskaźniki wiodące oraz sposoby  ich monitorowania,</li>
  <li>opracowujemy strukturę i szczegółowe programy szkoleń dostosowane do tabeli  celów,</li>
  <li>proponujemy strukturę merytoryczną formularza PDW,</li>
  <li>przedstawiamy &nbsp;ekspertów-trenerów realizujących szkolenie,</li>
  <li>opracowujemy &nbsp;kalendarium szkoleń (projekty długoterminowe),</li>
  <li>planujemy logistykę szkoleń.</li>
</ul>
<p>&nbsp;</p>
</div>
<div id="closedTabSContent_3">
<h5 class="antonioSmall">Krok III </h5>
<h5 class="antonioSmall red">Realizacja projektu </h5>
<p>Projekt szkoleniowy:</p>
<ul>
  <li>oparty jest  zawsze o analizę  potrzeb i dostosowany do postawionych celów,</li>
  <li>osadzony w realiach firmy,&nbsp;</li>
  <li>skoncentrowany na dostarczaniu skutecznych narzędzi i wspólnym  wypracowywaniu rozwiązań,</li>
  <li> zawsze wzmocniony indywidualnym  arkuszem rozwoju, multimedialnymi pigułkami wiedzy i fiszkami.</li>
</ul>
<p><strong>Projekt  szkoleniowy nie  kończy się na samym  szkoleniu. To dopiero początek pracy.</strong><br>
  Proponujemy:</p>
<ul>
  <li>sesje follow up,</li>
  <li>grupowe spotkania wdrożeniowe Uczestników z  trenerem,</li>
  <li>interaktywne bloki wiedzy&nbsp;(nawiązujące  do planów rozwojowych&nbsp; i umiejętności rozwijanych na szkoleniu)</li>
  <li>monitorowanie wdrożenia&nbsp;  planów rozwojowych,</li>
  <li>coaching przełożonych,</li>
  <li>e-mentoring,</li>
  <li>komunikację z Uczestnikami przez platformę  on-line.</li>
</ul>
<p>&nbsp;</p>
</div>
<div id="closedTabSContent_4">
<h5 class="antonioSmall">Krok IV</h5>
<h5 class="antonioSmall red">Faza wdrożeń i oceny</h5>
<p>Podsumowanie i ocena wyników projektu</h5>
<p>W ramach podsumowania  przygotowujemy zestawienia, które m. in. zawierają:</p>
<ul>
  <li>poziom zaangażowania, zdarzenia krytyczne, </li>
  <li>ocenę stopnia faktycznego wykorzystania  szkolenia na stanowisku pracy,</li>
  <li>ocenę stopnia realizacji pomysłów  wdrożeniowych,</li>
  <li>ocenę zmiany zachowań, </li>
  <li>wnioski na przyszłość z perspektywy firmy (inwestora) jak i Uczestnika  szkolenia.</li>
</ul>
<p><br>
  <strong><em>W ocenie efektów szkoleń stosujemy &nbsp;metody, dzięki którym można  powiązać efekty szkoleń &nbsp;z rezultatami, jakie chcą osiągnąć nasi Klienci  (stosujemy nowy model ewaluacji szkoleń Donalda Kirkpatricka, metodę Success  Case).</em></strong><strong></strong></p>
<p>&nbsp;</p>
</div>
</div>
@endsection