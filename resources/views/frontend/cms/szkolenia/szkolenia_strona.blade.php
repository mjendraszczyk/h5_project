@extends('frontend.main.cms')
@section('menu')
@section('title_menu') Szkolenia online
@endsection

@include('frontend.cms.szkolenia.menu')
@include('frontend.cms.szkolenia.tagCloud') @foreach($szkolenia as $key => $szkolenie)
@if(in_array($szkolenie->trainingID.'.jpg',$array_books))
@if($key == 0)
<h2>Do szkolenia książka gratis</h2>
@endif
<img src="{{asset('img/frontend/books')}}/{{$szkolenie->trainingID}}.jpg" alt="" style="display:block;margin:auto;" />
@else
@endif @endforeach {{-- {{print_r($terms)}} --}}

<h3>Szkolenia w tej kategorii</h3>
<div class="tab-content" style="padding:3px;">
    <div class="tab-pane active" id="terminy-warszawa" data-miasto="Warszawa">
        <ul class="list-unstyled" style="padding: 3px;">
            @foreach($terms as $term)
            @include('frontend.homepage.shortTerms.index') @endforeach
        </ul>
    </div>
    @endsection

    @section('content') @foreach($szkolenia as $key => $szkolenie) @if($key == 0)
    @section('title') {{$szkolenie->training_title}}
    - szkolenie otwarte: Warszawa
    @endsection

    @section('keywords') online, warszawa, @foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif
    @endforeach
    @endsection

    @section('description') {{$szkolenie->training_title}} {{$szkolenie->training_lid}} - szkolenie, online, otwarte:
    Warszawa
    @endsection








    <div id="trainingTitle" class="Lmargin col-sm-12">
        <h1 class="antonioBig red" itemprop="name">
            <span class="overtitle">
                Szkolenie @if($szkolenie->training_type == '3') (stacjonarne / online) @elseif($szkolenie->training_type
                == '2') (online) @else
                (stacjonarne) @endif
            </span> {{$szkolenie->training_title}} </h1>
        @foreach($tag as $key => $t) @if($key==0)
        <h5><a href="{{Config('url')}}/tag/{{str_slug($t->tag)}}">#{{$t->tag}}</a></h5>
        @endif @endforeach
        <h3 style="padding-bottom: 20px; margin-top: -5px">{{$szkolenie->training_lid}}</h3>
    </div>

    <blockquote class="cytSzkolenie">
        @if(!empty($szkolenie->training_cite) && ($szkolenie->training_cite != 0)) @foreach($cite as $cit)
        @if($cit->citeID == $szkolenie->training_cite)

        <!--<img src="{{asset('img/frontend/cite')}}/{{$cit->photo}}" width="80" height="90" class="citeimg" alt="" />-->
        {{$cit->cite_text}}
        <br /> <span style="float:right"> {{$cit->cite_author}}</span> @endif @endforeach @endif
    </blockquote>
    <div class="korzysc col-sm-12">
        @if($szkolenie->training_type != '2')
        {{-- <a
            href="{{route('szkolenia_strona_online',['id'=>$term->categoryID,'title'=>str_slug($term->category_title),'id_kurs'=>$szkolenie->trainingID,'title_kurs'=>str_slug($szkolenie->training_title)])}}"><img
            src="{{Config('url')}}/img/frontend/online.png" alt="szkolenia online" style="float: right"></a> --}}
        @endif
        <h3> Korzyści ze szkolenia</h3>
        {!!$szkolenie->training_advantage!!}
    </div>
    <!--
                                <div class="korzysc col-sm-12">
                                    <h3> Dla kogo?</h3>
                                    {!!$szkolenie->training_target!!}
                                    <br>&nbsp;</div>
                                   -->
    <div class="terminy_szkolenia col-sm-12">
        <div class="col-sm-8" style="border-right: 1px solid #e7e7e7;">
            <ul id="terminy-top" class="list-unstyled white" style="padding: 5px 0px 10px 0px; margin-bottom: 0px;"
                itemscope="" itemtype="https://schema.org/Event">
                <meta itemprop="name" content="{{$szkolenie->training_title}}">
                <span itemprop="performer" content="High5 Training Group"></span>
                <meta itemprop="image" content="https://www.high5.pl/img/frontend/img{{rand(1,5)}}.png" />
                <meta itemprop="description" content="{{$szkolenie->training_lid}}" />
                <span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                    <meta itemprop="priceCurrency" content="PLN">
                    <meta content="{{date('Y-m-d')}}" itemprop="validFrom">
                    <meta itemprop="price" content="{{$szkolenie->training_price}}">
                    <meta content="InStock" itemprop="availability" />
                    <meta itemprop="url"
                        content="{{route('szkolenia_strona',['id'=>$szkolenie->categoryID,'title'=>str_slug($szkolenie->category_title),'id_kurs'=>$term->trainingID,'title_kurs'=>str_slug($szkolenie->training_title)])}}">
                </span>


                @if($hotels->count() == 0)
                <ul class="list-unstyled white" style="padding: 5px 0px 10px 25px; margin-bottom: 0px;">
                    <li>do uzgodnienia</li>
                </ul>

                @else @foreach($terminy as $termin)
                {{-- @if($termin->term_training_type == '1') --}}
                @if($termin->term_closed == 'y')
                <li class="term_closed">
                    @else
                <li>
                    <span itemprop="startDate" style="display:none;">{{$termin->term_start}}</span>
                    <span itemprop="endDate" style="display:none;">{{$termin->term_end}}</span>

                    <span itemprop="location" itemscope="" itemtype="http://schema.org/Place"
                        content="{{$termin->term_place}}">
                        <meta itemprop="name" content="HIGH5 Group sp. z o.o.">
                        <span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
                            <meta itemprop="addressLocality" content="{{$termin->term_place}}">
                            <meta itemprop="addressCountry" content="Polska">
                        </span>
                    </span>
                    @endif {{date('d',strtotime($termin->term_start))}} - {{date('d',strtotime($termin->term_end))}}
                    {{Lang::get('date.month.'.date('n',strtotime($termin->term_end)))}}
                    {{date('Y',strtotime($termin->term_end))}},
                    @if($termin->training_type != '2')
                    {{$termin->term_place}}
                    @else
                    ON-LINE
                    @endif
                    @if($termin->term_state == '2')
                    <img src="{{asset('img/frontend/pewny2.png')}}" /> @endif @if($termin->term_closed == 'y')
                    <img src="{{asset('img/frontend/brak.png')}}" /> @endif
                </li>
                {{-- @endif --}}
                @endforeach @endif

            </ul>
        </div>
        <div class="col-sm-4">
            <a href="{{route('rezerwacja_szkolenia',['id'=>$szkolenie->trainingID])}}"
                style="float: right; color: #ed1c24"><span id="button_terminy" class="antonioSmall"
                    style="line-height: 30px;">Rezerwuj<br>termin</span></a>
        </div>
    </div>

    <div id="program" class="rozwijany col-sm-12">
        <h3>Program szkolenia <span class="prSmaller">{{$szkolenie->training_title}}</span></h3>
        <div class="wrap">
            {!! $szkolenie->training_program !!}
            <div class="gradient"></div>
        </div>
        <div class="read-more"></div>

    </div>
    @if(count($opinie) > 0)
    <div id="opinie" class="rozwijany col-sm-12">
        <!-- <img src="{{asset('img/frontend/stars.png')}}" alt="stars" id="stars">-->
        <div class="wrap" style="height: 100%;">
            <h3 class="antonioBig Lmargin white">Opinie o szkoleniu:</h3>
            <ul class="opinie">

                @foreach($opinie as $opinia)
                <li>
                    {{$opinia->imie}}, {{$opinia->firma}}
                    <br /> {{$opinia->tresc}}
                </li>
                @endforeach

            </ul>

        </div>
        <div class="read-more"></div>
    </div>
    @endif
    <div class="col-sm-12" style="padding-bottom: 20px;">
        <h3>Metody szkoleniowe</h3>
        @php $aktywnosci_szkolenia = explode(";", $szkolenie->training_activities);
        @endphp @foreach($aktywnosci_tab as $key => $aktywnosc)
        @if($key
        < 12) @if(in_array($key, $aktywnosci_szkolenia) && !is_null($szkolenie->training_activities))
            <div class="col-sm-3" style="text-align: center;">
                <img src="{{asset('img/frontend/aktywnosci/')}}/{{$aktywnosc}}" style="height:120px;">
            </div>
            @else

            <div class="col-sm-3" style="text-align: center;">
                <img src="{{asset('img/frontend/aktywnosci/hidden/')}}/{{$aktywnosc}}" style="height:120px;">
            </div>
            @endif @endif @endforeach

    </div>
    {{-- skzolenie stacjonarne --}}
    <div class="szczegoly @if($szkolenie->training_type == '3') col-sm-6 @else col-sm-12 @endif">
        <div class="col-sm-12 cena_zawiera">
            <h3 style="color: #c70c60;">Szkolenie @if(($id_category == 21) || ($szkolenie->training_type == '3') ||
                ($szkolenie->training_type
                == '1')) stacjonarne @endif
            </h3>
            <ul class="list-unstyled list-inline">
                <li>
                    @if(($id_category != 21) && ($szkolenie->training_type != '2'))
                    {{$szkolenie->training_hours/8}} @if((($szkolenie->training_hours)/8) > 1) dni @else dzień @endif
                    @else
                    {{ceil($szkolenie->training_hours/8)}} @if((($szkolenie->training_hours)/8) > 1) dni @else dzień
                    @endif
                    @endif
                    szkolenia ({{$szkolenie->training_hours}}
                    @if($szkolenie->training_hours == 1)
                    godzina
                    @elseif(($szkolenie->training_hours > 1) && ($szkolenie->training_hours < 5)) godziny @else godzin
                        @endif )</li> <li>skrypt szkoleniowy @if(($id_category == 21) || ($szkolenie->training_type ==
                        '2')) PDF @endif</li>
                <li>materiały dodatkowe @if(($id_category == 21) || ($szkolenie->training_type == '2')) ON-LINE @endif
                </li>
                @if(($id_category != 21) && ($szkolenie->training_type != '2'))
                <li>certyfikat</li>
                <li>monitorowanie zadań wdrożeniowych</li>
                <li>60 dniowy e-mentoring</li>
                <li>lunch (restauracja)</li>
                <li>serwis kawowy premium</li>
                @endif
            </ul>
        </div>
        <div class="cena_szkolenie cena_zawiera1" style="position: absolute;">
            <p class="vat" style="display:inline-block;">Cena:</p>
            <h3 style="display:inline-block;">{{$szkolenie->training_price}} zł</h3>
            <p class="vat" style="display:inline-block;">+ VAT</p>
        </div>
    </div>

    {{-- szkolenie online --}}
    <div class="szczegoly @if($szkolenie->training_type == '3') col-sm-6 @else col-sm-12 @endif">
        <div class="col-sm-12 cena_zawiera">
            <h3 style="color: #ed1c24;">Szkolenie @if(($id_category == 21) || ($szkolenie->training_type == '2') ||
                ($szkolenie->training_type
                == '3')) online @endif zawiera
            </h3>
            <ul class="list-unstyled list-inline">
                <li>
                    @if(($id_category != 21) && ($szkolenie->training_type != '2'))
                    {{$szkolenie->training_hours1/8}} @if((($szkolenie->training_hours1)/8) > 1) dni @else dzień @endif
                    @else
                    {{ceil($szkolenie->training_hours1/8)}} @if((($szkolenie->training_hours1)/8) > 1) dni @else dzień
                    @endif
                    @endif
                    szkolenia ({{$szkolenie->training_hours1}}
                    @if($szkolenie->training_hours1 == 1)
                    godzina
                    @elseif(($szkolenie->training_hours1 > 1) && ($szkolenie->training_hours1 < 5)) godziny @else godzin
                        @endif )</li> <li>skrypt szkoleniowy @if(($id_category == 21) || ($szkolenie->training_type ==
                        '2')) (pdf) @endif</li>
                <li>materiały dodatkowe @if(($id_category == 21) || ($szkolenie->training_type == '2')) ON-LINE @endif
                </li>
                @if(($id_category != 21) && ($szkolenie->training_type != '2'))
                <li>certyfikat (pdf)</li>
                <li>monitorowanie zadań wdrożeniowych</li>
                <li>60 dniowy e-mentoring</li>

                @endif
            </ul>
        </div>
        <div class="cena_szkolenie cena_zawiera1">
            <div class="cena_szkolenie">
                <p class="vat" style="display: inline-block">Cena:</p>
                <h3 style="color:#ed1c24;display:inline-block;">{{$szkolenie->training_price1}} zł</h3>
                <p class="vat" style="display:inline-block;">+ VAT</p>
            </div>
        </div>
    </div>

    <div id="rabaty">
        <h4 class="title_section rabaty-title">Rabaty:</h4>
        <ul class="list-unstyled list-inline rabaty-list">
            <li>2 uczestnik - 20% 3 uczestnik - 25%</li>
            <li>więcej - porozmawiajmy</li>
        </ul>
    </div>
    <div class="slides_js" style="cursor:pointer;overflow: hidden;">
        <div style="background-image:url({{asset('/img/frontend/img1.png')}})" class="img_slide"></div>
        <div style="background-image:url({{asset('/img/frontend/img2.png')}})" class="img_slide"></div>
        <div style="background-image:url({{asset('/img/frontend/img3.png')}})" class="img_slide"></div>
        <div style="background-image:url({{asset('/img/frontend/img4.png')}})" class="img_slide"></div>
        <div style="background-image:url({{asset('/img/frontend/img5.png')}})" class="img_slide"></div>

    </div>
    <!-- <img src="{{asset('/img/frontend/szkolenia_otwarte_sale.jpg')}}" alt="Sale szkoleniowe Warszawa" class="img-responsive" style="margin-bottom: 15px;"> -->

    <div class="content-rezerwacja" style="display:table;width:100%;">
        <div id="rezerwacja" class="col-sm-6"
            style="     padding: 0 10px 0 0;display: table-cell;background: #efefef;float: none;vertical-align: middle;">
            <div class="terminy_szkolenia_dol col-sm-12" itemprop="location" itemscope=""
                itemtype="http://schema.org/Place" style="padding: 0px;">
                <meta itemprop="name" content="High5 Training Centre">
                <div id="term_szkol" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
                    <h3 class="">Terminy szkolenia</h3>
                    {{-- TEST:
                    {{print_r($terms)}} --}}
                    <ul class="list-unstyled" style="padding: 5px 0px 10px 25px; margin-bottom: 0px;">
                        <meta itemprop="addressLocality" content="Warszawa     "> @if($hotels->count() == 0)
                        <ul class="list-unstyled" style="padding: 5px 0px 10px 25px; margin-bottom: 0px;">
                            <li>do uzgodnienia</li>
                        </ul>
                        <meta itemprop="name" content="{{$term->training_title}}">
                        <span itemprop="location" itemscope="" itemtype="http://schema.org/Place"
                            content="{{$term->term_place}}">
                            <meta itemprop="name" content="HIGH5 Group sp. z o.o.">
                            <span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
                                <meta itemprop="addressLocality" content="{{$term->term_place}}">
                                <meta itemprop="addressCountry" content="Polska">
                            </span>
                        </span>
                        @else @foreach($terminy as $termin)
                        {{-- @if($termin->term_training_type == '1') --}}
                        @if($termin->term_closed == 'y')
                        <li class="term_closed">
                            @else
                        <li>
                            @endif {{date('d',strtotime($termin->term_start))}} -
                            {{date('d',strtotime($termin->term_end))}}
                            {{Lang::get('date.month.'.date('n',strtotime($termin->term_end)))}}
                            {{date('Y',strtotime($termin->term_end))}} @if($termin->term_state ==
                            '2')
                            <span style="font-size: 1rem;color: #cc3c6c;"><img
                                    src="{{asset('img/frontend/pewny3.png')}}" style="max-width:12px;" />PEWNY</span>
                            @endif @if($termin->term_closed == 'y')
                            <img src="{{asset('img/frontend/brak.png')}}" /> @endif
                            {{-- <a href="{{route('hotel',['id'=>$hotel->placeId])}}"
                                class="normal oneGleft thickbox miejsce_termin cboxElement"> --}}
                                <img
                                    src="{{asset('img/frontend/place.gif')}}" style="margin: 0 2px;max-width: 14px;" />
                                @if(($id_category != 21) && ($szkolenie->training_type != '2'))
                                {{$termin->term_place}}
                                @else
                                ON-LINE
                                @endif
                            {{-- </a> --}}
                        </a>
                        </li>
                        {{-- @endif --}}

                        @endforeach @endif

                    </ul>
                </div>
            </div>
        </div>
        <div class="col-sm-6" style="text-align: center;background: #efefef;display: block;float: none;">
            <a class="btn" href="{{route('rezerwacja_szkolenia',['id'=>$szkolenie->trainingID])}}" rel="nofollow">
                <img src="{{asset('img/frontend/rejestruj.png')}}" />
            </a>
            <br><span
                style="color: #ec1d27;position: absolute;font-size: 26px;font-family: AntonioRegular, 'Gill Sans', 'Gill Sans MT', 'Myriad Pro', 'DejaVu Sans Condensed', Helvetica, Arial, sans-serif;right: 0%;top: 50%;transform: translateY(-50%);left: 175px;">Zarezerwuj<br />
            </span>

            <p>&nbsp;</p>
        </div>
    </div>
    <div id="pytanie" class="col-sm-6" style="margin:0;background:#888888;text-align: center; padding-top: 5px;">
        <button type="button" class="btn" style="background:none;background: none;text-align: left;width: 100%;"
            data-toggle="modal" data-target="#myModal">
            <h2 class="antonioBig white" style="margin:0;">Pytania?</h2>
            <p style="color:#fff;">Wyślij formularz
            </p>
            <img src="{{asset('img/frontend/pytania.png')}}" style="position:absolute;right: 20px;top: 15px;" />
        </button>


        <!--            modal z pytaniem -->
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Zapytaj o termin</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            Jeśli chcecie Państwo otrzymać więcej informacji o tym programie, prosimy o wypełnienie
                            poniższego formularza.
                        </p>
                        <div id="zapytanie_status"></div>
                        @include('frontend.forms.pytanie')
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6"
        style="background: #888;color: #fff;    background: #888;color: #fff;text-align: right;padding: 8px;max-height: 75px;">
        <img src="{{asset('img/frontend/pdf_doc.png')}}" style="position: absolute;left: 20px;top: 15px;" />
        Pobierz formularz<br />
        <a href="{{asset('pdf/formularz_zgloszenia.pdf')}}" target="_blank">.Szkolenie stacjonarne - pdf </a> <br /> <a
            href="{{asset('pdf/formularz_zgloszenia.pdf')}}" target="_blank">.Szkolenie online - pdf</a>
    </div>


    {{-- Hotele box --}}
    @if(($id_category != 21) && (($szkolenie->training_type != '2')))
    <div class="row" style="background: #efefef;margin:0;text-align:center;">
        <div class="col-md-2">
            <h1 class="antonioBig red">
                Hotele
            </h1>
            <img src="{{asset('img/frontend/hotele/hotel_icon.png')}}" />
            <p class="red" style="1.5rem;margin:10px 0;">
                blisko nas
            </p>
        </div>
        @foreach($noclegi as $key => $hotel)
        @if($key < 2) <div class="col-md-5 hotel-box" style="margin: 25px 0 5px 0;">
            <div class="col-md-6">
                <h3 class="red hotel-title">{{$hotel->nazwa}}</h3>
                {{$hotel->odleglosc_od}} od {{config('app.name')}}
                <br />
                {{$hotel->cena_pokoj_jedno}} zł za 1 os.
                <br />
                {{-- Uwagi:
                <br>
                {{$hotel->uwagi}} --}}
            </div>
            <div class="col-md-6 hotel-img-container">
                <a href="{{route('hotele',['id'=>$hotel->id_hotele])}}">
                    <img class="thumb-hotel" src="{{public_path('/img/frontend/hotele/')}}/{{$hotel->zdjecie1}}" />
                </a>
            </div>
    </div>
    @endif
    @endforeach
    <a class="antonioSmall" style="float: right;padding: 0 20px 20px 0;"
        href="{{route('hotele_all',['trainingID'=>$szkolenie->trainingID])}}">więcej</a>
</div>
@endif
{{-- end hotele box --}}

@if (!empty($szkolenie->training_endings))
<div id="ending" class="col-sm-12">
    <h3 class="antonioBig">Co jeszcze?</h3>
    {!!$szkolenie->training_endings!!}
</div>

@endif
<div class="col-sm-12" style="padding-bottom: 40px;">

    <div class="col-sm-4" style="text-align: center;">
        <a href="{{route('szkolenia_otwarte')}}"><img src="{{asset('img/frontend/szkolenia_otwarte.gif')}}"
                alt="Szkolenia otwarte"></a>
    </div>
    <div class="col-sm-4" style="text-align: center;">
        <a href="{{route('szkolenia_zamkniete')}}"><img src="{{asset('img/frontend/szkolenia_zamkniete.gif')}}"
                alt="Szkolenia zamknięte"></a>
    </div>
    <div class="col-sm-4" style="text-align: center;">
        <a href="{{route('szkolenia_indywidualne')}}"><img src="{{asset('img/frontend/szkolenia_indywidualne.gif')}}"
                alt="Szkolenia indywidualne"></a>
    </div>
</div>
<div id="manifest" class="col-sm-12">
    <h3 class="antonioBig">Manifest skuteczności i zadowolenia</h3>
    @include('frontend.cms.szkolenia.szkolenia_manifest')

    <img src="{{asset('/img/frontend/stamp.png')}}" alt="High5 Certified">
</div>


@endif @endforeach
<div class="toright" style="padding-top: 20px;">

</div>
@endsection