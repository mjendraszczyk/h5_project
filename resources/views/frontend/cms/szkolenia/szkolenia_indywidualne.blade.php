@extends('frontend.main.cms')
@section('menu')
@include('frontend.cms.szkolenia.menu')
@endsection
@section('content')
@section('title')
HIGH5: Szkolenia indywidualne, konsultacje, coaching  Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
Szkolenia indywidualne w kategoriach: sprzedaż, negocjacje, zarządzanie zespołem, zarządzanie procesami, rozwój osobisty realizowane jako szkolenia indywidualne lub konsultacje Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('keywords')
szkolenia indywidualne, konsultacje biznesowe, szkolenia biznesowe, program rozwoju, coaching
@endsection
<h3>Szkolenia indywidualne</h3>
<div class="toright">
<p>Ze względu na&nbsp;sytuację zmieniającą się z dnia na dzień musimy  zmienić swój sposób codziennego  funkcjonowania.  Nie zawsze jest to łatwe, zarówno dla pracowników jak i zarządzających.  Spójrzmy jednak na ten czas jako na duże wyzwanie, z którym musimy się  zmierzyć. Nie zmarnujmy go. </p>
<p>&nbsp;Przygotowaliśmy propozycję  dla:</p>
<ul>
  <li>menedżerów  odpowiedzialnych za  funkcjonowanie zespołów w&nbsp;okresie przejściowym,</li>
  <li>pracowników  funkcjonujących w modelu pracy zdalnej, </li>
  <li>pracowników doświadczających  niepewności,  niepokoju związanego ze zmieniającą się  rzeczywistością).</li>
  <li>pracowników, którzy czas przestoju chcą  wykorzystać do własnego rozwoju. </li>
</ul>
<h3>Szkolenia indywidualne on-line</h3>
<p>Szkolenia i treningi indywidualne on-line  to  rozwiązanie, które umożliwia:&nbsp;</p>
<ul>
  <li>pracę w dowolnym terminie i miejscu,</li>
  <li>precyzyjne dostosowanie programu szkolenia do  potrzeb i oczekiwań,</li>
  <li>pracę na indywidualnych sytuacjach bez  konieczności dzielenia się nimi z grupą,</li>
  <li>łączenie wielu zagadnień w jednym szkoleniu,</li>
  <li>okazję do bardzo intensywnego, praktycznego treningu umiejętności w relacji indywidualnej z trenerem,</li>
  <li>pracę z narzędziami  diagnostyczno-rozwojowymi,</li>
  <li>wybór eksperta/trenera.</li>
</ul>
<p>Większość programów, które  proponujemy w formie stacjonarnej, zrealizujemy również w formie  sesji online. <br>
Dodatkowo,  odpowiadając na obecną sytuację, przygotowaliśmy dodatkowe tematy: Wyzwania 2020</p>
<ul>
  <li>Strategie budowania własnej odporności  psychicznej.</li>
  <li>Przeprowadzić zespół w zmianie – siła i  odporność psychiczna lidera, jak ją wzmacniać?</li>
  <li>Mam tę moc! Jak naturalnie zwiększyć swoją  efektywność osobistą/ sesja opartą na raporcie badania CliftonStrengths  (talenty Top 5).</li>
  <li>Zmiana a talenty – jak wykorzystać swoje  mocne strony.</li>
  <li>Ofertowanie online – jak budować relację z  Klientem w kanale wirtualnym.</li>
  <li>Prezentacje online, czyli &quot;gadające  głowy&quot;-&nbsp; jak mówić aktywnie. Praktyczny trening perswazyjnego głosu.</li>
  <li>&bdquo;Give Great Video Presentations&rdquo; – jak  wykorzystać technikę i budować autorytet.</li>
  <li>Zarządzanie zespołem rozproszonym –  funkcjonowanie zespołu online.</li>
  <li>&quot;Przecież ja pracuję!&quot; -  zarządzanie sobą w czasie... podczas pracy zdalnej.</li>
  <li>Trening zarządzania stresem i  radzenia sobie ze zmianami</li>
  <li>Komunikacja kryzysowa – konsulting przekazów  komunikacyjnych kierowanych do pracowników</li>
  <li>Motywowanie pracowników w trakcie pracy zdalnej.</li>
</ul>
 <img class="img-responsive" src="{{Config('url')}}/img/frontend/indywidualne2a.jpg" width="100%" alt="Szkolenie on-line">
<p>Pracujemy na narzędziach, które  nie wymagają  żadnego specjalnego  oprogramowania, wystarczy dostęp do&nbsp;internetu. Możliwa jest także praca za  pośrednictwem&nbsp;urządzeń mobilnych&nbsp; (smartfon lub tablet).</p>
<p><strong><em>Przykład:</em></strong><em> Nasz  klient (branża motoryzacyjna) zwrócił się do nas o wsparcie w zbudowaniu  odporności psychologicznej i radzenia sobie z trudnymi emocjami wynikającymi z  konieczności komunikowania pracownikom i związkom zawodowym przeprowadzenia  dużych i szybkich zmian wynikających z konieczności połączenia dwóch fabryk.  Zaproponowaliśmy trening indywidualny w obszarze radzenia sobie z trudnymi  emocjami i zaawansowaną pracę z nastawieniem do zaistniałej sytuacji. Kolejnym  etapem było wsparcie klienta w obszarze strategii zarządzania emocjami jego  rozmówców i zbudowanie przekazu komunikacyjnego mającego na celu przekonanie  odbiorców do sensowności nowych rozwiązań.</em></p>
<p><strong><em>Przykład:</em></strong><em> Nasz  klient (Prezes jednej z dużych spółek akcyjnych) zdecydował się na regularne,  indywidualne doradztwo w kwestii wsparcia w przygotowywaniu prezentacji on-line   i przekazów komunikacyjnych możliwych do  zastosowania w trakcie kluczowych wydarzeń takich jak: wystąpienia kierowane do  wszystkich pracowników, spotkania zarządu, rozmowy indywidualne z kluczowymi  osobami w firmie. Dzięki sesjom treningowym udało się wypracować szereg  rozwiązań komunikacyjnych i psychologicznych, które ułatwiły klientowi  osiąganie złożonych celów strategicznych.</em></p>
<p><strong><em>Przykład: </em></strong><em>Nasza  klientka (branża budowlana) zgłosiła się do nas w celu zwiększenia swojej  efektywności i umiejętności w obszarze zarządzania sobą w czasie. W trakcie  sesji indywidualnych klientka otrzymała wsparcie w obszarze analizy i  skutecznej zmiany swoich nawyków, wytypowała narzędzia i aplikacje ułatwiające  skuteczne zarządzanie osobistą efektywnością i na bazie testów dopasowała  wypracowane rozwiązania do specyfiki swojej pracy.</em></p>
<h3>Konsultacje indywidulane: <br>
  Ekspert on-line   (ekspert od spraw ważnych i   bardzo ważnych)</h3>
<p>Jeśli  nie potrzebujesz całego szkolenia, a tylko chcesz&nbsp;rozwiązać konkretny  problem, skonsultować decyzję   - jesteśmy do dyspozycji.<br>
Konsultacje przeprowadzamy w odpowiednim dla Zamawiającego czasie i miejscu,  również w formie&nbsp;on-line lub telefoniczne.</p>
<p>Obszary naszych konsultacji:  prawo, zarządzanie, psychologia, HR, procesy, projekty, sprzedaż.</p>
<h3>Doradztwo indywidualne kryzysowe </h3>
<p>&nbsp;Udzielamy  też wsparcia psychologicznego kluczowych pracowników, w trudnych momentach oraz  pomoc w odbudowaniu stabilności psychologicznej, poczucia bezpieczeństwa,  redukowania lęku. Pracujemy z najlepszymi psychologami i terapeutami.</p>
<p>&nbsp;<strong>Jeśli szukają  Państwo rozwiązań wspierających pracowników   w&nbsp;obecnej trudnej sytuacji, zapraszamy do kontaktu.</strong></p>
<p>&nbsp;<strong>Jakie są skutki życia w kryzysie, jeśli nie  zainterweniujemy w porę:</strong></p>
<p>Wymiar fizjologiczny: bezsenność, bóle głowy, zaburzenia ze  strony układu trawiennego i inne,<br>
  Wymiar emocjonalny: dominujący lęk, złość, poczucie  bezradności <br>
  Wymiar behawioralny: trudność w wykonywaniu codziennych obowiązków, zachowania ryzykowne (nałogi), powtarzane, nieskuteczne próby  wyjścia z kryzysu,<br>
Wymiar poznawczy: zaburzenia koncentracji, natrętne myśli,  trudność z obiektywną oceną sytuacji.</p>
<p><strong>Co to oznacza dla pracodawców?</strong></p>
<p>Osoby funkcjonujące w kryzysie środowiskowym (pandemia  koronawirusa) mogą ulegać trudnym emocjom, takim jak lęk, złość i poczucie, że  nic nie są w stanie zrobić. Mogą zdarzać się nagłe wybuchy złości lub płaczu.  Dominuje zmienność nastrojów, drażliwość. W niektórych przypadkach mogą pojawić  się stany lękowe i epizody depresyjne. Myślenie krytyczne i kreatywne będą  zaburzone. Codzienne funkcjonowanie może ulegać pogorszeniu – problemy ze snem  przekładają się na&nbsp; ciągłe zmęczenie i brak energii, trudności ze skupieniem  myśli na pracy, zaplanowaniem kolejności czynności i konsekwentnym ich  realizowaniem. Możemy dostrzegać rozkojarzenie, zapominanie, czy trudność w  podejmowaniu decyzji.</p>
<p><strong>W czym pomaga Coach / Interwent Kryzysowy?</strong></p>
<ul>
  <li> w zrozumieniu sytuacji poprzez psychoedukację (diagnozuje,  reinterpretuje)</li>
  <li> w odzyskaniu równowagi emocjonalnej, obniża poziom  napięcia, lęku</li>
  <li> w wypracowaniu nowych przystosowawczych strategii,  zachowań, reakcji i rozwiązań, które mogą pomóc wyjść z kryzysu</li>
  <li> w podniesieniu samooceny, wzmocnieniu poczucia własnej  wartości, kontroli i sprawczości nad sobą i nad własnym życiem</li>
  <li> w odnalezieniu sensu w sytuacji kryzysowej i w  wykorzystaniu rozwojowego potencjału kryzysu</li>
</ul>
 
        <p>&nbsp;</p>
        </div>
@endsection