<h1 class="antonioBig red">Szkolenia otwarte</h1>
<p>Dla wszytkich chętnych: organizujemy je cyklicznie.
<br/>
    Grupa to minimum 4 osoby, max. 16. </p>
<p>W naszej ofercie znajdziesz blisko 200 tematów szkoleń w 9 kategoriach.</p>
<p>Program szkolenia i ćwiczenia są określone z góry, ale na początku każdego szkolenia trener określa z
    uczestnikami najbardziej interesujące ich obszary. </p>
<p><a href="{{route('szkolenia_otwarte')}}">Kliknij tu, aby dowiedzieć się, czym się różnią szkolenia otwarte w High5
        oraz zobaczyć nasz <strong>katalog szkoleń</strong> (zawiera ceny i terminy szkoleń otwartych.</a></p>
 