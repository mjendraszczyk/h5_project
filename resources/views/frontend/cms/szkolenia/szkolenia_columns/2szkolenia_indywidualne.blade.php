<h1 class="antonioBig red">Szkolenia indywidualne</h1>

<p>Jeżeli potrzebujesz szybkiej pomocy w postaci konsultacji, lub wsparcia rozwoju w formie coachingu, nasi
    trenerzy są do twojej dyspozycji.

    Szkolenia indywidualne to najlepsza droga dla tych, którzy się spieszą, lub po prostu wolą pracę w
    pojedynkę.</p>
<p><a href="{{route('szkolenia_indywidualne')}}">Kliknij tu, aby otworzyć stronę, na której przygotowaliśmy propozycje
        szkoleń indywidualnych.</a></p>
 