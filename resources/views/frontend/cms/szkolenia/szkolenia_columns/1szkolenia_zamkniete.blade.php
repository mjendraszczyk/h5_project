<h1 class="antonioBig red">Szkolenia zamknięte</h1>

            <p>Szkolenia organizowane dla konkretnej firmy (in-company) – w jej siedzibie, w naszych salach lub na
                wyjeździe.

                Program szkolenia tworzony jest dla osiągnięcia określonych celów biznesowych. </p>
            <p>Szkolenie może być zbudowane na podstawie programu dostępnego na stronach High5, ale w przypadku szkoleń
                zamkniętych oferujemy znacznie szerszy zakres tematyczny. </p>
            <p><a href="{{route('szkolenia_zamkniete')}}">Kliknij tu, aby zobaczyć, co i w jaki sposób możemy dla Ciebie
                    zrobić</a>.</p>
 