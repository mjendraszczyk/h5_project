@extends('frontend.main.fullwidth')
@section('content')
@section('title')
HIGH5 Training Group: Szkolenia dla firm Warszawa - szkolimy najlepszych  Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
Strona firmy szkoleniowej: szkolenia dla firm otwarte i szkolenia zamknięte, coaching. Rozwój osobisty, negocjacje, zarządzanie projektami i procesami, Szkolenia dla managerów i dla działów HR Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('keywords')
Szkolenia otwarte, szkolenia zamknięte, coaching, rozwój, zarządzanie, warszawa, szkolenia dla firm, projekty, procesy, HR, negocjacje, sprzedaż
@endsection
<div id="trainingHead" class="col-sm-12"></div>
    <div class="row">
             
                    <div id="colmn1" class="col-sm-4 szkolenia_main_col">      
            @include('frontend.cms.szkolenia.szkolenia_columns.0szkolenia_otwarte')
                    </div>
                    <div id="colmn2" class="col-sm-4 szkolenia_main_col">      
            @include('frontend.cms.szkolenia.szkolenia_columns.1szkolenia_zamkniete')
                    </div>
            <div id="colmn3" class="col-sm-4 szkolenia_main_col">      
            @include('frontend.cms.szkolenia.szkolenia_columns.2szkolenia_indywidualne')       
            </div>
       </div>
    <div class="row">
         
    <div class="col-sm-4 icon_sz">
       <img src="{{Config('url')}}img/frontend/szkolenia_otwarte.png" alt="" />
       </div>
       <div class="col-sm-4 icon_sz">
            <img src="{{Config('url')}}img/frontend/szkolenia_zamkniete.png" alt="" />
            </div>
            <div class="col-sm-4 icon_sz">
                    <img src="{{Config('url')}}img/frontend/szkolenia_indywidualne.png" alt="" />
                    </div>
    </div>
      <div class="row">
            <div id="otwarteMain" class="col-sm-4">
                <h1 class="antonioBig ">Katalog szkoleń otwartych:</h1>
                <ul class="list-unstyled">
                    @foreach($cattrain as $cat)
                    <li><a href="{{route('szkolenia_kategoria',['id'=>$cat->categoryID,'title'=>str_slug($cat->category_title)])}}">{{$cat->category_title}}</a></li>
                    @endforeach
                </ul>
            </div>
            <div id="zamknieteMain" class="col-sm-4">
                <h1 class="antonioBig">Katalog szkoleń zamkniętych:</h1>
                <ul class="list-unstyled">
                        @foreach($cattrain as $cat)
                        <li><a href="{{route('szkolenia_kategoria_zamkniete',['id'=>$cat->categoryID,'title'=>str_slug($cat->category_title)])}}">{{$cat->category_title}}</a></li>
                        @endforeach
                </ul>
            </div>
        </div>       
    </div>   
</div>             
@endsection