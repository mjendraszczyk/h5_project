@extends('frontend.main.fullwidth')
@section('content')
@section('title')
HIGH5 Training Group: Szkolenia dla firm Warszawa - szkolimy najlepszych
@endsection
@section('description')
Strona firmy szkoleniowej: szkolenia dla firm otwarte i szkolenia zamknięte, coaching. Rozwój osobisty, negocjacje,
zarządzanie projektami i procesami, Szkolenia dla managerów i dla działów HR
@endsection
@section('keywords')
Szkolenia otwarte, szkolenia zamknięte, coaching, rozwój, zarządzanie, warszawa, szkolenia dla firm, projekty, procesy,
HR, negocjacje, sprzedaż
@endsection

<h1 class="antonioBig red">Hotele i miejsca szkoleń</h1>
@foreach ($hotel as $h)
<div class="row flex">
    <div class="col-md-7">
        <h1 class="antonioBig">{{$h->miasto}}</h1>
        <h3>{{$h->nazwa}}</h3>
        {{$h->ulica}},<br />
        {{$h->kod}} {{$h->miasto}}<br />
        <br />
        <br /> {{$h->odleglosc_od}} metrów od {{config('app.name')}}
        <br />
        <br />
        Specjalny cennik dla uczestników szkoleń organizowanych przez {{config('app.name')}} w {{date('Y')}} roku:
        <br />
        <br />
        Pokój 1 - osobowy - {{$h->cena_pokoj_jedno}} PLN/doba <br />
        <br>
        Uwagi: <br>

        {{$h->uwagi}} <br />
        <br />
        @if($h->parking == '1')
        Parking - {{$h->cena_parking}} PLN/doba
        @endif

    </div>
    <div class="col-md-5">
        <div class="col-md-6">
            <img src="{{public_path('/img/frontend/hotele/')}}/{{$h->zdjecie1}}" style="max-width:100%;" />
        </div>
        <div class="col-md-6">
            <img src="{{public_path('/img/frontend/hotele/')}}/{{$h->zdjecie2}}" style="max-width:100%;" />
        </div>
    </div>
</div>
@endforeach
@endsection