@extends('frontend.main.cms')
@section('title')
HIGH5: Trenerzy i instruktorzy prowadzący szkolenia, konsultanci, coache  Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
Trenerzy i szkoleniowcy, którzy pracują dla High5 Training Group. Zobacz naszą kadrę Tag:   @foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('menu')
@include('frontend.cms.firma.menu')
@endsection
@section('content')
<h1 class="antonioBig red">Trenerzy i konsultanci High5 Group</h1>
                 <div id="listaTren" class="toright">
                    @foreach($trenerzy as $trener)
                    <a href="{{route('trenerzy')}}/{{$trener->instructorID}}"><img src="{{Config('url')}}/img/frontend/trainers/{{str_replace_last('.jpg','.gif',$trener->instructor_photo)}}"/>{{strtolower(str_after($trener->instructor_name,' '))}}</a>       
                    @endforeach              
            </div>
@endsection 