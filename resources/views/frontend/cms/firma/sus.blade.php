@extends('frontend.main.cms')
@section('title')
HIGH5 Training Group: Kim jesteśmy, usługi i szkolenia, które realizujemy Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
High5 Training Group: firma szkoleniowo-doradcza. Szkolimy umiejętności, które są niezbędne w efektywnym funkcjonowaniu każdego przedsiębiorstwa. Tag:   @foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('menu')
@include('frontend.cms.firma.menu')
@endsection
@section('content')
<h1 class="antonioBig red">Standard Usługi Szkoleniowej</h1><h2>Certyfikat Jakości <br>
    Standard Usługi Szkoleniowej SUS 2.0</h2>
    <h3>Baza Usług Rozwojowych </h3>
    <p><strong>HIGH5 firmą  szkoleniowo-doradczą z certyfikatem jakości kształcenia SUS 2.0.</strong></p>
    <p>Nowy  Standard Usług Szkoleniowo-Rozwojowych SUS 2.0 jest standardem zarządzania i  świadczenia usług przez podmioty specjalizujące się w usługach  szkoleniowo-rozwojowych. Instytucje szkoleniowe, które odznaczają się  certyfikatami umożliwiają swoim Klientom dostęp do środków unijnych z EFS za  pośrednictwem Regionalnych Programów Operacyjnych.<br>
    SUS 2.0 jest  standardem certyfikowanym, jego spełnienie potwierdza audyt przeprowadzony  przez profesjonalną i niezależną organizację certyfikującą o międzynarodowej  renomie, specjalizującą się w audytach systemów zarządzania – DEKRA  Certification.</p>
    <img src="{{Config('url')}}/img/frontend/certyfikat-SUS.jpg" alt="Certyfikat Standard Usługi Szkoleniowej">
    <h3>Potrzebujesz wsparcia w zakresie szkoleń, coachingu, doradztwa? Chcesz  wzmocnić swoje mocne strony?</h3>
    <p><strong>Przez cały 2017 rok, kolejne województwa będą rozpoczynały projekty i  ogłaszały rekrutację, można uzyskać nawet do 80% dofinansowania!</strong><br>
    Firma HIGH5 spełnia wymagania systemu zapewnienia jakości  dla firm szkoleniowo-rozwojowych, które są zainteresowane wpisem do Bazy Usług  Rozwojowych (BUR), prowadzonej przez Polską Agencję Rozwoju Przedsiębiorczości  (PARP) (<em>d. Rejestr Usług Rozwojowych -  RUR</em>) i świadczeniem usług dofinansowanych z Europejskiego Funduszu  Społecznego za pośrednictwem Regionalnych Programów Operacyjnych.<br>
    Obecność firmy w BUR jest niezbędnym warunkiem świadczenia  usług. <strong>Jeżeli jesteś zainteresowana/y  podniesieniem swoich kompetencji w oparciu o szkolenia, zachęcamy do  skorzystania z dofinansowania, jakie daje system Podmiotowego Finansowaniu  Usług Rozwojowych.</strong></p>
    <h3>Czym jest Baza Usług  Rozwojowych (BUR)?</h3>
    Baza Usług rozwojowych to serwis przygotowany przez PARP, w  którym znajdują się wszystkie szkolenia wraz z terminami realizacji jednostek  posiadających pozytywną akredytację PARP na świadczenie usług i szkoleń  dofinansowanych. <em>Dofinansowanie obejmuje  wyłącznie usługi szkoleniowe wpisane do BUR, przez podmioty posiadające  pozytywną zweryfikowaną akredytację na realizację usług dofinansowanych.</em><br>
    Przez cały 2017 rok, kolejne województwa będą rozpoczynały  projekty i ogłaszały rekrutację, można uzyskać nawet do 80% dofinansowania. Firma  HIGH5 wspiera w doborze szkoleń, czy wypełnieniu dokumentów.<br>
    Instytucja (Operator), która realizuje podmiotowy system finansowania  w danym województwie, dysponuje i przyjmuje wnioski zgłoszeniowe oraz  dystrybuuje środki do do osób i firm zainteresowanych dofinansowaniem szkoleń,  które takie wsparcie otrzymają.<br>
    <h3>Kto może skorzystać z  dofinansowania na szkolenia?</h3>
    <p>Z dofinansowania unijnego do szkoleń mogą skorzystać mikro,  małe i średnie przedsiębiorstwa. Niezbędny jest adres siedziby firmy  zarejestrowanej &nbsp;na obszarze danego  województwa, oraz zatrudnienie pracowników na podstawie umowy o pracę, z  obszaru tego samego województwa. </p>
    <h3>Na co można uzyskać dofinansowanie?</h3>
    <p>Dofinansowanie z BUR można uzyskać na rozwój kompetencji  osób samozatrudnionych i pracowników MMŚP zgodnie ze zidentyfikowanymi potrzebami  przedsiębiorstw oraz na kompleksowe usługi rozwojowe odpowiadające na ich  potrzeby, m.in. właśnie na szkolenia.</p>
    <h3>Jaki jest poziom  dofinansowania do szkoleń?</h3>
    <p>Szkolenia mogą zostać dofinansowane w wysokości od 50% do  80%, w zależności od województwa oraz założonych kryteriów. </p>
    <h3>Jaki jest okres  realizacji projektu dofinansowania do szkoleń?</h3>
    <p>Dofinansowanie będzie realizowane od roku 2017 r. do 2019 r.<br>
    <em>Więcej informacji nt.  dofinansowanych przez BUR szkoleniach dowiesz się w Instytucji Zarządzającej  Regionalnym Programem Operacyjnym w Twoim regionie. Zachęcamy do odwiedzania  strony </em><a href="http://serwis-uslugirozwojowe.parp.gov.pl/"><em>http://serwis-uslugirozwojowe.parp.gov.pl/</em></a><em>&nbsp;  -&nbsp; w zakładce „Dofinansowanie”  wyczerpujące informacje.</em></p>
    @endsection