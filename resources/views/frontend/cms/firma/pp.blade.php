@extends('frontend.main.cms')
@section('title')
HIGH5 Training Group: Kim jesteśmy, usługi i szkolenia, które realizujemy Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
High5 Training Group: firma szkoleniowo-doradcza. Szkolimy umiejętności, które są niezbędne w efektywnym funkcjonowaniu każdego przedsiębiorstwa. Tag:   @foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('menu')
@include('frontend.cms.firma.menu')
@endsection
@section('content')
<h1 class="antonioBig red">Polityka prywatności</h1><h2>Informacja zgodna z art. 13 ogólnego rozporządzenia o ochronie danych osobowych (RODO)</h2>
    <p>W związku ze zmianą przepisów dotyczących  ochrony danych osobowych i rozpoczęciem stosowania od dnia 25 maja 2018 roku  Rozporządzenia Parlamentu Europejskiego i Rady (UE) 2016/679 z dnia 27 kwietnia  2016 r. w sprawie ochrony osób fizycznych w związku z&nbsp;przetwarzaniem  danych osobowych i w sprawie swobodnego przepływu takich danych oraz uchylenia  dyrektywy 95/46/WE (ogólne rozporządzenie o ochronie danych) &quot;RODO&quot;  informujemy, że:</p>
<h3 class="antonioSmall">Administrator  danych osobowych (ADO)</h3>
<p>Zgodnie z art. 13 rozporządzenia Parlamentu  Europejskiego i Rady (UE) 2016/679 z dnia 27 kwietnia 2016 r. w sprawie ochrony  osób fizycznych w związku z przetwarzaniem danych osobowych i w sprawie  swobodnego przepływu takich danych oraz uchylenia dyrektywy 95/46/WE zwanego  dalej &bdquo;RODO&rdquo;, informujemy, że Administratorem pozyskanych danych osobowych jest  spółka HIGH5 Group Sp. z o. o. z siedzibą w&nbsp;Warszawie przy ul.  Wroniej 45 lok. 129. tel. 22-824-50-25.</p>
<h3 class="antonioSmall">Kontakt ws. ochrony  danych osobowych</h3>
<p>Od dnia 25.05.2018 jest możliwość skontaktowania się z osobami  odpowiedzialnymi za Państwa dane za pośrednictwem adresu email: biuro@high5.pl</p>
<h3 class="antonioSmall">Jakie  dane osobowe zbieramy?</h3>
<p>Kiedy  wchodzą Państwo z nami w relację, w zależności od jej charakteru, możemy  zbierać i przetwarzać wszystkie lub część następujących kategorii danych  osobowych:</p>
<ul>
  <li>imię i nazwisko, firma oraz Państwa stanowisko,</li>
  <li>informacje kontaktowe, takie jak nazwa i adres  Państwa firmy, numery telefonów i adresy email, które nam Państwo podają,</li>
  <li>udział w naszych wydarzeniach takich jak szkolenia  i kursy, sympozja, kongresy, seminaria itp.,</li>
  <li>naszą komunikację z Państwem,</li>
  <li>Państwa zainteresowania naszymi produktami lub  usługami.</li>
</ul>
<p>Podanie danych jest zawsze dobrowolne. Przetwarzamy  Twoje dane w minimalnym zakresie, który jest niezbędny do realizacji jednego  lub większej liczby celów. Nie wykraczamy poza zakres danych, który jest określony  w danym formularzu (newsletter, formularz kontaktowy, formularz zgłoszeniowy  itp.).</p>
<h3 class="antonioSmall">Cele,  podstawy przetwarzania i okres przechowywania danych </h3>
<p>Poniżej  znajdują się wszystkie występujące standardowo w działalności cele  przetwarzania danych przez Administratora. <br>
Szczegółowy cel przetwarzania danych jest  każdorazowo komunikowany przez Administratora w momencie pozyskiwana danych.</p>
<table width="100%" border="1" cellpadding="3" cellspacing="1"  style='border-collapse:collapse; padding:3px'  >
  <tbody>
    <tr>
      <td>Cel przetwarzanie</td>
      <td>Podstawa</td>
      <td>Okres</td>
    </tr>
    <tr>
      <td>Wypełnianie zobowiązań umownych. </td>
      <td>Art. 6 ust. 1 lit. b RODO</td>
      <td>Dane osobowe związane z realizacją umowy będą przetwarzane przez      okres związany z&nbsp;realizacją umowy, a&nbsp;także realizacją praw i&nbsp;obowiązków      stron wynikających z istoty umowy. </td>
    </tr>
    <tr>
      <td>Prowadzenie korespondencji e-mailowej, drogą tradycyjną podczas      rozmów, kontaktów telefonicznych, korespondencji w ramach formularzy      elektronicznych, niezwiązanej z usługami świadczonymi na rzecz nadawcy lub inną zawartą z&nbsp;nim umową,      dane osobowe zawarte w tej korespondencji są przetwarzane wyłącznie      w&nbsp;celu komunikacji i&nbsp;załatwienia sprawy, której dotyczy ta      korespondencja</td>
      <td>Art. 6 ust. 1 lit. f RODO, podstawą prawną przetwarzania jest      uzasadniony interes Administratora, polegający na prowadzeniu      korespondencji kierowanej do niego w związku z&nbsp;prowadzoną działalnością gospodarczą.      Administrator przetwarza jedynie dane osobowe odpowiednie dla sprawy,      której dotyczy korespondencja. Całość korespondencji jest przechowywana w      sposób zapewniający bezpieczeństwo zawartych w niej danych osobowych oraz      innych informacji i&nbsp;ujawniana jedynie osobom upoważnionym.</td>
      <td>Dane osobowe przetwarzane w związku z&nbsp;korespondencją      przechowywane są, w&nbsp;zależności od charakteru zapytania, albo przez      okres obsługi umowy, albo przez okres niezbędny do udzielenia osobie ostatecznej odpowiedzi.</td>
    </tr>
    <tr>
      <td>Archiwizowanie danych na    podstawie powszechnie obowiązujących przepisów prawa, np. rachunkowości    i&nbsp;ustawa - Ordynacja podatkowa. </td>
      <td>Art. 6 ust. 1 lit. c RODO,    ustawa o rachunkowości i&nbsp;ustawa - Ordynacja podatkowa. </td>
      <td>Dane osobowe przetwarzane w    celu wykonania obowiązków wynikających z przepisów prawa, przez okres    wynikający z przepisów prawa. </td>
    </tr>
    <tr>
      <td>Ewentualnego ustalenia,    dochodzenia lub obrony przed roszczeniami będącego realizacją naszego prawnie    uzasadnionego w tym interesu. </td>
      <td>Art. 6 ust. 1 lit. f RODO</td>
      <td>Dane osobowe przetwarzane w    związku z&nbsp;dochodzeniami roszczeń w związku z&nbsp;wykonywaniem umowy    przetwarzane są przez okres trwania roszczenia</td>
    </tr>
    <tr>
      <td>W celach analitycznych oraz    innych działaniach mających na celu rozwój prowadzonej działalnością np.    podczas spotkań biznesowych, na wydarzeniach branżowych </td>
      <td>Art. 6 ust. 1 lit f RODO,    podstawą prawną przetwarzania jest w tym wypadku uzasadniony interes Administratora    polegający na tworzeniu sieci kontaktów w związku z prowadzoną działalnością </td>
      <td>Dane kontaktowe    przechowujemy do czasu prawnie uzasadnionego celu</td>
    </tr>
  </tbody>
</table>
<h3 class="antonioSmall">Usługi newsletter – klienci indywidualni</h3>
<p> Jeżeli poprosiliśmy Państwa o zgodę na  przetwarzanie danych osobowych dla celów realizacji usługi newsletter – mogą  Państwo wycofać zgodę w każdym czasie za pomocą specjalnego odnośnika zawartego  w każdej wiadomości lub przez skontaktowanie się z&nbsp;nami telefonicznie lub  mailowo (biuro@high5.pl). Dane przekazane nam przez Państwa dla realizacji  usługi newsletter zbieramy w oparciu o przepisy ustawy z dnia 18 lipca  2002&nbsp;r. o świadczeniu usług drogą elektroniczną (tekst jednolity: Dz. U. z  2017 r. poz. 1219 z późn. zm.).</p>
<h3 class="antonioSmall">Usługi newsletter – klienci biznesowi</h3>
<p>  Jeżeli występują Państwo w imieniu firmy lub  instytucji, są Państwo naszym klientem, kontrahentem lub innym partnerem  biznesowym - możemy kierować do Państwa organizacji na podany przez Państwa  adres korespondencji lub poczty elektronicznej informację o naszych produktach  i usługach na podstawie naszych uzasadnionych interesów. Dane podane jako  reprezentanta firmy poprzez usługę newsletter traktujemy jednak jako Państwa  prywatne dane. Szanując wspólne relacje zapewniamy możliwość rezygnacji z  otrzymywania tych informacji w ten sam sposób, jak w przypadku klientów  indywidualnych. Korzystając z tego prawa firma lub instytucja, którą Państwo reprezentują  zostanie wypisana z usługi newsletter, a Państwa dane zostaną usunięte. Nasza  oferta produktów i usług może jednakże trafić do Państwa organizacji w związku  z&nbsp;realizacją naszych prawnie usprawiedliwionych interesów i może być  związana z inną formą naszej współpracy.</p>
<h3 class="antonioSmall"> Formularze kontaktowe</h3>
<p>  Jeżeli chcą   Państwo  nawiązać z nami  współpracę, skorzystać z naszych usług lub produktów lub zadać nam pytanie, mogą  Państwo przekazać nam swoje dane osobowe poprzez dostępne na naszych stronach  formularze kontaktowe. Nie żądamy wówczas zgody na przetwarzanie Twoich danych,  gdyż przetwarzamy je w oparciu o nasze usprawiedliwione interesy, którymi są  udzielenie odpowiedzi na zadanie pytanie, marketing naszych produktów i usług  (np. jeżeli chcą Państwo otrzymać od nas ofertę, odpowiedź na zapytania) albo  działania zmierzające do zawarcia umowy.</p>
<h3 class="antonioSmall">Odbiorcy  danych </h3>
<p>Państwa  dane osobowe możemy udostępniać następującym kategoriom podmiotów: </p>
<ul>
  <li>Osoby upoważnione przez Administratora – pracownicy  oraz współpracownicy; </li>
  <li>Podmioty, którym Administrator powierzył  przetwarzania danych osobowych (podmioty przetwarzające) na podstawie zawartych  umów; </li>
  <li>Podmioty, którym Administrator zobligowany lub  uprawniony będzie udostępnić dane na podstawie przepisów prawa. </li>
</ul>
<h3 class="antonioSmall">Przekazywanie  danych do państw trzecich lub organizacji międzynarodowych </h3>
<p>Dane osobowe nie będą przekazywane do innych  państw trzecich niż należące do Europejskiego Obszaru Gospodarczego lub  zapewniających odpowiedni stopień ochrony stwierdzony decyzją Komisji  Europejskiej na podstawie art. 45 ust. 3 rozporządzenia.</p>
<h3 class="antonioSmall">Twoje  prawa </h3>
<p>Uprzejmie informujemy, że w dowolnym momencie macie Państwo prawo:</p>
<ul>
  <li>dostępu do treści swoich danych osobowych,</li>
  <li>do sprostowania danych,</li>
  <li>żądania od Administratora usunięcia danych lub  ograniczenia przetwarzania danych,</li>
  <li>do przenoszenia danych,</li>
  <li>wniesienia sprzeciwu wobec przetwarzania danych, </li>
  <li>wniesienia skargi do polskiego organu  nadzorczego tj. Prezesa Urzędu Ochrony Danych Osobowych lub organu  nadzorczego innego państwa członkowskiego Unii Europejskiej, właściwego ze  względu na miejsce zwykłego pobytu lub pracy osoby, której dane dotyczą lub ze  względu na miejsce domniemanego naruszenia RODO,</li>
  <li>do cofnięcia zgody w dowolnym momencie (bez  wpływu na zgodność z prawem przetwarzania, którego dokonano na podstawie zgody  przed jej cofnięciem)</li>
  <li>cofnięcie zgody jest możliwe w każdej  chwili i może nastąpić w drodze mailowej na adres: <a href="mailto:biuro@high5.pl">biuro@high5.pl</a></li>
</ul>
<h3 class="antonioSmall"> Informacja o profilowaniu </h3>
<p>Przetwarzane  dane osobowe nie będą podlegały zautomatyzowanemu przetwarzaniu ani  profilowaniu. </p>
<h3 class="antonioSmall">Miejsce przechowywania danych</h3>
<p>HIGH5  Group sp. z o.o. zobowiązuje się właściwie chronić Państwa dane osobowe zgodnie  z przyjętymi wewnętrznymi politykami, procedurami i standardami tak, aby dane  osobowe były chronione przed nieautoryzowanym użyciem lub dostępem, bezprawnymi  modyfikacjami, utratą lub zniszczeniem.<br>
Państwa  dane są przechowywane w bezpiecznej bazie danych na&nbsp; serwerze zapewnianym  przez hostingodawcę, odnośnie której zastosowano środki techniczne i  organizacyjne zapewniające ochronę przetwarzanych danych zgodne z wymaganiami  określonymi przez powszechnie obowiązujące przepisy prawa dotyczące ochrony  danych osobowych oraz Rozporządzenie UE z 27 kwietnia 2016 roku.&nbsp;</p>
<h3 class="antonioSmall">Informacja  o dobrowolności podania danych </h3>
<p>Przekazanie danych osobowych jest dobrowolne,  nie mniej jednak w zależności od sytuacji, w której Administrator może pozyskać  Twoje dane, brak podania określonych informacji może uniemożliwić zawarcie  umowy, skorzystanie z usług, otrzymanie odpowiedzi na korespondencję, złożenie  reklamacji lub skargi.</p>
<h3 class="antonioSmall">Zmiany  w polityce prywatności</h3>
<p>Niniejsza  polityka prywatności wchodzi w życie 25.05.2018 r. W przypadku zmiany polityki  prywatności obowiązującej w Serwisach, zamieścimy odpowiednią informację na tej  podstronie.&nbsp;</p>
<h3 class="antonioSmall">Polityka Cookies</h3>

<p>Niniejsza  Polityka Cookies ma zastosowanie do stron <a href="http://www.high5.pl">www.high5.pl</a>, <a href="http://www.hrna5.pl">www.hrna5.pl</a>, <a href="http://www.prism-diagnostics.pl">www.prism-diagnostics.pl</a></p>
<p>Serwis  nie zbiera w sposób automatyczny żadnych informacji, z wyjątkiem informacji  zawartych w plikach cookies. Pliki cookies (tzw. &bdquo;ciasteczka&rdquo;) stanowią dane  informatyczne, w szczególności pliki tekstowe, które przechowywane są w  urządzeniu końcowym Użytkownika Serwisu i&nbsp;przeznaczone są do korzystania  ze stron internetowych Serwisu. Cookies zazwyczaj zawierają nazwę strony internetowej,  z której pochodzą, czas przechowywania ich na urządzeniu końcowym oraz unikalny  numer. Podmiotem  zamieszczającym na urządzeniu końcowym Użytkownika Serwisu pliki cookies oraz  uzyskującym do nich dostęp jest operator Serwisu HIGH5 Group sp. z o.o. z siedzibą  w Warszawie na ul. Wroniej 45 lok. 129. 
Zawartość  plików cookies nie pozwala na identyfikację użytkownika. Za pomocą plików  cookies nie są przetwarzane lub przechowywane dane osobowe. Są one  przechowywane na komputerach użytkowników w celu:</p>
<ol>
  <li> Utrzymania sesji użytkownika (po zalogowaniu), dzięki której użytkownik nie  musi na każdej stronie wpisywać nazwy użytkownika i hasła.</li>
  <li> Tworzenia ankiet internetowych i zabezpieczania ich przed wielokrotnym  głosowaniem przez te same osoby.</li>
  <li> Tworzenia statystyk, które pomagają zrozumieć, w jaki sposób Użytkownicy  serwisu korzystają ze stron internetowych, co umożliwia ulepszanie ich  struktury i zawartości.</li>
</ol>
<p> Istnieje  możliwość takiego skonfigurowania przeglądarki internetowej, która spowoduje,  że całkowicie zostanie wyłączona możliwość przechowywania plików cookies na  dysku twardym komputera Użytkownika.</p>
    @endsection