@extends('frontend.main.cms')
@section('title')
HIGH5 Training Group: Galeria  Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
High5 Training Group: firma szkoleniowo-doradcza. Szkolimy umiejętności, które są niezbędne w efektywnym funkcjonowaniu każdego przedsiębiorstwa. 
@endsection
@section('menu')
@include('frontend.cms.firma.menu')
@endsection
@section('content')
<div class="image-set">
 <h1 class="antonioBig red">Galeria zdjęć High5</h1>   
@foreach($galeria_full as $key => $obrazek)
@if($key>1)
    <a href="{{Config('url')}}/img/frontend/wronia/full/{{$obrazek}}" class="gallery cboxElement" >
    <img src="{{Config('url')}}/img/frontend/wronia/min/{{str_before($obrazek,'.jpg')}}a.jpg" width="155" height="130" alt=""/>
    </a>
    @endif
    @endforeach
    </div> 
@endsection