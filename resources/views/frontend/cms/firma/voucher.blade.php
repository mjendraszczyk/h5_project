@extends('frontend.main.cms')
@section('title')
HIGH5 Training Group: Kim jesteśmy, usługi i szkolenia, które realizujemy Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
High5 Training Group: firma szkoleniowo-doradcza. Szkolimy umiejętności, które są niezbędne w efektywnym funkcjonowaniu każdego przedsiębiorstwa. Tag:   @foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('menu')
@include('frontend.cms.firma.menu')
@endsection
@section('content')
<h1 class="antonioBig red">Voucher szkoleniowy</h1>

 <h4>Co to jest voucher szkoleniowy?</h4>
        
        <ul>
	<li>Voucher  uprawnia do udziału w szkoleniu w terminie, który nie jest określony w dniu  zakupy vouchera,</li>
          <li>Voucher  dotyczy dowolnie wybranego szkolenia otwartego  z oferty HIGH5 Training,</li>
          <li>Voucher  ma określony okres ważności (rok kalendarzowy),</li>
          <li>Voucher  może dotyczyć również szkoleń zamkniętych ( w sprawie szczegółów prosimy o  kontakt : <a href="mailto:biuro@high5.pl">biuro@high5.pl</a>, tel:  602.35.46.45</li>
        </ul>
        <h4>Co daje  voucher?</h4>
        <ul>
          <li>możliwość  efektywnego wykorzystania budżetu w danym okresie rozliczeniowym  (czyli zaksięgowania przez Firmę kosztów w  bieżącym okresie rozliczeniowym),</li>
          <li>swobodę  w wyborze tematu i  terminu szkolenia  dla pracowników, </li>
          <li>gwarancję  stałej ceny,</li>
         </ul>
        <p><img src="{{asset('img/frontend/voucher1.jpg')}}" alt="voucher"/></p>
          <h4>Jak zamówić voucher(y)?</h4>
        
        <p>W celu zakupu vouchera prosimy o kontakt biuro@high5.pl</p>
        <p>&nbsp;</p>
        
		

    @endsection