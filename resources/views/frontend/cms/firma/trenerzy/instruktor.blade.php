@extends('frontend.main.cms')
@section('menu')
@include('frontend.cms.firma.menu')
@endsection
@section('content')
<div class="image-set">
@foreach($instruktorzy as $key => $instruktor)
@section('title')
HIGH5: Trenerzy i instruktorzy: {{$instruktor->instructor_name}} Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
High5 trener: {{$instruktor->instructor_name}}  {{str_limit($instruktor->instructor_lid,60)}}
@endsection

<h1 class="antonioBig red">{{$instruktor->instructor_name}}</h1> 
<div id="trenDet">
    <img src="{{Config('url')}}/img/frontend/trainers/big/{{$instruktor->instructor_photo}}" class="trainerBigPicture">
	<p>{!!$instruktor->instructor_lid!!}</p>
    {!!$instruktor->instructor_desc!!}
</div>
@if($instruktor->instructor_refs != '')
 <p class="trainerInfo">Szkolenia realizowane m.in. dla pracowników firm:
 <br/><br/>
 {!!$instruktor->instructor_refs!!}</p>
@endif
 <p class="trainerInfo">Kontakt: {!!$instruktor->instructor_fakemail!!}</p>
    @endforeach
    </div>
@endsection