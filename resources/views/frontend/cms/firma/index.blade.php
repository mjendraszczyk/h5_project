
@extends('frontend.main.cms')
@section('title')
HIGH5 Training Group: Kim jesteśmy, usługi i szkolenia, które realizujemy Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
High5 Training Group: firma szkoleniowo-doradcza. Szkolimy umiejętności, które są niezbędne w efektywnym funkcjonowaniu każdego przedsiębiorstwa. Tag:   @foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('menu')
@include('frontend.cms.firma.menu')
@endsection
@section('content')
<h1 class="antonioBig red">Kim jesteśmy, co robimy</h1> <p>High5 Training Group należy do  elitarnego grona najbardziej cenionych firm szkoleniowych i doradczych w  Polsce. Działamy od 2006 roku i dzięki innowacyjności, nieszablonowości i  wysokiej jakości naszych działań zdobyliśmy zaufanie blisko 20&nbsp;000 osób,  które skorzystały z wiedzy naszych trenerów. Efektywnie wspieramy  przedsiębiorstwa, instytucje i organizacje aby osiągały najwyższą pozycję na  konkurencyjnych rynkach – krajowym i zagranicznym. Efekty naszej pracy są  wielokrotnie doceniane poprzez pozytywne opinie i znakomite oceny uzyskiwane od  Uczestników szkoleń.&nbsp; Nasze działania  wspierają wszystkie obszary funkcjonowania każdego przedsiębiorstwa. Posiadamy  grono ponad dwustu stałych klientów, którzy  zaufali naszej wiedzy, kompetencjom i umiejętnościom, czemu dają wyraz w systematycznym  korzystaniu z naszych usług. Cenimy każdego klienta i otaczamy Go profesjonalną  opieką i służymy wparciem. 
    </p>
  <p>Aby sprostać wyzwaniom współczesnego  biznesu, nasze programy szkoleniowe i doradcze są projektowane ściśle w oparciu  o potrzeby szkoleniowe Partnerów, do których badania przykładamy szczególną  uwagę. Każde szkolenie jest niczym „ubranie szyte na miarę” i z tego powodu  jest projektem dostosowanym do realnych potrzeb, innowacyjnym i nieszablonowym.
  </p>
  <p>&nbsp;</p>
  <h2 class="antonioBig">Dlaczego warto zaufać profesjonalnemu  zespołowi HIGH5?
  </h2>
  <ul>
    <li>Działamy <strong>dynamicznie,  elastycznie i entuzjastycznie.</strong></li>
    <li>Fundamentem tych działań jest <strong>rozpoznanie Państwa potrzeb szkoleniowych</strong> i oczekiwań.</li>
    <li>Nieprzerwanie&nbsp;  podnosimy nasze kwalifikacje tak aby spełnić Państwa rosnące wymagania. </li>
    <li>Projektując&nbsp; produkty  patrzymy w przyszłość aby sprostać nowym wyzwaniom, jakie stawia przed nami  konkurencyjna rzeczywistość biznesu.</li>
    <li>Oferujemy szkolenia w najwyższej jakości, a nasza polityka  kosztowa umożliwia zaoferowanie ich naszym Klientom w konkurencyjnych cenach.</li>
    <li>W naszym portfolio znajduje się ponad trzydziestu trenerów  gotowych sprostać najwyższym wymaganiom we wszystkich dziedzinach biznesu. </li>
    <li><strong>„Szkolimy  najlepszych” i jesteśmy przygotowani, aby robić to najlepiej.</strong></li>
  </ul><br>
  <img src="{{Config('url')}}/img/frontend/wronia.jpg" class="img-responsive" width="632" height="449" alt="Zapraszamy na szkolenia">
@endsection