@extends('frontend.main.cms')
@section('title')
HIGH5 Training Group: Kontakt Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach dane
kontaktowe
@endsection
@section('description')
Dane kontaktowe do firmy High5 Training Group: Wronia 45/129, tel. 797 896 141 Tag: dane kontaktowe
high5 training group
@endsection
@section('menu')
@include('frontend.cms.firma.menu')
@endsection
@section('content')
<h1 class="antonioBig red">High5 Group Sp. z o.o.</h1>
<p class="big">00-870 Warszawa<br />ul. Wronia 45/129</p>
<p class="big red">tel.: 797 896 141</p>
<h3 class="big red">email:
    <script type="text/javascript" language="javascript">
        {
            coded = "UXF3K@uXduI.Wh"
            key = "ntp0FB4IfPQjS1kYW9DNMrzmlisV7xROZ2C3aohXcGL8vbg6JqEKuew5yAHUdT"
            shift = coded.length
            link = ""
            for (i = 0; i < coded.length; i++) {
                if (key.indexOf(coded.charAt(i)) == -1) {
                    ltr = coded.charAt(i)
                    link += (ltr)
                }
                else {
                    ltr = (key.indexOf(coded.charAt(i)) - shift + key.length) % key.length
                    link += (key.charAt(ltr))
                }
            }
            document.write("<a href='mailto:" + link + "'>" + link + "</a>")
        }
        //-->
    </script>
    <noscript>Sorry, you need Javascript on to email me.</noscript>

</h3>
<h3 class="antonioSmall red">Dyrektor ds. merytorycznych:</h3>
<p>Renata Jerzowska<br>
    <script type="text/javascript" language="javascript">
        {
            coded = "BQKihi.XQBPITqmi@YNwYW.Rk"
            key = "gcAi3qJuOyYG4Up06WEkn9t2BwbeN7DVj1ozZfLRaCsxmK8hHSTvQM5dlXFIPr"
            shift = coded.length
            link = ""
            for (i = 0; i < coded.length; i++) {
                if (key.indexOf(coded.charAt(i)) == -1) {
                    ltr = coded.charAt(i)
                    link += (ltr)
                }
                else {
                    ltr = (key.indexOf(coded.charAt(i)) - shift + key.length) % key.length
                    link += (key.charAt(ltr))
                }
            }
            document.write("<a href='mailto:" + link + "'>" + link + "</a>")
        }
        //-->
    </script>
    <noscript>Sorry, you need Javascript on to email me.</noscript>
    <br>
    tel.: 602 608 269
</p>
<p>
    KRS: 0000611100<br />
    NIP: 5272765357<br />
    REGON: 36414376600000<br />
    Nr w Rejestrze Instytucji Szkoleniowych: <strong>2.14/00215/2017</strong></p>
<p>
    <strong>Konta bankowe</strong>:<br> Volkswagen Bank<br />48 2130 0004 2001 0738 0157 0001<br>
    Bank Pekao S.A.<br>28 1240 6292 1111 0010 9485 3257


</p>
<iframe
    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9773.903177540664!2d20.985003!3d52.234741!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa17a4ab70c6484b3!2sHigh5+Training+Group!5e0!3m2!1spl!2spl!4v1493714943468"
    width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
<p><img src="./img/frontend/QRhigh5.png" alt="KodQR do nawigacji">Zeskanuj kod QR, aby otworzyć Google Maps i wyznaczyć
    trasę</p>
<p><a href="#" data-toggle="modal" data-target="#myModal"><img src="./img/frontend/parking_min.jpg"
            alt="Miejsca parkingowe"></a></p>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Miejsca parkingowe</h4>
            </div>
            <div class="modal-body">
                <img src="{{asset('/img/frontend/parking.jpg')}}" style="max-width: 100%;" />
            </div>

        </div>
    </div>
</div>
{{-- 
<h3 class="antonioSmall red">Możesz wysłać nam wiadomość z poniższego formularza </h3>
@if($errors->count() > 0)
<div class="alert alert-danger">
<ul>
@foreach($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>
@endif
@if($resultat > 0)
<div class="alert alert-danger">Przepraszamy - twój komentarz został uznany za spam!</div>
@endif

@if(!empty(Session::get('kontakt')))
<div class="alert alert-success">
    {!! Session::get('kontakt') !!}
</div>
@endif
@if(!empty(Session::get('kontakt_recaptcha')))
<div class="alert alert-danger">
    {!! Session::get('kontakt_recaptcha') !!}
</div>
@endif
@include('frontend.forms.kontakt') --}}
@endsection