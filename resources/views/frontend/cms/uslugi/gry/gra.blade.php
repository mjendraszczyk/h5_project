@extends('frontend.main.cms')
@section('menu')
@include('frontend.cms.uslugi.gry.menu')
@endsection
@section('content')
@foreach($gra_info as $info)
@section('title')
HIGH5 Gra szkoleniowa {{$info->nazwa}} Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
Strona gry rozwojowej {{$info->nazwa}} Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('keywords')
Gry szkoleniowe, narzędzia, development center, integracyjne, projekty rekrutacyjne, symulacje
@endsection

<div id="trainingTitle" class="Lmargin col-sm-12">
        <h2 class="h2red">
            <small>Gra szkoleniowa</small>
            <br></h2>
        <h1 class="antonioBig red" style="font-size: 30px;" itemprop="name">{{$info->nazwa}}</h1>

        <h3 style="padding-bottom: 20px;" itemprop="description">
            {{$info->podtytul}}
        </h3>
            <div class="korzysc col-sm-12 luzno">

     {!! $info->opis !!}   
    </div>
     <div class="korzysc col-sm-12 luzno">
            <h3>Dla kogo</h3>
            {!! $info->dla_kogo !!} 
            </div>   
          <div class="korzysc col-sm-12 luzno">
                <h3>Organizacja</h3>
                    {!! $info->organizacja !!} 
                    </div>
                    <div class="korzysc col-sm-12 luzno">
                            <h3>Korzyści</h3>
                            {!! $info->korzysci !!} 
                            </div>   

                            
    </div>
    <img src="{{Config('url')}}/img/frontend/{{$info->zdjecie}}" class="img-responsive" alt="" />
                          
    <a class="btn btn-danger" href="{{route('gry')}}" style="margin: 20px 0px;">Powrót do listy gier</a>
@endforeach

@endsection