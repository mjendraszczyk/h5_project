@extends('frontend.main.fullwidth') @section('content')
@section('title')
HIGH5 Lista gier symulacyjno-szkoleniowych Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
Gry symulacyjno-szkoleniowe zwiększają trawołść i operatywność zdobytych wiadomości. Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('keywords')
Gry szkoleniowe, narzędzia, development center, integracyjne, projekty rekrutacyjne, symulacje
@endsection
<div class="col-sm-12" id="Gry"></div>
<div class="row">

	<div class="col-sm-6" style="padding-bottom: 20px;">

		<h1 class="antonioBig Lmargin red">Gry symulacyjno - szkoleniowe</h1>
		<h2 class="AntonioBig">&bdquo;Metodą  prób i błędów&rdquo;</h2>
<p>  Gry szkoleniowe i narzędzia  grywalizacji nie są ani chwilowym trendem, ani chwilową metodą szkoleniową. <br>
  Są koniecznością biznesową.  Zwłaszcza teraz, kiedy na rynek pracy wchodzą pokolenia wychowane na grach: Y i  Z, gry odgrywają i będą odgrywały coraz większą rolę. <strong>To najskuteczniejsze narzędzia ułatwiające przyswajanie wiedzy</strong> i usprawniające nabywanie nowych umiejętności, a także dające uczestnikom <strong>szansę doświadczenia konsekwencji własnych wyborów.</strong></p>
<p>Mechanizmy gier  symulacyjno-szkoleniowych zakładają, że każdy kolejny krok pociąga za sobą  dalsze konsekwencje, a kolejne etapy są coraz trudniejsze. A to zawsze oznacza  dla Uczestnika wyzwanie, eksperymentowanie i naukę na własnych błędach. Można  powiedzieć, że gry to swoista <strong>próba generalna</strong>: pozwala sprawdzić swoje  umiejętności w bezpiecznym środowisku gry symulacyjnej.</p>
<h3 class="antonioSmall">Jakie są  korzyści ze stosowania gier symulacyjnych w projektach szkoleniowych?</h3>
<ul>
  <li>gry symulacyjne zwiększają trwałość i &bdquo;operatywność&rdquo; zdobytych wiadomości,</li>
  <li>rozwijają zarówno umiejętności interpersonalne, jak i umiejętności dotyczące       konkretnego obszaru merytorycznego np. budowania strategii marketingowych,</li>
  <li>zawierają elementy zarówno symulacji społecznych, jak i elementy edukacyjne, dzięki       czemu mają znaczący wpływ na kształtowanie odpowiednich postaw i relacji       biznesowych,</li>
  <li>umożliwiają sprawdzenie swoich umiejętności w bezpiecznych środowisku  oraz uczenie się na błędach,</li>
  <li>rozwijają spostrzegawczość,  koncentrację uwagi, twórcze myślenie i       intuicję,</li>
  <li>mechanizmy gier symulacyjnych dają możliwość warunkowania pozytywnych zachowań,       skłaniając do ich ponawiania oraz budowania zaangażowania.</li>
</ul>
<h3 class="antonioSmall">Gry i symulacyjne mogą być z  sukcesem stosowane w:</h3>
<ul>
  <li>projektach szkoleniowych dla specjalistów i menedżerów,</li>
  <li>projektach rozwojowych,</li>
  <li>projektach Development/Assessment Center.</li>
  <li>projektach rekrutacyjnych,</li>
  <li>diagnozie potrzeb szkoleniowych pracowników.</li>
</ul>
<p>HIGH5 Group oferujemy  nowoczesne rozwiązania: szkolenia z grami uniwersalnymi lub symulacjami, gry  kastomizowane, projekty zgrywalizowane, platformy on-line.</p>
<p align="center">Myślisz o projektach  zgrywalizowanych w Twojej organizacji?&nbsp; <br>
  <strong>Sprawdź, jakie rozwiązanie zaproponujemy.</strong></p>



	</div>

	<div class="col-sm-6" style="padding-bottom: 20px;">
		<h1 class="antonioBig Lmargin red">Gry szkoleniowe</h2>
		<div class="toright" style="padding-top: 20px;">
			<ul id="trainingList">
				@foreach($gry_lista as $gra)
				<li>
					<a style="text-decoration: none; color: #333;" href="{{route('gra_detal',['id'=>$gra->graID])}}" title="{{$gra->nazwa}}">
						<h4>{{$gra->nazwa}}</h4>
						<span class="trainingLid">
							{{$gra->podtytul}}

						</span>
					</a>
				</li>
				<li>
					@endforeach
			</ul>
		</div>

	</div>
</div>
	@endsection