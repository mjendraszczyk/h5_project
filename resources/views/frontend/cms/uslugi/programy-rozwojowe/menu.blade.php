@foreach($programy_all as $program)
<li><a href="{{route('program',['id'=>$program->programID])}}">{{$program->nazwa}}</a></li>
@endforeach
<div>
     
@foreach($programy_szczegol as $key=> $program)
@if($loop->first)
<h3 class="antonioBig red">{{$program->nazwa}}</h3>
<p></p>
<ol class="rectangle-list">
@endif
    <li>
            <a href="{{route('modul',['id' => $program->programID,'id_modul' => $program->trainingID])}}" title="{{$program->training_title}}">{{$program->training_title}}</a>
    </li>
@if($loop->last)
</ol>
@endif
@endforeach
</div>