@extends('frontend.main.cms')
@section('menu')
@section('title_menu')
Programy rozwojowe
@endsection
@include('frontend.cms.uslugi.programy-rozwojowe.menu')
@endsection
@section('content')
@foreach($moduly as $modul)
@section('title')
Moduł: {{$modul->training_title}} Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
{{$modul->training_lid}} Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('keywords')
Programy rozwojowe, warsztaty, coaching, sesje follow-up, on-line, symulacje, gry szkoleniowe
@endsection

<div id="trainingTitle" class="Lmargin col-sm-12">
        <h2 class="h2red">
            <small>Moduł</small>
            <br></h2>
        <h1 class="antonioBig red" style="font-size: 30px;" itemprop="name">
    {{$modul->training_title}}
        </h1>
        <h5><a href="#/tag/">#</a></h5>           
         <h3 style="padding-bottom: 20px;" itemprop="description">
            {{$modul->training_lid}}</h3>
    </div>

    <div class="korzysc col-sm-12 luzno">
        <h3>Cele modułu</h3>
           {!!$modul->training_advantage!!}
    </div>

    <div id="program" class="rozwijany col-sm-12">
            <h3>Program modułu:</h3>
            <div class="luzno">
                {!!$modul->training_program!!}
            </div>
            <!--            <a href="#" class="maxlist-more">więcej<span class="glyphicon glyphicon-chevron-down"></span></a>-->

        </div>
        <div class="col-sm-12" style="padding-bottom: 20px;">
                <h3>Metody szkoleniowe</h3>
                @php $aktywnosci_szkolenia = explode(";", $modul->training_activities); @endphp
                 
                    @foreach($aktywnosci_tab as $key => $aktywnosc) 
                    @if($key < 12)
                        @if(in_array($key, $aktywnosci_szkolenia) && !is_null($modul->training_activities)) 
                    <div class="col-sm-3" style="text-align: center;">
                        <img src="{{asset('img/frontend/aktywnosci/')}}/{{$aktywnosc}}" style="height:120px;">
                    </div>
                    @else
    
                    <div class="col-sm-3" style="text-align: center;">
                            <img src="{{asset('img/frontend/aktywnosci/hidden/')}}/{{$aktywnosc}}" style="height:120px;">
                        </div>
                    @endif
                    @endif
                @endforeach
            </div>

            <a class="btn btn-danger" href="{{route('program',['id'=>$modul->programID])}}" style="margin: 20px 0px;">Powrót do programu</a>
    @endforeach
@endsection