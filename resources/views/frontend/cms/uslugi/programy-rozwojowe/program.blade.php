@extends('frontend.main.cms')
@section('menu')
@section('title_menu')
Programy rozwojowe
@endsection
@include('frontend.cms.uslugi.programy-rozwojowe.menu')
@endsection
@section('content')
@foreach($programy_szczegol as $key => $program)
@if($key == 0)
@section('title')
HIGH5 Program rozwojowy: {{$program->nazwa}} Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
{{$program->nazwa}}, {{$program->podtytul}} Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('keywords')
Programy rozwojowe, warsztaty, coaching, sesje follow-up, on-line, symulacje, gry szkoleniowe
@endsection

<div class="col-sm-12 Lmargin" id="trainingTitle">  
    <h1 class="antonioBig red">{{$program->nazwa}}</h1>
    <h2>{{$program->podtytul}}</h2>
</div>
<div class="col-md-12" id="prozwoj">
     {!! $program->korzysc !!}  
    <h2>Moduły programu rozwojowego</h2>
@endif
@endforeach
@php $godziny = 0; @endphp
@foreach($programy_szczegol as $k => $program)
 
    <div class="panel panel-default">
        <div class="panel-heading">Moduł {{$k+1}}:</div>
        <div class="panel-body">
            <h3>
                <a href="{{route('modul',['id' => $program->programID,'id_modul' => $program->trainingID])}}" title="{{$program->training_title}}">
               {{$program->training_title}}
            </a>
        </h3>
        <p>{{$program->training_lid}}</p>
    </div>
    <div class="panel-footer">
        <a href="{{route('modul',['id' => $program->programID,'id_modul' => $program->trainingID])}}" title="{{$program->training_title}}">Zobacz program modułu szkolenia</a>
    </div>
</div>

@php $godziny += $program->training_hours @endphp

@if(!in_array($program->training_tr1,$trenerzy_array) && !empty($program->training_tr1))
@php $trenerzy_array[$k] = $program->training_tr1; @endphp
@endif

@if($loop->last == false)
    <img src="{{asset('img/frontend/arrowSm.gif')}}" style="margin-left:120px; margin-bottom:5px">
    @endif
  @endforeach

  @foreach($programy_szczegol as $key => $program)
    @if($key == 0)
     {!! $program->dla_kogo !!}
     <h2>Cena zawiera</h2>
    <div class="col-sm-6">
    <ul class="cena_zawiera list-unstyled list-inline">
    <li>{{$godziny/8}} dni szkolenia    </li>
    <li>skrypt szkoleniowy</li>
    <li>materiały dodatkowe</li>
    <li>certyfikat</li>
    <li>monitorowanie zadań wdrożeniowych</li>
    <li>60 dniowy e-mentoring</li>
    <li>lunch (restauracja)</li>
    <li>serwis kawowy premium</li>
    </ul>
</div>

<div class="col-sm-6">
    @if(in_array($program->programID,$certyfikaty))
    <img src="{{asset('img/frontend/certyfikaty/')}}/{{$program->programID}}.png" class="img-responsive" alt="Certyfikat">
    @else
    @endif
</div>

<h2>Trenerzy</h2>
@foreach($trenerzy as $t => $trener)
@if(in_array($trener->instructorID,$trenerzy_array))
<div class="col-sm-4">
        <h3>{{$trener->instructor_name}}</h3><a href="{{route('instruktor',[$trener->instructorID])}}">
            <img src="{{asset('img/frontend/trainers/big/')}}/{{str_slug($trener->instructor_name,'_')}}.jpg"></a></div>
@endif
@endforeach

<div class="row col-md-12" id="terminy_program">
        <div id="rezerwacja" class="col-sm-6">
            <h2>Terminy</h2>
            <!-- Tutaj trzeba to zrobić inteligentnie, żeby umiał grupowac terminy -->
            <ul class="list-unstyled" style="padding-left: 30px;">
       {{--  {{$moduly}}  --}}
                @foreach($moduly as $klucz => $modul)
<li>Moduł {{$klucz+1}} {{date('d', strtotime($modul->term_start))}} @if($modul->term_start != $modul->term_end)- {{date('d',strtotime($modul->term_end))}} @endif
        {{Lang::get('date.month.'.date('n',strtotime($modul->term_end)))}}
        {{date('Y',strtotime($modul->term_end))}}    
</li>
                @endforeach
        {{--  do edycji  --}}
            </ul>
        </div>
    
        <div class="col-sm-6">
            <h2>Cena</h2>
            <span style="padding-left: 30px;">
                {{$program->cena}} zł + VAT            <br><br>
                Przy zapisie większej ilości uczestników korzystne rabaty
            </span>
        </div>
    </div>

    <div class="row col-md-12" style="padding-top: 40px; margin-bottom: 50px;">
            <div id="rezerwacja" class="col-sm-6">
                <div class="center-block feature" id="program_zapisy">
                    <a href="{{route('rezerwacja_programy',['id'=>$program->programID])}}"></a>
                    <h2 style="font-weight: 700">Zapisy</h2>
                    formularz rejestracji uczestnika
                </div>
            </div>
            <div class="col-sm-6">
                <h2 style="margin-bottom: 0px; margin-top: 0px;">Masz pytania?</h2>
                Jeśli chcesz uzyskać więcej informacji o tym programie, skorzystaj z <a href="{{Request::path()}}#"  data-toggle="modal" data-target="#myModal" style="color: inherit; text-decoration: underline;">formularza</a> - nasi konsultanci odpowiedzą najszybciej, jak to będzie możliwe.
            </div>
        </div>
            </div>
@endif


 <!-- Modal -->
 <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Zapytaj o termin</h4>
            </div>
            <div class="modal-body">
              <p>
                  Jeśli chcecie Państwo otrzymać więcej informacji o tym programie, prosimy o wypełnienie poniższego formularza.
                </p>
                @include('frontend.forms.pytanie')
            </div>
           
          </div>
        </div>
      </div>
@endforeach
@endsection