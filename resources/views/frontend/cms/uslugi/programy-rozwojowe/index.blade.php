@extends('frontend.main.fullwidth') 
@section('content')
@section('title')
HIGH5 Lista programów rozwojowych Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
Projektujemy i wdrażamy nasze autorskie programy rozwojowe Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('keywords')
Programy rozwojowe, warsztaty, coaching, sesje follow-up, on-line, symulacje, gry szkoleniowe
@endsection
<div class="col-sm-12" id="Programy"></div>
<div class="row">
	<div class="col-sm-6" style="padding-bottom: 20px;">
		<h2 class="antonioBig Lmargin red">Czym są programy rozwojowe?</h2>
		<p> Programy rozwojowe to tematyczne cykle szkoleń charakteryzujące się kompleksowym podejściem (diagnostyka + rozwój) i innowacyjnością (platformy on-line, symulacje i gry szkoleniowe) </p>
<p>Projektujemy i prowadzimy  autorskie, długofalowe programy rozwojowe, np.: </p>
<ul>
  <li> Programy Rozwoju Talentów,  </li>
  <li>HIGH Potential Leaders, </li>
  <li>Akademia Lidera, </li>
  <li>Akademia Menadżera, </li>
  <li>Akademia  Handlowca, </li>
  <li>Szkoła Negocjatora, </li>
  <li>Akademia Trenera Wewnętrznego, </li>
  <li>Akademia  Prezentera, </li>
  <li>Programy Mentoringowe, </li>
  <li>Projekt: Sukcesja.</li>
</ul>
<h3 class="antonioSmall">Długofalowe i kompleksowe  działania pozwalają na:</h3>
<ul >
  <li>wprowadzenie       realnych       zmian w       organizacjach i osiągnięcie oczekiwanych rezultatów, </li>
  <li>spójne       i systemowe       podejście w       kształtowaniu umiejętności i postaw,</li>
  <li>wdrożenie       jednolitego standardu , który można by nazwać &bdquo;jednym językiem&rdquo; współpracy       w całej organizacji.</li>
</ul>

<h3 class="antonioSmall">Nasze programy charakteryzują  się:</h3>
<ul>
  <li>wszechstronnym  podejściem - składają się z części analitycznej, diagnostycznej,  rozwojowej i doradczej,</li>
  <li>nowoczesnością - wykorzystujemy platformy on-line, symulacje, gry strategiczne,  multimedialne pigułki wiedzy,  </li>
  <li>wykorzystaniem narzędzi diagnostyczno-rozwojowych o wysokiej trafności i  rzetelności psychometrycznej,</li>
  <li>personalizacji i multisensoryczności,</li>
  <li>stosowaniem metod - action learning, facylitacja, LEGO® SERIOUS PLAY®, gamyfikacja, design thinking, </li>
  <li>podejściem systemowym i procesowym oraz pracą projektową.</li>
</ul>
<p>Projektując każdy program  rozwojowy patrzymy w przyszłość, aby sprostać nowym wyzwaniom, jakie stawia przed  nami konkurencyjna rzeczywistość biznesu.</p>
	</div>
	<div class="col-sm-6" style="padding-bottom: 20px;">
			<h1 class="antonioBig Lmargin red">Programy rozwojowe - otwarte</h1>
		<ul id="trainingList">
		@foreach($programy as $program)
		<li>
			<a style="text-decoration: none; color: #333;" href="{{route('program',['id'=>$program->programID])}}" title="{{$program->nazwa}}">
			<h4>{{$program->nazwa}}<span class="pull-right">6 dni</span></h4><span class="trainingLid">{{$program->podtytul}}</span></a>
		</li>
		@endforeach
	</ul>
	<img class="img-responsive" src="{{asset('img/frontend/program_ex.jpg')}}" width="100%" alt="Schemat programu rozwojowego">
	</div>
</div>
@endsection