<h2 class="antonioSmall TgridLeft red">Regulamin zgłaszania uczestnika na szkolenie on-line</h2>
<ol class="regulamin">
        <li>Zgłoszenie na szkolenie realizowane jest w formie elektronicznej poprzez wypełnienie formularza on-line.
        </li>
        <li>Przesłanie formularza jest równoznaczne z zawarciem umowy zakupu szkolenia z firmą High5 Group Sp. z. o.o. (zwaną dalej Organizatorem) na zasadach określonych w niniejszym regulaminie i upoważnia firmę High5 Group Sp. z. o.o. do wystawienia po szkoleniu faktury VAT bez składania podpisu przez osobę upoważnioną ze strony zgłaszającego.</li>
        <li>Udział w szkoleniu on-line jest płatny zgodnie z opłatami zamieszczonymi na stronie z programem szkolenia. Warunkiem uczestnictwa w szkoleniu jest poprawne wypełnienie formularza rejestracji oraz otrzymanie zaproszenia. Opłata za szkolenie on-line uiszczana jest na podstawie faktury VAT wystawionej przez High5 Group Sp. z. o.o. (faktura płatna przelewem w ciągu 14 dni).
        </li>
        <li>Uczestnik ma prawo do rezygnacji z uczestnictwa w wybranym przez siebie szkoleniem on-line na 3 dni przed planowaną datą szkolenia on-line.</li>
        <li>W celu prawidłowego i pełnego korzystania ze szkoleń on-line należy dysponować urządzeniem mającym dostęp do Internetu oraz wyposażonym w przeglądarkę internetową.
        </li>
        <li>Korzystanie ze szkolenia może być uzależnione od instalacji oprogramowania typu Java, Flash oraz akceptacji cookies.</li>
        <li>High5 Group Sp.zo.o. nie ponosi odpowiedzialności za zakłócenia podczas szkolenia on-line nie leżące po stronie High5 lub spowodowane siłą wyższą lub niedozwoloną ingerencją uczestników lub osób trzecich.</li>
        <li>High5 Group Sp.zo.o. posiada prawa autorskie do materiałów edukacyjnych powstałych i wykorzystywanych w trakcie szkolenia on-line . Jakiekolwiek kopiowanie, upowszechnianie, wprowadzanie zmian, przesyłanie, drukowanie, publiczne odtwarzanie otrzymanych materiałów jest zabronione bez uprzedniej zgody z High5 Group Sp.zo.o.</li>
        <li>Dane osobowe uczestników szkolenia on-line będą przetwarzane przez High5 Group Sp.zo.o. w celu świadczenia usług w obrębie szkolenia on-line. Administratorem danych osobowych jest High5 Group Sp.zo.o. Uczestnikowi przysługuje m.in. prawo wglądu do dotyczących go danych osobowych oraz ich poprawiania. Podanie danych osobowych jest dobrowolne.
        </li>
        <li>Organizator zastrzega sobie prawo do odwołania szkolenia lub zmiany jego terminu na 3 dni przed terminem szkolenia, o czym zobowiązany jest poinformować uczestników szkolenia i Zamawiającego.
        </li>
</ol>