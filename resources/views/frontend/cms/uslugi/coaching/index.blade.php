@extends('frontend.main.cms') 
@section('menu') 
@include('frontend.cms.uslugi.coaching.menu')
@endsection 
@section('content')
<div id="coaching">
	<h1 class="antonioBig red">Coaching</h1>
	<h2 class="antonioBig">Czym jest coaching? </h2><div id="OfertySide">
<h2 class="antonioBig Lmargin white">Masz pytania ?</h2>
<p>(+4822) 824 50 25 <br>
    <a href="mailto:biuro@high5.pl">
        biuro@high5.pl</a></p>
<p>Zapytania o coaching: <a href="mailto:renata.jerzowska@high5.pl" class="antonioSmall">renata.jerzowska@high5.pl</a>
</p>
<p class="antonioSmall">tel: 602 608 269</p>
</div>
	<p>Coaching to metoda wpierania rozwoju,  towarzyszenie  klientowi   w wyznaczaniu ważnych dla niego celów,
		<strong>odnajdywaniu wewnętrznych zasobów</strong> potrzebnych do ich realizacji oraz ustalaniu i inspirowanie do zmian.
		</p>
		<blockquote><em>Technika coachingu polega na&nbsp;wydobywaniu mocnych stron ludzi, pomaganiu im w omijaniu osobistych barier i ograniczeń
			w celu&nbsp;osiągnięcia celu, a także ułatwieniu im bardziej efektywnego funkcjonowania w zespole. Coaching koncentruje
			się na rozwiązaniu (a nie na poszukiwaniu przyczyn problemów), promując rozwój nowych strategii myślenia i działania.
			<br> R.Dilts</em>
		</blockquote>	
	<p>Coaching może mieć formę pracy indywidualnej, pracy z grupą lub zespołem.</p>
	<p>
<h2 class="antonioBig">Trzy zasady coachingu:</h2>
<ol>
  <li>Poufność </li>
  <li> Odpowiedzialność   </li>
  <li>Praca z mierzalnymi kryteriami sukcesu</li>
</ol>
<h2 class="antonioBig">Jakie są  korzyści z coachingu?</h2>
<h3 class="antonioSmall"> Z perspektywy organizacji:</h3>
<ul type="disc">
  <li>zwiększenie aktywność oraz skuteczności zawodowej pracowników,</li>
  <li>podniesienie poziomu odpowiedzialności i decyzyjności,</li>
  <li>zmiana postaw i zachowań w kierunku osiągania sukcesów,</li>
  <li>poprawa komunikacji i współpracy w zespołach,</li>
  <li>zwiększenie motywacji pracowników i silniejszą identyfikację z&nbsp; firmą.</li>
</ul>
<h3 class="antonioSmall">Z  perspektywy menedżera/klienta:</h3>
<ul type="disc">
  <li>wzmocnienie produktywności poprzez zwiększenie efektywności działania,</li>
  <li>eliminacja barier i ograniczenia w osiąganiu celów,</li>
  <li>wspieranie poczucia własnej wartości i sprawczości,</li>
  <li>poprawa jakości życia poprzez odkrywanie nowych przekonań wspierających,</li>
  <li>zwiększenie odpowiedzialności za podejmowane zobowiązania,</li>
  <li>zmiana niepożądanych wzorców myślenia, </li>
  <li>pomoc w znalezieniu rozwiązań dla poszczególnych sytuacji zawodowych.</li>
</ul>
<img class="img-responsive" src="{{asset('img/frontend/coaching_m.jpg')}}" width="100%" alt="High5 coaching">
<h2 class="antonioBig">Jak działa coaching?</h2>
<p> Coaching to:</p>
<ul>
  <li>praca skoncentrowana na&nbsp; zmianie nawyku, postawy, zachowań,</li>
  <li>proces doskonalenia  kompetencji,  metod działania i cech wewnętrznych w  obszarach, który chce rozwijać się osoba lub firma.&nbsp;</li>
</ul>
<p>Potrzebujemy dotrzeć do głębszych motywacji, do źródła postawy klienta (czyli przekonań i wartości) tak, żeby zmiana była możliwa poprzez internalizację. Internalizacja to  uznanie za swoje nowych zachowań i powodów  ich stosowania. Bez tego motywacja jest tylko mniej lub bardziej zewnętrzną  presją wynikającą z doraźnych potrzeb.&nbsp;<br>
  <em>Uruchomienia własnej,&nbsp;<strong>wewnętrznej  motywacji</strong>&nbsp;pozwala odnosić większe, szybsze i bardziej trwałe efekty w  praktyce.</em></p>
<h2 class="antonioBig"> Jaka jest  rola coacha?</h2>
<p> Coach to&nbsp;<strong>towarzysz  zmiany, przewodnik</strong>, który prowadzi klienta do celu, dbając o parametry tej  podróży. Zasoby potrzebne do zmiany lub do osiągnięcia celu dobiera sam  klient.&nbsp;<br>
  Praca coacha polega na&nbsp;<strong>inspirowaniu klienta</strong>&nbsp;i wskazywaniu  obszarów odpowiedzialności&nbsp;i sprawczości. Jej efekty są określone  wskaźnikami,  po których można rozpoznać  postępy &nbsp;w osiąganiu celu. Klient jest odpowiedzialny za wynik, coach zaś  za warunki brzegowe relacji i  obiektywnie najlepsze warunki do zmiany.<br>
  Jak  wygląda prowadzenie procesu coachingowego?<br>
Głównym narzędziem coachingu  jest&nbsp;<strong>&nbsp;rozmowa</strong>&nbsp; coacha i klientem: rozmowa&nbsp; oparta na  partnerstwie, wzmacnianiu zaangażowania oraz wykazywaniu związku między zachowaniami&nbsp;a  stawianymi sobie celami.</p>
<h2 class="antonioBig">Struktura procesu:</h2>
<h3 class="antonioSmall">Pierwsze spotkanie coachingowe</h3>

<p>Celem  tego spotkania jest ustalenie:</p>
<ul>
  <li>kontraktu (etyka, zasady pracy, role i odpowiedzialność stron),</li>
  <li>celu biznesowego oraz celów rozwojowych,</li>
  <li>mierzalnych kryteriów sukcesu.</li>
</ul>
<h3 class="antonioSmall">Kolejne sesje (6-12)</h3>

<p>Każda  z sesji może mieć inny kontekst, wyznaczony przez konkretną sytuację biznesową,  w jakiej w danym momencie znajduje się klient. Jednak mimo to podczas każdej  sesji ( trwającej 60-90 minut) ma miejsce:</p>
<ul>
  <li>przegląd zadań, których wykonanie zostało  zaplanowane na poprzedniej sesji,</li>
  <li>analiza i wnioski z sukcesów i porażek, </li>
  <li>przegląd celów, priorytetów działania lub też  ich ewentualna zmiana,</li>
  <li>planowanie kolejnych działań.</li>
</ul>
<h3 class="antonioSmall">Czas pomiędzy sesjami</h3>

<p>Czas  pomiędzy sesjami coachingowymi jest tak samo istotny, jak sama sesja. Jest  to  czas, w którym uczestnik pracuje nad  wdrożeniem w  życie uzgodnionych z  coachem zachowań i działań. Jest to część codziennej pracy. Realna zmiana  zawsze zachodzi między spotkaniami, podczas realizacji ustalonego planu, przy wykorzystaniu  narzędzi  poznanych podczas spotkań  coachingowych. To także czas na poszukiwanie zasobów. </p>
<p>Proces  może także być dodatkowo wsparty różnymi narzędziami psychometrycznymi.</p>
<h3 class="antonioSmall">Sesja podsumowująca.</h3>
<h2 class="antonioBig">Jakie są najczęściej  wybierane przez klientów rodzaje coachingu?</h2>
<p> <strong>Coaching przywódczy</strong>&nbsp;<em>(leadership  coaching)</em>&nbsp;<br>
  – koncentruje się na pracy z  kierownikami, kadrą zarządzającą przedsiębiorstw i organizacji w zakresie  rozwoju potencjału związanego z przewodzeniem innym; <br>
  <br>
  <strong>Coaching umiejętności</strong>&nbsp;<em>(skills coaching</em>) <br>
  – koncentruje się na pracy ze  specjalistami w danej dziedzinie (sprzedawcami, pracownikami działów obsługi  klienta itp.), jest ukierunkowany na rozwój umiejętności i jest procesem  wyposażania ludzi w narzędzia, wiedzę i szanse, jakich potrzebują, aby się rozwijać  i pracować coraz wydajniej; często wykorzystywanym sposobem pracy coacha, jest  obserwacja klienta w miejscu jego pracy.&nbsp;<br>
  <br>
  <strong>Coaching strategiczny</strong>&nbsp;<em>(strategic coaching, executive coaching)</em>&nbsp;<br>
  – jest ukierunkowany na pracę  z najwyższą kadrą zarządzającą w obszarze określania kierunku rozwoju i  tworzenia strategii przedsiębiorstw i organizacji.&nbsp;<br>
  <br>
  <strong>Coaching biznesowy</strong>&nbsp;<em>(business coaching)</em>&nbsp;<br>
  – koncentruje się na  biznesowym rozwoju organizacji - może dotyczyć również pracy z klientami  rozpoczynającymi lub prowadzącymi własną działalność przedsiębiorcami.&nbsp;<br>
  <br>
  <strong>Coaching kariery</strong>&nbsp;<em>(career coaching)</em>&nbsp;<br>
  – jest skierowany do osób  potrzebujących wsparcia w doprecyzowaniu ścieżki kariery zawodowej pozwalającej  na wykorzystanie potencjału zawodowego klienta, przy jednoczesnym osiąganiu  pełnej satysfakcji z wykonywanej pracy; coachowie kariery pracują z  różnymi narzędziami badania potencjału i  predyspozycji zawodowych.&nbsp;<br>
  <br>
  <strong>Coaching osobisty </strong><em>(personal/life coaching)</em>&nbsp;<br>
  – jest realizowany poza  kontekstem organizacji, na zlecenie prywatnej osoby. Temat coachingu zależy od  indywidualnych potrzeb klienta i może dotyczyć realizacji wizji w życiu,  przygotowywania planów, podejmowania decyzji w kluczowych dla klienta  sprawach.&nbsp;<br>
  <br>
  <strong>Coaching zespołu&nbsp;</strong><em>(team coaching</em>) <br>
  - jest procesem interaktywnym,  koncentruje się na obszarach &nbsp;i celach istotnych z punktu widzenia całego  zespołu, jego istotą jest wzięcie odpowiedzialności przez członków zespołu za  zespół jako całość, za realizowanie wspólnych celów i wartości.&nbsp;<br>
  <br>
  <strong>Coaching grupowy&nbsp;</strong><em>(group coaching</em>) <br>
– jest procesem prowadzonym  metodami pracy warsztatowej, nastawionym na indywidualną pracę każdego członka  grupy nad istotnymi dla niego obszarami &nbsp;i celami.</p>
<p>&nbsp;</p>
<p align="center">Planujesz wdrożenie coachingu w  Twojej organizacji?&nbsp; <br>
  <strong>Sprawdź, jakie rozwiązanie zaproponujemy.</strong></p>	
</div>

@endsection