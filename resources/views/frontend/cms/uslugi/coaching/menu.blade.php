@section('title_menu')
Katalog szkoleń otwartych
@endsection
{{--  {{$categories_szkolenia}}  --}}
@foreach($categories_szkolenia as $szkolenie_cat) 
<li><a href="{{route('szkolenia_kategoria',['id'=>$szkolenie_cat->categoryID,'title'=>str_slug($szkolenie_cat->category_title)])}}">{{$szkolenie_cat->category_title}}</a></li>
@endforeach
<a href="{{asset('pdf/katalog.pdf')}}"><img class="img-responsive center-block" src="{{asset('img/frontend/katalog.jpg')}}" alt="pobierz katalog szkoleń"></a>