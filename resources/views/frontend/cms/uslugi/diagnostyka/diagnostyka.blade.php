@extends('frontend.main.cms')
@section('menu')
@include('frontend.cms.uslugi.diagnostyka.menu')
@endsection
@section('content')
@foreach($single as $diag)
@section('title')
HIGH5 Training Group Warszawa Diagnostyka: {{$diag->title}}
@endsection
@section('description')
Szkolenia otwarte i szkolenia zamknięte, coaching. Diagnostyka: {{$diag->title }}
@endsection
<h1 class="antonioBig Lmargin red">{{$diag->title}}</h1>
{!! $diag->tekst !!}
@endforeach
@endsection