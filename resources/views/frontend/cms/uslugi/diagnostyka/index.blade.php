@extends('frontend.main.cms')
@section('menu')
@include('frontend.cms.uslugi.diagnostyka.menu')
@endsection
@section('content')
@section('title')
HIGH5: Diagnostyka - wsparcie w zakresie diagnoz  potencjału i projektów rozwojowych
@endsection
@section('description')
Narzędzia diagnostyczne HR pozwalające podjąć istotną  decyzję personalną, zaprojektować ścieżki rozwoju, wyłonić talenty w organizacji, lepiej zarządzać zespołem
@endsection
@section('keywords')
Narzędzia diagnostyczne, Assessment Center/Development Center, Sesje 'on the job', Warsztaty diagnostyczne, Modele kompetencyjne, Ocena 360 stopni, Discovery Insights/ Extended DISC
@endsection

<h1 class="antonioBig Lmargin red">Diagnostyka i rozwój</h1>
<div id="staticContent" class="toright">
<div id="OfertySide">
<h2 class="antonioBig Lmargin white">Masz pytania ?</h2>
<p>Zapytania o narzędzia diagnostyczne: </p>
<p>(+4822) 824 50 25 <br>
    <a href="mailto:biuro@high5.pl">
        biuro@high5.pl</a></p>


</div>
<p>High5 Group sp. z o.o. świadczy usługi w zakresie diagnostyki potencjału i projektów rozwojowych.</p>
<p>  W każdej sytuacji, w której chcecie Państwo:</p>
<ul>
  <li>podjąć istotną decyzję personalną,</li>
  <li>zaprojektować ścieżki rozwoju,</li>
  <li>wyłonić talenty w organizacji,</li>
  <li>zdiagnozować mocne strony i obszary rozwojowe pracowników,</li>
  <li>zdiagnozować potrzeby szkoleniowe i coachingowe,</li>
  <li>zwiększyć efektywność zawodową pracowników i całych zespołów,</li>
  <li>podnieść jakość pracy w zespołach,</li>
  <li>zbudować skuteczne i kreatywne zespoły,</li>
</ul>

<p>nasi konsultanci są do Państwa dyspozycji.</p>
<img class="img-responsive" src="{{asset('img/frontend/diagnostyka.jpg')}}" width="100%" alt="Diagnostyka High5">
<p>Prowadzimy kompleksowe projekty diagnostyczno-rozwojowe, które są ściśle powiązane z sytuacją organizacji, planowanymi zmianami i celami strategicznymi. <br>
  Podchodzimy do projektów  systemowo: pomagamy w przygotowaniu, wdrażaniu, komunikacji, przeprowadzaniu i wykorzystywaniu efektów projektów rozwojowych.<br>
  Jesteśmy wyłącznym&nbsp;<strong>Autoryzowanym Partnerem</strong> na Polskę  narzędzia <strong>PRISM  Brain Mapping International</strong>.</p>
<h3 class="antonioBig">Narzędzia diagnostyczne z jakich korzystają nasi konsultanci:</h3>
<ul>
        @foreach($diagnostyka as $diag)
        <li><a href="{{route('diagnostyka')}}/{{$diag->diagID}}/{{str_slug($diag->title)}}">{{$diag->title}}</a></li>
        @endforeach
        <li>Ocena 360 stopni</li>
{{--  <li><a href="{{route()}}">Assessment Center/Development Center</a></li>  --}}
{{--  <li><a href="./diagnostyka/sesje%20-%20warsztaty-3/">Sesje &quot;on the job&quot;</a></li>
<li><a href="./diagnostyka/sesje%20-%20warsztaty-3/">Warsztaty diagnostyczne</a></li>
<li><a href="./diagnostyka/model%20kompetencyjny-4/">Modele kompetencyjne</a></li>

<li><a href="./diagnostyka/discovery%20insights%20-%20extended%20disc-5/">Discovery Insights / Extended DISC</a></li>  --}}
</ul>
</div>

@endsection