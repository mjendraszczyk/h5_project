@extends('frontend.main.fullwidth') @section('content')
@section('title')
HIGH5 Ceryfikacja PRISM Brain Mapping Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('description')
Centrum certyfikacyjne PRISM, szkolenia dla praktyków PRISM. Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('keywords')
PRISM Brain Mapping, narzedzia HR, certyfikacja
@endsection

<div class="col-sm-12" id="Certyfikacja"></div>
<div class="col-sm-12" style="line-height:1.8em;">
<h2 class="antonioBig" style="padding:1em 0;">Autoryzowane Centrum certyfikacji Praktyków PRISM Brain Mapping</h2>
<img src="./img/frontend/PRISM-logonew.jpg" width="270" height="130" alt="" class="floatLeftImgA">
<p style="padding-top: 12px;">HIGH5 Group sp. z o.o. jest wyłącznym <strong>Autoryzowanym Partnerem</strong> na Polskę  narzędzia<strong> PRISM Brain Mapping International</strong>.</p>
<p>HIGH5 Group sp. z o.o. jako jedyna  firma w Polsce ma uprawnienia <strong>do certyfikacji Praktyków PRISM Brain  Mapping</strong>.</p>
<p><em>PRISM jest  zarejestrowanym znakiem firmowym Centrum Neurologii Stosowanej.</em></p>
<h2 class="antonioBig listyRef_a" style="padding-top:1em;">Kurs Certyfikacyjny dla Praktyków PRISM</h2>
    <img src="./img/frontend/PRISM-certyfikat.jpg" alt="PRISM Certyfikat" class="floatRightImg">
<p>Jako jedyny w Polsce autoryzowany  ośrodek prowadzimy szkolenia przygotowujące do egzaminu dla&nbsp;praktyków  narzędzia diagnostyczno-rozwojowego PRISM Brain Mapping.</p>
<p>Program certyfikacyjny PRISM  Brain Mapping&nbsp; to <strong>3 dni intensywnego szkolenia stacjonarnego oraz praca wdrożeniowa  z mentorem przygotowująca do egzaminu</strong>. To nowe kompetencje, wiedza i skuteczne  narzędzia.</p>
<p>Certyfikat uprawnia do  korzystania z narzędzi PRISM na całym świecie. <br>
  Jest wydawany bezterminowo,  nie wymaga aktualizowania.<br>
  Po  ukończeniu szkolenia i pozytywnym wyniku egzaminu końcowego Certyfikowany  Praktyk PRISM nabywa&nbsp;uprawnienia&nbsp;do profesjonalnego korzystania z  narzędzia, otrzymuje&nbsp;pakiet materiałów informacyjnych, 100 punktów PRISM w  pakiecie startowym do dowolnego wykorzystania i dostęp do własnego konta  online.</p>
<p><a href="https://www.high5.pl/szkolenia/szkolenie/3/hr-kadry/451/certyfikowany-praktyk-prism">Przejdź do pełnego programu szkolenia &quot;Certyfikowany praktyk PRISM&quot; oraz warunków uczestniczenia.</a><br>
</p>
<h3 class="antonioSmall">Konto on-line Praktyka PRISM umożliwia:</h3>
<ul>
  <li>administrowanie on-line wszystkimi dostępnymi narzędziami PRISM,</li>
  <li>generowanie on-line pełnych raportów, we wszystkich dostępnych wersjach  językowych, jak również wybranych fragmentów raportów, na życzenie klienta,</li>
  <li>zamawianie punktów PRISM na swoje konto,</li>
  <li>sprawdzanie na bieżąco stanu wysłanych/odebranych kwestionariuszy do  narzędzi PRISM,</li>
  <li>dostęp do &bdquo;tutoriali&rdquo;, które służą pomocą w nawigowaniu strony i  korzystaniu z narzędzi,</li>
  <li>korzystanie z gotowych zestawów kompetencji PRISM do oceny 360 stopni,</li>
  <li>korzystanie z gotowych wzorców benchmarkingowych zawartych w systemie  PRISM, jako pomocy,</li>
  <li>samodzielne inicjowanie sesji Talent Finder, Career Match,Team  Performance Diagnostics czy oceny 360 stopni.</li>
</ul>
<h2 class="antonioBig">Korzyści z bycia Certyfikowanym  Praktykiem PRISM BRAIN MAPPING:</h2>
<h3 class="antonioSmall">Dla działu HR:</h3>
<ul type="disc">
  <li>będziesz mógł przygotować trafne plany działań rozwojowych w swojej organizacji,</li>
  <li>opracujesz ścieżki karier i programy rozwoju talentów,</li>
  <li>zaproponujesz swoim pracownikom rzetelne psychometrycznie raporty, indywidulne i zespołowe,</li>
  <li>przeprowadzisz ocenę 360,</li>
  <li>zidentyfikujesz mocne i słabe stron zespołu, wzorce preferowanych zachowań spójnych z kulturą organizacyjną,</li>
  <li>zidentyfikujesz źródła stresu i konfliktów.</li>
</ul>
<h3 class="antonioSmall">Dla rekruterów:</h3>
<ul type="disc">
  <li>zwiększysz       trafność przeprowadzanych rekrutacji,</li>
  <li>będziesz       potrafił tworzyć kompleksowe analizy profili kandydatów w odniesieniu do       stanowiska pracy,</li>
  <li>będziesz       korzystał z narzędzia&nbsp;<em>PRISM Talent Finder i tworzył&nbsp;</em>benchmarki       stanowisk.</li>
</ul>
<h3 class="antonioSmall">Dla trenerów i coachów:</h3>
<ul type="disc">
  <li>wzbogacisz       swoją ofertę szkoleniową, coachingową lub doradczą o elementy PRISM Brain       Mapping,</li>
  <li>zaproponujesz       swoim klientom rzetelne psychometrycznie raporty, indywidulne i zespołowe.</li>
</ul>
</div>
<div class="col-sm-12" id="Certyfikacja2"></div>
    <div class="row">
	<div class="col-sm-12" ><h2 class="antonioBig">Dlaczego PRISM jest wyjątkowy?</h2></div>
	
 <div class="col-sm-10">
 <h3 class="Inicjal">1 Uniwersalny</h3>
  <p>PRISM bada to, co ludzie lubią robić i następnie dopasowuje adekwatne cechy zachowań, które tym czynnościom odpowiadają.  Tak powstaje mapa bazowa określająca stopień, w którym badana osoba będzie prezentować dane zachowania bez odczuwania stresu lub zachowania instynktowne,  w sytuacji silnej presji.
Wyniki badań wskazują, że ludzie, którzy w środowisku pracy pełnią role oraz wykonują zadania odpowiadające ich potencjałowi, odnajdują większą satysfakcję z pracy, a także są bardziej efektywni i zmotywowani.
Kluczowe jest więc, aby poznać predyspozycje i preferowane środowisko pracy już na etapie zatrudniania.
Wyniki badań przeprowadzone przez zespół Harvard Business Review  mówią, że fluktuacja pracowników zwiększa się niemal dwukrotnie w przypadku, gdy stopień dopasowania preferencyjnych zachowań  pracownika do roli nie był brany pod uwagę w momencie zatrudnienia, a ok.  80% rotacji pracowników wynika z błędów popełnianych w trakcie procesów rekrutacji i selekcji.</p>
<p>PRISM  obrazuje też zakres, w jakim osoba badana modyfikuje swoje naturalne zachowania, gdy sytuacja tego wymaga oraz pokazuje stopień oddalenia się od preferowanych przez nią naturalnie zachowań (mapa adaptacyjna).
Określa, w jaki sposób najprawdopodobniej badana osoba będzie się zachowywać przez większość swojego czasu (około 70%) w otoczeniu innych ludzi  oraz jak jest widziana  oczami innych (mapa zgodności).</p>
<p>Wyniki i porównania tych  3 map można wykorzystać do dobierania zespołów do projektów, awansowania, rekrutacji z wewnątrz firmy oraz budowania ścieżki kariery dla pracowników, jak również diagnozy potrzeb szkoleniowych, rozwojowych , a także do delegowania określonych zadań.
    Uniwersalność  PRISM Brain Mapping pozwala na jego szerokie zastosowanie w obszarze HR.</p>
</div>
    <div class="col-sm-2">
         <img src="./img/frontend/PR1.png" class="prismIcon" alt="Uniwersalny">
     </div>
</div>


<div class="row">
         <div class="col-sm-2">
         <img src="./img/frontend/PR2.png" class="prismIcon" alt="Precyzyjny">
     </div>
        <div class="col-sm-10">
<h3 class="Inicjal">2 Precyzyjny</h3>
            <p>PRISM identyfikuje 8 wymiarów behawioralnych PRISM  oraz  mierzy aż 26 kluczowych preferencji badanych osób w środowisku pracy. Na diagramie PRISM każda ćwiartka jest podzielona jest jeszcze na dwa wymiary: posiadają one pewne wspólne charakterystyki , ale także mają między sobą znaczące różnice. Dokładne rozumienie różnic pomiędzy wymiarami znajdującymi się w każdej z ćwiartek jest niezmiernie istotne, bo właśnie te subtelności powodują, że PRISM jest narzędziem tak precyzyjnym.</p>
<p>Wpływ na interpretację głównego wyniku będa miały również dodatkowe informację, które otrzymamy z PRISM Brain Mapping: poziom inteligencji emocjonalnej, odporności psychicznej oraz raportu tożsamego z „The Big Five” („Wielkiej Piątki”).
</p>
</div>
</div>

<div class="row">
<div class="col-sm-10">
<h3 class="Inicjal">3 Czytelny</h3>
  <p>Dla zwiększenia czytelności  narzędzia i ułatwienia interpretacji wyników, zastosowano kolory ilustrujące preferencje behawioralne. Chociaż model PRISM stanowi jedynie metaforę funkcjonowania mózgu, mapy PRISM reprezentują dynamiczne interakcje zachodzące w mózgu i oparte są o zasadę mówiącą, że żadna z części mózgu nie odpowiada wyłącznie za jedną czynność, i żadna z części mózgu nie funkcjonuje samodzielnie. Zastosowane kolory mają zmieniającą się intenstywność: na diagramie (kole) PRISM, mniej intensywne lub blade kolory (niska intensywność) reprezentują niską preferencję dla danego zachowania, a nasilone barwy (o wysokiej intensywno ści) przedstawiają silną preferencję dla danego zachowania. Model pokazuje również relacje pomiędzy półkulami: prawą (zielony i niebieski) i lewą (złoty i czerwony) oraz przednią częścią mózgu – korą ruchową (złoty i zielony) i tylną częścią mózgu – korą czuciową (czerwony i niebieski).</p>
<p>Wyniki zobrazowane są,  poza  diagramem,  na 3 różnych mapach, które nałożone na siebie ułatwiają interpretację, wyciąganie wniosków i planowanie dalszych działań. W szybki sposób (przez nałożenie kolejnej, 4-tej mapy) możemy porównać profil kandydata   z benchmarkiem stanowiskowym (uniwersalnym lub stworzonym przy pomocy planera benchamarkingu).
</p>
</div>
    <div class="col-sm-2">
         <img src="./img/frontend/PR3.png" class="prismIcon" alt="Czytelny">
     </div>
</div>

<div class="row">
         <div class="col-sm-2">
         <img src="./img/frontend/PR4.png" class="prismIcon" alt="Rzetelny">
     </div>
       <div class="col-sm-10">
<h3 class="Inicjal">4 Rzetelny i trafny</h3>
            <p>PRISM spełnia bardzo wysokie kryteria w zakresie trafności i rzetelności psychometrycznej, dzięki czemu mamy pewność, że uzyskane informacje są zgodne z rzeczywistością. </p>
<p>Prace nad PRISM trwały od lat 90., a system przeszedł  trzy studia walidacyjne na bardzo  dużych próbach badawczych [2003, 2006-2007, 2014]  Ostatnia walidacja przeprowadzona była w 2014r. na próbie ponad 4,5 tys. osób z różnych grup etnicznych.  </p>
<p>PRISM na bardzo wysoki stopień zgodności wewnętrznej [Alpha Cronbacha > 0.90, przy czym  przyjmuje się, że wartości powyżej 0,7 oznaczają prawidłową rzetelność skali)  Badania trafności prognostycznej PRISM (czyli prawdopodobieństwo, że dana osoba w przyszłości zachowa się tak, jak na to wskazują wyniki testu) przeprowadzone na 2132 kandydatach  dały wynik: 54% bardzo wysoka trafność i  40,3% wysoka trafność.</p>
<p>PRISM ma również wyniki spójne w czasie: stosując technikę test-retest osiągnięto wynik 0,78-0,94 (dla różnych wymiarów).
    PRISM jest spójny z powszechnie znanymi metodykami pomiaru [MBTI, NEO PI-R, 16 PF]. </p>
</p>
</div> 
</div>

    <div class="row">    
      <div class="col-sm-10">
<h3 class="Inicjal">5 Naukowy</h3>
  <p>PRISM jest narzędziem opartym na osiągnięciach nowoczesnej neurologii [studium prof. Richarda Restaka]. 
</p>
  <p>Uwzględnia:    </p>
  <ul>
    <li> elastyczność mózgu, jeśli chodzi  o możliwość dostosowywania się i zmiany,      </li>
    <li> fakt, że nasze myśli, odczucia, percepcje i zachowania stanowią efekt  komunikacji komórek mózgu, za pomocą impulsów elektrycznych i substancji chemicznych,      </li>
    <li> możliwość aktywnego wpływania  i kreowania naszych zachowań  (wzmacniania lub  osłabiania) poprzez umiejętne regulowanie poziomu neuroprzekaźników i hormonów. </li>
  </ul>
      </div>
    <div class="col-sm-2">
         <img src="./img/frontend/PR5.png" class="prismIcon" alt="Czytelny">
     </div>
</div>

	@endsection