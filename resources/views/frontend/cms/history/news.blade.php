@extends('frontend.main.cms')
@section('menu')
@include('frontend.cms.history.menu')
@endsection
@section('content')
<div id="news_container">
        @foreach($news as $nowosc)
        @section('title')
        HIGH5 Training Group Artykuł: {{$nowosc->news_title}}
        @endsection
        @section('keywords')
        Szkolenia otwarte, szkolenia zamknięte, coaching, rozwój, zarządzanie, warszawa, projekty, procesy, HR, negocjacje, sprzedaż
        @endsection
        @section('description')
       Artykuł z dnia {{$nowosc->news_data}} {{$nowosc->news_title}}
        @endsection
                <br/>
                <h3 class="newsListTitle">{{$nowosc->news_title}}</h3>
                @if($nowosc->news_picture != '')
                        <img src="{{asset('/img/frontend/news')}}/{{$nowosc->news_picture}}" class="newsPicleft" alt="">
                @endif
                <p class="newsListData">{{$nowosc->news_data}}</p>
                <p class="newsListBody">
                        {!!$nowosc->news_lid!!}  
                        {!!$nowosc->news_text!!}
        @endforeach
</div>
<br class="clear">
<hr/>
<div class="col-sm-12">
                <h2 class="antonioBig lmargin h2red">Baza wiedzy</h2>
                @foreach($blog as $b)
                        <div class="blogOther">
                        <a href="{{route('blog_post',['id'=>$b->blogID,'title'=>str_slug($b->title)])}}">
                                <h3>{{$b->title}}</h3>
                        </a>
                        <div class="blogOther_author">
                                <img src="{{asset('/img/frontend/trainers/')}}/{{str_slug($b->instructor_name,'_')}}.gif" alt="{{$b->instructor_name}}">
                                <p>{{$b->instructor_name}}</p>
                        </div>
                        <p class="blog_data">{{$b->created}}</p>
                        <a href="{{route('blog_post',['id'=>$b->blogID,'title'=>str_slug($b->title)])}}">
                                <p class="blog_lid">
                                                {!!$b->lid!!}
                                </p>
                        </a>
                        <p class="blog_tagi">
                                        @foreach($tags as $klucz => $tag)
                                        @if($tag->blogID == $b->blogID)
                                        <a href="{{route('blog_tag',['tag'=>$tag->tag])}}">#{{$tag->tag}}</a>
                                         @endif
                                        @endforeach
                        </p>
                        </div>
                @endforeach
        </div>
@endsection