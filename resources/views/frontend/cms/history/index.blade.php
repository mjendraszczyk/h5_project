@extends('frontend.main.cms')
@section('menu')
@include('frontend.cms.history.menu')
@endsection
@section('content')

<div id="news_container">
    @foreach($history as $hist)
    @if($loop->first) 
    @section('title')
    HIGH5 - Artykuły Rok: {{date('Y',strtotime($hist->news_data))}} Artykuł - {{$hist->news_title}} 
    @endsection
    @section('keywords')
    Szkolenia otwarte, szkolenia zamknięte, coaching, rozwój, zarządzanie, warszawa, projekty, procesy, HR, negocjacje, sprzedaż
    @endsection
    @section('description')
   Artykuł z dnia {{$hist->news_data}} {{$hist->news_title}}
    @endsection
    @endif
    <br/>
        <h3 class="newsListTitle">{{$hist->news_title}}</h3>
@if($hist->news_picture != '')
        <img src="{{asset('/img/frontend/news')}}/{{$hist->news_picture}}" class="newsPicleft" alt="">
@endif
        <p class="newsListData">{{$hist->news_data}}</p>
        <p class="newsListBody">
                {!!$hist->news_lid!!}
            <a href="{{route('aktualnosci_news',['rok'=>date('Y',strtotime($hist->news_data)),'id'=>$hist->newsID])}}">[Więcej...]</a></p>
        @endforeach
</div>
@endsection