@section('title_menu')
Aktualnosci z lat
@endsection 
@foreach($getLata as $key => $rok)
    @if(!in_array(date('Y',strtotime($rok->news_data)),$arrayLata))
        <li><a href="{{route('aktualnosci_kategoria',['rok'=>date('Y',strtotime($rok->news_data))])}}">Rok {{date('Y',strtotime($rok->news_data))}}</a>
        @php array_push($arrayLata,date('Y',strtotime($rok->news_data))); @endphp
    @endif
@endforeach