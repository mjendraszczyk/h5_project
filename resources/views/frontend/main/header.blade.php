<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @if(Route::currentRouteName() == 'home')
    <link rel='canonical' href='{{ URL::to("/") }}' />
    @else
    <link rel='canonical' href='{{URL::current()}}' />
    @endif
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="{{ URL::to('/') }}">
    <link rel="shortcut icon" href="{{asset('icon16.ico')}}">
    <title>
        @yield('title')
    </title>
    <meta name="keywords" content="@yield('keywords')">
    <meta name="description" content="@yield('description')">
    <link type="text/css" href="{{asset('css/frontend/bootstrap.min.css')}}" rel="stylesheet">
    <link onload="if(media!='all')media='all'" href="{{asset('css/frontend/colorbox.css')}}" rel='stylesheet'
        type='text/css'>
    <link onload="if(media!='all')media='all'" href="{{asset('css/frontend/modern-business.css')}}" rel="stylesheet">
    <link onload="if(media!='all')media='all'" href="{{asset('css/frontend/font-awesome.css')}}" rel="stylesheet"
        type="text/css">
    @if(env('APP_ENV') == 'production')
    <link onload="if(media!='all')media='all'" href="{{public_path('css/frontend/kuba.css')}}?v={{time()}}"
        rel="stylesheet" type="text/css">
    @else
    <link onload="if(media!='all')media='all'" href="{{asset('css/frontend/kuba.css')}}?v={{time()}}" rel="stylesheet"
        type="text/css">
    @endif
    <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- Hotjar Tracking Code for www.high5.com.pl -->
    {{--  <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script> --}}
    {{--  <script type="text/javascript" src="{{asset('js/frontend/_high5.js')}}?v={{time()}}"></script> --}}
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:92544,hjsv:5};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
</head>

<body>
    <div id="fb-root"></div>
    {{-- @include('frontend.main.modal') --}}

    {!! App\Http\Controllers\backend\ModalWindowController::showFrontendModal()!!}
    @include('frontend.main.nav')