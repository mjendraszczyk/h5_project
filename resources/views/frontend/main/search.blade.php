@section('title')
HIGH5 Training Group Warszawa Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('keywords')
Szkolenia otwarte, szkolenia zamknięte, coaching, rozwój, zarządzanie, warszawa, projekty, procesy, HR, negocjacje, sprzedaż
@endsection
@section('description')
Szkolenia otwarte i szkolenia zamknięte, coaching. Rozwój osobisty, negocjacje, zarządzanie projektami i procesami, Szkolenia dla managerów i dla działów HR Tag:   @foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@include('frontend.main.header')
<div class="container maincontainer">
<div class="col-sm-3">
<img class="img-responsive hidden-xs" src="{{asset('img')}}/frontend/side.jpg">
</div>
<div class="col-sm-9" style="padding-bottom: 20px;">
@yield('content')
</div>
</div>
@include('frontend.main.footer')