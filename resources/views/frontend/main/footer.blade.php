<div class="push" style="height: 155px;"></div>

<div class="row footer" id="stopka">
    <div class="container">
        <div class="col-md-6">

            <h5>Kontakt</h5>
            <br />
            <p>ul. Wronia 45/129, 00-870 Warszawa<br>
                <br />
                <i class="fa fa-phone-square"></i> Telefon: 797 896 141<br>
                <i class="fa fa-envelope"></i> E-mail: <a class="mail-link"
                    href="mailto:biuro@high5.pl">biuro@high5.pl</a></p>


        </div>
        <div class="col-md-6">
            @if(!empty(Session::get('kontakt')))
            <div class="alert alert-success">
                {!! Session::get('kontakt') !!}
            </div>
            @endif
            @if($errors->count() > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            @include('frontend.forms.kontakt')
        </div>
    </div>

    <div class="footer_bottom">
        <div class="container">
            <div class="col-md-4">
                <a href="{{Config('url')}}">
                    <img src="{{asset('img/frontend/high5l.png')}}" style="padding-top:20px;" height="60"
                        alt="HIGH5 Group">
                </a>
            </div>
            <div class="col-md-8">
                <ul class="list-unstyled">
                    <li class="inline"><a href="{{route('szkolenia_otwarte')}}">Szkolenia otwarte</a></li>
                    <li class="inline"><a href="{{route('szkolenia_zamkniete')}}">Szkolenia zamknięte</a></li>
                    <li class="inline"><a
                            href="{{route('szkolenia_kategoria',['id'=>'2','title'=>str_slug('Szkolenia dla menedżerów')])}}">Szkolenia
                            dla menedżerów</a></li>
                    <li class="inline"><a
                            href="{{route('szkolenia_kategoria',['id'=>'6','title'=>str_slug('Szkolenia dla handlowców')])}}">Szkolenia
                            dla handlowców</a></li>
                    <li class="inline"><a
                            href="{{route('szkolenia_kategoria',['id'=>'8','title'=>str_slug('Szkolenia dla sekretarek')])}}">Szkolenia
                            dla sekretarek</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

</div>
@if(env('APP_ENV') == 'production')
<script src="{{public_path('/js/frontend/_high5.js')}}?v={{time()}}"></script>
@else
<script src="{{asset('/js/frontend/_high5.js')}}?v={{time()}}"></script>
@endif
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
@if(Route::currentRouteName() == 'homepage')
<script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    });
</script>
@endif
@if(Route::currentRouteName() != 'homepage')
<script>
    jQuery(document).ready(function ($) {
        
                $val = $('#counter').text();
                $val = parseInt($val);
        
                fu = function () {
                    jQuery({someValue: 10000}).animate({someValue: $val}, {
                        duration: 1500,
                        easing: 'swing', // can be anything
                        step: function () { // called on every step
                            // Update the element's text with rounded-up value:
                            $('#counter').text(Math.floor(this.someValue));
                        }
                    });
                };
        
                $('#counter').waypoint(fu, {offset: "100%", triggerOnce: !0});
        
                var $wysokosc = $("#terminy-top").height();
                $wysokosc /= 2;
                $wysokosc += 5;
                if ($wysokosc < 30) {
                    $wysokosc = 30;
                }
                $wysokosc = $wysokosc + 'px';
                $("#button_terminy").css('line-height', $wysokosc);
            });
        
          
        
            $('a.thickbox').colorbox({'width': '750px', 'top': '5%'});
</script>
@endif
@if(Route::currentRouteName() == 'galeria')
<script>
    $('a.gallery').colorbox({rel:'gal',maxWidth:'90%',maxHeight:'85%',top:'5%'});
</script>
@endif
<!-- Matomo -->
<script src="https://www.google.com/recaptcha/api.js?render=6LcTI7wUAAAAAHOJgUdvNJLtRTVqz-fC_mq6T-rt"></script>
<script>
    grecaptcha.ready(function() {
    grecaptcha.execute('6LcTI7wUAAAAAHOJgUdvNJLtRTVqz-fC_mq6T-rt', {action: 'homepage'}).then(function(token) {
       //...
    });
});
</script>
@if(Route::currentRouteName() == 'homepage')
<script type="text/javascript">
    var slideHeight = 130;
</script>
@else
<script type="text/javascript">
    var slideHeight = 350;
</script>
@endif
<script type="text/javascript">
    $(".rozwijany").each(function () {
                var $this = $(this);
                var $wrap = $this.children(".wrap");
                var startHeight = $wrap.height();
            var defHeight = "100%";
            console.log("test");
                if (startHeight >= slideHeight) {
                    var $readMore = $this.find(".read-more");
                    $wrap.css("height", slideHeight + "px");
                    $readMore.append("<a href='#'>Więcej <span class='glyphicon glyphicon-chevron-down'></span></a>");
                    $readMore.children("a").bind("click", function (event) {
                        var curHeight = $wrap.height();
                        if (curHeight == slideHeight) {
                            $wrap.animate({
                                height: defHeight
                            }, "normal");
                            $(this).text("Zwiń");
                            $wrap.children(".gradient").fadeOut();
                        } else {
                            $wrap.animate({
                                height: slideHeight
                            }, "normal");
                            $(this).text("Więcej");
                            $wrap.children(".gradient").fadeIn();
                        }
                        return false;
                    });
                }
            });

    var _paq = _paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
        var u="//www.high5.pl/piwik/";
        _paq.push(['setTrackerUrl', u+'piwik.php']);
        _paq.push(['setSiteId', '1']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
    })();
</script>
<!-- End Matomo Code -->
</body>

</html>