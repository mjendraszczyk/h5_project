@section('title')
HIGH5 Training Group Warszawa Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('keywords')
Szkolenia otwarte, szkolenia zamknięte, coaching, rozwój, zarządzanie, warszawa, projekty, procesy, HR, negocjacje, sprzedaż
@endsection
@section('description')
Szkolenia otwarte i szkolenia zamknięte, coaching. Rozwój osobisty, negocjacje, zarządzanie projektami i procesami, Szkolenia dla managerów i dla działów Tag:   @foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@include('frontend.main.header')
<div class="container maincontainer">
        <div class="col-sm-4">
                <nav class="navbar navbar-default" role="navigation">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#" style="padding-left: 0px; font-family: AntonioRegular, Gill Sans, Gill Sans MT, Myriad Pro, DejaVu Sans Condensed, Helvetica, Arial, sans-serif; font-size: 24px; color: #333; margin-top:9px;">
                        @yield('title_menu')
                        </a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse" style="padding-left: 0px;">
                        @yield('col-left')
                        <ul class="nav navbar-nav sidebar leftmenu">
@yield('menu')
</ul>
</div><!-- /.navbar-collapse -->
</nav>
@yield('menu_extends')
</div>
<div class="col-sm-8" style="padding-bottom: 20px;">
  @yield('content') 
</div>
</div>
@include('frontend.main.footer')