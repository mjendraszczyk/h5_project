<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Nawigacja</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{route('homepage')}}"><img src="{{asset('img/frontend/high5l.png')}}"
                    style="height:41px;    margin: -10px 0px 0 -10px;" alt="Logo High5 Group" class="logo"></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-left">
                {{--<li><a href="{{route('szkolenia')}}" @if((Route::currentRouteName() == 'szkolenia'))
                class="menuActive" @endif>Szkolenia</a></li>--}}
                <li class="dropdown">
                    <a href="#"
                        class="dropdown-toggle @if((Route::currentRouteName() == 'szkolenia' || (Route::currentRouteName() == 'szkolenia_otwarte') || (Route::currentRouteName() == 'szkolenia_zamkniete') || (Route::currentRouteName() == 'szkolenia_indywidualne') || (Route::currentRouteName() == 'programy'))) menuActive @endif"
                        data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Szkolenia <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{route('szkolenia_otwarte')}}">Szkolenia otwarte</a></li>
                        {{-- <li><a href="{{route('szkolenia_online')}}">Szkolenia online</a>
                </li> --}}
                <li><a href="{{route('szkolenia_zamkniete')}}">Szkolenia zamknięte</a></li>
                <li><a href="{{route('szkolenia_indywidualne')}}">Szkolenia indywidualne</a></li>
                <li><a href="{{route('programy')}}">Programy rozwojowe</a></li>
            </ul>
            </li>
            <li class="dropdown">
                <a href="#"
                    class="dropdown-toggle @if((Route::currentRouteName() == 'diagnostyka' || (Route::currentRouteName() == 'gry') || (Route::currentRouteName() == 'coaching'))) menuActive @endif"
                    data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Usługi <span
                        class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{route('diagnostyka')}}">Diagnostyka</a></li>
                    <li><a href="{{route('coaching')}}">Coaching</a></li>
                    <li><a href="{{route('gry')}}">Gry</a></li>
                    <!--<li><a href="{{route('certyfikacja')}}">Certyfikacja PRISM</a></li> -->
                </ul>
            </li>
            <li><a href="{{route('terminy')}}" @if(Route::currentRouteName()=='terminy' ) class="menuActive"
                    @endif>Terminy</a></li>
            <li><a href="{{route('firma')}}" @if(Route::currentRouteName()=='firma' ) class="menuActive"
                    @endif>Firma</a></li>
            <li><a href="{{route('referencje_firmy')}}" @if(Route::currentRouteName()=='referencje_firmy' )
                    class="menuActive" @endif>Referencje</a></li>
            <li><a href="{{route('blog')}}" @if(Route::currentRouteName()=='blog' ) class="menuActive" @endif>Baza
                    wiedzy</a></li>
            <li><a href="{{route('kontakt')}}" @if(Route::currentRouteName()=='kontakt' ) class="menuActive"
                    @endif>Kontakt</a></li>
            </ul>
            <div class="navbar-header navbar-right hidden-sm col-sm-3" id="search" style="padding-top: 8px; ">
                <form class="form-inline" name="fastsearch" method="get" action="{{route('search')}}" id="s1"
                    style="margin-bottom:8px;">
                    <div class="form-group">
                        <div id="zainteresowaniasearch" class="input-group" style="">
                            <input class="form-control" type="text" name="text_search" id="text_search"
                                placeholder="Szukaj szkolenia">
                            <span class="input-group-btn" style="">
                                <button type="submit" class="btn btn-default"
                                    style="color: white;background-color: #b90000;border: 0px;padding: 3px 10px;">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container -->
</nav>