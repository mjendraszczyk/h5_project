@php echo '<?xml version="1.0" encoding="UTF-8"?>'; @endphp
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    @foreach($routes_array as $key => $route)
    <url>
        <loc>{{$route}}</loc>
        <lastmod>{{date('Y-m-d')}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>1</priority>
        </url>
    @endforeach
    @foreach($getSzkolenia as $szkolenie)
    <url>
        <loc>{{route('szkolenia_strona',['id'=>$szkolenie->categoryID,'title'=>str_slug($szkolenie->category_title),'id_kurs'=>$szkolenie->trainingID,'title_kurs'=>str_slug($szkolenie->training_title)])}}</loc>
        <lastmod>{{date('Y-m-d')}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
        </url>
    @endforeach

</urlset>