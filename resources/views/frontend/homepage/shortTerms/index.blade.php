@if(Route::currentRouteName() == 'szkolenia_strona')
<li class="li-tabs" style=" font-weight: 400;">
    {{----}}
    {{date('d',strtotime($term->term_start))}} - {{date('d',strtotime($term->term_end))}}
    {{Lang::get('date.month.'.date('n',strtotime($term->term_end)))}}
    ({{Lang::get('date.day.'.date('w',strtotime($term->term_start)))}}-{{Lang::get('date.day.'.date('w',strtotime($term->term_end)))}});
    {{($term->training_hours)/8}} @if($term->training_hours/8 == 1) dzień @else dni @endif @if($term->term_state == '2')
    <img src="{{asset('img/frontend/pewnytermin.png')}}" width="52" height="18" alt="Pewny termin szkolenia"
        class="imgPewny pull-right"> @endif
    <br>
    {{-- <a itemprop="url" href="{{route('szkolenia_strona',['id'=>$term->categoryID,'title'=>str_slug($term->category_title),'id_kurs'=>$term->trainingID,'kurs_title'=>str_slug($term->training_title)])}}"
    title="{{$term->training_title}}"> --}}
    <a itemprop="url"
        href="{{route('szkolenia_strona',['id'=>$term->categoryID,'title'=>str_slug($term->category_title),'id_kurs'=>$term->trainingID,'title_kurs'=>str_slug($term->training_title)])}}">
        {{$term->training_title}}
        <p class="term--podtytul">
            {{-- itemprop="description" --}}
            {{$term->training_lid}}
        </p>
    </a>

</li>
@else
<li class="li-tabs" style=" font-weight: 400;" itemscope="" itemtype="http://schema.org/Event">

    <meta itemprop="startDate" content="{{$term->term_start}}">
    <meta itemprop="endDate" content="{{$term->term_end}}">
    <meta itemprop="name" content="{{$term->training_title}}">
    <span itemprop="organizer" typeof="Organization">
        <meta content="High5 Training Group" property="name">
        <meta content="https://www.high5.pl" property="url">
    </span>
    <span itemprop="performer" content="High5 Training Group"></span>
    <span itemprop="location" itemscope="" itemtype="http://schema.org/Place" content="{{$term->term_place}}">
        <meta itemprop="name" content="HIGH5 Group sp. z o.o.">
        <span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
            <meta itemprop="addressLocality" content="{{$term->term_place}}">
            <meta itemprop="addressCountry" content="Polska">
        </span>
    </span>

    <span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
        <meta itemprop="priceCurrency" content="PLN">
        <meta content="{{date('Y-m-d')}}" itemprop="validFrom">
        <meta itemprop="price" content="{{$term->training_price}}">
        <meta content="InStock" itemprop="availability" />
        <meta itemprop="url"
            content="{{route('szkolenia_strona',['id'=>$term->categoryID,'title'=>str_slug($term->category_title),'id_kurs'=>$term->trainingID,'title_kurs'=>str_slug($term->training_title)])}}">
    </span>
    <meta itemprop="image" content="https://www.high5.pl/img/frontend/img{{rand(1,5)}}.png" />
    {{----}}
    {{date('d',strtotime($term->term_start))}} - {{date('d',strtotime($term->term_end))}}
    {{Lang::get('date.month.'.date('n',strtotime($term->term_end)))}}
    ({{Lang::get('date.day.'.date('w',strtotime($term->term_start)))}}-{{Lang::get('date.day.'.date('w',strtotime($term->term_end)))}});
    {{($term->training_hours)/8}} @if($term->training_hours/8 == 1) dzień @else dni @endif @if($term->term_state == '2')
    <img src="{{asset('img/frontend/pewnytermin.png')}}" width="52" height="18" alt="Pewny termin szkolenia"
        class="imgPewny pull-right"> @endif
    <br>
    {{-- <a itemprop="url" href="{{route('szkolenia_strona',['id'=>$term->categoryID,'title'=>str_slug($term->category_title),'id_kurs'=>$term->trainingID,'kurs_title'=>str_slug($term->training_title)])}}"
    title="{{$term->training_title}}"> --}}
    <a itemprop="url"
        href="{{route('szkolenia_strona',['id'=>$term->categoryID,'title'=>str_slug($term->category_title),'id_kurs'=>$term->trainingID,'title_kurs'=>str_slug($term->training_title)])}}">
        {{$term->training_title}}
        <p class="term--podtytul">
            {{-- itemprop="description" --}}
            {{$term->training_lid}}
        </p>
    </a>

</li>
@endif