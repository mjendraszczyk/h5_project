@foreach($last_blog as $blog)
<div class="col-md-3" style="padding: 5px;">
    <div class="blogp" style="background-image:url({{Config('url')}}/public/img/frontend/blog/{{$blog->picture}}); ">
        <div class="blogT_info_main"><a
                href="{{route('blog_post',['id'=>$blog->blogID,'title'=>str_slug($blog->title)])}}">
                {{$blog->title}}
            </a>

        </div>

    </div>
    <p>
        {{str_limit($blog->lid,200)}}
    </p>
</div>
@endforeach