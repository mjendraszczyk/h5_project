@extends('frontend.main.content') 
@section('title')
HIGH5 Training Group: Szkolenia dla firm Warszawa - szkolimy najlepszych Tag:@foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('keywords')
Szkolenia otwarte, szkolenia zamknięte, coaching, rozwój, zarządzanie, warszawa, szkolenia dla firm, projekty, procesy, HR, negocjacje, sprzedaż
@endsection
@section('description')
Strona firmy szkoleniowej: szkolenia dla firm otwarte i szkolenia zamknięte, coaching. Rozwój osobisty, negocjacje, zarządzanie projektami i procesami, Szkolenia dla managerów i dla działów HR Tag: @foreach($meta as $m) {{$m->tag}} @if($loop->last) @else,@endif @endforeach
@endsection
@section('content')
<div class="col-md-12 firstLineSlider no-padding">
    @include('frontend.homepage.headlines.index')
</div> 
@inject('terms', 'App\Http\Controllers\HomeController')
 
{{--<div class="row firstRow">
  
{{--  Szkolenia otwarte / zamkniete + menu right  --}}


<div class="row">
    <div class="col-md-4"> 
        <div class="feature featureRed otwarte">
            <a href="{{route('szkolenia_otwarte')}}"></a>
            <div class="panel panel-default " style="background-color: inherit;">
            <div class="panel panel-heading feature-panel color-purple bg-transparent">
                <h1 class="no-margin FPantonioBig color-purple">
                Szkolenia otwarte
            </h1>
            </div>
                <div class="featureText">
                345 tematów szkoleń w 7 kategoriach.<br>
                Programy stworzone na podstawie wieloletnich doświadczeń trenerów.<br>
                Wyłącznie praktyczna wiedza.<br>
                    Pewne terminy szkoleń<br><br>
                Znajdź szkolenie dla siebie!
                    </div>

                    <i class="fa fa-long-arrow-right arrow_icon icon_pos_absolute" aria-hidden="true" style="bottom:10px;"></i>


            </div>
        </div>
</div>
<div class="col-md-4">
        <div class="zamkniete feature featurePurple">
            <a href="{{route('szkolenia_zamkniete')}}"></a>
            <div class="panel panel-default " style="background-color: inherit;">
                <div class="panel panel-heading feature-panel bg-transparent">
                    <h1 class="no-margin FPantonioBig">
                        Szkolenia zamknięte
                    </h1>
                </div>
                <div class="featureText">
                    Dedykowane szkolenia dla Twojej firmy.<br>
                Precyzyjnie zrealizujemy potrzeby Twojego zespołu.<br><br>
                Indywidualne dopasowanie programu szkolenia w 7 zakresach tematycznych.<br><br>
                Znajdź szkolenie dla swojej firmy!
                    </div>
                    <i class="fa fa-long-arrow-right arrow_icon icon_pos_absolute" aria-hidden="true" style="bottom:20px;"></i>
            </div>
        </div>
        </div>
        <div class="col-md-4">
        <div class="konsultacje coaching" style="min-height: 200px;">
            <ul class="list-konsultacje-coaching">
            <li>
               <a href="{{route('programy')}}"> 
                    <i class="fa fa-long-arrow-right arrow_icon color-purple" aria-hidden="true"></i>
                    Programy rozwojowe</a>
            </li>
            <li>
                <a href="{{route('gry')}}">
              <i class="fa fa-long-arrow-right arrow_icon color-purple" aria-hidden="true"></i>  Gry szkoleniowe
                </a>
            </li>
            <li>
				<a href="https://www.high5.pl/szkolenia/szkolenia-online">
               <i class="fa fa-long-arrow-right arrow_icon color-purple" aria-hidden="true"></i>  Szkolenia on-line
                </a>
            </li>
            </ul>

        </div>
        </div>

    </div>

    <div class="row">
    <div class="terminy col-md-4">
        <h3>
            Terminy szkoleń
        </h3>

        <div class="tabbable" id="terminy">
            <ul class="nav nav-tabs nav-tabs-justified">
                <li class="active">
                    <a href="#terminy-warszawa" data-toggle="tab">Warszawa</a>
                </li>
                <li>
                    <a href="#terminy-wroclaw" data-toggle="tab">Wrocław</a>
                </li>
                <li>
                    <a href="#terminy-gdansk" data-toggle="tab">Gdańsk</a>
                </li>
                <li>
                    <a href="#terminy-poznan" data-toggle="tab">Poznań</a>
                </li>
            </ul>

<div class="tab-content" style="padding:3px;">
    <div class="tab-pane active" id="terminy-warszawa" data-miasto="Warszawa">
        <ul class="list-unstyled" style="padding: 3px;">
                @foreach($terms->getTerms('2', 'Warszawa' ,'',10) as $term)
                @include('frontend.homepage.shortTerms.index')
                    @endforeach
</ul>
    </div>
    <div class="tab-pane" id="terminy-wroclaw" data-miasto="Wrocław">
            <ul class="list-unstyled" style="padding: 3px;">
            @foreach($terms->getTerms('7', 'Wrocław','',10) as $term)
            @include('frontend.homepage.shortTerms.index')
                @endforeach
            </ul>
        </div>
        <div class="tab-pane" id="terminy-gdansk" data-miasto="Gdańsk">
                <ul class="list-unstyled" style="padding: 3px;">
                @foreach($terms->getTerms('4', 'Gdańsk', '',10) as $term)
                @include('frontend.homepage.shortTerms.index')
                    @endforeach
                </ul>
            </div>
            <div class="tab-pane" id="terminy-poznan" data-miasto="Poznań">
                    <ul class="list-unstyled" style="padding: 3px;">
                    @foreach($terms->getTerms('8', 'Poznań', '',10) as $term)
                    @include('frontend.homepage.shortTerms.index')
                        @endforeach
                    </ul>
                </div>
</div>

        </div>

    </div>

    <div class="aktualnosci col-md-4">
        <h3>
            Aktualności
        </h3>
@include('frontend.homepage.history.index')

    </div>
<div class="facebook col-md-4">
    <div class="row feature feature-medium feature-other bg-img-hrna5" >
        <a href="http://www.high5.pl/kfs"></a>
        <div class="feature-medium-text" style="text-align: right;">
            Jak uzyskać<br>
            dofinansowanie?
        </div>
    </div>
    <div class="row feature feature-medium feature-other" >
        <a href="http://szkolenia-dla-sekretarek.pl/"></a>
        <div class="feature-medium-text">
            Szkolenia<br>
            dla sekretarek
        </div>
    </div>
    <div class="fb-page" data-href="https://www.facebook.com/High5-Training-Group-146638568702913/"
         data-small-header="false" data-adapt-container-width="true" data-width="500" data-hide-cover="false"
         data-show-facepile="true">
    </div>
    <div id="cloudTag" style="width: 100%">
        {!!$tagCloud!!} 
		<span class="tag size6" style="opacity:1;"><a href="https://www.high5.pl/szkolenia/szkolenie/4/kompetencje-biznesowe-rozwoj-osobisty/22/asertywnosc-i-komunikacja-w-biznesie-premium">asertywność szkolenie</a>
		</span>
		<span class="tag size5" style="opacity:1;">
		<a href="https://high5.pl/szkolenia/szkolenie/4/kompetencje-biznesowe-rozwoj-osobisty/22/asertywnosc-i-komunikacja-w-biznesie-premium">komunikacja szkolenie</a>
		</span>
		<span class="tag size6" style="opacity:1;">
		<a href="https://www.high5.pl/szkolenia/szkolenie/4/kompetencje-biznesowe-rozwoj-osobisty/30/akademia-prezentacji-profesjonalne-prezentacje-i-wystapienia-publiczne">prezentacje szkolenie</a>
		</span>
		<span class="tag size4" style="opacity:1;">
		<a href="https://high5.pl/szkolenia/szkolenie/4/kompetencje-biznesowe-rozwoj-osobisty/30/akademia-prezentacji-profesjonalne-prezentacje-i-wystapienia-publiczne">wystąpienia publiczne szkolenie</a>
		</span>
		<span class="tag size7" style="opacity:1;">
		<a href="https://www.high5.pl/szkolenia/szkolenie/6/sprzedaz-obsluga-klienta/68/akademia-negocjacji-negocjacje-w-biznesie-premium">negocjacje szkolenia</a>
		</span>
		<span class="tag size5" style="opacity:1;">
		<a href="https://www.high5.pl/szkolenia/szkolenie/3/hr-kadry/109/rekrutacja-i-selekcja-pracownikow-z-wywiadem-behawioralnym-premium">rekrutacja szkolenie</a>
		</span>
		<span class="tag size8" style="opacity:1;">
		<a href="https://www.high5.pl/szkolenia/szkolenie/8/asystentki-sekretarki/13/profesjonalna-asystentka-sekretarka-plus-premium">sekretarki szkolenie</a>
		</span>
		<span class="tag size4" style="opacity:1;">
		<a href="https://www.high5.pl/szkolenia/szkolenie/8/szkolenia-dla-sekretarek">szkolenia dla sekretarek</a>
		</span>
		<span class="tag size7" style="opacity:1;">
		<a href="https://www.high5.pl/szkolenia/szkolenie/2/dla-menedzerow/62/akademia-menedzera-zarzadzanie-zespolem-premium">zarządzanie zespołem szkolenie</a>
		</span>
		<span class="tag size5" style="opacity:1;">
	<a href="https://www.high5.pl/szkolenia/dla-pracownikow-4/zarzadzanie-czasem-time-management-premium-117.htm">zarządzanie czasem szkolenie</a>
	</span>

</div>
</div>


</div>

    <div class="row" style="margin-top: 10px; margin-bottom: 20px;">
        @include('frontend.homepage.blog.index')
    </div>
    
	<div class="row">
	<div class="col-md-3" id="welcome"><img src="./img/frontend/standingHigh5.jpg" alt="witamy w High5"></div>
	 <div class ="col-md-9" style="margin-top: 10px; margin-bottom: 20px;" id="mood">
     <h2>Bądź gotowy na sukces, zaufaj High5 Training Group</h2>

		<p>High5 Training Group składa się ze specjalistów z wieloletnim doświadczeniem z dziedziny programów rozwojowych. Nasi trenerzy wykorzystują swoją rozległą wiedzę, by prowadzić skuteczne szkolenia dla handlowców, menedżerów i liderów odpowiedzialnych za zarządzanie zasobami ludzkimi. Program szkolenia dostosowujemy indywidualnie do potrzeb klienta, rozwijając takie umiejętności jak sztuka prezentacji, wywierania wpływu czy rekrutacji i selekcji pracowników.</p>

<p>Jesteśmy na rynku firm szkoleniowych od 2006 roku, stale przeprowadzamy dokładne badanie potrzeb, a nasz zespół nieustannie ulepsza oferowane przez nas projekty szkoleniowe. Prowadzimy menedżerskie szkolenia, gdzie celem są m.in. lepsza organizacja czasu oraz zarządzanie talentami. Nie jest tajemnicą, że zespół odnoszący sukcesy to zespół stworzony z odpowiednich osób, a proces poszukiwania i rekrutowania takich pracowników nie jest zadaniem, które można wykonać bez uprzedniego przygotowania.</p>

<h3>Sprzedawanie bez tajemnic</h3>

<p>Nasze szkolenia dla handlowców również stoją na najwyższym poziomie i dzięki nim przygotowujemy wszystkich ambitnych pracowników działu sprzedaży, by mogli pokazać się z jak najlepszej strony podczas rozmowy z klientem. Szkolenia dla sprzedawców mają na celu nauczyć każdego zaawansowanych technik sprzedaży (również techniki sprzedaży b2b), wśród których znajduje się umiejętne wywieranie wpływu, profesjonalna obsługa klienta oraz perswazja (negocjacje sprzedaż).</p>

<p>Najbardziej skuteczne są szkolenia sprzedażowe skierowane indywidualnie na jedną firmę. Tego typu szkolenia zamknięte są dedykowane dla potrzeb Twojego zespołu. To poprzez menedżerskie lub sprzedażowe szkolenia szlifujemy takie umiejętności jak zarządzanie projektami czy zwiększanie produktywności. Starannie dobrane materiały szkoleniowe sprawią, że cały proces zwiększania swoich możliwości przebiegnie interesująco i bezproblemowo.</p>

<h3>Otwarte szkolenia Warszawa</h3>

<p>Jeśli chodzi o szkolenia otwarte Warszawa jest najlepszym do tego miejscem, choć prowadzimy również kursy i szkolenia we Wrocławiu i Gdańsku. A czym jest samo szkolenie otwarte? To spotkania z profesjonalistami w niewielkich, ogólnodostępnych grupach, skupione na rozwoju wybranych umiejętności, gdzie przekazywana jest wyłącznie wiedza praktyczna. Zostawmy teorię teoretykom - my stawiamy na szkolenia handlowców i menedżerów. Nasze biznesowe szkolenia są skupione przede wszystkim na rozwój osobisty, a przez to - wzmacnianie firmy u podstaw. Dokładnie taki jest główny cel naszych szkoleń otwartych. </p>

<p>Dostosowujemy się do klientów, stąd w ramach naszej oferty znajdziesz również konsultację online, tak zwany e-mentoring, oraz szkolenia dofinansowane przez przez Unię Europejską. Jeśli Twoim wyborem stanie się nasza firma szkoleniowa Warszawa jest miejscem, gdzie otrzymasz to wszystko - dla siebie i dla całego Twojego zespołu. Począwszy od szkoleń sprzedażowych, na zarządzaniu ludźmi i projektami kończąc, High5 Training Group to najlepszy początek drogi do sukcesu.</p>

    
     
    </div>
	</div>
@endsection