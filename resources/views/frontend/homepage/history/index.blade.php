@foreach($histories as $history)
<div class="panel panel-default">
    <div class="panel-heading">
        <a href="{{route('aktualnosci_news',['rok'=>date('Y',strtotime($history->news_data)),'id'=>$history->newsID])}}" title="{{$history->news_title}}">{{$history->news_title}}</a>
        </div>
        <div class="panel-body">
            <span>{{$history->news_data}}</span>
            <p class="TgridLeft">
                {!!str_limit($history->news_lid,128)!!}
                <a href="{{route('aktualnosci_news',['rok'=>date('Y',strtotime($history->news_data)),'id'=>$history->newsID])}}" class="pull-right" title="{{$history->news_title}}">[.więcej.]</a></p></div>
</div>
@endforeach