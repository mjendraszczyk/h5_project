
<div class="carousel slide container no-padding" id="carousel-578474">
    <ol class="carousel-indicators hidden-xs">
@foreach($last_sliders as $key => $slider)
        <li data-slide-to="{{$key}}" data-target="#carousel-578474" class="@if($key == 0)active @endif"></li> 
@endforeach
</ol>
<div class="carousel-inner">

@foreach($last_sliders as $key => $slider)
                    @if($key == 0)  
                    <div class="item active">
                    @else
                    <div class="item">
                    @endif
                   {{--  <img alt="{{$slider->title}}" style="width: 100%" src="{{asset('/img/frontend/headlines')}}/{{$slider->picture}}"> --}}
                    <div class="carousel-caption">
                        <a style="color: #FFF" href="{{$slider->link}}">
                        <h2 class="OpenSansText" style="margin-top: 5px">
                           {{$slider->title}}
                        </h2>
                        {{--<p>--}}
                                {!!$slider->content!!}
                                 
                        {{--</p>--}}
                        <i class="fa fa-long-arrow-right arrow_icon" aria-hidden="true" style="margin-top:10px;"></i>
                        </a>
                    </div>
                </div>
@endforeach
        <video autoplay muted loop id="myVideo">
  <source src="{{asset('video/high5.mp4')}}" type="video/mp4">
</video>
 </div>
        {{--<a class="left carousel-control" href="#carousel-578474" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
        <a class="right carousel-control" href="#carousel-578474" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>--}}
</div>