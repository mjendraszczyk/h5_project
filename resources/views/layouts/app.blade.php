@include('backend.main.head')
<body>
    <div @if(Auth::user()) id="wrapper" @endif class="toggled">
    @include('backend.main.menu')
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div id="app">
    @include('backend.main.nav')
                    <div class="app-content">
                        <div class="col-md-12">
                            @if(View::hasSection('title'))
                            <div class="panel-heading">@yield('title')</div>
                            @endif
                            {{--<div class="panel-body">--}}
                                @yield('content')
                            {{--</div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.main.footer')